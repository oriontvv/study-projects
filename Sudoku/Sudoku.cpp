#include "Sudoku.h"

Sudoku::Sudoku()
{
    Init();
}

void Sudoku::Init()
{
    for(int i = 0; i < 1000; ++i)
    mag_x[i] = mag_y[i] = 0;

    for(int i=0; i<10; ++i)
        used[i] = 0;

    np = 1;
    im = 1;
    mag[im-1] = np;

    for(int i = 1; i < 10; ++i)
        for(int j = 1; j < 10; ++j)
            mat[i][j] = 0;
}

bool Sudoku::correct(int x, int y, int np)
{
    int z;
    // in the row:
    for (z=1; z<10; ++z)
        if (z!= x && mat[z][y] == np) return false;
    // in the line:
    for (z=1; z<10; ++z)
        if (z!= y && mat[x][z] == np) return false;
    // in the subsquare:
    int x_, y_; //start positions in subsquare
    if (x > 6) x_ = 7;
    else if (x > 3) x_ = 4;
    else x_ = 1;

    if (y > 6) y_ = 7;
    else if (y > 3) y_ = 4;
    else y_ = 1;

    // check in subsquare
    for (int i=x_; i<x_+3; ++i)
        for (int j=y_; j<y_+3; ++j)
            if (i!=x && j!=y && mat[i][j] == np) return false;

    // all checks complited - np correct number.
    return true;
}

bool Sudoku::Correct_matrix()
{
    for(int x=1; x<10; ++x)
        for(int y=1; y<10; ++y)
            if(mat[x][y])
                if(!correct(x, y, mat[x][y])) return false;
    return true;
}

bool Sudoku::choose_next_empty_field()
{
    int q, w;
    for(w=mag_y[im-1]+1; w<10; ++w)
        if(mat[mag_x[im-1]][w]==0){
            mag_x[im] = mag_x[im-1];
            mag_y[im] = w;
            return false;
        }
    for(q=mag_x[im-1]+1; q<10; ++q)
        for(w=1; w<10; ++w)
            if(mat[q][w]==0){
                mag_x[im] = q;
                mag_y[im] = w;
                return false;
            }
    return true;
}

bool Sudoku::fool()
{
    int sum = 0;
    for (int i=1; i<10; ++i)
        sum += used[i];
    return (sum == 80);	// +np = 81: (9x9 - 1) = 80
}

int Sudoku::Solution()
{
    // Begin
    while(im>=0){
        if(np<10){
            if(mat[mag_x[im-1]][mag_y[im-1]]==0 && correct(mag_x[im-1], mag_y[im-1], np))
                if(fool()){
                    // solution is found
                    mat[mag_x[im-1]][mag_y[im-1]] = np;
                    return 0;
                }
                else {
                    // forward (add in stack - push)
                    used[np]++;
                    mat[mag_x[im-1]][mag_y[im-1]] = np;
                    mag[im]=np;
                    np = 0;
                    if(choose_next_empty_field()) return 2;
                    im++;
                }
        }
        else{
            // back(eject from stack - pop)
            used[mag[im-1]]--;
            np = mag[--im];
            mat[mag_x[im-1]][mag_y[im-1]] = 0;
        }
        np++;
    }
    return 1;
}
