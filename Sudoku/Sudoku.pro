# -------------------------------------------------
# Project created by QtCreator 2009-12-05T04:00:46
# -------------------------------------------------
TARGET = Sudoku
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    Sudoku.cpp \
    FieldWidget.cpp
HEADERS += mainwindow.h \
    Sudoku.h \
    FieldWidget.h
FORMS += mainwindow.ui
