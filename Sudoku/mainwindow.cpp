#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "FieldWidget.h"

#include <QMessageBox>
#include <QFileDialog>
#include <QString>
#include <fstream>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    move(100, 100);

    mas = new FieldWidget**[11];

    for(int i = 0; i < 11; ++i)
        mas[i] = new FieldWidget*[11];

    for(int i = 0; i < 11; ++i)
        for(int j = 0; j < 11; ++j)
            mas[i][j] = NULL;

    for(int x = 0; x < 3; ++x){
        for(int y = 0; y < 3; ++y){
            for(int i = 0; i < 3; ++i){
                for(int j = 0; j < 3; ++j){
                    FieldWidget *field = new FieldWidget;
                    field->setAlignment(Qt::AlignHCenter);
                    field->setMaximumWidth(40);
                    field->setFont(QFont("Times", 20, 20, 1));
//                    field->setText("0");
                    ui->FieldLayout->addWidget(field, 3 * x + i, 3 * y + j, 1, 1);
                    mas[3 * x + i + 1][3 * y + j + 1] = field;
                }

            }
        }
    }

    // connecting
    connect(ui->pushButtonClear, SIGNAL(clicked()), this, SLOT(clearField()));
    connect(ui->pushButtonRun, SIGNAL(clicked()), this, SLOT(run()));
    connect(ui->pushButtonRead, SIGNAL(clicked()), this, SLOT(readFromFile()));

    sudokuSolver = new Sudoku;

}

MainWindow::~MainWindow()
{
    delete ui;
    for(int i = 0; i < 11; ++i)
        if (mas[i]) delete mas[i];
    if (mas) delete mas;
}

void MainWindow::clearField()
{
    for(int i = 1; i <= 9; ++i)
        for(int j = 1; j <= 9; ++j)
            mas[i][j]->setText("");
}

void MainWindow::run()
{
	sudokuSolver->Init();
	
    // pass values to internal mas
    for(int i = 1; i <= 9; ++i){
        for(int j = 1; j <= 9; ++j){
            sudokuSolver->mat[i][j] = mas[i][j]->text().toInt();
        }
    }

    // check correctless of input matrix:
    if (!sudokuSolver->Correct_matrix() || mas[1][1]==0 || mas[1][2]==0){
        QMessageBox::critical(0, "Error", "Incorrect matrix", QMessageBox::Ok);
        return;
    }

    // calculate:
    if (sudokuSolver->Solution() == 1){
        QMessageBox::information(0, "Ups!", "Sorry, but this hasn\'t solution");
        return;
    }
    else{
        // solution find
        for(int i = 1; i <= 9; ++i){
            for(int j = 1; j <= 9; ++j){
                mas[i][j]->setText(QString::number(sudokuSolver->mat[i][j]));
                sudokuSolver->mat[i][j] = 0;
            }
        }
    }
}

bool MainWindow::readFromFile()
{
    bool end = false;
    char ch;
    int i, j;

    QString *fName = new QString(QFileDialog::getOpenFileName(0, "Open sudoku - text file",
                                                             "", "*.* - text format"));

    std::ifstream in(fName->toAscii());

    delete fName;

    for(i=1; i<10 && !end; ++i){
        for(j=1; j<10; ++j){
            in.get(ch);
            while(int(ch) == 10 || ch == ' ') in.get(ch);
            if (ch >= '1' && ch <= '9'){
                mas[i][j]->setText(QString::number(sudokuSolver->mat[i][j] = int(ch) - int('0')));
            }
            if (in.eof()) {
                end = true;
                break;
            }
        }
    }
    in.close();
    if (i == 10 && j == 10) return 0;// all matrix was read
    else return end;
}
