#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>

#include "FieldWidget.h"
#include "Sudoku.h"

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void clearField();
    void run();
    bool readFromFile();

private:
    Ui::MainWindow *ui;
    FieldWidget ***mas;
    Sudoku *sudokuSolver;
};

#endif // MAINWINDOW_H
