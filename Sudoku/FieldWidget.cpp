#include "FieldWidget.h"

#include <QRegExp>
#include <QRegExpValidator>


FieldWidget::FieldWidget()
{
    QRegExp rx("[1-9]\\d{0}");
    QValidator *validator = new QRegExpValidator(rx, this);
    setValidator(validator);
}
