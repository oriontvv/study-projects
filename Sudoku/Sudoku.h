#ifndef SUDOKU_H
#define SUDOKU_H

class Sudoku
{
    int mag_x[1000], mag_y[1000],mag[1000];
    int np, im;
    int used[10];
public:
    Sudoku();
    void Init();
    int mat[11][11];

    bool correct(int x, int y, int np);
    bool Correct_matrix();
    bool choose_next_empty_field();
    bool fool();
    int Solution();

};

#endif // SUDOKU_H
