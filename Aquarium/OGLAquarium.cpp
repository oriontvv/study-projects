
/*
 * NAME: Taranov Vassiliy Vassilevich, 325
 * ASGN: N4
 */

#include "OGLAquarium.h"
#include <QCoreApplication>
#include <QProgressDialog>

#include <cmath>
#include <stdlib.h>
#include <stdio.h>


OGLAquarium::OGLAquarium(QWidget *pwgt) : QGLWidget(pwgt)
{
    music = new QSound("sound.wav", this);
    music->setLoops(-1);
//    music->play();

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(redrawing()));

    camera = new Camera;
    antiAliasing = 0;

    setWindowTitle("Aquarium");
    resize(640, 480);

    fogColor[0] = 0.1;
    fogColor[1] = 0.2;
    fogColor[2] = 0.3;
    fogColor[3] = 0.6;

    light1On = light3On = false;
    light2On = light4On = true;

    cur_time = 0.0;

    moveMouseToCenter(0, 0);
    setMouseTracking(true);
    setCursor(Qt::BlankCursor);
}

OGLAquarium::~OGLAquarium()
{
    if (music) delete music;
    //textures
    if (textureID) delete textureID;
    //fishes
    for(int i = 0; i < fishesCount; ++i)
        if (fishes[i]) delete []fishes[i];
    //plants
    for(int i = 0; i < plantsCount; ++i)
        if (plants[i]) delete []plants[i];
    if (plants) delete plants;

    if (floor) delete floor;
    if (timer) delete timer;

    if (bubbles) delete bubbles;
    if (fishesMoved) delete fishesMoved;

    for(int i = 0; i < fishesMovedCount; ++i)
        if (fishProperties[i]) delete []fishProperties[i];
    if (fishProperties) delete fishProperties;
}

int OGLAquarium::loadTextures()
{
    fishesCount = 8;
    plantsCount = 3;

    texturesCount = fishesCount + plantsCount + 5; // for  water, floor, bubble, mirror, pomp

    QProgressDialog *progress = new QProgressDialog("Please wait... Loading textures...",
                                                    0,
                                                    0,
                                                    2 * texturesCount, this);
    progress->setWindowModality(Qt::WindowModal);
    int cur_pr = 0;
    textureID = new GLuint[texturesCount];

    int texWidth[texturesCount];
    int texHeight[texturesCount];
    unsigned char *texBits[texturesCount];

    //allocate memory for 3ds objects
    fishes = new L3DS*[fishesCount];
    for(int i = 0; i < fishesCount; ++i)
        fishes[i] = new L3DS;
    plants = new L3DS*[plantsCount];
    for(int i = 0; i < plantsCount; ++i)
        plants[i] = new L3DS;
    floor = new L3DS;

    glGenTextures(texturesCount, textureID);

    // load models and textures
    fishes[0]->LoadFile("models\\Fish3ds\\BlueTang0.3ds");
    fishes[0]->LoadNormals("models\\Fish3ds\\BlueTang0.nrm");
    if(!(texBits[0] = LoadTrueColorBMPFile("textures\\Fishes\\BlueTaT.bmp", &texWidth[0], &texHeight[0]))){
        qDebug() << "Error: texture can''t be load fishes[0]";
        return false;
    }
    progress->setValue(++cur_pr);
    fishes[1]->LoadFile("models\\Fish3ds\\NeonTetra0.3ds");
    fishes[1]->LoadNormals("models\\Fish3ds\\NeonTetra0.nrm");
    if(!(texBits[1] = LoadTrueColorBMPFile("textures\\Fishes\\NeonTeT.bmp", &texWidth[1], &texHeight[1]))){
        qDebug() << "Error: texture can''t be load fishes[1]";
        return false;
    }
    progress->setValue(++cur_pr);
    fishes[2]->LoadFile("models\\Fish3ds\\RedBetta0.3ds");
    fishes[2]->LoadNormals("models\\Fish3ds\\RedBetta0.nrm");
    if(!(texBits[2] = LoadTrueColorBMPFile("textures\\Fishes\\RedBetT.bmp", &texWidth[2], &texHeight[2]))){
        qDebug() << "Error: texture can''t be load fishes[2]";
        return false;
    }
    progress->setValue(++cur_pr);
    fishes[3]->LoadFile("models\\Fish3ds\\TurquoiseDiscus0.3ds");
    fishes[3]->LoadNormals("models\\Fish3ds\\TurquoiseDiscus0.nrm");
    if(!(texBits[3] = LoadTrueColorBMPFile("textures\\Fishes\\TurquoT.bmp", &texWidth[3], &texHeight[3]))){
        qDebug() << "Error: texture can''t be load fishes[3]";
        return false;
    }
    progress->setValue(++cur_pr);
    fishes[4]->LoadFile("models\\Fish3ds\\Tanago0.3ds");
    fishes[4]->LoadNormals("models\\Fish3ds\\Tanago0.nrm");
    if(!(texBits[4] = LoadTrueColorBMPFile("textures\\Fishes\\TanagoT.bmp", &texWidth[4], &texHeight[4]))){
        qDebug() << "Error: texture can''t be load fishes[4]";
        return false;
    }
    progress->setValue(++cur_pr);
    fishes[5]->LoadFile("models\\Fish3ds\\Kumanomi0.3ds");
    fishes[5]->LoadNormals("models\\Fish3ds\\Kumanomi0.nrm");
    if(!(texBits[5] = LoadTrueColorBMPFile("textures\\Fishes\\KumanoT.bmp", &texWidth[5], &texHeight[5]))){
        qDebug() << "Error: texture can''t be load fishes[5]";
        return false;
    }
    progress->setValue(++cur_pr);
    fishes[6]->LoadFile("models\\Fish3ds\\AsiaArowana0.3ds");
    fishes[6]->LoadNormals("models\\Fish3ds\\AsiaArowana0.nrm");
    if(!(texBits[6] = LoadTrueColorBMPFile("textures\\Fishes\\AsiaArT.bmp", &texWidth[6], &texHeight[6]))){
        qDebug() << "Error: texture can''t be load fishes[6]";
        return false;
    }
    progress->setValue(++cur_pr);
    fishes[7]->LoadFile("models\\Fish3ds\\GuppiBlueGrass0.3ds");
    fishes[7]->LoadNormals("models\\Fish3ds\\GuppiBlueGrass0.nrm");
    if(!(texBits[7] = LoadTrueColorBMPFile("textures\\Fishes\\GuppyBT.bmp", &texWidth[7], &texHeight[7]))){
        qDebug() << "Error: texture can''t be load fishes[7]";
        return false;
    }
    progress->setValue(++cur_pr);
    plants[0]->LoadFile("models\\Plants3ds\\40.3ds");
    plants[0]->LoadNormals("models\\Plants3ds\\40.nrm");
    if (!(texBits[fishesCount] = LoadTrueColorBMPFile("textures\\Plants\\40.bmp", &texWidth[fishesCount], &texHeight[fishesCount]))){
        qDebug() << "Error: texture can''t be load plants[0]";
        return false;
    }
    progress->setValue(++cur_pr);
    plants[1]->LoadFile("models\\Plants3ds\\42.3ds");
    plants[1]->LoadNormals("models\\Plants3ds\\42.nrm");
    if (!(texBits[fishesCount + 1] = LoadTrueColorBMPFile("textures\\Plants\\42.bmp", &texWidth[fishesCount + 1], &texHeight[fishesCount + 1]))){
        qDebug() << "Error: texture can''t be load plants[1]";
        return false;
    }
    progress->setValue(++cur_pr);
    plants[2]->LoadFile("models\\Plants3ds\\38.3ds");
    plants[2]->LoadNormals("models\\Plants3ds\\38.nrm");
    if (!(texBits[fishesCount + 2] = LoadTrueColorBMPFile("textures\\Plants\\38.bmp", &texWidth[fishesCount + 2], &texHeight[fishesCount + 2]))){
        qDebug() << "Error: texture can''t be load plants[2]";
        return false;
    }
    progress->setValue(++cur_pr);

    if(!(texBits[texturesCount - 5] = LoadTrueColorBMPFile("textures\\water.bmp",
                                                           &texWidth[texturesCount - 5],
                                                           &texHeight[texturesCount - 5]))){
        qDebug() << "Error: texture can''t be load water";
        return false;
    }

    if(!(texBits[texturesCount - 4] = LoadTrueColorBMPFile("textures\\mirror.bmp",
                                                           &texWidth[texturesCount - 4],
                                                           &texHeight[texturesCount - 4]))){
        qDebug() << "Error: texture can''t be load mirror";
        return false;
    }

    if(!(texBits[texturesCount - 3] = LoadTrueColorBMPFile("textures\\pomp.bmp",
                                                           &texWidth[texturesCount - 3],
                                                           &texHeight[texturesCount - 3]))){
        qDebug() << "Error: texture can''t be load pomb";
        return false;
    }
    QImage *im = new QImage("textures\\bubble.png","PNG");
//    im->load("bubble4.bmp", QImage::Format_ARGB32_Premultiplied);

    floor->LoadFile("models\\floor.3ds");
    floor->LoadNormals("models\\floor.nrm");
    if ( !(texBits[texturesCount - 1] = LoadTrueColorBMPFile("textures\\floor.bmp", &texWidth[texturesCount - 1], &texHeight[texturesCount - 1]))){
        qDebug() << "Error: texture can''t be load";
        return false;
    }
    progress->setValue(++cur_pr);

    for(int i = 0; i < texturesCount; ++i){

        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glBindTexture(GL_TEXTURE_2D, textureID[i]);

        if(i != texturesCount - 2)
            gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, texWidth[i], texHeight[i],
                              GL_RGB, GL_UNSIGNED_BYTE, texBits[i]);
        else
            // for bubbles
            gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8, im->width(), im->height(),
                              GL_RGBA, GL_UNSIGNED_BYTE, im->bits());

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

        if (i != texturesCount - 2) delete []texBits[i];
        else delete im;
        progress->setValue(++cur_pr);
    }
    delete progress;
    qDebug() << "All textures was loaded.";
    return true;
}

void OGLAquarium::initializeGL()
{
    qglClearColor(Qt::gray);
//    glClearColor(0, 0, 0, 0);
    glDisable(GL_BLEND);

    glClearDepth(1.0);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);

    glEnable(GL_NORMALIZE);
    glEnable(GL_AUTO_NORMAL);
//    glDepthFunc(GL_LEQUAL);

    if(!loadTextures()){
        qDebug() << "Error loadTextures";
    }
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT_FACE);


    glEnable(GL_FOG);
    //setup mode
    glFogi(GL_FOG_MODE, GL_LINEAR);
    //setup color
    glFogfv(GL_FOG_COLOR, fogColor);
    //power of fog;)
    glFogf(GL_FOG_DENSITY, 0.2);
    glFogf(GL_FOG_HINT, GL_NICEST);
    glFogf(GL_FOG_START, 4);
    glFogf(GL_FOG_END, 6);

    bubblesInit();
    fishesInit();

    createBoxList();
    createFishList();
    createPlantList();
    createFloorList();
    createLampList();
    createPompList();
    createMirrorList();
    createBubbleList(0.1);

    timer->start(30);
}

void OGLAquarium::resizeGL(int nWidth, int nHeight)
{
    glViewport(0, 0, nWidth, nHeight);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0 ,(GLdouble) ( width() / height()), 0.1, 100.0);
}

void OGLAquarium::paintGL()
{
    if(antiAliasing){
        float x, y;
        float r = 0.5;
        camera->Translate();
        for(float angle = 0; angle < 2 * M_PI; angle += 2.0 * M_PI / (float)(antiAliasing)){
            x = r * sin(angle);
            y = r * cos(angle);
            camera->Rotate(x, y);

            qglClearColor(Qt::gray);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();

            camera->Translate();

            gluLookAt(camera->getX(), camera->getY(), camera->getZ(),
                      camera->getX1(), camera->getY1(), camera->getZ1(),
                      0.0, 1.0, 0.0);

            renderMirror();

            lighting();

            renderAllObjects();
            renderBubbles();

            if (angle == 0) glAccum(GL_LOAD, 1.0 / (float)antiAliasing);
            else glAccum(GL_ACCUM, 1.0 / (float)antiAliasing);

            glReadBuffer(GL_BACK);

            camera->Rotate(-x, -y);
        }
        glAccum(GL_RETURN, 1);
        glDrawBuffer(GL_BACK);
    }
    else {
        qglClearColor(Qt::gray);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        camera->Translate();

        gluLookAt(camera->getX(), camera->getY(), camera->getZ(),
                  camera->getX1(), camera->getY1(), camera->getZ1(),
                  0.0, 1.0, 0.0);

        renderMirror();

        lighting();

        renderAllObjects();
        renderBubbles();
    }
//    qDebug() << "cam_a1 = " << camera->getAngle1() << "\tcam_a2 = " << camera->getAngle2();
}

void OGLAquarium::bubblesInit()
{
    // allocate memory
    bubblesCount = 40;
    bubbles = new Bubble[bubblesCount];

    //initialization
    for(int i = 0; i < bubblesCount; ++i){
        bubbles[i].x = 0.0;
        bubbles[i].y = 2.3 * rand() / (RAND_MAX + 1.0) - 0.7;
        bubbles[i].z = 0.0;

        bubbles[i].x1 = 0.0;
        bubbles[i].y1 = 0.001;
        bubbles[i].z1 = 0;

        bubbles[i].r = 0.1 * rand() / (RAND_MAX + 1.0);
    }
}

void OGLAquarium::bubblesMove()
{
    for (int i = 0 ; i < bubblesCount; ++i){
        bubbles[i].x += bubbles[i].x1;
        bubbles[i].y += bubbles[i].y1;
        bubbles[i].z += bubbles[i].z1;

        bubbles[i].y1 = 0.006 * (rand() % 10);
        if (bubbles[i].y1 + bubbles[i].y >= 1.5){
            // generate new bubble
            bubbles[i].x = 0;
            bubbles[i].y = 2.3 * rand() / (RAND_MAX + 1.0) - 0.7;
            bubbles[i].z = 0;

            bubbles[i].x1 = 0.0;
            bubbles[i].y1 = 0.001;
            bubbles[i].z1 = 0;

            bubbles[i].r = 0.1 * rand() / (RAND_MAX + 1.0);
        }
        else{
            bubbles[i].x1 = 0.006 * (rand() % 10) - 0.03;
            bubbles[i].z1 = 0.006 * (rand() % 10 )- 0.03;
        }
    }
}

void OGLAquarium::fishesInit()
{
    fishesMovedCount = 9;

    fishesMoved = new Fish[fishesMovedCount];
    fishProperties = new float*[fishesMovedCount];

    for(int i = 0; i < fishesMovedCount; ++i)
        fishProperties[i] = new float[4];

    fishProperties[0][0] = 2.7;//a
    fishProperties[0][1] = 1.8;//b
    fishProperties[0][2] = 0.0;//delta_y
    fishProperties[0][3] = 0;//type


    fishProperties[1][0] = 2.5;
    fishProperties[1][1] = 1.5;
    fishProperties[1][2] = 0.3;
    fishProperties[1][3] = 0;

    fishProperties[2][0] = 2.5;
    fishProperties[2][1] = 1.5;
    fishProperties[2][2] = -0.2;
    fishProperties[2][3] = 0;

    fishProperties[3][0] = 2.6;
    fishProperties[3][1] = 1.9;
    fishProperties[3][2] = 0;
    fishProperties[3][3] = 0;

    fishProperties[4][0] = 2.3;
    fishProperties[4][1] = 1.7;
    fishProperties[4][2] = 0.4;
    fishProperties[4][3] = 0;

    fishProperties[5][0] = 1.2;
    fishProperties[5][1] = 1.5;
    fishProperties[5][2] = 0.2;
    fishProperties[5][3] = 1;

    fishProperties[6][0] = 1.3;
    fishProperties[6][1] = 1.2;
    fishProperties[6][2] = 1.0;
    fishProperties[6][3] = 2;


    fishProperties[7][0] = 1;
    fishProperties[7][1] = 1;
    fishProperties[7][2] = 0.1;
    fishProperties[7][3] = 3;

    fishProperties[8][0] = 1.2;
    fishProperties[8][1] = 1.3;
    fishProperties[8][2] = -0.3;
    fishProperties[8][3] = 3;


    for(int i = 0; i < fishesMovedCount; ++i){
        fishesMoved[i].x = 0.0;
        fishesMoved[i].z = 0.0;
        fishesMoved[i].y = fishProperties[i][2];

        fishesMoved[i].rot_angle = 0;

        fishesMoved[i].type = fishProperties[i][3];
    }


}

void OGLAquarium::fishesMove()
{
    int side = 1;
    for(int i = 0; i < fishesMovedCount; ++i){
        if(fishesMoved[i].type == 2 || fishesMoved[i].type == 3) side = -1;
        else side = 1;
      //  if (fishesMoved[i].type == 0 || ){
            fishesMoved[i].rot_angle = 83 + 120* atan(-fishProperties[i][1] / fishProperties[i][0] * tan(side * cur_time)) +
                                       10.0 * rand() / RAND_MAX - 5.0;

            fishesMoved[i].x1 = cos(2 * side * cur_time);
            fishesMoved[i].y1 = sin(side * cur_time * 10) / 10;
            fishesMoved[i].z1 = sin(2 * side *cur_time);

            fishesMoved[i].x = fishProperties[i][0] * fishesMoved[i].x1;
            fishesMoved[i].y = fishesMoved[i].y1 + fishProperties[i][2];
            fishesMoved[i].z = fishProperties[i][1] * fishesMoved[i].z1;
      //  }
    }
}

void OGLAquarium::lighting()
{
//    glEnable(GL_BLEND);

//    glBlendFunc(GL_ONE_MINUS_SRC_ALPHA,GL_SRC_ALPHA);

    GLfloat l_diffuse[4] = { 1.0f, 1.0f, 1.0f, 0.0f };
    GLfloat l_specular[4] = { 1.0f, 1.0f, 1.0f, 0.0f };
    GLfloat l_ambient[4] = { 1.0f, 1.0f, 1.0f, 0.0f };

    if (light1On || light2On || light3On || light4On)
        glEnable(GL_LIGHTING);	//�������� ���������

    if(light1On){
        glEnable(GL_LIGHT0);
        GLfloat l_position0[4] = {2.7, 1.7, 2.7, 1.0};
        glLightfv(GL_LIGHT0,GL_DIFFUSE,l_diffuse);
        glLightfv(GL_LIGHT0,GL_SPECULAR,l_specular);
        glLightfv(GL_LIGHT0,GL_AMBIENT,l_ambient);
        glLightfv(GL_LIGHT0, GL_POSITION, l_position0);
        glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 1);
    }
    else glDisable(GL_LIGHT0);
    if(light2On){
        glEnable(GL_LIGHT1);
        GLfloat l_position1[4] = {2.7, 1.7, -2.7, 1.0};
        glLightfv(GL_LIGHT1,GL_DIFFUSE,l_diffuse);
        glLightfv(GL_LIGHT1,GL_SPECULAR,l_specular);
        glLightfv(GL_LIGHT1,GL_AMBIENT,l_ambient);
        glLightfv(GL_LIGHT1, GL_POSITION, l_position1);
        glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 1);
    }
    else glDisable(GL_LIGHT1);
    if(light3On){
        glEnable(GL_LIGHT2);
        GLfloat l_position2[4] = {-2.7, 1.7, -2.7, 1.0};
        glLightfv(GL_LIGHT2,GL_DIFFUSE,l_diffuse);
        glLightfv(GL_LIGHT2,GL_SPECULAR,l_specular);
        glLightfv(GL_LIGHT2,GL_AMBIENT,l_ambient);
        glLightfv(GL_LIGHT2, GL_POSITION, l_position2);
        glLightf(GL_LIGHT2, GL_CONSTANT_ATTENUATION, 1);
    }
    else glDisable(GL_LIGHT2);
    if(light4On){
        glEnable(GL_LIGHT3);
        GLfloat l_position3[4] = {-2.7, 1.7, 2.7, 1.0};
        glLightfv(GL_LIGHT3,GL_DIFFUSE,l_diffuse);
        glLightfv(GL_LIGHT3,GL_SPECULAR,l_specular);
        glLightfv(GL_LIGHT3,GL_AMBIENT,l_ambient);
        glLightfv(GL_LIGHT3, GL_POSITION, l_position3);
        glLightf(GL_LIGHT3, GL_CONSTANT_ATTENUATION, 1);
    }
    else glDisable(GL_LIGHT3);
//    glDisable(GL_BLEND);
}

void OGLAquarium::drawAxeis()
{
    glPushMatrix();
//    glTranslatef(-1, -1, -1);
    // axe ox
    glBegin(GL_LINES);
        glColor3f(1, 0, 0);
        glVertex3f(0, 0, 0);
        glVertex3f(1, 0, 0);
    glEnd();
    glBegin(GL_LINES);
        // axe oy
        glColor3f(0, 1, 0);
        glVertex3f(0, 0, 0);
        glVertex3f(0, 1, 0);
    glEnd();
    glBegin(GL_LINES);
        // axe oz
        glColor3f(0, 0, 1);
        glVertex3f(0, 0, 0);
        glVertex3f(0, 0, 1);
    glEnd();

    glPopMatrix();

}

void OGLAquarium::createFishList()
{
    int faceCount;
    LVector3 v0, v1, v2;
    LFace *face;
    for(int i1 = 0; i1 < fishesCount; ++i1){
        if(i1 == 0) glNewList(PLANT1_LIST, GL_COMPILE);
        else if(i1 == 1) glNewList(FISH1_LIST, GL_COMPILE);
        else if(i1 == 2) glNewList(FISH2_LIST, GL_COMPILE);
        else if(i1 == 3) glNewList(FISH3_LIST, GL_COMPILE);
        else if(i1 == 4) glNewList(FISH4_LIST, GL_COMPILE);
        else if(i1 == 5) glNewList(FISH5_LIST, GL_COMPILE);
        else if(i1 == 6) glNewList(FISH6_LIST, GL_COMPILE);
        else if(i1 == 7) glNewList(FISH7_LIST, GL_COMPILE);
        glEnable(GL_TEXTURE_2D);

        glColor4f(0, 0, 0, 1);
        glRotatef(-90, 1, 0, 0);
        glBindTexture(GL_TEXTURE_2D, textureID[i1]);
        for(int i2 = 0; i2 < fishes[i1]->GetMeshCount(); ++i2){
            faceCount = fishes[i1]->GetMesh(i2)->GetFaceCount();
            for(int i3 = 0; i3 < faceCount; ++i3){
                face = &(fishes[i1]->GetMesh(i2)->GetFace(i3));
                v0 = face->vertices[0];
                v1 = face->vertices[1];
                v2 = face->vertices[2];
                glBegin(GL_TRIANGLES);
                    glTexCoord2f(face->uv[0].u, face->uv[0].v);
                    glNormal3f(face->normals[0].x, face->normals[0].y, face->normals[0].z);
                    glVertex3f(v0.x, v0.y, v0.z);

                    glTexCoord2f(face->uv[1].u, face->uv[1].v);
                    glNormal3f(face->normals[1].x, face->normals[1].y, face->normals[1].z);
                    glVertex3f(v1.x, v1.y, v1.z);

                    glTexCoord2f(face->uv[2].u, face->uv[2].v);
                    glNormal3f(face->normals[2].x, face->normals[2].y, face->normals[2].z);
                    glVertex3f(v2.x, v2.y, v2.z);
                glEnd();
            }
        }
        glDisable(GL_TEXTURE_2D);
        glEndList();
    }
}

void OGLAquarium::createPlantList()
{
    int faceCount;
    LVector3 v0, v1, v2;
    LFace *face;

    for(int i1 = 0; i1 < plantsCount; ++i1){
        if(i1 == 0) glNewList(PLANT1_LIST, GL_COMPILE);
        else if(i1 == 1) glNewList(PLANT2_LIST, GL_COMPILE);
        else if(i1 == 2) glNewList(PLANT3_LIST, GL_COMPILE);
        glEnable(GL_TEXTURE_2D);
        glColor4f(0, 0, 0, 1);
        glRotatef(-90, 1, 0, 0);
        glBindTexture(GL_TEXTURE_2D, textureID[i1 + fishesCount]);
        for(int i2 = 0; i2 < plants[i1]->GetMeshCount(); ++i2){
            faceCount = plants[i1]->GetMesh(i2)->GetFaceCount();
            for(int i3 = 0; i3 < faceCount; ++i3){
                face = &(plants[i1]->GetMesh(i2)->GetFace(i3));
                v0 = face->vertices[0];
                v1 = face->vertices[1];
                v2 = face->vertices[2];
                 glBegin(GL_TRIANGLES);
                    glTexCoord2f(face->uv[0].u, face->uv[0].v);
                    glNormal3f(face->normals[0].x, face->normals[0].y, face->normals[0].z);
                    glVertex3f(v0.x, v0.y, v0.z);

                    glTexCoord2f(face->uv[1].u, face->uv[1].v);
                    glNormal3f(face->normals[1].x, face->normals[1].y, face->normals[1].z);
                    glVertex3f(v1.x, v1.y, v1.z);

                    glTexCoord2f(face->uv[2].u, face->uv[2].v);
                    glNormal3f(face->normals[2].x, face->normals[2].y, face->normals[2].z);
                    glVertex3f(v2.x, v2.y, v2.z);
                glEnd();
            }
        }
        glDisable(GL_TEXTURE_2D);
        glEndList();
    }
}

void OGLAquarium::createBoxList()
{
    glPushMatrix();
    glNewList(BOX_LIST, GL_COMPILE);
    glEnable(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, textureID[texturesCount - 4]); // "water.bmp"
    glScalef(3, 3, 3);

    //up
    glNormal3d(0.0, -1.0, 0.0);
    glBegin(GL_QUADS);
//        qglColor(Qt::white);
//        glColor4f(0.2, 0.3, 0.5, 0.1);
        glTexCoord2f(1, 1); glVertex3f(-1,  0.5001, -1);
        glTexCoord2f(1, 0); glVertex3f( 1,  0.5001, -1);
        glTexCoord2f(0, 0); glVertex3f( 1,  0.5001,  1);
        glTexCoord2f(0, 1); glVertex3f(-1,  0.5001,  1);
    glEnd();

     //walls
    glBegin(GL_QUADS);
        glColor4f(0.1, 0.2, 0.5, 0.3);
        //front
        glNormal3d(0.0, 0.0, 1.0);
        glTexCoord2f(1, 1);        glVertex3f( 1,   0.5, -1);
        glTexCoord2f(1, 0);        glVertex3f(-1,   0.5, -1);
        glTexCoord2f(0, 0);        glVertex3f(-1,  -0.5, -1);
        glTexCoord2f(0, 1);        glVertex3f( 1,  -0.5, -1);

        //left
        glNormal3d(1.0, 0.0, 0.0);
        glTexCoord2f(1, 1);        glVertex3f(-1,   0.5, -1);
        glTexCoord2f(1, 0);        glVertex3f(-1,   0.5,  1);
        glTexCoord2f(0, 0);        glVertex3f(-1,  -0.5,  1);
        glTexCoord2f(0, 1);        glVertex3f(-1,  -0.5, -1);

        //back
        glNormal3d(0.0, 0.0, -1.0);
        glTexCoord2f(1, 1);        glVertex3f(-1,   0.5,  1);
        glTexCoord2f(1, 0);        glVertex3f( 1,   0.5,  1);
        glTexCoord2f(0, 0);        glVertex3f( 1,  -0.5,  1);
        glTexCoord2f(0, 1);        glVertex3f(-1,  -0.5,  1);

        //right
        glNormal3d(-1.0, 0.0, 0.0);
        glTexCoord2f(1, 1);        glVertex3f( 1,   0.5,  1);
        glTexCoord2f(1, 0);        glVertex3f( 1,   0.5, -1);
        glTexCoord2f(0, 0);        glVertex3f( 1,  -0.5, -1);
        glTexCoord2f(0, 1);        glVertex3f( 1,  -0.5,  1);
    glEnd();

    glDisable(GL_TEXTURE_2D);
    glEndList();
    glPopMatrix();
}

void OGLAquarium::createFloorList()
{
    int faceCount;
    LVector3 v0, v1, v2;
    LFace *face;

    glPushMatrix();
    glNewList(FLOOR_LIST, GL_COMPILE);
    glScalef(3.0/4.5, 1.0, 3.0/2.5);
    glRotatef(-90, 1, 0, 0);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureID[texturesCount - 1]);
    glColor4f(0, 0, 0, 1);
    //  3ds model
    for(int i2 = 0; i2 < floor->GetMeshCount(); ++i2){
        faceCount = floor->GetMesh(i2)->GetFaceCount();
        for(int i3 = 0; i3 < faceCount; ++i3){
            face = &(floor->GetMesh(i2)->GetFace(i3));
            v0 = face->vertices[0];
            v1 = face->vertices[1];
            v2 = face->vertices[2];
             glBegin(GL_TRIANGLES);
                glTexCoord2f(face->uv[0].u, face->uv[0].v);
                glNormal3f(face->normals[0].x, face->normals[0].y, face->normals[0].z);
                glVertex3f(v0.x, v0.y, v0.z);

                glTexCoord2f(face->uv[1].u, face->uv[1].v);
                glNormal3f(face->normals[1].x, face->normals[1].y, face->normals[1].z);
                glVertex3f(v1.x, v1.y, v1.z);

                glTexCoord2f(face->uv[2].u, face->uv[2].v);
                glNormal3f(face->normals[2].x, face->normals[2].y, face->normals[2].z);
                glVertex3f(v2.x, v2.y, v2.z);
            glEnd();
        }
    }
    glDisable(GL_TEXTURE_2D);
    glEndList();

////////////////////////////////////////////////////////////////////////////////
    //splines
   /* int n1 = bsp->GetUBasis()->GetTesselation();
    int n2 = bsp->GetVBasis()->GetTesselation();
    for(int i = 0; i < n1-1; i++) {
        glBegin(GL_QUAD_STRIP);
       // glBegin(GL_QUADS);
        for(int j = 0; j < n2; j++) {
            glTexCoord2f(0, j);
            glNormal(bsp->GetNormal(i, j));
            glVertex(bsp->GetPoint(i, j));
            glTexCoord2f(1, j);
            glNormal(bsp->GetNormal(i+1, j));
            glVertex(bsp->GetPoint(i+1, j));
        }
        glEnd();
    }*/

////////////////////////////////////////////////////////////////////////////////
    // just texture
//    glNormal3d(0.0, 1.0, 0.0);
//    qglColor(Qt::yellow);
//    glEnable(GL_TEXTURE_2D);
//
//    glBindTexture(GL_TEXTURE_2D, textureID[1]);
//    glBegin(GL_QUADS);
//        glTexCoord2f(0, 0);		glVertex3f(-1,  -0.5, -1);
//        glTexCoord2f(0, 1);		glVertex3f(-1,  -0.5,  1);
//        glTexCoord2f(1, 1);		glVertex3f( 1,  -0.5,  1);
//        glTexCoord2f(1, 0);		glVertex3f( 1,  -0.5, -1);
//    glEnd();

    glEndList();
    glDisable(GL_TEXTURE_2D);
    glPopMatrix();
}

void OGLAquarium::createLampList()
{
//    glColor3f(0.2, 0.2, 1.0);
    GLUquadricObj* pQuadric = gluNewQuadric();
    gluQuadricTexture(pQuadric, GL_TRUE);

    glNewList(LAMP_LIST, GL_COMPILE);

    glColor4f(1, 0, 0, 0.8);
    if(light1On){
        glPushMatrix();
        glTranslatef(2.7, 1.7, 2.7);
        gluSphere(pQuadric, 0.1 ,12, 12);
        glPopMatrix();
    }
    if(light2On){
        glPushMatrix();
        glTranslatef(2.7, 1.7, -2.7);
        gluSphere(pQuadric, 0.1 ,12, 12);
        glPopMatrix();
    }
    if(light3On){
        glPushMatrix();
        glTranslatef(-2.7, 1.7, -2.7);
        gluSphere(pQuadric, 0.1 ,12, 12);
        glPopMatrix();
    }
    if(light4On){
        glPushMatrix();
        glTranslatef(-2.7, 1.7, 2.7);
        gluSphere(pQuadric, 0.1 ,12, 12);
        glPopMatrix();
    }
    glEndList();
}

void OGLAquarium::createPompList()
{
    glPushMatrix();
    glNewList(POMP_LIST, GL_COMPILE);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureID[texturesCount - 3]);

    glNormal3f(0.0, 1.0, 0.0);

    GLUquadricObj* pQuadric = gluNewQuadric();
    gluQuadricTexture(pQuadric, GL_TRUE);

    glNewList(LAMP_LIST, GL_COMPILE);

    glColor4f(1, 0, 0, 1.0);
    glPushMatrix();
    glTranslatef(0, -0.7, 0);
    glRotatef(90, 1, 0, 0);
    gluCylinder(pQuadric, 0.05 , 0.1, 0.1, 12, 12);
    glPopMatrix();

    glDisable(GL_TEXTURE_2D);
    glEndList();
    glPopMatrix();
}

void OGLAquarium::createBubbleList(float size)
{
    glPushMatrix();
    glNewList(BUBBLE_LIST, GL_COMPILE);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureID[texturesCount - 2]);

    glNormal3f(0.0, 0.0, 1.0);
    glBegin(GL_QUADS);
        glTexCoord2f(0,0); glVertex3f(-size, -size, 0);
        glTexCoord2f(0,1); glVertex3f(size, -size, 0);
        glTexCoord2f(1,1); glVertex3f(size, size, 0);
        glTexCoord2f(1,0); glVertex3f(-size, size, 0);
    glEnd();
    glBegin(GL_QUADS);
        glTexCoord2f(0,0); glVertex3f(-size, -size, 0);
        glTexCoord2f(1,0); glVertex3f(-size, size, 0);
        glTexCoord2f(1,1); glVertex3f(size, size, 0);
        glTexCoord2f(0,1); glVertex3f(size, -size, 0);
    glEnd();

    glEndList();
    glPopMatrix();
    glDisable(GL_TEXTURE_2D);
}

void OGLAquarium::createMirrorList()
{
    glDisable(GL_TEXTURE_2D);

    glNewList(MIRROR_LIST,GL_COMPILE);

        GLfloat diffuse_1[4]  = { 0.0, 0.0f, 0.2f, 0.1f };
        GLfloat specular_1[4]  = { 0.9f, 0.9f, 0.9f, 0.1f };
        GLfloat ambient_1[4]  = { 0.1f, 0.1f, 0.2f, 0.1f };

        glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse_1);
        glMaterialfv(GL_FRONT, GL_SPECULAR, specular_1);
        glMaterialfv(GL_FRONT, GL_AMBIENT, ambient_1);
//        glMaterialf(GL_FRONT, GL_SHININESS, 10);

        glColor4f(0,0, 0.1, 0.2);

        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, textureID[texturesCount - 4]);
        //����
        glNormal3d(0.0, -1.0, 0.0);

        //glutSolidTorus(0.4, 0.0, 30,2);
        int N = 30;
        float r = 2.0, alpha, betta;
        for (int i = 0; i < N; i++)
        {
            alpha = 2.0 * M_PI * i / float(N);
            betta = 2.0 * M_PI * (i + 1) / float(N);
            glBegin(GL_TRIANGLES);
                glTexCoord2f(0, 0);
                glVertex3f(0, 0, 0);
                glTexCoord2f(-cos(betta),sin(betta));
                glVertex3f(-r * cos(betta), 0 , r * sin(betta));
                glTexCoord2f(-cos(alpha), sin(alpha));
                glVertex3f(-r * cos(alpha), 0 , r * sin(alpha));
            glEnd();
        }
        glDisable(GL_TEXTURE_2D);
    glEndList();

    glEnable(GL_TEXTURE_2D);
}

void OGLAquarium::renderFishes()
{
    fishesMove();

//    if (fishesMoved[0].x == 1 && fishesMoved[0].y == 0 && fishesMoved[0].z == 0){
//        qDebug() << "!!!!!! time = " << cur_time ;
//    }
    for(int i = 0; i < fishesMovedCount; ++i){
        glPushMatrix();
//        glRotatef(cur_time , fishesMoved[i].x, fishesMoved[i].y, fishesMoved[i].z);
        glTranslatef(fishesMoved[i].x, fishesMoved[i].y, fishesMoved[i].z);
        glRotatef (fishesMoved[i].rot_angle, 0, 1, 0);

//        glRotatef(cur_time * 100, 1, 0, 0);
        if(i == 0) glCallList(FISH1_LIST);
        if(i == 1) {glScalef(0.7, 0.7, 0.7); glCallList(FISH1_LIST);}
        if(i == 2) glCallList(FISH1_LIST);
        if(i == 3) {glScalef(0.5, 0.5, 0.5); glCallList(FISH1_LIST);}
        if(i == 4) {glScalef(0.6, 0.6, 0.6); glCallList(FISH1_LIST);}
        if(i == 5) {glCallList(FISH3_LIST);}
        if(i == 6) { glTranslatef(1, 0, 1); glRotatef(180, 0, 1, 0); glCallList(FISH2_LIST);}
        if(i == 7) {glRotatef(180, 0, 1, 0); glCallList(FISH4_LIST);}
        if(i == 8) {glRotatef(180, 0, 1, 0); glCallList(FISH5_LIST);}

        glPopMatrix();
    }
}

void OGLAquarium::renderPlants()
{
    glPushMatrix();
    glTranslatef(0.2, -0.6, -1.5);
    glScalef(0.025, 0.025, 0.025);
    glCallList(PLANT1_LIST);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-1.7, -0.55, 1.1);
    glRotatef(35, 0, 1, 0);
    glScalef(0.04, 0.04, 0.04);
    glCallList(PLANT1_LIST);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(2.7, 0.7, 1.0);
    glScalef(0.02, 0.02, 0.02);
    glCallList(PLANT2_LIST);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(1, -0.25, 1);
    glScalef(0.02, 0.02, 0.02);
    glCallList(PLANT3_LIST);
    glPopMatrix();
}

void OGLAquarium::renderBorders()
{
    glPushMatrix();
    glTranslatef(0.0, -1.5, 0.0);
    glCallList(FLOOR_LIST);
    glPopMatrix();

    glEnable(GL_CULL_FACE);
    glPushMatrix();
    glCallList(BOX_LIST);
    glPopMatrix();
    glDisable(GL_CULL_FACE);
}

void OGLAquarium::renderLamp()
{
    glPushMatrix();
    glCallList(LAMP_LIST);
    glPopMatrix();
}

void OGLAquarium::renderBubbles()
{
    glColor4f(0.0, 0.0, 0.0, 0.3);

    glDisable(GL_FOG);
    glEnable(GL_BLEND);
//    glEnable(GL_ALPHA_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureID[texturesCount - 2]);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    bubblesMove();

    const int N = bubblesCount;

    // count a distances from the visor to each bubble
    float matrix[16];
    float distance[N];
    for(int i = 0 ; i < N; ++i){
        glPushMatrix();
        glTranslatef(bubbles[i].x, bubbles[i].y, bubbles[i].x);
        rotateCameraToVisor();
        glGetFloatv(GL_MODELVIEW_MATRIX, matrix);
        distance[i] =  pow(matrix[12], 2) + pow(matrix[13], 2) + pow(matrix[14], 2);
        glPopMatrix();
    }

    //sorting bubbles by index // by bubble sorting ;)
    int ind[N];
    for (int i = 0; i < N; i++)
        ind[i] = i;
    int tmp, flag;
    for(int i = N - 1; i > 0; --i){
        flag = 0;
        for(int j = 0; j < i; ++j)
            if(distance[ind[j]] < distance[ind[j + 1]]){
                tmp = ind[j];
                ind[j] = ind[j + 1];
                ind[j + 1] = tmp;
                flag = 1;
            }
        if(!flag) break;
    }

    //drawing
    float size;
//    qDebug() << "----------------";
    for (int i = 0; i < N ; ++i)  {
//        qDebug() << "i = " << i << "\t dist[i] = " << distance[ind[i]];
        glPushMatrix();
        glTranslatef(bubbles[ind[i]].x, bubbles[ind[i]].y, bubbles[ind[i]].z);
        rotateCameraToVisor();
        size = bubbles[ind[i]].r;

        glNormal3f(0.0, 0.0, 1.0);
        glBegin(GL_QUADS);
            glTexCoord2f(0,0); glVertex3f(-size, -size, 0);
            glTexCoord2f(0,1); glVertex3f(size, -size, 0);
            glTexCoord2f(1,1); glVertex3f(size, size, 0);
            glTexCoord2f(1,0); glVertex3f(-size, size, 0);
        glEnd();
        glNormal3f(0.0, 0.0, -1.0);
        glBegin(GL_QUADS);
            glTexCoord2f(0,0); glVertex3f(-size, -size, 0);
            glTexCoord2f(1,0); glVertex3f(-size, size, 0);
            glTexCoord2f(1,1); glVertex3f(size, size, 0);
            glTexCoord2f(0,1); glVertex3f(size, -size, 0);
        glEnd();
        glPopMatrix();
    }

//    glDisable(GL_ALPHA_TEST);
    glEnable(GL_CULL_FACE);
    glDisable(GL_TEXTURE_2D);

//    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glDisable(GL_BLEND);
    glEnable(GL_FOG);
}

void OGLAquarium::renderPomp()
{
    glPushMatrix();
    glCallList(POMP_LIST);
    glPopMatrix();
}

void OGLAquarium::renderMirror()
{
    glPushMatrix();

    //mark a trafaret's pixels of reflecting surface in the buffer
    glEnable(GL_STENCIL_TEST);
    glStencilOp(GL_ZERO, GL_ZERO, GL_REPLACE);
    glStencilFunc(GL_ALWAYS, 1, 1);
    glDepthMask(GL_FALSE);
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

    glPushMatrix();
    glTranslatef(0, 1.5, 0);
//    glRotatef(180, 1, 0, 0);
    glCallList(MIRROR_LIST);
    glPopMatrix();

    glDepthMask(GL_TRUE);
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE,GL_TRUE);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
    glStencilFunc(GL_EQUAL, 1, 1);

    // draw reflecting objects
    glPushMatrix();
    glTranslatef(0, 1.5, 0);
//    glRotatef(180, 1, 0, 0);
    glScalef(1, -1, 1);
    glFrontFace(GL_CW);

    lighting();
    renderAllObjects();
    glFrontFace(GL_CCW);

//    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    renderBubbles();
    glEnable(GL_DEPTH_TEST);

    glPopMatrix();
    glDisable(GL_STENCIL_TEST);


    // draw surface
    glDisable(GL_LIGHTING);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glPushMatrix();
    glTranslatef(0, 1.5, 0);
    glCallList(MIRROR_LIST);
    glPopMatrix();


    glEnable(GL_LIGHTING);
    glDisable(GL_BLEND);

    glPopMatrix();
}

void OGLAquarium::renderAllObjects()
{
    renderFishes();
    renderPlants();
    renderBorders();
    renderPomp();
    renderLamp();
//    renderBubbles();
}

void OGLAquarium::rotateCameraToVisor()
{
    //undo all rotates
    GLfloat _old[16];
    glGetFloatv(GL_MODELVIEW_MATRIX, _old);
    const GLfloat _new[16] = {1.0, 0.0, 0.0, 0.0,
                            0.0, 1.0, 0.0, 0.0,
                            0.0, 0.0, 1.0, 0.0,
                            _old[12], _old[13], _old[14], _old[15]};
    glLoadMatrixf(_new);
}

void OGLAquarium::moveMouseToCenter(int x, int y)
{
    if(x == width() / 2 && y == height() / 2) return;
    QPoint pos = mapToGlobal(QPoint(0, 0));
    SetCursorPos(width() / 2 + pos.x(), height() / 2 + pos.y());
//    updateGL();
    //redrawing();
}

void OGLAquarium::mouseMoveEvent(QMouseEvent *event)
{
    int delta_x = event->x() - width() / 2;
    int delta_y = event->y() - height() / 2;

    camera->Rotate(-delta_x, delta_y);

    moveMouseToCenter(event->x(), event->y());
}

void OGLAquarium::keyPressEvent(QKeyEvent *event)
{
    switch(event->key()){
        case Qt::Key_W:
            camera->setKeyW(true);
            break;
        case Qt::Key_S:
            camera->setKeyS(true);
            break;
        case Qt::Key_A:
            camera->setKeyA(true);
            break;
        case Qt::Key_D:
            camera->setKeyD(true);
            break;
        case Qt::Key_Space:
            camera->setKeySpace(true);
            break;
        case Qt::Key_C:
            camera->setKeyC(true);
            break;
        case Qt::Key_F9:
            if(antiAliasing == 2) antiAliasing = 0;
            else antiAliasing = 2;
            break;
        case Qt::Key_F10:
            if(antiAliasing == 4) antiAliasing = 0;
            else antiAliasing = 4;
            break;
        case Qt::Key_F11:
            if(antiAliasing == 8) antiAliasing = 0;
            else antiAliasing = 8;
            break;
        case Qt::Key_F12:
            if(antiAliasing == 12) antiAliasing = 0;
            else antiAliasing = 12;
            break;
        case Qt::Key_Q:
            exit(0);
        case Qt::Key_1:
            light1On = !light1On;
            createLampList();
            break;
        case Qt::Key_2:
            light2On = !light2On;
            createLampList();
            break;
        case Qt::Key_3:
            light3On = !light3On;
            createLampList();
            break;
        case Qt::Key_4:
            light4On = !light4On;
            createLampList();
            break;
    }
//    camera->Translate();
    redrawing();
}

void OGLAquarium::keyReleaseEvent(QKeyEvent *event)
{
    switch(event->key()){
        case Qt::Key_W:
            camera->setKeyW(false);
            break;
        case Qt::Key_S:
            camera->setKeyS(false);
            break;
        case Qt::Key_A:
            camera->setKeyA(false);
            break;
        case Qt::Key_D:
            camera->setKeyD(false);
            break;
        case Qt::Key_Space:
            camera->setKeySpace(false);
            break;
        case Qt::Key_C:
            camera->setKeyC(false);
            break;
    }
//    camera->Translate();
//    redrawing();
}

void OGLAquarium::redrawing()
{
    cur_time += 0.03;
//    if (cur_time == 1000) cur_time = 0.0;
    updateGL();
}
