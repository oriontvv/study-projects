TEMPLATE = app
QT += opengl
SOURCES = main.cpp \
    OGLAquarium.cpp \
    l3ds.cpp \
    BmpLoad.cpp \
    Camera.cpp
TARGET = OGLAquarium
HEADERS += OGLAquarium.h \
    l3ds.h \
    Formats.h \
    BmpLoad.h \
    Camera.h
