
/*
 * NAME: Taranov Vassiliy Vassilevich, 325
 * ASGN: N4
 */

#include <QApplication>

#include "OGLAquarium.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    OGLAquarium Aquarium;

//    Aquarium.show();
    Aquarium.showFullScreen();

    return app.exec();
}

