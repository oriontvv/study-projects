
/*
 * NAME: Taranov Vassiliy Vassilevich, 325
 * ASGN: N4
 */

#ifndef OGLAQUARIUM_H
#define OGLAQUARIUM_H

#include <QGLWidget>
#include <QMouseEvent>
#include <QSound>
#include <QTimer>
#include <QImage>

#include <QDebug>

#include "l3ds.h"
#include "BmpLoad.h"
#include "Camera.h"

#define FISH1_LIST 21
#define FISH2_LIST 22
#define FISH3_LIST 23
#define FISH4_LIST 24
#define FISH5_LIST 25
#define FISH6_LIST 26
#define FISH7_LIST 27
#define FISH8_LIST 28

#define PLANT1_LIST 7
#define PLANT2_LIST 8
#define PLANT3_LIST 9
#define BOX_LIST 10
#define FLOOR_LIST 11
#define LAMP_LIST 12
#define BUBBLE_LIST 13
#define POMP_LIST 14
#define MIRROR_LIST 15


struct Bubble
{
    float x, y, z;  //current position
    float x1, y1, z1; //draught vector
    float r; // radius
};

struct Fish
{
    int type; // type = 0..fishesMovedCount
    float x, y, z;  //current position
    float x1, y1, z1;   //draught vector
    float a, b; // parameters for moving
    float rot_angle;
};

class OGLAquarium : public QGLWidget
{
    Q_OBJECT
private:
    QSound *music;

    int texturesCount;
    GLuint* textureID;

    int fishesCount, plantsCount;
    L3DS **fishes, **plants, *floor;

    int loadTextures();

    void renderAllObjects();
    void renderFishes();
    void renderPlants();
    void renderBorders();
    void renderLamp();
    void renderBubbles();
    void renderPomp();
    void renderMirror();

    void createFishList();
    void createPlantList();
    void createBoxList();
    void createFloorList();
    void createLampList();
    void createBubbleList(float size);
    void createPompList();
    void createMirrorList();

    void bubblesInit();
    void bubblesMove();
    void fishesInit();
    void fishesMove();

    void drawAxeis();

    QTimer *timer;
    float cur_time;
    void lighting();
    bool light1On, light2On, light3On, light4On;
    int antiAliasing;

    void rotateCameraToVisor();
    Camera *camera;

    //fog
    float fogColor[4];

    //bubbles
    int bubblesCount;
    Bubble *bubbles;
    //fishes
    int fishesMovedCount;
    Fish *fishesMoved;
    float **fishProperties;

protected:
    virtual void initializeGL();
    virtual void resizeGL(int nWidth, int nHeight);
    virtual void paintGL();
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void keyReleaseEvent(QKeyEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void moveMouseToCenter(int x, int y);

public:
    OGLAquarium(QWidget* pwgt = 0);
    ~OGLAquarium();
public slots:
    void redrawing();
};

#endif // OGLAQUARIUM_H
