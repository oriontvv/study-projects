#ifndef SOUND_H
#define SOUND_H

#include <QWidget>
#include "bass.h"
#include "FftWrapper.h"

#define HEADER_SIZE 20

class Sound : public QWidget
{
	Q_OBJECT

public:
	Sound(QWidget *parent);
	Sound();
	~Sound();

private:
	Fft FFT;
	HSAMPLE sample;
	HCHANNEL channel;
	qint16 *file_ptr;
	quint32 data_size;
	qint16 *data_ptr;
	//float *float_data;
	uint FFT_size;
	uint MAX_amplitude;
	float *phases;
	QImage *spectrogram;

	void paintEvent(QPaintEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void drawSpecrogram();
	void updateData();

public slots:
	void loadFile();
	void playSound();
	void pauseSound();
	void stopSound();
	void setFFT_size(QString FFT_size);
};

#endif // SOUND_H
