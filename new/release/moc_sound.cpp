/****************************************************************************
** Meta object code from reading C++ file 'sound.h'
**
** Created: Sat 14. Nov 14:15:28 2009
**      by: The Qt Meta Object Compiler version 61 (Qt 4.5.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../sound.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'sound.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 61
#error "This file was generated using the moc from 4.5.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Sound[] = {

 // content:
       2,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   12, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors

 // slots: signature, parameters, type, tag, flags
       7,    6,    6,    6, 0x0a,
      18,    6,    6,    6, 0x0a,
      30,    6,    6,    6, 0x0a,
      43,    6,    6,    6, 0x0a,
      64,   55,    6,    6, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Sound[] = {
    "Sound\0\0loadFile()\0playSound()\0"
    "pauseSound()\0stopSound()\0FFT_size\0"
    "setFFT_size(QString)\0"
};

const QMetaObject Sound::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Sound,
      qt_meta_data_Sound, 0 }
};

const QMetaObject *Sound::metaObject() const
{
    return &staticMetaObject;
}

void *Sound::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Sound))
        return static_cast<void*>(const_cast< Sound*>(this));
    return QWidget::qt_metacast(_clname);
}

int Sound::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: loadFile(); break;
        case 1: playSound(); break;
        case 2: pauseSound(); break;
        case 3: stopSound(); break;
        case 4: setFFT_size((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
