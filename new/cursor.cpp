#include "cursor.h"
#include <math.h>
#include <QPainter>

#define CURSOR_SIZE 55

Cursor::Cursor(QWidget *parent):QFrame(parent)
{
	QPalette palette;

	palette.setColor(backgroundRole(), Qt::white);
	setPalette(palette);
	setAutoFillBackground(true);

	cursor = new QImage(CURSOR_SIZE, CURSOR_SIZE, QImage::Format_ARGB32);

	radius = 1;
	power = 0;

	drawCursor();
}

Cursor::~Cursor()
{
	delete cursor;
}

void Cursor::paintEvent(QPaintEvent *event)
{

	QPainter painter(this);

	painter.drawImage(0, 0, *cursor);

}

void Cursor::drawCursor()
{
	//float k1 = power/(1-exp(radius));
	//float k2 = power/(1-1/exp(radius));
	//int current_radius;


	for(int x = 0; x < CURSOR_SIZE; x++)
	{
		for(int y = 0; y < CURSOR_SIZE; y++)
		{
			cursor->setPixel(x, y, qRgba(0, 0, 0, 0));
		}
	}


/*
	QPainter painter(cursor);

	painter.setWindow(-CURSOR_SIZE/2, -CURSOR_SIZE/2, CURSOR_SIZE, CURSOR_SIZE);
	painter.setRenderHint(QPainter::Antialiasing, true);

	for(int x = 0; x < CURSOR_SIZE/2; x++)
	{
		if(x >= radius)
		{
		//	painter.setPen(QPen(QColor(255, 255, 255, 0), 0));
		//	painter.drawEllipse(QPoint(0, 0), x, x);
		}
		else
		{
			painter.setPen(QPen(QColor(255, 255, 255, k1*exp(x)+k2), 0));
			painter.drawEllipse(QPoint(0, 0), x, x);
		}
	}
*/
QPainter painter(cursor);
QPointF ptCenter (CURSOR_SIZE/2.0, CURSOR_SIZE/2.0) ;
QRadialGradient gradient(ptCenter, radius, ptCenter);
gradient.setColorAt(0, QColor(0, 0, 0, power));
//gradient.setColorAt(0.3, QColor(0, 0, 0, power));
gradient.setColorAt(1, QColor(0, 0, 0, 0));
painter.setBrush(gradient);
painter.drawRect(-1, -1, CURSOR_SIZE+1, CURSOR_SIZE+1 );

painter.setPen(QPen(QColor(255, 0, 0), 0));
painter.drawPoint(27, 27);

	update();
}

void Cursor::setRadius(int radius)
{
	this->radius = radius;
	drawCursor();

	emit cursorChanged();
}

void Cursor::setPower(int power)
{
	this->power = power;
	drawCursor();

	emit cursorChanged();
}

QPixmap Cursor::getCursor()
{
	return QPixmap::fromImage(*cursor);
}
