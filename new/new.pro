# -------------------------------------------------
# Project created by QtCreator 2009-10-30T01:14:59
# -------------------------------------------------
LIBS += bass.lib
TARGET = new
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    cursor.cpp \
    FftWrapper.cpp \
    sound.cpp
HEADERS += mainwindow.h \
    cursor.h \
    bass.h \
    FftWrapper.h \
    sound.h
FORMS += mainwindow.ui
RESOURCES += rc.qrc
