#include "sound.h"
#include "math.h"
#include <QPainter>
#include <QFileDialog>
#include <QMessageBox>
#include <QMouseEvent>

#include <QDebug>

Sound::Sound(QWidget *parent) : QWidget(parent)
{
	QPalette palette;

	palette.setColor(backgroundRole(), Qt::black);
	setPalette(palette);
	setAutoFillBackground(true);

	spectrogram = NULL;
	FFT_size = 256;
}

Sound::Sound()
{
	QPalette palette;

	palette.setColor(backgroundRole(), Qt::black);
	setPalette(palette);
	setAutoFillBackground(true);

	spectrogram = NULL;
	FFT_size = 256;
}

Sound::~Sound()
{
}

void Sound::paintEvent(QPaintEvent *event)
{

	QPainter painter(this);


	if(spectrogram)
	{
		//painter.scale(1.0*width()/spectrogram->width(), 1.0*height()/spectrogram->height());
		painter.drawImage(0, 0, *spectrogram);
	}
}

void Sound::loadFile()
{
	//BASS_SampleFree(sample);

	const char *file_name = QFileDialog::getOpenFileName(0, "Open File", "", "*.wav\n*.*").toAscii().constData();
	qint16 header[HEADER_SIZE];
	uint i;

	FILE *input_file = fopen(file_name, "rb");

	if(!input_file)
	{
		qDebug() << "Can not open file!";
		return;
	}

	fread(header, sizeof(qint16), HEADER_SIZE, input_file);
	fread(&data_size, sizeof(data_size), 1, input_file);

	file_ptr = new qint16[(sizeof(header) + sizeof(data_size) + data_size)/2];
	//float_data = new float[data_size/2];
	MAX_amplitude = 0;

	for(i = 0; i < HEADER_SIZE; i++)
		file_ptr[i] = header[i];

	*(quint32 *)&file_ptr[HEADER_SIZE] = data_size;
	data_ptr = &file_ptr[(sizeof(header) + sizeof(data_size))/2];

	for(i = 0; i < data_size/2; i++)
	{
		fread(&file_ptr[(sizeof(header) + sizeof(data_size))/2 + i], sizeof(qint16), 1, input_file);
		qint16 tmp = file_ptr[(sizeof(header) + sizeof(data_size))/2 + i];

		if(abs(tmp) > MAX_amplitude)
			MAX_amplitude = abs(tmp);
	}
/*
	sample = BASS_SampleLoad(TRUE, file_ptr, 0, sizeof(header) + sizeof(data_size) + data_size, 1,
							 BASS_SAMPLE_LOOP | BASS_SAMPLE_MONO );//| BASS_SAMPLE_FLOAT);
	if(!sample)
	{
		QMessageBox *msg = new QMessageBox("Error", "BASS: Can't read file!", QMessageBox::Information,
										   0, QMessageBox::Ok, 0);
		msg->exec();
		qDebug() << BASS_ErrorGetCode();
		return;
	}

	channel = BASS_SampleGetChannel(sample, FALSE);
	qDebug() << BASS_ChannelGetLength(channel) << data_size;*/
	MAX_amplitude *= FFT_size;drawSpecrogram();
}

void Sound::playSound()
{
	//qDebug() << "play channel" << channel;	//channel = BASS_SampleGetChannel(sample, FALSE);



	sample = BASS_SampleLoad(TRUE, file_ptr, 0, HEADER_SIZE*2 + sizeof(data_size) + data_size, 1,
							 BASS_SAMPLE_LOOP | BASS_SAMPLE_MONO );
	qDebug() << BASS_ErrorGetCode();
	channel = BASS_SampleGetChannel(sample, FALSE);
	qDebug() << BASS_ErrorGetCode();

	BASS_ChannelPlay(sample, FALSE);
}

void Sound::pauseSound()
{
	//updateData();
	drawSpecrogram();
	BASS_ChannelPause(sample);
}

void Sound::stopSound()
{updateData();
	BASS_ChannelStop(sample);

}

void Sound::setFFT_size(QString FFT_size)
{
	this->FFT_size = FFT_size.toUInt();

	if(spectrogram)
		drawSpecrogram();
}


void Sound::drawSpecrogram()
{
	spectrogram = new QImage(data_size * 2 / FFT_size, FFT_size/2 + 1,  QImage::Format_ARGB32);
	float part[FFT_size];
	Cmplx spectr[FFT_size];
	float column[FFT_size/2 + 1];

	resize(spectrogram->width(), spectrogram->height());
	phases = new float[spectrogram->width() * spectrogram->height()];
	//MAX_amplitude *= FFT_size;
	FFT.SetMode(FFT_REAL, FFT_size);

	for(uint i = 0; data_size/2 - i * FFT_size/4 > FFT_size * 3/4; i++)
	{
		for(uint j = 0; j < FFT_size; j++)
		{
			if(i * FFT_size/4 + j < data_size/2)
				part[j] = data_ptr[i * FFT_size/4 + j] * (1 - cos(2*M_PI * j / (FFT_size - 1))) / 2;
			else
				part[j] = 0;
		}

		FFT.FftReal(part, spectr);

		for(uint j = 0; j < FFT_size/2 + 1; j++)
		{
			float amplitude = sqrt(pow(spectr[j].im, 2) + pow(spectr[j].re, 2));

			if(amplitude / MAX_amplitude > 0.000001)
				column[j] = 20 * log10(1.0 * amplitude / MAX_amplitude);
			else
				column[j] = -120;

			spectrogram->setPixel(i, spectrogram->height() - j - 1,
								  qRgba(255, 0, 0, 255 + 2.12/*1.8*/ * column[j]));

			if(spectr[j].re > 0)
				phases[i * (FFT_size/2 + 1) + j] = atan(spectr[j].im / spectr[j].re);
			else
				if(spectr[j].re < 0)
					phases[i * (FFT_size/2 + 1) + j] = atan(spectr[j].im / spectr[j].re) + M_PI;
			else
				phases[i * (FFT_size/2 + 1) + j] = M_PI / 2;
		}
	}

	update();
}

void Sound::updateData()
{
	float part[FFT_size];
	Cmplx spectr[FFT_size];

	for(uint k = 0; k < data_size/2; k++)
			data_ptr[k] = 0;

	for(uint i = 0; data_size/2 - i * FFT_size/4 > FFT_size * 3/4; i++)
	{
		for(uint j = 0; j < FFT_size/2 + 1; j++)
		{
			uchar alpha = qAlpha(spectrogram->pixel(i, spectrogram->height() - j - 1));
			float amplitude = pow(10, (alpha - 255)/(2.12 * 20)) * MAX_amplitude;
			if(amplitude <= 0.000001 * MAX_amplitude)
				amplitude = 0;

			spectr[j].re = amplitude * cos(phases[i * (FFT_size/2 + 1) + j]);
			spectr[j].im = amplitude * sin(phases[i * (FFT_size/2 + 1) + j]);
		}

		FFT.InvFftReal(spectr, part);

		for(uint k = 0; k < FFT_size; k++)
			part[k] *= (1 - cos(2*M_PI * k / (FFT_size - 1))) / 3.0;

		for(uint k = 0; (k < FFT_size) && (i * FFT_size/4 + k < data_size/2); k++)
			data_ptr[i * FFT_size/4 + k] += (qint16 )part[k];

	}
}

void Sound::mouseMoveEvent(QMouseEvent *event)
{
	QPoint position = event->pos();

	if(position.x() >= 0 && position.y() >= 0 &&
	   position.x() < width() && position.y() < height())
	{
		QImage kern(cursor().pixmap().toImage());

		for(int x = position.x() - kern.width()/2; x <= position.x() + kern.width()/2; x++)
		{
			for(int y = position.y() - kern.height()/2; y <= position.y() + kern.height()/2; y++)
			{
				if(x >= 0 && y >= 0 && x < spectrogram->width() && y < spectrogram->height())
				{
					short alpha = qAlpha(spectrogram->pixel(x, y))-qAlpha(spectrogram->pixel(x, y))*(
								  qAlpha(kern.pixel(x - position.x() + kern.width()/2,
													y - position.y() + kern.height()/2)))/7040.0;

					if(alpha < 0)
						spectrogram->setPixel(x, y, qRgba(255, 255, 255, 0));
					else
						spectrogram->setPixel(x, y, qRgba(255, 0, 0, alpha));
				}
			}
		}

		update();
	}
}
