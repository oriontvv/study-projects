#ifndef CURSOR_H
#define CURSOR_H

#include <QFrame>

class Cursor : public QFrame
{
	Q_OBJECT

	QImage *cursor;
	int radius;
	int power;

public:
	Cursor(QWidget *parent);
	~Cursor();
	QPixmap getCursor();

private:
	void drawCursor();
	void paintEvent(QPaintEvent *event);

public slots:
	void setRadius(int radius);
	void setPower(int power);

signals:
	void cursorChanged();
};

#endif // CURSOR_H
