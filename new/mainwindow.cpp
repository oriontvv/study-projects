#include "mainwindow.h"
#include "ui_mainwindow.h"
//#include "sound.h"
#include "cursor.h"
#include <QScrollArea>
//#include "bass.h"
//#include "FftWrapper.h"

//#include <QMessageBox>
//#include <math.h>
//#include <stdlib.h>

#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent), ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	connect(ui->play, SIGNAL(clicked()), &scrollWidget, SLOT(playSound()));
	connect(ui->pause, SIGNAL(clicked()), &scrollWidget, SLOT(pauseSound()));
	connect(ui->stop, SIGNAL(clicked()), &scrollWidget, SLOT(stopSound()));
	connect(ui->FFT_size, SIGNAL(activated(QString)), &scrollWidget, SLOT(setFFT_size(QString)));
	connect(ui->actionOpen, SIGNAL(triggered()), &scrollWidget, SLOT(loadFile()));

    ui->scrollArea->resize(500, 400);

	ui->scrollArea->setWidget(&scrollWidget);
    ui->scrollArea->viewport()->show();
	ui->scrollArea->show();
    ui->scrollArea->resize(500, 400);

}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::updateCursor()
{
	scrollWidget.setCursor(QCursor(ui->cursor->getCursor()));
	scrollWidget.resize(1800, 600);
}




