/********************************************************************************
** Form generated from reading ui file 'mainwindow.ui'
**
** Created: Fri 13. Nov 02:58:46 2009
**      by: Qt User Interface Compiler version 4.5.3
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QScrollArea>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QStatusBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "cursor.h"
#include "sound.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *verticalLayout;
    QScrollArea *scrollArea;
    Sound *picture;
    QSlider *slider;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *brush;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    Cursor *cursor;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_2;
    QSlider *radiusSlider;
    QLabel *label_3;
    QSlider *powerSlider;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QComboBox *FFT_size;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QPushButton *play;
    QPushButton *pause;
    QPushButton *stop;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(658, 363);
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout_4 = new QHBoxLayout(centralWidget);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setMargin(11);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        picture = new Sound();
        picture->setObjectName(QString::fromUtf8("picture"));
        picture->setGeometry(QRect(0, 0, 459, 273));
        scrollArea->setWidget(picture);

        verticalLayout->addWidget(scrollArea);

        slider = new QSlider(centralWidget);
        slider->setObjectName(QString::fromUtf8("slider"));
        slider->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(slider);


        horizontalLayout_4->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        brush = new QGroupBox(centralWidget);
        brush->setObjectName(QString::fromUtf8("brush"));
        verticalLayout_3 = new QVBoxLayout(brush);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setMargin(11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        cursor = new Cursor(brush);
        cursor->setObjectName(QString::fromUtf8("cursor"));
        cursor->setMinimumSize(QSize(55, 55));
        cursor->setMaximumSize(QSize(55, 55));
        cursor->setFrameShape(QFrame::StyledPanel);
        cursor->setFrameShadow(QFrame::Raised);

        horizontalLayout_2->addWidget(cursor);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout_3->addLayout(horizontalLayout_2);

        label_2 = new QLabel(brush);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_3->addWidget(label_2);

        radiusSlider = new QSlider(brush);
        radiusSlider->setObjectName(QString::fromUtf8("radiusSlider"));
        radiusSlider->setMinimum(1);
        radiusSlider->setMaximum(27);
        radiusSlider->setPageStep(1);
        radiusSlider->setOrientation(Qt::Horizontal);

        verticalLayout_3->addWidget(radiusSlider);

        label_3 = new QLabel(brush);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        verticalLayout_3->addWidget(label_3);

        powerSlider = new QSlider(brush);
        powerSlider->setObjectName(QString::fromUtf8("powerSlider"));
        powerSlider->setMaximum(255);
        powerSlider->setOrientation(Qt::Horizontal);

        verticalLayout_3->addWidget(powerSlider);


        verticalLayout_2->addWidget(brush);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_3->addWidget(label);

        FFT_size = new QComboBox(centralWidget);
        FFT_size->setObjectName(QString::fromUtf8("FFT_size"));

        horizontalLayout_3->addWidget(FFT_size);

        horizontalLayout_3->setStretch(1, 1);

        verticalLayout_2->addLayout(horizontalLayout_3);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        play = new QPushButton(centralWidget);
        play->setObjectName(QString::fromUtf8("play"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(5);
        sizePolicy.setVerticalStretch(2);
        sizePolicy.setHeightForWidth(play->sizePolicy().hasHeightForWidth());
        play->setSizePolicy(sizePolicy);
        play->setMaximumSize(QSize(50, 20));

        horizontalLayout->addWidget(play);

        pause = new QPushButton(centralWidget);
        pause->setObjectName(QString::fromUtf8("pause"));
        sizePolicy.setHeightForWidth(pause->sizePolicy().hasHeightForWidth());
        pause->setSizePolicy(sizePolicy);
        pause->setMaximumSize(QSize(50, 20));

        horizontalLayout->addWidget(pause);

        stop = new QPushButton(centralWidget);
        stop->setObjectName(QString::fromUtf8("stop"));
        sizePolicy.setHeightForWidth(stop->sizePolicy().hasHeightForWidth());
        stop->setSizePolicy(sizePolicy);
        stop->setMaximumSize(QSize(50, 20));

        horizontalLayout->addWidget(stop);


        verticalLayout_2->addLayout(horizontalLayout);

        verticalLayout_2->setStretch(2, 4);

        horizontalLayout_4->addLayout(verticalLayout_2);

        horizontalLayout_4->setStretch(0, 1);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 658, 22));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuFile->addAction(actionOpen);

        retranslateUi(MainWindow);
        QObject::connect(radiusSlider, SIGNAL(valueChanged(int)), cursor, SLOT(setRadius(int)));
        QObject::connect(powerSlider, SIGNAL(valueChanged(int)), cursor, SLOT(setPower(int)));
        QObject::connect(cursor, SIGNAL(cursorChanged()), MainWindow, SLOT(updateCursor()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        actionOpen->setText(QApplication::translate("MainWindow", "Open ...", 0, QApplication::UnicodeUTF8));
        brush->setTitle(QApplication::translate("MainWindow", "Brush", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "Radius", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindow", "Power", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "FFT size", 0, QApplication::UnicodeUTF8));
        FFT_size->clear();
        FFT_size->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "256", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "512", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "1024", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "2048", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "4096", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "8192", 0, QApplication::UnicodeUTF8)
        );
        play->setText(QApplication::translate("MainWindow", "play", 0, QApplication::UnicodeUTF8));
        pause->setText(QApplication::translate("MainWindow", "pause", 0, QApplication::UnicodeUTF8));
        stop->setText(QApplication::translate("MainWindow", "stop", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
