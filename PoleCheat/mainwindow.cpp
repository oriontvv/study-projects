#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qalgorithms.h>
#include <QVector>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);



    //load words
    QFile infile("dic.txt");
    infile.open(QIODevice::ReadOnly);
    QTextStream in(&infile);

    QVector<QString> w[40];
    QString tmp;
    words.reserve(90100);
    counter = 0;
    while(in.status() != QTextStream::ReadPastEnd){
       in >> tmp;
//       if(!tmp.isEmpty())counter++;
       words.push_back(tmp);
       w[tmp.length()].push_back(tmp);
    }

    QFile outfile("out.txt");
    outfile.open(QIODevice::WriteOnly);
    QTextStream out(&outfile);
    for(int i = 3; i <= 10; ++i){
        out << "=========================\t " << i << "\n";
        for(QVector<QString>::iterator it = w[i].begin(); it != w[i].end(); it++){
            out << *it << "\n";
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    QRegExp regExp(ui->input->text().replace('#', '.').toUpper());
    ui->answer->setText(regExp.pattern() + "\n===================\n");

    QProgressDialog *dialog = new QProgressDialog("Please wait...", "cancel", 0, counter, this);
    dialog->setWindowModality(Qt::WindowModal);
    int progress = 0;
    QString tmp;
    for(QVector<QString>::iterator i = words.begin(), end = words.end(); i != end; i++, progress++){
        dialog->setValue(progress);
        if(dialog->wasCanceled())
            break;
        tmp = *i;
        if(tmp.contains(regExp) && tmp.length() == regExp.pattern().length()){
            ui->answer->append(tmp + "\n");
        }
    }
    delete dialog;
}
