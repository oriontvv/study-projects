#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSet>
#include <QtGui>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void changeEvent(QEvent *e);
    QVector<QString> words;
    int counter;
private:
    Ui::MainWindow *ui;

private slots:
    void on_pushButton_2_clicked();
};

#endif // MAINWINDOW_H
