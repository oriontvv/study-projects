/********************************************************************************
** Form generated from reading ui file 'mainwindow.ui'
**
** Created: Sat 24. Oct 21:12:55 2009
**      by: Qt User Interface Compiler version 4.4.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *action;
    QAction *Open;
    QAction *Exit;
    QAction *Binorization;
    QAction *Detection;
    QAction *Save;
    QAction *ChangeOptions;
    QAction *Info;
    QAction *ShowOriginal;
    QAction *medianFilter;
    QAction *colorCorectionFilter;
    QAction *ErosionFilter;
    QAction *DilationFilter;
    QAction *ClosingFilter;
    QAction *OpeningFilter;
    QAction *Autodetection;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QMenuBar *menuBar;
    QMenu *menu;
    QMenu *Run_2;
    QMenu *menuFiltr;
    QMenu *menuMath_morphology;
    QMenu *Options;
    QMenu *More;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
    if (MainWindow->objectName().isEmpty())
        MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
    MainWindow->resize(520, 400);
    action = new QAction(MainWindow);
    action->setObjectName(QString::fromUtf8("action"));
    Open = new QAction(MainWindow);
    Open->setObjectName(QString::fromUtf8("Open"));
    Exit = new QAction(MainWindow);
    Exit->setObjectName(QString::fromUtf8("Exit"));
    Binorization = new QAction(MainWindow);
    Binorization->setObjectName(QString::fromUtf8("Binorization"));
    Detection = new QAction(MainWindow);
    Detection->setObjectName(QString::fromUtf8("Detection"));
    Save = new QAction(MainWindow);
    Save->setObjectName(QString::fromUtf8("Save"));
    ChangeOptions = new QAction(MainWindow);
    ChangeOptions->setObjectName(QString::fromUtf8("ChangeOptions"));
    Info = new QAction(MainWindow);
    Info->setObjectName(QString::fromUtf8("Info"));
    ShowOriginal = new QAction(MainWindow);
    ShowOriginal->setObjectName(QString::fromUtf8("ShowOriginal"));
    medianFilter = new QAction(MainWindow);
    medianFilter->setObjectName(QString::fromUtf8("medianFilter"));
    colorCorectionFilter = new QAction(MainWindow);
    colorCorectionFilter->setObjectName(QString::fromUtf8("colorCorectionFilter"));
    ErosionFilter = new QAction(MainWindow);
    ErosionFilter->setObjectName(QString::fromUtf8("ErosionFilter"));
    DilationFilter = new QAction(MainWindow);
    DilationFilter->setObjectName(QString::fromUtf8("DilationFilter"));
    ClosingFilter = new QAction(MainWindow);
    ClosingFilter->setObjectName(QString::fromUtf8("ClosingFilter"));
    OpeningFilter = new QAction(MainWindow);
    OpeningFilter->setObjectName(QString::fromUtf8("OpeningFilter"));
    Autodetection = new QAction(MainWindow);
    Autodetection->setObjectName(QString::fromUtf8("Autodetection"));
    centralWidget = new QWidget(MainWindow);
    centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
    verticalLayout = new QVBoxLayout(centralWidget);
    verticalLayout->setSpacing(6);
    verticalLayout->setMargin(11);
    verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
    MainWindow->setCentralWidget(centralWidget);
    menuBar = new QMenuBar(MainWindow);
    menuBar->setObjectName(QString::fromUtf8("menuBar"));
    menuBar->setGeometry(QRect(0, 0, 520, 21));
    menu = new QMenu(menuBar);
    menu->setObjectName(QString::fromUtf8("menu"));
    Run_2 = new QMenu(menu);
    Run_2->setObjectName(QString::fromUtf8("Run_2"));
    menuFiltr = new QMenu(Run_2);
    menuFiltr->setObjectName(QString::fromUtf8("menuFiltr"));
    menuMath_morphology = new QMenu(menuFiltr);
    menuMath_morphology->setObjectName(QString::fromUtf8("menuMath_morphology"));
    Options = new QMenu(menuBar);
    Options->setObjectName(QString::fromUtf8("Options"));
    More = new QMenu(menuBar);
    More->setObjectName(QString::fromUtf8("More"));
    MainWindow->setMenuBar(menuBar);
    statusBar = new QStatusBar(MainWindow);
    statusBar->setObjectName(QString::fromUtf8("statusBar"));
    MainWindow->setStatusBar(statusBar);

    menuBar->addAction(menu->menuAction());
    menuBar->addAction(Options->menuAction());
    menuBar->addAction(More->menuAction());
    menu->addAction(Open);
    menu->addAction(Save);
    menu->addSeparator();
    menu->addAction(ShowOriginal);
    menu->addSeparator();
    menu->addAction(Autodetection);
    menu->addAction(Run_2->menuAction());
    menu->addSeparator();
    menu->addAction(Exit);
    Run_2->addAction(menuFiltr->menuAction());
    Run_2->addSeparator();
    Run_2->addAction(Binorization);
    Run_2->addAction(Detection);
    menuFiltr->addAction(medianFilter);
    menuFiltr->addSeparator();
    menuFiltr->addAction(colorCorectionFilter);
    menuFiltr->addSeparator();
    menuFiltr->addAction(menuMath_morphology->menuAction());
    menuMath_morphology->addAction(ErosionFilter);
    menuMath_morphology->addAction(DilationFilter);
    menuMath_morphology->addAction(ClosingFilter);
    menuMath_morphology->addAction(OpeningFilter);
    Options->addAction(ChangeOptions);
    More->addAction(Info);

    retranslateUi(MainWindow);
    QObject::connect(Open, SIGNAL(activated()), MainWindow, SLOT(openFile()));
    QObject::connect(Exit, SIGNAL(activated()), MainWindow, SLOT(close()));
    QObject::connect(Info, SIGNAL(activated()), MainWindow, SLOT(about()));
    QObject::connect(Save, SIGNAL(activated()), MainWindow, SLOT(saveFile()));
    QObject::connect(Binorization, SIGNAL(activated()), MainWindow, SLOT(Binorization()));
    QObject::connect(Detection, SIGNAL(activated()), MainWindow, SLOT(Detection()));
    QObject::connect(ShowOriginal, SIGNAL(activated()), MainWindow, SLOT(ShowOriginal()));
    QObject::connect(ChangeOptions, SIGNAL(activated()), MainWindow, SLOT(changeOptions()));
    QObject::connect(medianFilter, SIGNAL(activated()), MainWindow, SLOT(medianFilter()));
    QObject::connect(colorCorectionFilter, SIGNAL(activated()), MainWindow, SLOT(colorCorectionFilter()));
    QObject::connect(ErosionFilter, SIGNAL(activated()), MainWindow, SLOT(erosionFilter()));
    QObject::connect(DilationFilter, SIGNAL(activated()), MainWindow, SLOT(dilationFilter()));
    QObject::connect(ClosingFilter, SIGNAL(activated()), MainWindow, SLOT(closingFilter()));
    QObject::connect(OpeningFilter, SIGNAL(activated()), MainWindow, SLOT(openingFilter()));
    QObject::connect(Autodetection, SIGNAL(activated()), MainWindow, SLOT(autoDetection()));

    QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
    MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
    action->setText(QApplication::translate("MainWindow", "\320\236\321\207\320\270\321\201\321\202\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
    Open->setText(QApplication::translate("MainWindow", "Open file ...", 0, QApplication::UnicodeUTF8));
    Exit->setText(QApplication::translate("MainWindow", "Exit", 0, QApplication::UnicodeUTF8));
    Binorization->setText(QApplication::translate("MainWindow", "Binaryzation", 0, QApplication::UnicodeUTF8));
    Detection->setText(QApplication::translate("MainWindow", "Detection", 0, QApplication::UnicodeUTF8));
    Save->setText(QApplication::translate("MainWindow", "Save file ...", 0, QApplication::UnicodeUTF8));
    ChangeOptions->setText(QApplication::translate("MainWindow", "Change", 0, QApplication::UnicodeUTF8));
    Info->setText(QApplication::translate("MainWindow", "About", 0, QApplication::UnicodeUTF8));
    ShowOriginal->setText(QApplication::translate("MainWindow", "Show original", 0, QApplication::UnicodeUTF8));
    medianFilter->setText(QApplication::translate("MainWindow", "Median", 0, QApplication::UnicodeUTF8));
    colorCorectionFilter->setText(QApplication::translate("MainWindow", "Color corection", 0, QApplication::UnicodeUTF8));
    ErosionFilter->setText(QApplication::translate("MainWindow", "Erosion", 0, QApplication::UnicodeUTF8));
    DilationFilter->setText(QApplication::translate("MainWindow", "Dilation", 0, QApplication::UnicodeUTF8));
    ClosingFilter->setText(QApplication::translate("MainWindow", "Closing", 0, QApplication::UnicodeUTF8));
    OpeningFilter->setText(QApplication::translate("MainWindow", "Opening", 0, QApplication::UnicodeUTF8));
    Autodetection->setText(QApplication::translate("MainWindow", "Autodetection", 0, QApplication::UnicodeUTF8));
    menu->setTitle(QApplication::translate("MainWindow", "Cashier", 0, QApplication::UnicodeUTF8));
    Run_2->setTitle(QApplication::translate("MainWindow", "Run", 0, QApplication::UnicodeUTF8));
    menuFiltr->setTitle(QApplication::translate("MainWindow", "Filter", 0, QApplication::UnicodeUTF8));
    menuMath_morphology->setTitle(QApplication::translate("MainWindow", "Math morphology", 0, QApplication::UnicodeUTF8));
    Options->setTitle(QApplication::translate("MainWindow", "Options", 0, QApplication::UnicodeUTF8));
    More->setTitle(QApplication::translate("MainWindow", "Information", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
