
/*
 * NAME: Taranov Vassiliy Vassilevich., 325
 * ASGN: N2
 */

#include <QtGui/QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWindow w;
    w.setWindowTitle("Electronic CASHIER");
    w.show();

    return a.exec();
}
