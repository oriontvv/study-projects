/*
 * NAME: Taranov Vassiliy V., 325
 * ASGN: N2
 */

#ifndef CHANGEOPTIONSDIALOG_H
#define CHANGEOPTIONSDIALOG_H

#include <QtGui/QDialog>

namespace Ui
{
    class ChangeOptionsDialog;
}
enum FilterType{
    MEDIAN_SQUARE,
    MEDIAN_CIRCLE
};

class ChangeOptionsDialog : public QDialog
{
    Q_OBJECT

public:
    ChangeOptionsDialog(QWidget *parent = 0);
    ~ChangeOptionsDialog();
    void setFilterSize1(int n) {filter_size1 = n;}
    int getFilterSize1() {return filter_size1;}
    void setFilterSize2(int n) {filter_size2 = n;}
    int getFilterSize2() {return filter_size2;}
    FilterType getFilterType() {return filter_type;}
    void setFilterType(FilterType t) {filter_type = t;}

public slots:
    void ChangeFilterSize1(int n) {filter_size1 = n;}
    void ChangeFilterSize2(int n) {filter_size2 = n;}
    void setSquare() {filter_type = MEDIAN_SQUARE;}
    void setCircle() {filter_type = MEDIAN_CIRCLE;}
protected:
    void changeEvent(QEvent *e);

private:
    Ui::ChangeOptionsDialog *m_ui;
    int filter_size1;
    int filter_size2;
    FilterType filter_type;
};

#endif // CHANGEOPTIONSDIALOG_H
