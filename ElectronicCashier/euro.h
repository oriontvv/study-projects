/*
 * NAME: Taranov Vassiliy V., 325
 * ASGN: N2
 */

#ifndef EURO_H
#define EURO_H

#include <iostream>

enum TYPE{
        EURO_NULL,
        EURO_COIN,
        EURO_NOTE,
        EURO_UNDEF
};
class Euro
{
    friend std::ostream &operator<<(std::ostream &, Euro &);
private:
    double prize;
    //geometric properties:
    double square;
    double perimetr;
    double compactness;
    double oblongness;
    double x_center;
    double y_center;

    //photometric properties:
    double average_brightness;
    double average_r;
    double average_g;
    double average_b;
    double average_color;
    double dispersion;

    /////////////////////////
    TYPE type;

public:
    Euro(TYPE t = EURO_NULL);
    //set:
    void setType(TYPE t) {type = t;}
    void setPrize(double p) {prize = p;}
    void setPerimetr(double p) {perimetr = p;}
    void setSquare(double s) {square = s;}
    void setCompactness(double c) {compactness = c;}
    void setAverageBrightness(double ab) {average_brightness = ab;}
    void setAverageColor(double ac) {average_color = ac;}
    void setDispersion(double d) {dispersion = d;}
    void setAverageR(double r) {average_r = r;}
    void setAverageG(double g) {average_g = g;}
    void setAverageB(double b) {average_b = b;}
    void setXCenter(double x) {x_center = x;}
    void setYCenter(double y) {y_center = y;}


    //get:
    TYPE getType() {return type;}
    double getPrize() {return prize;}
    double getPerimetr() {return perimetr;}
    double getSquare() {return square;}
    double getCompactness() {return compactness;}
    double getAverageBrightness() {return average_brightness;}
    double getAverageColor() {return average_color;}
    double getDispersion() {return dispersion;}
    double getAverageR() {return average_r;}
    double getAverageG() {return average_g;}
    double getAverageB() {return average_b;}
    double getXCenter() {return x_center;}
    double getYCenter() {return y_center;}


};

#endif // EURO_H
