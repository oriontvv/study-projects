/*
 * NAME: Taranov Vassiliy V., 325
 * ASGN: N2
 */

#include "euro.h"

Euro::Euro(TYPE t)
{
    setAverageBrightness(0);
    setAverageColor(0);
    setCompactness(0);
    setDispersion(0);
    setPerimetr(0);
    setSquare(0);
    setPrize(0);
    setType(t);
    // ;)
    setAverageR(0);
    setAverageG(0);
    setAverageB(0);
}

std::ostream &operator<<(std::ostream &out , Euro &euro)
{
    if (euro.getType() == EURO_COIN) out << "COIN :";
    else if(euro.getType() == EURO_NOTE) out << "NOTE :";
    else out << "UNDEFINED";
    out << "\nprize = " << euro.getPrize() << "\n\n";

    out << "perimetr = " << euro.getPerimetr() << "\n";
    out << "square = " << euro.getSquare() << "\n";
    out << "compactness = " << euro.getCompactness() << "\n";
    out << "av_r = " << euro.getAverageR() << "\n";
    out << "av_g = " << euro.getAverageG() << "\n";
    out << "av_b = " << euro.getAverageB() << "\n";
    out << "x_center = " << euro.getXCenter() << "\n";
    out << "y_center = " << euro.getYCenter() << "\n";

    return out;
}
