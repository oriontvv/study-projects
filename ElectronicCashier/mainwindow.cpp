/*
 * NAME: Taranov Vassiliy V., 325
 * ASGN: N2
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    file = new QString("");
    //openFile();
    painter = new QPainter(this);

    original = new QImage;
    image = new QImage;
    img_scaled = new QImage;
    img_obj = new QImage;

    alloc_memory(image->width(), image->height());

    for(int i = 0; i < OBJ_COUNT; ++i)
            ms[i] = i;

    result_sum = 0;
    number_euro = 0;
    masEuro = new Euro[number_euro];
    filtr_eps = 2;
    loadProperties();

    dialog = new ChangeOptionsDialog;
}
void MainWindow::loadProperties()
{
// 0.01:
    size[0] = 0.01;
    EuroProperties[0][0] = 185.786; //r
    EuroProperties[0][1] = 100.499; //g
    EuroProperties[0][2] = 72.2921; //b
// 0.1:
    size[1] = 0.1;
    EuroProperties[1][0] = 204.479; //r
    EuroProperties[1][1] = 166.292; //g
    EuroProperties[1][2] = 51.8323; //b
// 1:
    size[2] = 1;
    EuroProperties[2][0] = 210.060; //r
    EuroProperties[2][1] = 199.785; //g
    EuroProperties[2][2] = 122.765; //b
// 2:
    size[3] = 2;
    EuroProperties[3][0] = 205.896; //r
    EuroProperties[3][1] = 201.051; //g
    EuroProperties[3][2] = 133.105; //b
// 5:
    size[4] = 5;
    EuroProperties[4][0] = 187.010; //r
    EuroProperties[4][1] = 192.170; //g
    EuroProperties[4][2] = 192.402; //b
// 10:
    size[5] = 10;
    EuroProperties[5][0] = 221.497; //r
    EuroProperties[5][1] = 187.747; //g
    EuroProperties[5][2] = 190.538; //b
// 20:
    size[6] = 20;
    EuroProperties[6][0] = 176.706; //r
    EuroProperties[6][1] = 192.948; //g
    EuroProperties[6][2] = 206.365; //b
// 50:
    size[7] = 50;
    EuroProperties[7][0] = 213.913; //r
    EuroProperties[7][1] = 195.306; //g
    EuroProperties[7][2] = 166.676; //b
// 100:
    size[8] = 100;
    EuroProperties[8][0] = 175.838; //r
    EuroProperties[8][1] = 201.312; //g
    EuroProperties[8][2] = 173.571; //b
// 200:
    size[9] = 200;
    EuroProperties[9][0] = 211.529; //r
    EuroProperties[9][1] = 204.830; //g
    EuroProperties[9][2] = 155.842; //b
// 500:
    size[10] = 500;
    EuroProperties[10][0] = 191.699; //r
    EuroProperties[10][1] = 168.214; //g
    EuroProperties[10][2] = 189.265; //b
//////////////////////////////////////////////

// 0.01:
    size[11] = 0.01;
    EuroProperties[11][0] = 197.216; //r
    EuroProperties[11][1] = 102.985; //g
    EuroProperties[11][2] = 60.7511; //b
// 0.1:
    size[12] = 0.1;
    EuroProperties[12][0] = 217.700; //r
    EuroProperties[12][1] = 175.123; //g
    EuroProperties[12][2] = 36.7004; //b
// 1:
    size[13] = 1;
    EuroProperties[13][0] = 219.797; //r
    EuroProperties[13][1] = 209.952; //g
    EuroProperties[13][2] = 119.826; //b
// 2:
    size[14] = 2;
    EuroProperties[14][0] = 214.647; //r
    EuroProperties[14][1] = 211.713; //g
    EuroProperties[14][2] = 134.571; //b
// 5:
    size[15] = 5;
    EuroProperties[15][0] = 187.918; //r
    EuroProperties[15][1] = 193.174; //g
    EuroProperties[15][2] = 193.543; //b
// 10:
    size[16] = 10;
    EuroProperties[16][0] = 222.754; //r
    EuroProperties[16][1] = 188.817; //g
    EuroProperties[16][2] = 191.433; //b
// 20:
    size[17] = 20;
    EuroProperties[17][0] = 177.241; //r
    EuroProperties[17][1] = 193.702; //g
    EuroProperties[17][2] = 207.316; //b
// 50:
    size[18] = 50;
    EuroProperties[18][0] = 215.361; //r
    EuroProperties[18][1] = 196.488; //g
    EuroProperties[18][2] = 167.782; //b
// 100:
    size[19] = 100;
    EuroProperties[19][0] = 176.534; //r
    EuroProperties[19][1] = 202.225; //g
    EuroProperties[19][2] = 174.167; //b
// 200:
    size[20] = 200;
    EuroProperties[20][0] = 212.855; //r
    EuroProperties[20][1] = 205.897; //g
    EuroProperties[20][2] = 156.040; //b
// 500:
    size[21] = 500;
    EuroProperties[21][0] = 192.842; //r
    EuroProperties[21][1] = 169.163; //g
    EuroProperties[21][2] = 190.173; //b
}
MainWindow::~MainWindow()
{
    delete ui;
   // delete image;
    //delete original;
   // delete img_scaled;
   // delete img_obj;
    delete painter;
    //delete []masEuro;
    //destroy_memory();
    delete dialog;
}
void MainWindow::alloc_memory(int _w, int _h)
{
    h = _h;
    w = _w;
    bin_mat = new int*[_w];
    for(int i = 0; i < _w; ++i)
        bin_mat[i] = new int[_h];

    for(int i = 0; i < _w; ++i)
        for(int j = 0; j < _h; ++j)
            bin_mat[i][j] = 0;
}
void MainWindow::destroy_memory()
{
    for(int i = 0; i < w; ++i)
        delete bin_mat[i];
    delete []bin_mat;
}

void MainWindow::about()
{
    QMessageBox::information(0,
            "About",
            "<h3><u>project</u>:<b>Electronic Cashier</b><br>"
            "<u>author</u>:<b>Taranov V.V.<\b>     <br>"
            "<u>group</u>    :<b>325<\b>     <br>"
            "<u>mailto</u>    :<b>taranov.vv@gmail.com<\b>     <br>");
}
long MainWindow::my_proection(QRgb rgb)
{
    double x = qRed(rgb);
    double y = qGreen(rgb);
    double z = qBlue(rgb);
    return (long) sqrt( x*x + y*y + z*z -
                ( pow((x - y), 2) + pow((x - z), 2) + pow((z - y), 2) ) / 3
                );
}

void MainWindow::openFile()
{
    destroy_memory();
    //delete file;
    QString *f = file;
    file = new QString(QFileDialog::getOpenFileName(0, "Choose test picture", "",
                                                    "*.bmp - support picture file\n*.*  - any file"));

    image->load(*file);
    setWindowTitle(QString("Electronic CASHIER << ")+= (file->isNull() ? (*f) : (*file)));
    if(!file->isNull()) {
        delete f;
        if(!original->isNull())delete original;
        original = new QImage(*image);
    }

    w = image->width();
    h = image->height();

    alloc_memory(w, h);

    result_sum = 0;
    if(!file->isNull()) resize(image->width(), image->height() + 20);

    reimg();
    update();
}

void MainWindow::saveFile()
{
    if (!img_scaled->isNull()){
        QString fName = QFileDialog::getSaveFileName(0, "Set name of the saving picture", "",
                                                 "*.bmp - picture file");
        img_scaled->save(fName);
    }
    else {
        int n = QMessageBox::information(0,
                                "Ups!",
                                "<h3>Firstly you must to open file. Do you want to open test file?", QMessageBox::Yes, QMessageBox::No);

        if (n == QMessageBox::Yes) openFile();
    }

}

void MainWindow::changeOptions()
{
    dialog->show();
    update();
}
void MainWindow::colorCorectionFilter()
{
    QRgb rgb;
    QImage *tmp = new QImage(*image);
    int max_r = -1, max_g = -1, max_b = -1;
    int min_r = 300, min_g = 300, min_b = 300;
    int r, g, b;
    //search min and max for each componentes
    for(int x = 0; x < w; ++x)
        for(int y = 0; y < h; ++y){
            rgb = image->pixel(x, y);
            r = qRed(rgb);
            g = qGreen(rgb);
            b = qBlue(rgb);
            if(r > max_r) max_r = r;
            if(r < min_r) min_r = r;
            if(g > max_g) max_g = g;
            if(g < min_g) min_g = g;
            if(b > max_b) max_b = b;
            if(b < min_b) min_b = b;
        }
    //correction
    max_r -= min_r;
    max_g -= min_g;
    max_b -= min_b;
    for(int x = 0; x < w; ++x)
        for(int y = 0; y < h; ++y){
            rgb = image->pixel(x, y);
            r = 255 * (qRed(rgb) - min_r) / max_r;
            g = 255 * (qGreen(rgb) - min_g) / max_g;
            b = 255 * (qBlue(rgb) - min_b) / max_b;
            tmp->setPixel(x, y, qRgb(r, g, b));
        }
    update();
    if (!image->isNull())delete image;
    image = tmp;
}
void MainWindow::erosionFilter()
{
    int Ker[3][3] = {
        {1, 1, 1},
        {1, 0, 1},
        {1, 1, 1}
    };
    QRgb black = qRgb(0, 0, 0), white = qRgb(255, 255, 255);
    QImage *tmp_img = new QImage(*image);
    int **tmp = new int*[w];
    for(int i = 0; i < w; ++i)
        tmp[i] = new int[h];
    int f = 1;
    for(int i = 0; i < w && f; ++i)
        for(int j = 0; j < h && f; ++j)
            if(image->pixel(i, j) == black)tmp[i][j] = 0;
            else if(image->pixel(i, j) == white)tmp[i][j] = 1;
            else {f = 0; break;}
    if (f == 0){
        QMessageBox::information(0, "Ups!", "<h3>Binarization not be done", QMessageBox::Ok);
        return;
    }
    int p;
    for(int x = 0; x < w; ++x)
        for(int y = 0; y < h; ++y){
            p = 0;
            for(int i = -1; i <= 1; ++i)
                for(int j = -1; j <= 1; ++j)
                    if(x+i >= 0 && x+i < w && y+j >= 0 && y+j < h)
                        if(Ker[i][j])
                            if(tmp[x+i][y+j]) p = 1;
            if(!p) tmp_img->setPixel(x, y, qRgb(0, 0, 0));
            else tmp_img->setPixel(x, y, qRgb(255, 255, 255));
        }

    for(int i = 0; i < w; ++i)
        delete tmp[i];
    delete []tmp;
    delete image;
    image = tmp_img;
    reimg();
    update();
}
void MainWindow::dilationFilter()
{
    int Ker[3][3] = {
        {1, 1, 1},
        {1, 0, 1},
        {1, 1, 1}
    };
    QRgb black = qRgb(0, 0, 0), white = qRgb(255, 255, 255);
    QImage *tmp_img = new QImage(*image);
    int **tmp = new int*[w];
    for(int i = 0; i < w; ++i)
        tmp[i] = new int[h];
    int f = 1;
    for(int i = 0; i < w && f; ++i)
        for(int j = 0; j < h && f; ++j)
            if(image->pixel(i, j) == black)tmp[i][j] = 0;
            else if(image->pixel(i, j) == white)tmp[i][j] = 1;
            else {f = 0; break;}
    if (f == 0){
        QMessageBox::information(0, "Ups!", "<h3>Binarization not be done", QMessageBox::Ok);
        return;
    }

    int p;
    for(int x = 0; x < w; ++x)
        for(int y = 0; y < h; ++y){
            p = 0;
            for(int i = -1; i <= 1; ++i)
                for(int j = -1; j <= 1; ++j)
                    if(x+i >= 0 && x+i < w && y+j >= 0 && y+j < h)
                        if(Ker[i][j])
                            if(tmp[x+i][y+j]) p++;
            if(p == 8 || p == 7) tmp_img->setPixel(x, y, qRgb(255, 255, 255));
            else tmp_img->setPixel(x, y, qRgb(0, 0, 0));
        }

    for(int i = 0; i < w; ++i)
        delete tmp[i];
    delete []tmp;
    delete image;
    image = tmp_img;
    reimg();
    update();
}
void MainWindow::closingFilter()
{
    erosionFilter();
    dilationFilter();
}
void MainWindow::openingFilter()
{
    dilationFilter();
    erosionFilter();
}

void MainWindow::medianFilter()
{
    QProgressDialog *pr_dialog = new QProgressDialog("Progress of median filter...", "cancel", 0, w, this);
    pr_dialog->setWindowModality(Qt::WindowModal);
    int pr_count = 0;
    if(dialog->getFilterType() == MEDIAN_CIRCLE){
    // CIRCLE
        int n = dialog->getFilterSize2();// okrestnost'
        QImage *tmp = new QImage(*image);
        int q;
        int size = 2*n*n + 2*n + 2;
        QVector<long> masR(size), masG(size), masB(size);

        for(int x = 0; x < w; ++x){
            pr_dialog->setValue(++pr_count);
            if(pr_dialog->wasCanceled()) return;
            for(int y = 0; y < h; ++y){
                q = 0;
                for(int x1 = x - n; x1 <= x + n; ++x1)
                    for(int y1 = abs(x - x1) + y - n; y1 <= y + n - abs(x - x1); ++y1)
                        if( x1 >=0 && x1 < w && y1 >=0 && y1 < h){
                            masR[q] = qRed(image->pixel(x1, y1));
                            masG[q] = qGreen(image->pixel(x1, y1));
                            masB[q] = qBlue(image->pixel(x1, y1));
                            q++;
                        }
                        else masR[q] = masG[q] = masB[q++] = 0;
                q--;
                qSort(masR);
                qSort(masG);
                qSort(masB);
                tmp->setPixel(x, y, qRgb(masR[q / 2], masG[q / 2], masB[q / 2]));
            }
        }
        if (!image->isNull())delete image;
        image = tmp;
        reimg();
        update();
    }
    else  if(dialog->getFilterType() == MEDIAN_SQUARE){
//  SQUARE
        int n = dialog->getFilterSize1();// okrestnost'
        QImage *tmp = new QImage(*image);
        int q;
        int size = (2 * n + 2) * (2 * n + 2);
        QVector<long> masR(size), masG(size), masB(size);

        for(int x = 0; x < w; ++x){
            pr_dialog->setValue(++pr_count);
            if(pr_dialog->wasCanceled()) return;
            for(int y = 0; y < h; ++y){
                q = 0;
                for(int x1 = x - n; x1 <= x + n; ++x1)
                    for(int y1 = y - n; y1 <= y + n; ++y1)
                        if(x1 >=0 && x1 < w && y1 >=0 && y1 < h){
                            masR[q] = qRed(image->pixel(x1, y1));
                            masG[q] = qGreen(image->pixel(x1, y1));
                            masB[q++] = qBlue(image->pixel(x1, y1));
                        }
                        else masR[q] = masG[q] = masB[q++] = 0;
                q--;
                qSort(masR);
                qSort(masG);
                qSort(masB);
                tmp->setPixel(x, y, qRgb(masR[q / 2], masG[q / 2], masB[q / 2]));
            }
        }
        if (!image->isNull())delete image;
        image = tmp;
        reimg();
        update();
    }
    pr_dialog->setValue(w);
    pr_dialog->update();
    delete pr_dialog;

}
int MainWindow::friends8(int x, int y, int n)
{
    int sum = 0;
    for(int i = x - n; i <= x + n; ++i)
        for(int j = y - n; j <= y + n; ++j)
            if(i >= 0 && i < w && j >= 0 && j < h)
                if(bin_mat[i][j])sum++;
    return sum;
}
int MainWindow::friends4(int x, int y, long color)
{
    int sum = 0;
    if(x - 1 >=0 && bin_mat[x - 1][y] == color) sum++;
    if(x + 1 < w && bin_mat[x + 1][y] == color) sum++;
    if(y - 1 >=0 && bin_mat[x][y - 1] == color) sum++;
    if(y + 1 < h && bin_mat[x][y + 1] == color) sum++;
    return sum;
}

void MainWindow::Binorization()
{
    QRgb rgb, black = qRgb(0, 0, 0), white = qRgb(255, 255, 255);

    long histo[450], histo_max;
    //  counting histogram:
    for(int i = 0; i < 450; ++i)
        histo[i] = 0;

    for(int i = 0; i < OBJ_COUNT; ++i)
        ms[i] = i;

    for(int x = 0; x < w; ++x)
        for(int y = 0; y < h; ++y){
            rgb = image->pixel(x, y);
            histo[my_proection(rgb)]++;
        }
    histo_max = 0;
    long max = histo[0];
    for(int i = 1; i < 450; ++i)
        if(max < histo[i]) {max = histo[i]; histo_max = i;}

    for (int x = 0; x < w; ++x)
        for(int y = 0; y < h; ++y){
            rgb = image->pixel(x, y);
            if (labs(my_proection(rgb) - histo_max) < 20) {
                bin_mat[x][y] = 0;
                image->setPixel(x, y, black);
            }
            else {
                bin_mat[x][y] = 1;
                image->setPixel(x, y, white);
            }
        }
    //image->invertPixels(QImage::InvertRgb);

    //noise_correction();

    reimg();
    update();
}
void MainWindow::reimg()
{
    if(!img_scaled->isNull())delete img_scaled;

    if(!image->isNull())
        img_scaled = new QImage(image->scaled(width(), height()-20, Qt::IgnoreAspectRatio));
    else img_scaled = new QImage();
}

void MainWindow::minimaze_colors()
{
    int color = -1, c, flag = 0;
    while(!flag){
        flag = 1;
        for(int x = 0; x < w && flag; ++x)
            for(int y = 0; y < h && flag; ++y){
                c = bin_mat[x][y];
                if(c > 0){
                    // change all pixel with color "c" on "color"
                    for(int i = 0; i < image->width(); ++i)
                        for(int j = 0; j < image->height(); ++j)
                            if(bin_mat[i][j] == c) bin_mat[i][j] = color;
                    flag = 0;
                }
            }
        color--;
    }
    color++;
    for(int x = 0; x < image->width(); ++x)
        for(int y = 0; y < image->height(); ++y)
            if(bin_mat[x][y]) bin_mat[x][y] *= (-1);
    number_euro = -color;//total number of euro
}

int MainWindow::_min(int color)
{
    while(ms[color] < color) color = ms[color];
    return color;
}

void MainWindow::search_binding_areas()
{
    int color = 2;
    int B, C, c1, c2;
    for(int x = 1; x < w; ++x){
        for(int y = 1; y < h; ++y)
            if(bin_mat[x][y]){
                c1 = bin_mat[x - 1][y];
                c2 = bin_mat[x][y - 1];
                B = (c1 != 0);
                C = (c2 != 0);
                if(color > 1000){
                    QMessageBox::critical(0, "Ups!", "Filter not be done", QMessageBox::Ok);
                    return;
                }
                if(!B && !C) bin_mat[x][y] = color++;
                else if(B && C){
                    bin_mat[x][y] = bin_mat[x - 1][y];
                    //mark equal colors
                    if(ms[c1] > c2) ms[c1] = c2;
                    if(ms[c2] > c1) ms[c2] = c1;
                }
                else bin_mat[x][y] = c1 * B + c2 * C;
            }
    }
}
void MainWindow::search_equal_colors()
{
    //min_color();
    for(int x = 0; x < w; ++x)
        for(int y = 0; y < h; ++y)
            if(bin_mat[x][y]) bin_mat[x][y] = _min(bin_mat[x][y]);
}
void MainWindow::show_result()
{
   // qDebug() <<"###################################################\n";
    QString str("<h2><u>Total sum</u><b> = ");
    QString tmp;
    for(int i = 1; i < number_euro - 1; ++i)
        if(masEuro[i].getType() != EURO_NULL){
            str += tmp.setNum((masEuro[i]).getPrize());
            str += " + ";
        }
    if(masEuro[number_euro - 1].getType() != EURO_NULL)
        str += tmp.setNum(masEuro[number_euro - 1].getPrize());
    else str = str.left(str.length() - 3);//delete " + " from end of str
       // str -= " - ";
    str += " = ";
    str += tmp.setNum(result_sum);
    str += "</b><br>";
    QMessageBox::information(0, "Result", str);
}
void MainWindow::autoDetection()
{
    QImage *tmp = new QImage(*image);

    colorCorectionFilter();

    dialog->setFilterSize1(1);
    medianFilter();
    medianFilter();

    dialog->setFilterSize1(3);
    medianFilter();

    dialog->setFilterSize1(2);

    Detection();

    if(!image->isNull())delete image;
    image = tmp;
    //draw border:
    QRgb Red = qRgb(255, 0, 0), Green = qRgb(0, 255, 0), Blue = qRgb(0, 0, 255);
    for(int color = 1; color < number_euro; ++color)
        if(masEuro[color].getType() != EURO_NULL)
            for(int x = 0; x < w; ++x)
                for(int y = 0; y < h; ++y)
                    if(bin_mat[x][y] == color && friends8(x, y, 1) < 8){
                        if(masEuro[color].getPrize() <= 2) draw_border(tmp, x, y, Red);
                        else if(masEuro[color].getPrize() <= 500) draw_border(tmp, x, y, Green);
                        else tmp->setPixel(x, y, Blue);
                    }


    //draw nominal:
    QString str;
    painter->begin(image);
    painter->setPen(QPen(Qt::blue));
    painter->setFont(QFont("Times", 30, QFont::Bold));
    for(int color = 1; color < number_euro; ++color)
        if(masEuro[color].getType() != EURO_NULL){
            str.setNum(masEuro[color].getPrize());
            painter->drawText((int)masEuro[color].getXCenter() - 8*str.length() , (int)masEuro[color].getYCenter() + 10 , str);
        }
    painter->end();
    update();
}
void MainWindow::draw_border(QImage *img, int x, int y, QRgb color)
{
    for(int i = x - 1; i <= x + 1; ++i)
        for(int j = y - 1; j <= y + 1; ++j)
            if(i > 0 && i < w && j > 0 && j < h)
                img->setPixel(i, j, color);
}
void MainWindow::set_euro_parametrs()
{
    if (masEuro != NULL)delete []masEuro;
    masEuro = new Euro[number_euro + 1];
    result_sum = 0;
  //  qDebug() << "===============================================\nnumber_euro = " << number_euro << "\n";
    //find border:
    double p, sq;
    double r, g, b, x_center, y_center;
    for(int color = 1; color < number_euro; ++color){
        x_center = y_center = sq = p = r = g = b = 0;
        for(int x = 0; x < w; ++x)
            for(int y = 0; y < h; ++y)
                if(bin_mat[x][y] == color) {
                    sq++;
                    x_center += x;
                    y_center += y;
                    //if(friends4(x, y, color) < 4) p++;
                    if(friends8(x, y, 1) < 8) p++;
                    r += qRed(image->pixel(x, y));
                    g += qGreen(image->pixel(x, y));
                    b += qBlue(image->pixel(x, y));
                }
        if(sq < 100) masEuro[color].setType(EURO_NULL);
        else {
            (masEuro[color]).setSquare(sq);
            (masEuro[color]).setPerimetr(p);
            (masEuro[color]).setCompactness(p * p / sq);
            (masEuro[color]).setAverageR(r / sq);
            (masEuro[color]).setAverageG(g / sq);
            (masEuro[color]).setAverageB(b / sq);
            (masEuro[color]).setXCenter(x_center / sq);
            (masEuro[color]).setYCenter(y_center / sq);
            if(fabs( p * p / sq - 9.5) < 2)(masEuro[color]).setType(EURO_COIN);
            else if(fabs( p * p / sq - 25) < 13.5)(masEuro[color]).setType(EURO_NOTE);
            //else (masEuro[color]).setType(EURO_NULL);
          //  qDebug() << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
          //  qDebug() << color <<"\t"<< masEuro[color].getType()<<"\tsquare=" << sq
           //         << "\tperimetr=" << p <<"\tcom="<<p * p / sq<<"\n";
        }
    }
    // search most oppropriate euro:
    int best_template;
    double best_r, best_g, best_b;
    double delta_r, delta_g, delta_b;
    double best, delta;

    for(int color = 1; color < number_euro; ++color)
        if(masEuro[color].getType() != EURO_NULL){
            best_template = 0;
            best_r = pow(EuroProperties[0][0] - (masEuro[color]).getAverageR(), 2);
            best_g = pow(EuroProperties[0][1] - (masEuro[color]).getAverageG(), 2);
            best_b = pow(EuroProperties[0][2] - (masEuro[color]).getAverageB(), 2);
            best = best_r + best_g + best_b;
            for(int _template = 1; _template < 21; ++_template){
                delta_r = pow(EuroProperties[_template][0] - (masEuro[color]).getAverageR(), 2);
                delta_g = pow(EuroProperties[_template][1] - (masEuro[color]).getAverageG(), 2);
                delta_b = pow(EuroProperties[_template][2] - (masEuro[color]).getAverageB(), 2);
                delta = delta_r + delta_g + delta_b;
                if (best > delta){
                    best = delta;
                    best_template = _template;
                }
            }
            masEuro[color].setPrize(size[best_template]);
            result_sum += size[best_template];
        }
    //check 1, 2, 200 euro:
//    1 - 212
//2 - 49 / 24
//200 - 145 / 135 / 130
    int x, y;
    for(int color = 1; color < number_euro; ++color)
        if(masEuro[color].getType() != EURO_NULL)
        {
            x = (int)masEuro[color].getXCenter();
            y = (int)masEuro[color].getYCenter();
            int q = qBlue(image->pixel(x, y));
            if(masEuro[color].getPrize() == 1 && q < 200){
                if(masEuro[color].getType() == EURO_NOTE){
                    result_sum -= masEuro[color].getPrize();
                    masEuro[color].setPrize(200);
                    result_sum += 200;
                    continue;
                }
                if (q < 120 && q > 70){
                    result_sum -= masEuro[color].getPrize();
                    masEuro[color].setPrize(200);
                    result_sum += 200;
                }
                else if (q <= 70){
                    result_sum -= masEuro[color].getPrize();
                    masEuro[color].setPrize(2);
                    result_sum += 2;
                }

            }
            else if(masEuro[color].getPrize() == 2 && !(q <= 70)){
                if(masEuro[color].getType() == EURO_NOTE){
                    result_sum -= masEuro[color].getPrize();
                    masEuro[color].setPrize(200);
                    result_sum += 200;
                    continue;
                }
                if (q < 120 && q > 70){
                    result_sum -= masEuro[color].getPrize();
                    masEuro[color].setPrize(200);
                    result_sum += 200;
                }
                else if (q >= 200){
                    result_sum -= masEuro[color].getPrize();
                    masEuro[color].setPrize(1);
                    result_sum += 1;
                }
            }
            else if(masEuro[color].getPrize() == 200 && !(q < 120 && q > 70)){
                if(masEuro[color].getType() == EURO_NOTE){
                    continue;
                }
                else if (q <= 70){
                    result_sum -= masEuro[color].getPrize();
                    masEuro[color].setPrize(2);
                    result_sum += 2;
                }
                else if (q >= 200){
                    result_sum -= masEuro[color].getPrize();
                    masEuro[color].setPrize(1);
                    result_sum += 1;
                }
            }
         //   qDebug() << masEuro[color].getPrize() << ":\t" << qBlue(image->pixel(x, y)) << "\n";
        }

    //draw border:
    QRgb Red = qRgb(255, 0, 0), Green = qRgb(0, 255, 0), Blue = qRgb(0, 0, 255);
    for(int color = 1; color < number_euro; ++color)
        if(masEuro[color].getType() != EURO_NULL)
            for(int x = 0; x < w; ++x)
                for(int y = 0; y < h; ++y)
                    if(bin_mat[x][y] == color && friends8(x, y, 1) < 8){
                        if(masEuro[color].getPrize() <= 2) draw_border(img_obj, x, y, Red);
                        else if(masEuro[color].getPrize() <= 500) draw_border(img_obj, x, y, Green);
                        else img_obj->setPixel(x, y, Blue);
                    }
    //draw nominal:
    QString str;
    painter->begin(img_obj);
    painter->setPen(QPen(Qt::blue));
    painter->setFont(QFont("Times", 30, QFont::Bold));
    for(int color = 1; color < number_euro; ++color)
        if(masEuro[color].getType() != EURO_NULL){
            str.setNum(masEuro[color].getPrize());
            painter->drawText((int)masEuro[color].getXCenter() - 8*str.length() , (int)masEuro[color].getYCenter() + 10 , str);
        }
    painter->end();
}
void MainWindow::Detection()
{
    QImage *tmp = new QImage(*image);

    Binorization();

    if(!image->isNull())delete image;
    image = new QImage(*tmp);
    if(!tmp->isNull())delete tmp;

    if (!img_obj->isNull())delete img_obj;
    img_obj = new QImage(*image);

// bind states by sequensctly scanning:
    search_binding_areas();
    search_equal_colors();
    minimaze_colors();

    set_euro_parametrs();
    show_result();

    if (!image->isNull())delete image;
    image = new QImage(*img_obj);
    reimg();
    update();
}
void MainWindow::resizeEvent(QResizeEvent *event)
{
    update();
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    painter->begin(this);
    reimg();
    painter->drawImage(0, 20, *img_scaled);
    painter->end();
}
void MainWindow::ShowOriginal()
{
    if(!image->isNull())delete image;
    image = new QImage(*original);
    reimg();
    update();
}

