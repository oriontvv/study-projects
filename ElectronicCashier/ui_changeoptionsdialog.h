/********************************************************************************
** Form generated from reading ui file 'changeoptionsdialog.ui'
**
** Created: Sat 24. Oct 13:56:19 2009
**      by: Qt User Interface Compiler version 4.4.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_CHANGEOPTIONSDIALOG_H
#define UI_CHANGEOPTIONSDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QFormLayout>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QLabel>
#include <QtGui/QRadioButton>
#include <QtGui/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_ChangeOptionsDialog
{
public:
    QGridLayout *gridLayout_3;
    QDialogButtonBox *buttonBox;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_4;
    QGroupBox *groupBox_3;
    QFormLayout *formLayout;
    QRadioButton *radioButton;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QSpinBox *spinBox;
    QRadioButton *radioButton_2;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QSpinBox *spinBox_2;

    void setupUi(QDialog *ChangeOptionsDialog)
    {
    if (ChangeOptionsDialog->objectName().isEmpty())
        ChangeOptionsDialog->setObjectName(QString::fromUtf8("ChangeOptionsDialog"));
    ChangeOptionsDialog->resize(400, 300);
    gridLayout_3 = new QGridLayout(ChangeOptionsDialog);
    gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
    buttonBox = new QDialogButtonBox(ChangeOptionsDialog);
    buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

    gridLayout_3->addWidget(buttonBox, 1, 0, 1, 1);

    groupBox = new QGroupBox(ChangeOptionsDialog);
    groupBox->setObjectName(QString::fromUtf8("groupBox"));
    gridLayout_4 = new QGridLayout(groupBox);
    gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
    groupBox_3 = new QGroupBox(groupBox);
    groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
    formLayout = new QFormLayout(groupBox_3);
    formLayout->setObjectName(QString::fromUtf8("formLayout"));
    radioButton = new QRadioButton(groupBox_3);
    radioButton->setObjectName(QString::fromUtf8("radioButton"));
    radioButton->setChecked(true);

    formLayout->setWidget(0, QFormLayout::LabelRole, radioButton);

    groupBox_2 = new QGroupBox(groupBox_3);
    groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
    groupBox_2->setCheckable(false);
    gridLayout_2 = new QGridLayout(groupBox_2);
    gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
    horizontalLayout = new QHBoxLayout();
    horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
    label = new QLabel(groupBox_2);
    label->setObjectName(QString::fromUtf8("label"));
    label->setEnabled(true);

    horizontalLayout->addWidget(label);

    spinBox = new QSpinBox(groupBox_2);
    spinBox->setObjectName(QString::fromUtf8("spinBox"));
    spinBox->setEnabled(true);
    spinBox->setMinimum(1);
    spinBox->setMaximum(10);
    spinBox->setValue(2);

    horizontalLayout->addWidget(spinBox);


    gridLayout_2->addLayout(horizontalLayout, 0, 0, 1, 1);


    formLayout->setWidget(1, QFormLayout::LabelRole, groupBox_2);

    radioButton_2 = new QRadioButton(groupBox_3);
    radioButton_2->setObjectName(QString::fromUtf8("radioButton_2"));
    radioButton_2->setChecked(false);

    formLayout->setWidget(2, QFormLayout::LabelRole, radioButton_2);

    groupBox_4 = new QGroupBox(groupBox_3);
    groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
    groupBox_4->setEnabled(true);
    groupBox_4->setCheckable(false);
    gridLayout = new QGridLayout(groupBox_4);
    gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
    horizontalLayout_2 = new QHBoxLayout();
    horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
    label_2 = new QLabel(groupBox_4);
    label_2->setObjectName(QString::fromUtf8("label_2"));
    label_2->setEnabled(true);

    horizontalLayout_2->addWidget(label_2);

    spinBox_2 = new QSpinBox(groupBox_4);
    spinBox_2->setObjectName(QString::fromUtf8("spinBox_2"));
    spinBox_2->setEnabled(true);
    spinBox_2->setMinimum(1);
    spinBox_2->setMaximum(10);
    spinBox_2->setValue(3);

    horizontalLayout_2->addWidget(spinBox_2);


    gridLayout->addLayout(horizontalLayout_2, 0, 0, 1, 1);


    formLayout->setWidget(3, QFormLayout::LabelRole, groupBox_4);


    gridLayout_4->addWidget(groupBox_3, 0, 0, 1, 1);


    gridLayout_3->addWidget(groupBox, 0, 0, 1, 1);


    retranslateUi(ChangeOptionsDialog);
    QObject::connect(buttonBox, SIGNAL(accepted()), ChangeOptionsDialog, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), ChangeOptionsDialog, SLOT(reject()));
    QObject::connect(spinBox, SIGNAL(valueChanged(int)), ChangeOptionsDialog, SLOT(ChangeFilterSize1(int)));
    QObject::connect(spinBox, SIGNAL(valueChanged(int)), ChangeOptionsDialog, SLOT(ChangeFilterSize2(int)));
    QObject::connect(radioButton_2, SIGNAL(toggled(bool)), groupBox_4, SLOT(setVisible(bool)));
    QObject::connect(radioButton, SIGNAL(toggled(bool)), groupBox_2, SLOT(setVisible(bool)));
    QObject::connect(radioButton, SIGNAL(toggled(bool)), ChangeOptionsDialog, SLOT(setSquare()));
    QObject::connect(radioButton_2, SIGNAL(toggled(bool)), ChangeOptionsDialog, SLOT(setCircle()));

    QMetaObject::connectSlotsByName(ChangeOptionsDialog);
    } // setupUi

    void retranslateUi(QDialog *ChangeOptionsDialog)
    {
    ChangeOptionsDialog->setWindowTitle(QApplication::translate("ChangeOptionsDialog", "Dialog", 0, QApplication::UnicodeUTF8));
    groupBox->setTitle(QApplication::translate("ChangeOptionsDialog", "Options", 0, QApplication::UnicodeUTF8));
    groupBox_3->setTitle(QApplication::translate("ChangeOptionsDialog", "MedianFilter", 0, QApplication::UnicodeUTF8));
    radioButton->setText(QApplication::translate("ChangeOptionsDialog", "Use square mask", 0, QApplication::UnicodeUTF8));
    groupBox_2->setTitle(QString());
    label->setText(QApplication::translate("ChangeOptionsDialog", "Size (pixels)", 0, QApplication::UnicodeUTF8));
    radioButton_2->setText(QApplication::translate("ChangeOptionsDialog", "Use circle mask", 0, QApplication::UnicodeUTF8));
    groupBox_4->setTitle(QString());
    label_2->setText(QApplication::translate("ChangeOptionsDialog", "Size (pixels)", 0, QApplication::UnicodeUTF8));
    Q_UNUSED(ChangeOptionsDialog);
    } // retranslateUi

};

namespace Ui {
    class ChangeOptionsDialog: public Ui_ChangeOptionsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHANGEOPTIONSDIALOG_H
