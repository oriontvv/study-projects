# -------------------------------------------------
# Project created by QtCreator 2009-10-09T18:02:30
# -------------------------------------------------
TARGET = ElectronicCashier
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    euro.cpp \
    changeoptionsdialog.cpp
HEADERS += mainwindow.h \
    euro.h \
    changeoptionsdialog.h
FORMS += mainwindow.ui \
    changeoptionsdialog.ui
