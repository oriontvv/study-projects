/****************************************************************************
** Meta object code from reading C++ file 'changeoptionsdialog.h'
**
** Created: Sat 24. Oct 21:34:33 2009
**      by: The Qt Meta Object Compiler version 59 (Qt 4.4.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../changeoptionsdialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'changeoptionsdialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.4.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ChangeOptionsDialog[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      23,   21,   20,   20, 0x0a,
      46,   21,   20,   20, 0x0a,
      69,   20,   20,   20, 0x0a,
      81,   20,   20,   20, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ChangeOptionsDialog[] = {
    "ChangeOptionsDialog\0\0n\0ChangeFilterSize1(int)\0"
    "ChangeFilterSize2(int)\0setSquare()\0"
    "setCircle()\0"
};

const QMetaObject ChangeOptionsDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ChangeOptionsDialog,
      qt_meta_data_ChangeOptionsDialog, 0 }
};

const QMetaObject *ChangeOptionsDialog::metaObject() const
{
    return &staticMetaObject;
}

void *ChangeOptionsDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ChangeOptionsDialog))
        return static_cast<void*>(const_cast< ChangeOptionsDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int ChangeOptionsDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: ChangeFilterSize1((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: ChangeFilterSize2((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: setSquare(); break;
        case 3: setCircle(); break;
        }
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
