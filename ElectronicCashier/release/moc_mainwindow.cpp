/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created: Sat 24. Oct 21:34:30 2009
**      by: The Qt Meta Object Compiler version 59 (Qt 4.4.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.4.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x0a,
      23,   11,   11,   11, 0x0a,
      34,   11,   11,   11, 0x0a,
      50,   11,   11,   11, 0x0a,
      65,   11,   11,   11, 0x0a,
      80,   11,   11,   11, 0x0a,
      96,   11,   11,   11, 0x0a,
     113,   11,   11,   11, 0x0a,
     129,   11,   11,   11, 0x0a,
     145,   11,   11,   11, 0x0a,
     168,   11,   11,   11, 0x0a,
     183,   11,   11,   11, 0x0a,
     195,   11,   11,   11, 0x0a,
     211,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0openFile()\0saveFile()\0"
    "changeOptions()\0ShowOriginal()\0"
    "medianFilter()\0erosionFilter()\0"
    "dilationFilter()\0closingFilter()\0"
    "openingFilter()\0colorCorectionFilter()\0"
    "Binorization()\0Detection()\0autoDetection()\0"
    "about()\0"
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, 0 }
};

const QMetaObject *MainWindow::metaObject() const
{
    return &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: openFile(); break;
        case 1: saveFile(); break;
        case 2: changeOptions(); break;
        case 3: ShowOriginal(); break;
        case 4: medianFilter(); break;
        case 5: erosionFilter(); break;
        case 6: dilationFilter(); break;
        case 7: closingFilter(); break;
        case 8: openingFilter(); break;
        case 9: colorCorectionFilter(); break;
        case 10: Binorization(); break;
        case 11: Detection(); break;
        case 12: autoDetection(); break;
        case 13: about(); break;
        }
        _id -= 14;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
