
/*
 * NAME: Taranov Vassiliy V., 325
 * ASGN: N2
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui>
#include <QVector>

#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <cmath>

#include "euro.h"
#include "changeoptionsdialog.h"


namespace Ui
{
    class MainWindow;
}
#define OBJ_COUNT 10000

enum CUR_IMG{
    IMG_NULL,
    IMG_ORIG,
    IMG,
    IMG_BIN,
    IMG_FILTR,
    IMG_OBJ
};
typedef long(*f_cmp)(const void*,const void*);

class MainWindow : public QMainWindow
{
    Q_OBJECT

    friend class Euro;
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void openFile();
    void saveFile();
    void changeOptions();
    void ShowOriginal();
    void medianFilter();
    void erosionFilter();
    void dilationFilter();
    void closingFilter();
    void openingFilter();
    void colorCorectionFilter();
    void Binorization();
    void Detection();
    void autoDetection();
    void about();

private:
    Ui::MainWindow *ui;
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);

    int filtr_eps;

    QString *file;
    QPainter *painter;
    QImage *original, *image, *img_scaled, *img_obj, *img_filtr;
    ChangeOptionsDialog *dialog;
    int **bin_mat;
    int h, w;
    int ms[OBJ_COUNT];
    CUR_IMG cur_img;
    double EuroProperties[25][3];
    void loadProperties();

    double size[25];
    double result_sum;

    int friends8(int x, int y, int n);
    int friends4(int x, int y, long color);
    void reimg();
    void minimaze_colors();
    int _min(int color);
    void search_binding_areas();
    void search_equal_colors();
    void set_euro_parametrs();
    void show_result();
    void draw_border(QImage *img, int x, int y, QRgb color);
    void alloc_memory(int _w, int _h);
    void destroy_memory();
    long my_proection(QRgb rgb);

    int number_euro;
    Euro *masEuro;
};

#endif // MAINWINDOW_H
