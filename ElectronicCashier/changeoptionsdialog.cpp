/*
 * NAME: Taranov Vassiliy V., 325
 * ASGN: N2
 */

#include "changeoptionsdialog.h"
#include "ui_changeoptionsdialog.h"

ChangeOptionsDialog::ChangeOptionsDialog(QWidget *parent) :
    QDialog(parent),
    m_ui(new Ui::ChangeOptionsDialog)
{
    m_ui->setupUi(this);
    setWindowTitle("Change options");
    filter_size1 = 2;
    filter_size2 = 3;
    filter_type = MEDIAN_SQUARE;
}

ChangeOptionsDialog::~ChangeOptionsDialog()
{
    delete m_ui;
}

void ChangeOptionsDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
