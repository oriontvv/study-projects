/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Sun 25. Sep 20:46:04 2011
**      by: Qt User Interface Compiler version 4.7.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QScrollArea>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>
#include "mywidget.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_4;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QGridLayout *gridLayout_5;
    MyWidget *myWidget;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QPushButton *open;
    QPushButton *save;
    QSpacerItem *verticalSpacer;
    QPushButton *exit;
    QGroupBox *view;
    QGridLayout *gridLayout_2;
    QPushButton *initImage;
    QSpinBox *rotatingPower;
    QFrame *line_2;
    QDoubleSpinBox *scalePower;
    QPushButton *rotate;
    QPushButton *scale;
    QLabel *label;
    QLabel *label_2;
    QPushButton *deselect;
    QSpacerItem *verticalSpacer_2;
    QSpacerItem *verticalSpacer_3;
    QGroupBox *edit;
    QGridLayout *gridLayout_3;
    QComboBox *comboBox;
    QPushButton *pushButton;
    QProgressBar *progressBar;
    QWidget *doublePowerWidget;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_4;
    QDoubleSpinBox *doubleSpinBox_2;
    QSpacerItem *horizontalSpacer_4;
    QWidget *gausParamWidget;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_3;
    QDoubleSpinBox *GaussSpinBox;
    QSpacerItem *horizontalSpacer;
    QWidget *integerParamWidget;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_5;
    QLabel *label_5;
    QSpinBox *integerSpinBox;
    QSpacerItem *horizontalSpacer_6;
    QWidget *kernelWidget;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *kernel;
    QSpacerItem *horizontalSpacer_8;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(461, 523);
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout_4 = new QGridLayout(centralWidget);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 189, 483));
        gridLayout_5 = new QGridLayout(scrollAreaWidgetContents);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        myWidget = new MyWidget(scrollAreaWidgetContents);
        myWidget->setObjectName(QString::fromUtf8("myWidget"));
        myWidget->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        gridLayout_5->addWidget(myWidget, 0, 0, 1, 1);

        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout_4->addWidget(scrollArea, 0, 1, 1, 1);

        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setMaximumSize(QSize(250, 16777215));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        open = new QPushButton(groupBox);
        open->setObjectName(QString::fromUtf8("open"));

        gridLayout->addWidget(open, 0, 0, 1, 1);

        save = new QPushButton(groupBox);
        save->setObjectName(QString::fromUtf8("save"));

        gridLayout->addWidget(save, 0, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 5, 0, 1, 2);

        exit = new QPushButton(groupBox);
        exit->setObjectName(QString::fromUtf8("exit"));

        gridLayout->addWidget(exit, 6, 0, 1, 1);

        view = new QGroupBox(groupBox);
        view->setObjectName(QString::fromUtf8("view"));
        gridLayout_2 = new QGridLayout(view);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        initImage = new QPushButton(view);
        initImage->setObjectName(QString::fromUtf8("initImage"));

        gridLayout_2->addWidget(initImage, 0, 0, 1, 2);

        rotatingPower = new QSpinBox(view);
        rotatingPower->setObjectName(QString::fromUtf8("rotatingPower"));
        rotatingPower->setMinimum(-360);
        rotatingPower->setMaximum(360);
        rotatingPower->setSingleStep(5);
        rotatingPower->setValue(90);

        gridLayout_2->addWidget(rotatingPower, 1, 1, 1, 1);

        line_2 = new QFrame(view);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_2, 1, 2, 2, 1);

        scalePower = new QDoubleSpinBox(view);
        scalePower->setObjectName(QString::fromUtf8("scalePower"));
        scalePower->setSingleStep(0.1);
        scalePower->setValue(1.5);

        gridLayout_2->addWidget(scalePower, 1, 4, 1, 1);

        rotate = new QPushButton(view);
        rotate->setObjectName(QString::fromUtf8("rotate"));

        gridLayout_2->addWidget(rotate, 2, 0, 1, 2);

        scale = new QPushButton(view);
        scale->setObjectName(QString::fromUtf8("scale"));

        gridLayout_2->addWidget(scale, 2, 3, 1, 2);

        label = new QLabel(view);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label, 1, 0, 1, 1);

        label_2 = new QLabel(view);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_2, 1, 3, 1, 1);

        deselect = new QPushButton(view);
        deselect->setObjectName(QString::fromUtf8("deselect"));

        gridLayout_2->addWidget(deselect, 0, 3, 1, 2);


        gridLayout->addWidget(view, 2, 0, 1, 2);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 1, 0, 1, 2);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_3, 3, 0, 1, 2);

        edit = new QGroupBox(groupBox);
        edit->setObjectName(QString::fromUtf8("edit"));
        gridLayout_3 = new QGridLayout(edit);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        comboBox = new QComboBox(edit);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout_3->addWidget(comboBox, 1, 0, 1, 2);

        pushButton = new QPushButton(edit);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        gridLayout_3->addWidget(pushButton, 8, 1, 1, 1);

        progressBar = new QProgressBar(edit);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(0);

        gridLayout_3->addWidget(progressBar, 8, 0, 1, 1);

        doublePowerWidget = new QWidget(edit);
        doublePowerWidget->setObjectName(QString::fromUtf8("doublePowerWidget"));
        horizontalLayout = new QHBoxLayout(doublePowerWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        label_4 = new QLabel(doublePowerWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout->addWidget(label_4);

        doubleSpinBox_2 = new QDoubleSpinBox(doublePowerWidget);
        doubleSpinBox_2->setObjectName(QString::fromUtf8("doubleSpinBox_2"));

        horizontalLayout->addWidget(doubleSpinBox_2);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);


        gridLayout_3->addWidget(doublePowerWidget, 2, 0, 1, 2);

        gausParamWidget = new QWidget(edit);
        gausParamWidget->setObjectName(QString::fromUtf8("gausParamWidget"));
        horizontalLayout_2 = new QHBoxLayout(gausParamWidget);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        label_3 = new QLabel(gausParamWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_2->addWidget(label_3);

        GaussSpinBox = new QDoubleSpinBox(gausParamWidget);
        GaussSpinBox->setObjectName(QString::fromUtf8("GaussSpinBox"));
        GaussSpinBox->setMinimum(0.3);
        GaussSpinBox->setMaximum(10);
        GaussSpinBox->setValue(1);

        horizontalLayout_2->addWidget(GaussSpinBox);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);


        gridLayout_3->addWidget(gausParamWidget, 3, 0, 1, 2);

        integerParamWidget = new QWidget(edit);
        integerParamWidget->setObjectName(QString::fromUtf8("integerParamWidget"));
        horizontalLayout_3 = new QHBoxLayout(integerParamWidget);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        label_5 = new QLabel(integerParamWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_3->addWidget(label_5);

        integerSpinBox = new QSpinBox(integerParamWidget);
        integerSpinBox->setObjectName(QString::fromUtf8("integerSpinBox"));

        horizontalLayout_3->addWidget(integerSpinBox);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);


        gridLayout_3->addWidget(integerParamWidget, 4, 0, 1, 2);

        kernelWidget = new QWidget(edit);
        kernelWidget->setObjectName(QString::fromUtf8("kernelWidget"));
        horizontalLayout_4 = new QHBoxLayout(kernelWidget);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_7);

        kernel = new QPushButton(kernelWidget);
        kernel->setObjectName(QString::fromUtf8("kernel"));

        horizontalLayout_4->addWidget(kernel);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_8);


        gridLayout_3->addWidget(kernelWidget, 5, 0, 1, 2);


        gridLayout->addWidget(edit, 4, 0, 1, 2);


        gridLayout_4->addWidget(groupBox, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);
        QObject::connect(exit, SIGNAL(clicked()), MainWindow, SLOT(close()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "ImageEditor", 0, QApplication::UnicodeUTF8));
        myWidget->setText(QString());
        groupBox->setTitle(QApplication::translate("MainWindow", "\320\234\320\265\320\275\321\216", 0, QApplication::UnicodeUTF8));
        open->setText(QApplication::translate("MainWindow", "\320\236\321\202\320\272\321\200\321\213\321\202\321\214...", 0, QApplication::UnicodeUTF8));
        save->setText(QApplication::translate("MainWindow", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214...", 0, QApplication::UnicodeUTF8));
        exit->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\205\320\276\320\264", 0, QApplication::UnicodeUTF8));
        view->setTitle(QApplication::translate("MainWindow", "\320\222\320\270\320\264", 0, QApplication::UnicodeUTF8));
        initImage->setText(QApplication::translate("MainWindow", "\320\236\321\200\320\270\320\263\320\270\320\275\320\260\320\273", 0, QApplication::UnicodeUTF8));
        rotate->setText(QApplication::translate("MainWindow", "\320\237\320\276\320\262\320\276\321\200\320\276\321\202", 0, QApplication::UnicodeUTF8));
        scale->setText(QApplication::translate("MainWindow", "\320\234\320\260\321\201\321\210\321\202\320\260\320\261", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "\321\203\320\263\320\276\320\273=", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "\320\232=", 0, QApplication::UnicodeUTF8));
        deselect->setText(QApplication::translate("MainWindow", "\320\243\320\261\321\200\320\260\321\202\321\214 \320\262\321\213\320\264\320\265\320\273\320\265\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        edit->setTitle(QApplication::translate("MainWindow", "\320\244\320\270\320\273\321\214\321\202\321\200", 0, QApplication::UnicodeUTF8));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "\320\232\320\276\321\200\321\200\320\265\320\272\321\206\320\270\321\217 \320\272\320\276\320\275\321\202\321\200\320\260\321\201\321\202\320\260", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "\320\237\320\276\320\272\320\260\320\275\320\260\320\273\321\214\320\275\320\260\321\217 \320\272\320\276\321\200\321\200\320\265\320\272\321\206\320\270\321\217 \320\272\320\276\320\275\321\202\321\200\320\260\321\201\321\202\320\260", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "\320\223\320\260\321\203\321\201\321\201\320\260", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "\320\237\320\276\320\262\321\213\321\210\320\265\320\275\320\270\320\265 \321\200\320\265\320\267\320\272\320\276\321\201\321\202\320\270", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "\320\234\320\265\320\264\320\270\320\260\320\275\320\275\320\260\321\217 \321\204\320\270\320\273\321\214\321\202\321\200\320\260\321\206\320\270\321\217", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "\320\241\320\265\321\200\321\213\320\271 \320\274\320\270\321\200", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "\320\237\321\200\320\276\320\270\320\267\320\262\320\276\320\273\321\214\320\275\320\276\320\265 \321\217\320\264\321\200\320\276", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "\320\255\321\204\321\204\320\265\320\272\321\202 \"\320\222\320\276\320\273\320\275\321\213\"", 0, QApplication::UnicodeUTF8)
        );
        pushButton->setText(QApplication::translate("MainWindow", "\320\237\321\200\320\270\320\274\320\265\320\275\320\270\321\202\321\214!", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MainWindow", "power =", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindow", "\317\203 =", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("MainWindow", "power =", 0, QApplication::UnicodeUTF8));
        kernel->setText(QApplication::translate("MainWindow", "\320\270\320\267\320\274\320\265\320\275\320\270\321\202\321\214 \321\217\320\264\321\200\320\276", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
