#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include "galaxy.h"

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void paintEvent(QPaintEvent *event);
    void timerEvent(QTimerEvent *event);
   // void keyPressEvent(QKeyEvent *event);
    QImage *img;

public slots:
    void ChangeAnimation();
    void Change_color_r(int n);
    void Change_color_g(int n);
    void Change_color_b(int n);



private:
    Ui::MainWindow *ui;
    Galaxy *galaxy;
};

#endif // MAINWINDOW_H
