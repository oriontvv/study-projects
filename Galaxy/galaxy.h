#ifndef GALAXY_H
#define GALAXY_H

#include <QGLWidget>
#include <QtOpenGL>
#include <GL\glext.h>
#include <GL\glu.h>
#include <GL\gl.h>
#include <QGL.h>

#include <cmath>
#include <stdlib.h>
#include <stdio.h>

struct Colors{
        unsigned char r;
        unsigned char g;
        unsigned char b;
};
enum GLX_TYPE {
        ELLIPTIC,
        MINDALE,
        SPIRALE,
};
class Galaxy : public QGLWidget
{
    Q_OBJECT

public:
    virtual void initializeGL();
    virtual void paintGL();
    virtual void resizeGL(int width, int height);
public slots:
    void setElliptic();
    void setMindale();
    void setSpirale();
// -------------------------
    void setRandomColor();
// -------------------------
    void Inc_center_x();
    void Dec_center_x();
    void Inc_center_y();
    void Dec_center_y();
// --------------------------
    void Inc_E_radius();
    void Dec_E_radius();
// --------------------------
    void Inc_M_angle();
    void Dec_M_angle();
    void Inc_M_a();
    void Dec_M_a();
    void Inc_M_b();
    void Dec_M_b();
// --------------------------


public:
    Galaxy(QWidget *parent = 0);
    ~Galaxy();
    void draw();
    QImage *img;

////////////////////////////////////////////////////////////////
    int Width, Height;
    int GLX_center_x, GLX_center_y;

    Colors GLX_E_color, GLX_M_color, GLX_S_color;
    Colors *color, *cl;
    GLX_TYPE GLX_type;
//==================	COMMON		====================
    double Stars[700][5];// x, y, r, g, b, size
    int Stars_count;
    int GLX_animation;
    int GLX_changed;
    int GLX_animation_angle_step;
    double GLX_angle;
//================	ELLIPTIC GALAXY	=====================
    double GLX_E_radius;
    double GLX_E_Points[100000][5];
    int GLX_E_Point_count;
    double GLX_E_K, GLX_E_K1;
    int GLS_E_Q1;
//================	MINDALEE GALAXY	=====================
    double GLX_M_a, GLX_M_b;
    double GLX_M_Points[100000][5];
    int GLX_M_Point_count;
//================	SPIRALE GALAXY	=====================
    double GLX_S_radius;
    double GLX_S_a, GLX_S_b, GLX_S_K, GLX_S_K1;
    double GLX_S_Points[100000][5];
    int GLX_S_Point_count;
    double GLX_S_AngleMax, GLX_S_AngleStep;
//===========	END GALAXY'S PROPERTIES 	=============
    void Init();
    void Init_tables();
    double distance (double x1, double y1, double x2, double y2);
    void PutPixel(double x, double y, double r, double g, double b);
    void DrawOtherStars();
    void MakeSpirale(double x, double y, double r, double g, double b);
    void Timer(int n);
    void changeColor();

////////////////////////////////////////////////////////////////

};

#endif // GALAXY_H
