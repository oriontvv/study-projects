# -------------------------------------------------
# Project created by QtCreator 2009-09-26T17:02:26
# -------------------------------------------------
QT += opengl
TARGET = Galaxy
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    galaxy.cpp
HEADERS += mainwindow.h \
    GalaxyWidget.h \
    galaxy.h
FORMS += mainwindow.ui
