#include <QtGui/QApplication>
#include <QPainter>

#include "mainwindow.h"
#include "galaxy.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWindow w;
    w.setFixedSize(614, 485);
    w.show();

    return a.exec();
}
