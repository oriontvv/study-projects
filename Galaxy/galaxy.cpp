
#include "galaxy.h"

void Galaxy::Init()
{
    // Common
    Width = 380;
    Height = 490;
    GLX_angle = 0;
    GLX_center_x = Width / 2;
    GLX_center_y = Height / 2;

    //Elliptic
    GLX_E_radius = 250;
    GLX_E_color.r = 255;
    GLX_E_color.g = 0;
    GLX_E_color.b = 0;

    //MINDALE
    GLX_M_a = 80;
    GLX_M_b = 30;
    GLX_M_color.r = 0;
    GLX_M_color.g = 255;
    GLX_M_color.b = 0;

    //Spirale
    GLX_S_radius = 500;
    GLX_S_a = 45;
    GLX_S_b = 9;
    GLX_S_K = 2000;
    GLX_S_AngleMax = 180;
    GLX_S_AngleStep = 45;
    GLX_S_color.r = 0;
    GLX_S_color.g = 0;
    GLX_S_color.b = 255;

    //glMatrixMode(GL_MODELVIEW);
    //glLoadIdentity();

}
double Galaxy::distance (double x1, double y1, double x2, double y2)
{
    return sqrt( (double)(x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2) );
}
void Galaxy::Init_tables()
{
    double d;
    Width = 380;
    Height = 490;
    // STARS:
    Stars_count = 0;
    while (Stars_count < 700){
        Stars[Stars_count][0] = Width * 1.0 * rand() / (RAND_MAX);
        Stars[Stars_count][1] = Height * 1.0 * rand() / (RAND_MAX);
        Stars[Stars_count][2] = 255.0 * rand() / (RAND_MAX);
        Stars[Stars_count][3] = 255.0 * rand() / (RAND_MAX);
        Stars[Stars_count][4] = 255.0 * rand() / (RAND_MAX);
        Stars[Stars_count][5] = 2.5 * rand() / (RAND_MAX);
        Stars_count++;
    }
    double rand_number, p, f;

    //GENERATING ELLIPTIC GALAXY:
    GLX_M_Point_count = 0;
    for(int x = 0; x < Width; ++x)
        for(int y = 0; y < Height; ++y){
            d = distance(x, y, GLX_center_x, GLX_center_y);
            //f = exp( -pow(d, 4.0 * GLS_E_Q1 / 10) / pow(GLX_E_radius, 3.0));
             f = exp( -pow(d, 4.0) / pow(GLX_E_radius, GLX_E_K));
            rand_number = 1.0 * rand() / (RAND_MAX);
            if (f > rand_number){ // - is star of galaxy;
                GLX_E_Points[GLX_E_Point_count][0] = x;
                GLX_E_Points[GLX_E_Point_count][1] = y;
                GLX_E_Points[GLX_E_Point_count][2] = d;
                GLX_E_Point_count++;
            }
        }

    //GENERATING MINDALEE GALAXY:
    GLX_M_Point_count = 0;
    for(int x = 0; x < Width; ++x)
        for(int y = 0; y < Height; ++y){
            d = distance(x, y, GLX_center_x, GLX_center_y);
            p = pow( (double) ((x - GLX_center_x) / GLX_M_a), 2)  +
                    pow( (double) ((y - GLX_center_y) / GLX_M_b), 2);
            f = exp(-pow((double)p, 1.2));
            rand_number = 1.0 * rand() / (RAND_MAX + 1.0);
            if (f > rand_number){ // - is star of galaxy;
                GLX_M_Points[GLX_M_Point_count][0] = x;
                GLX_M_Points[GLX_M_Point_count][1] = y;
                GLX_M_Points[GLX_M_Point_count][2] = d;
                GLX_M_Point_count++;
            }
        }

    //GENERATING SPIRALE GALAXY:
    GLX_S_Point_count = 0;
    for(int x = 0; x < Width; ++x)
        for(int y = 0; y < Height; ++y){
            d = distance(x, y, GLX_center_x, GLX_center_y);
            p = pow((double) ((x - GLX_center_x) / GLX_S_a), 2) +
                    pow( (double) ((y - GLX_center_y) / GLX_S_b), 2);
            f = exp(-pow(p, 0.7));
            rand_number = 1.0 * rand() / (RAND_MAX + 1.0);
            if (f > rand_number){ // - is star of galaxy;
                GLX_S_Points[GLX_S_Point_count][0] = x;
                GLX_S_Points[GLX_S_Point_count][1] = y;
                GLX_S_Points[GLX_S_Point_count][2] = d;
                GLX_S_Point_count++;
            }
        }

}
Galaxy::Galaxy(QWidget *parent)
       : QGLWidget(QGLFormat(QGL::DepthBuffer|QGL::StencilBuffer), parent)
{
    resize(360, 440);
    
    // Common
    GLX_E_radius = 250;
    Init();
    GLX_animation = 0;
    GLX_changed = 0;
    GLX_animation_angle_step = 3;
    GLX_S_K1 = 1;
    GLX_E_K = 3;
    GLX_E_K1 = 1;

    GLX_type = SPIRALE;

    Init_tables();
    color = new Colors;
    cl = new Colors;
    img = new QImage("background.bmp");
}
Galaxy::~Galaxy()
{
    delete color;
    delete cl;
    delete img;
}

void Galaxy::initializeGL()
{
   // qglClearColor(Qt::white);
    glClearColor(1.0, 1.0, 1.0, 1);
   // glPointSize(2.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0, 360.0, 0, 440.0);

    GLX_E_radius = 250;
    GLX_animation = 0;

}
void Galaxy::PutPixel(double x, double y, double r, double g, double b)
{
    glColor3ub((unsigned char)r, (unsigned char)g, (unsigned char)b);
    glBegin(GL_POINTS);
        glVertex2f((int)x,(int)y);
    glEnd();
}
void Galaxy::DrawOtherStars()
{
    for(int i = 0; i <Stars_count; ++i){
        glPointSize(fmod(Stars[i][5], 2.3));
        PutPixel(Stars[i][0], Stars[i][1], Stars[i][2], Stars[i][3], Stars[i][4]);
    }
    glPointSize(1.0);
}
void Galaxy::MakeSpirale(double x, double y, double r, double g, double b)
{
    double alpha = GLX_S_K / pow(distance(x, y, GLX_center_x, GLX_center_y), 0.07);
    for(double angle = 0; angle < GLX_S_AngleMax; angle += GLX_S_AngleStep){
        glPushMatrix();
        glTranslated(GLX_center_x, GLX_center_y, 0);
        glRotated(alpha + angle + GLX_angle, 0, 0, 1);
        glTranslated(-GLX_center_x, -GLX_center_y, 0);
        PutPixel(x, y, r, g, b);
        glPopMatrix();
    }
}

void Galaxy::resizeGL(int width, int height)
{

}
void Galaxy::paintGL()
{
    draw();
}
void Galaxy::draw()
{
    //glColor3f(0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT);
    QPainter painter(this);
    painter.drawImage(0, 0, *img, 0, 0, 360, 440);
/////////////////////////////////////////////////////////////////////
    //if (GLX_changed)Init_tables();
    DrawOtherStars();
    int m = 1;
    double d, f, rand_number;
    if (GLX_animation ){
        glPushMatrix();
        switch(GLX_type){
            case ELLIPTIC:
                if (GLX_E_K > 3.5) GLX_E_K1 = -1;
                else if (GLX_E_K < 2.6) GLX_E_K1 = 1;
                GLX_E_K += (0.1 * GLX_E_K1);
                break;
            case MINDALE:
                glTranslated(GLX_center_x, GLX_center_y, 0);
                glRotated(-GLX_angle, 0.0, 0.0, 1.0);
                glTranslated(-GLX_center_x, -GLX_center_y, 0);
                GLX_angle = (int)(GLX_angle + GLX_animation_angle_step);
                if (GLX_angle >= 360) GLX_angle = 0;
                break;
            case SPIRALE:
                if (GLX_S_K > 5000) GLX_S_K1 = -1;
                else if (GLX_S_K < -5000) GLX_S_K1 = 1;
                GLX_S_K += (65 * GLX_S_K1);
                break;
            default:
                exit(0);
        }
        GLX_changed = 0;
    }

    switch(GLX_type){
        case ELLIPTIC:
            m = 128;
            if(GLX_animation || GLX_changed){
                //GENERATING ELLIPTIC GALAXY:
                GLX_E_Point_count = 0;
                for(int x = 0; x < Width; ++x)
                    for(int y = 0; y < Height; ++y){
                        d = distance(x, y, GLX_center_x, GLX_center_y);
                        f = exp( -pow(d, 4.0) / pow(GLX_E_radius, GLX_E_K));
                        rand_number = 1.0 * rand() / (RAND_MAX);
                        if (f > rand_number){ // - is star of galaxy;
                            GLX_E_Points[GLX_E_Point_count][0] = x;
                            GLX_E_Points[GLX_E_Point_count][1] = y;
                            GLX_E_Points[GLX_E_Point_count][2] = d;
                            GLX_E_Point_count++;

                        }
                    }
            }

            for(int i = 0; i < GLX_E_Point_count; ++i)
                PutPixel(GLX_E_Points[i][0], GLX_E_Points[i][1],
                            255 + (GLX_E_color.r - 255) / m * GLX_E_Points[i][2],
                            255 + (GLX_E_color.g - 255) / m * GLX_E_Points[i][2],
                            255 + (GLX_E_color.b - 255) / m * GLX_E_Points[i][2]);
            break;
        case MINDALE:
            m = 128;
            for(int i = 0; i < GLX_M_Point_count; ++i)
                PutPixel(GLX_M_Points[i][0], GLX_M_Points[i][1],
                                255 + (GLX_M_color.r - 255) / m * GLX_M_Points[i][2],
                                255 + (GLX_M_color.g - 255) / m * GLX_M_Points[i][2],
                                255 + (GLX_M_color.b - 255) / m * GLX_M_Points[i][2]);
            break;
        case SPIRALE:
            m = 120;
            for(int i = 0; i < GLX_S_Point_count; ++i)
                MakeSpirale(GLX_S_Points[i][0], GLX_S_Points[i][1],
                            255 + (GLX_S_color.r - 255) / m * GLX_S_Points[i][2],
                            255 + (GLX_S_color.g - 255) / m * GLX_S_Points[i][2],
                            255 + (GLX_S_color.b - 255) / m * GLX_S_Points[i][2]);
            break;
        default:
            exit(0);
    }
    if (GLX_animation) {glPopMatrix();	}
    GLX_changed = 0;

    glFinish();
//////////////////////////////////////////////////////////////////////

}

void Galaxy::Timer(int n)
{
    //glutPostRedisplay();
    update();
    //glutTimerFunc(40, Timer, 0);
}

void Galaxy::changeColor()
{
    switch(GLX_type)
    {
        case ELLIPTIC:
            GLX_E_color.r = (int)(256.0 * rand() / RAND_MAX);
            GLX_E_color.g = (int)(256.0 * rand() / RAND_MAX);
            GLX_E_color.b = (int)(256.0 * rand() / RAND_MAX);
            break;
        case MINDALE:
            GLX_M_color.r = (int)(256.0 * rand() / RAND_MAX);
            GLX_M_color.g = (int)(256.0 * rand() / RAND_MAX);
            GLX_M_color.b = (int)(256.0 * rand() / RAND_MAX);
            break;
        case SPIRALE:
            GLX_S_color.r = (int)(256.0 * rand() / RAND_MAX);
            GLX_S_color.g = (int)(256.0 * rand() / RAND_MAX);
            GLX_S_color.b = (int)(256.0 * rand() / RAND_MAX);
            break;
        default:
            break;
    }
}


void Galaxy::setRandomColor()
{
    changeColor();
}

void Galaxy::setElliptic()
{
    GLX_type = ELLIPTIC;
    draw();
}
void Galaxy::setMindale()
{
    GLX_type = MINDALE;
    draw();
}
void Galaxy::setSpirale()
{
    GLX_type = SPIRALE;
    draw();

}

void Galaxy::Inc_center_x()
{
    glTranslated(+10, 0, 0);
    GLX_changed = 1;
    //update();
    draw();
}
void Galaxy::Dec_center_x()
{
    glTranslated(-10, 0, 0);
    GLX_changed = 1;
    //update();
    draw();
}
void Galaxy::Inc_center_y()
{
    glTranslated(0, +10, 0);
    GLX_changed = 1;
    //update();
    draw();
}
void Galaxy::Dec_center_y()
{
    glTranslated(0, -10, 0);
    GLX_changed = 1;
   // update();
    draw();
}
void Galaxy::Inc_E_radius()
{
    if(GLX_E_radius < 670) GLX_E_radius += 20;
    GLX_changed = 1;
    //update();
    draw();
}
void Galaxy::Dec_E_radius()
{
    if (GLX_E_radius > 20)  GLX_E_radius -= 20;

    GLX_changed = 1;
   // update();
   draw ();
}
void Galaxy::Inc_M_angle()
{
    GLX_E_K += GLX_E_K1;
    GLX_changed = 1;
   // update();
    draw();
}
void Galaxy::Dec_M_angle()
{
    GLX_E_K -= GLX_E_K1;
    GLX_changed = 1;
   // update();
    draw();
}
void Galaxy::Inc_M_a()
{
    GLX_M_a += 10;
    GLX_changed = 1;
    //update();
    draw();
}
void Galaxy::Dec_M_a()
{
    GLX_M_a -= 10;
    GLX_changed = 1;
   // update();
    draw();
}
void Galaxy::Inc_M_b()
{
    GLX_M_b += 10;
    GLX_changed = 1;
    Init_tables();
    //update();
    draw();
}
void Galaxy::Dec_M_b()
{
    GLX_M_b -= 10;
    GLX_changed = 1;
    Init_tables();
   // update();
    draw();
}

