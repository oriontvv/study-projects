#include <QPainter>
#include <QtOpenGL>
#include <QTimer>
#include <QLabel>
#include <QSlider>
#include <QSpinBox>
#include <QKeyEvent>
#include <cmath>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "galaxy.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    galaxy = new Galaxy(this);
    img = new QImage("background.bmp");

//  Connecting objects:


    connect(ui->Animation, SIGNAL(clicked()), SLOT(ChangeAnimation()));
    connect(ui->ChooseEleptic, SIGNAL(clicked()), galaxy, SLOT(setElliptic()));
    connect(ui->ChooseMindale, SIGNAL(clicked()), galaxy, SLOT(setMindale()));
    connect(ui->ChooseSpirale, SIGNAL(clicked()), galaxy, SLOT(setSpirale()));
    connect(ui->RandomColor, SIGNAL(clicked()), galaxy, SLOT(setRandomColor()));

    connect(ui->Common_color_spinbox_r, SIGNAL(valueChanged(int)), SLOT(Change_color_r(int)));
    connect(ui->Common_color_spinbox_g, SIGNAL(valueChanged(int)), SLOT(Change_color_g(int)));
    connect(ui->Common_color_spinbox_b, SIGNAL(valueChanged(int)), SLOT(Change_color_b(int)));

    connect(ui->Common_center_x_1, SIGNAL(clicked()), galaxy, SLOT(Inc_center_x()));
    connect(ui->Common_center_x_2, SIGNAL(clicked()), galaxy, SLOT(Dec_center_x()));
    connect(ui->Common_center_y_1, SIGNAL(clicked()), galaxy, SLOT(Inc_center_y()));
    connect(ui->Common_center_y_2, SIGNAL(clicked()), galaxy, SLOT(Dec_center_y()));

    connect(ui->E_radius_1, SIGNAL(clicked()), galaxy, SLOT(Inc_E_radius()));
    connect(ui->E_radius_2, SIGNAL(clicked()), galaxy, SLOT(Dec_E_radius()));

    connect(ui->M_a_1, SIGNAL(clicked()), galaxy, SLOT(Inc_M_a()));
    connect(ui->M_a_2, SIGNAL(clicked()), galaxy, SLOT(Dec_M_a()));
    connect(ui->M_b_1, SIGNAL(clicked()), galaxy, SLOT(Inc_M_b()));
    connect(ui->M_b_2, SIGNAL(clicked()), galaxy, SLOT(Dec_M_b()));
    connect(ui->M_Angle_1, SIGNAL(clicked()), galaxy, SLOT(Inc_M_angle()));
    connect(ui->M_angle_2, SIGNAL(clicked()), galaxy, SLOT(Dec_M_angle()));






    ui->E_layer->hide();
    ui->M_layer->hide();

    galaxy->show();
    startTimer(40);

}

MainWindow::~MainWindow()
{
    delete ui;
    delete galaxy;
    delete img;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.drawImage(0, 0, *img);

    switch(galaxy->GLX_type)
    {
        case ELLIPTIC:
            ui->Common_color_spinbox_r->setValue(galaxy->GLX_E_color.r);
            ui->Common_color_spinbox_g->setValue(galaxy->GLX_E_color.g);
            ui->Common_color_spinbox_b->setValue(galaxy->GLX_E_color.b);
            break;
        case MINDALE:
            ui->Common_color_spinbox_r->setValue(galaxy->GLX_M_color.r);
            ui->Common_color_spinbox_g->setValue(galaxy->GLX_M_color.g);
            ui->Common_color_spinbox_b->setValue(galaxy->GLX_M_color.b);
            break;
        case SPIRALE:
            ui->Common_color_spinbox_r->setValue(galaxy->GLX_S_color.r);
            ui->Common_color_spinbox_g->setValue(galaxy->GLX_S_color.g);
            ui->Common_color_spinbox_b->setValue(galaxy->GLX_S_color.b);
            break;
        default:
            exit(0);
    }

  //update();
 // galaxy->Init_tables();
 //   startTimer(40);
    galaxy->draw();
    //galaxy->update();


    //galaxy->GLX_E_radius = 0;


    //galaxy->show();


}
void MainWindow::timerEvent(QTimerEvent *enent)
{
    galaxy->repaint();
    update();
}
/*void MainWindow::keyPressEvent(QKeyEvent *e)
{
    switch(e->key()){
        case Qt::Key_Up:
            glTranslated(0, +10, 0);
            break;
        case Qt::Key_Down:
            glTranslated(0, -10, 0);
            break;
        case Qt::Key_Left:
            glTranslated(-10, 0, 0);
            break;
        case Qt::Key_Right:
            glTranslated(+10, 0, 0);
            break;            

        default:
          //  keyPressEvent(e);
            break;
    }
    update();
}*/
void MainWindow::ChangeAnimation()
{
    galaxy->GLX_animation = 1 - galaxy->GLX_animation;
    galaxy->Init();
    galaxy->draw();

    update();
}
void MainWindow::Change_color_r(int n)
{
    switch(galaxy->GLX_type){
        case ELLIPTIC:
            galaxy->GLX_E_color.r = n;
            break;
        case MINDALE:
            galaxy->GLX_M_color.r = n;
            break;
        case SPIRALE:
            galaxy->GLX_S_color.r = n;
            break;
        default:
            break;
    }
    update();
}
void MainWindow::Change_color_g(int n)
{
    switch(galaxy->GLX_type){
        case ELLIPTIC:
            galaxy->GLX_E_color.g = n;
            break;
        case MINDALE:
            galaxy->GLX_M_color.g = n;
            break;
        case SPIRALE:
            galaxy->GLX_S_color.g = n;
            break;
        default:
            break;
    }
    update();
}
void MainWindow::Change_color_b(int n)
{
    switch(galaxy->GLX_type){
        case ELLIPTIC:
            galaxy->GLX_E_color.b = n;
            break;
        case MINDALE:
            galaxy->GLX_M_color.b = n;
            break;
        case SPIRALE:
            galaxy->GLX_S_color.b = n;
            break;
        default:
            break;
    }
    update();
}


