#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <unistd.h>


#include "optionsdialog.h"

#include <QFile>
#include <QMessageBox>
#include <QFileDialog>
#include <QProgressDialog>
#include <QTimer>
#include <QThread>
#include <QKeyEvent>


#include <QDebug>


// main controller
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->loadMotions->setDisabled(true);
    scene = new QGraphicsScene(0, 0, 962, 537);
    view = new QGraphicsView(scene);

    optionsDialog = new OptionsDialog();
    optionsDialog->setSpeed(1);
    filterDialog = new FilterDialog();
    filterDialog->toAll(true);
    filterDialog->fromAll(true);
    filterDialog->setCount(false);

    movTable.clear();

    view->setRenderHint(QPainter::Antialiasing);
    view->setBackgroundBrush(QPixmap(":images/map.png"));
    view->setCacheMode(QGraphicsView::CacheBackground);
    view->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    view->setDragMode(QGraphicsView::ScrollHandDrag);
    //    view->setWindowTitle(QT_TRANSLATE_NOOP(QGraphicsView, "Animator"));
    view->setMaximumSize(970, 540);

    animationSpeed = NORMAL;

    loc_timer.setDuration(animationSpeed * 10);
    loc_timer.setFrameRange(0, 100);
    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(on_timeout()));
    connect(optionsDialog->getSpeedSpinBox(), SIGNAL(valueChanged(int)), SLOT(changeAnimationSpeed(int)));
    connect(ui->timeSlider, SIGNAL(valueChanged(int)), SLOT(setCurMonth(int)));

    ui->mapLayout->addWidget(view);
    view->show();
    setWindowTitle("Trace visualiser");
    setWindowIcon(QIcon(":/images/icon.ico"));

    on_Pause_clicked();

    parseRegions("data/regions_look_up.txt");
    parseCenters("data/centers.txt");
    parseMotions("data/raw.txt");

    anState = PAUSE;

    cur_month = 0;
    max_id = 0;

    timer->start(animationSpeed);
    scene->update();

    optionsDialog->getSpeedSpinBox()->setValue(1000);
    fmotions = new QVector <Motion>;
}

MainWindow::~MainWindow()
{
    delete ui;
    //    if (regions) delete regions;
}

// dir > 0  =>  forward
// dir < 0  =>  back
void MainWindow::calculateCurentMotions(int dir)
{
    const int n = ids.size();

    // init dynamic objects
    for (int from = 0; from < n; ++from) {
        for (int to = 0; to < n; ++to) {
            mvObjects[from][to]->init();
        }
    }
    // init static objects
    for (int i = 0; i < n; ++i) {
        stObjects[i]->init();
    }

    for (int m = 0; m < time_max; ++m)
        for (int from = 0; from < n; ++from)
            for (int to = 0; to < n; ++to)
                movTable[m][from][to]->init();

    if (!filterDialog->getCount()) {
        for (int i = 0; i < motions->size(); ++i) {
            Motion m = motions->at(i);
            movTable[m.month - 1][m.from][m.to]->incCount();
        }
    }
    else {
        // count
        int countFrom = filterDialog->getCountFrom();
        int countTo = filterDialog->getCountTo();
        int ids_count = obj_ids.size();

        for (int i = 0; i < ids_count; ++i)
            mvCount[i] = 0;

        for (int i = 0; i < motions->size(); ++i) {
            mvCount[objids2myid(motions->at(i).id)]++;
        }

        for (int i = 0; i < motions->size(); ++i) {
            Motion m = motions->at(i);
            int count = mvCount.at(objids2myid(m.id));
            if (countFrom <= count && count <= countTo) {
                movTable[m.month - 1][m.from][m.to]->incCount();
            }
        }
    }

    /////////////////////////////////////
    if (dir > 0) {
        if (cur_month <= time_max && cur_month > 0) {
            for (int from = 0; from < n; ++from) {
                for (int to = 0; to < n; ++to) {
                    int c = movTable[cur_month - 1][from][to]->getCount();
                    if (c) {
                        mvObjects[from][to]->setCount(c);
                    }
                }
            }
            //--------
            for (int from = 0; from < n; ++from) {
                stObjects[from]->setCountOut(0);
                for (int to = 0; to < n; ++to) {
                    int c = movTable[cur_month - 1][from][to]->getCount();
                    stObjects[from]->incFromCount(c);
                }
            }
        }
        /////////
        for (int month = 1; month <= cur_month; ++month) {
            for (int from = 0; from < n; ++from) {
                for (int to = 0; to < n; ++to) {
                    int c = movTable[month - 1][from][to]->getCount();
                    if (c){
                        stObjects[from]->decToCount(c);
                        stObjects[to]->incToCount(c);
                    }
                }
            }
        }

    } else { // dir < 0
        if (cur_month < time_max && cur_month >= 0) {
            for (int from = 0; from < n; ++from) {
                for (int to = 0; to < n; ++to) {
                    int c = movTable[cur_month][from][to]->getCount();
                    if (c) {
                        mvObjects[from][to]->setCount(c);
                    }
                }
            }
            //--------
            for (int from = 0; from < n; ++from) {
                stObjects[from]->setCountOut(0);
                for (int to = 0; to < n; ++to) {
                    int c = movTable[cur_month][from][to]->getCount();
                    stObjects[from]->incFromCount(c);
                }
            }
        }
        //////////
        for (int month = 0; month < cur_month; ++month) {
            for (int from = 0; from < n; ++from) {
                for (int to = 0; to < n; ++to) {
                    int c = movTable[month][from][to]->getCount();
                    if (c){
                        stObjects[from]->decToCount(c);
                        stObjects[to]->incToCount(c);
                    }
                }
            }
        }
    }
    for (int i = 0; i < n; ++i) {
        stObjects[i]->updateVisibility();
    }

    // apply filters
    filter(dir);

}

void MainWindow::filter(int dir)
{
    const int n = ids.size();

    if (!filterDialog->getCount()) {

        // dirrection
        QVector<bool> dirFrom = filterDialog->getDirFrom();
        QVector<bool> dirTo = filterDialog->getDirTo();
        for (int i = 0; i < n; ++i) {
            stObjects[i]->setFiltered(dirTo[i] || dirFrom[i]);
        }
        for (int from = 0; from < n; ++from) {
            for (int to = 0; to < n; ++to) {
                mvObjects[from][to]->setFiltered(dirFrom[from] && dirTo[to]);
            }
        }

        // month
        int monthFrom = filterDialog->getMonthFrom();
        int monthTo = filterDialog->getMonthTo();
        if (dir > 0) {
            if (cur_month < monthFrom) cur_month = monthFrom;
            else if (cur_month > monthTo) on_Pause_clicked();
        } else {
            if (cur_month > monthTo) cur_month = monthTo;
            else if (cur_month < monthFrom) on_Pause_clicked();
        }
    } else {

    }
}

void MainWindow::animation()
{
    //    qDebug() << "total: " << scene->items().size();
    calculateCurentMotions(anState == BACKWARD ? -1 : 1);
    int n = ids.size();

    timer->stop();
    if (anState == PAUSE) {
        // it's only static objects will be showed
        qDebug() << "pause";

    } else if (anState == FORWARD) { // forward
        if (!incMonth()) {
            on_Pause_clicked();
            return;
        }
        qDebug() << "forward";

        loc_timer.stop();

        const int ITER_COUNT = 10;

        // draw moving objects
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if (i != j) {
                    QPoint from = mvObjects[i][j]->getCenterFrom();
                    QPoint to = mvObjects[i][j]->getCenterTo();
                    for (int k = 0; k <= ITER_COUNT; ++k) {
                        double x = 1.0 * from.x() + (1.0 * to.x() - from.x()) / ITER_COUNT * k;
                        double y = 1.0 * from.y() +
                                (x - from.x()) / (to.x() - from.x()) * (to.y() - from.y());
                        anmvObjects[i][j]->setPosAt(1.0 * k / ITER_COUNT, QPointF(x, y));
                    }
                }
            }
        }
        // draw static objects
        for (int i = 0; i < n; ++i) {
            for (int k = 0; k <= ITER_COUNT; ++k) {
                anstObjects[i]->setPosAt(1.0 * k / ITER_COUNT, QPoint(0, qrand() % 2));
            }
        }
        loc_timer.start();

        timer->start(animationSpeed);

    } else if (anState == BACKWARD) { // backward

        if (!decMonth()) {
            on_Pause_clicked();
            return;
        }
        qDebug() << "back";

        loc_timer.stop();

        const int ITER_COUNT = 10;

        // draw moving objects
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if (i != j) {
                    QPoint from = mvObjects[i][j]->getCenterTo();
                    QPoint to = mvObjects[i][j]->getCenterFrom();
                    for (int k = 0; k <= ITER_COUNT; ++k) {
                        double x = 1.0 * from.x() + (1.0 * to.x() - from.x()) / ITER_COUNT * k;
                        double y = 1.0 * from.y() +
                                (x - from.x()) / (to.x() - from.x()) * (to.y() - from.y());
                        anmvObjects[i][j]->setPosAt(1.0 * k / ITER_COUNT, QPointF(x, y));
                    }
                }
            }
        }
        // draw static objects
        for (int i = 0; i < n; ++i) {
            for (int k = 0; k <= ITER_COUNT; ++k) {
                anstObjects[i]->setPosAt(1.0 * k / ITER_COUNT, QPoint(0, qrand() % 2));
            }
        }
        loc_timer.start();

        timer->start(animationSpeed);

    }
    scene->update();
}

bool MainWindow::incMonth()
{
    if (cur_month == time_max) return false;
    ++cur_month;
    ui->timeSlider->setValue(cur_month);
    return true;
}

bool MainWindow::decMonth()
{
    if (cur_month == 0) return false;
    --cur_month;
    ui->timeSlider->setValue(cur_month);
    return true;
}

void MainWindow::setCurMonth(int m)
{
    if (m < 0 || m > time_max) return;
    cur_month = m;
    //ui->timeSlider->setValue(cur_month);
    calculateCurentMotions(anState == BACKWARD ? -1 : 1);
    scene->update();
}

void MainWindow::parseRegions(QString fileName)
{
    if (!regions) delete regions;
    regions = new QMap<int, QString>;

    QFile inFile(fileName);
    if (!inFile.open(QIODevice::ReadOnly)) {
        QMessageBox::critical(0, "Error", "File " + fileName + " not found.");
        exit(1);
    }

    QTextStream in(&inFile);

    long long id;
    QString name;
    QStringList line;
    ids.clear();
    int c = 0;
    while (in.status() != QTextStream::ReadPastEnd) {
        QString l = in.readLine();
        if (l.trimmed().at(0) == '#') continue;
        line = l.split(";");
        bool ok;
        if (line.count() < 2) break;
        id =  line.at(0).toLongLong(&ok, 10);
        name = line.at(1).trimmed();

        if (ok && id && !name.isEmpty()) {
            regions->insert(id, name);
            ids.push_back(id);
            //            ids[id2myid(id)] = id;
            //            qDebug() << id << "\t" << name << "\t my_id = ";
        }
        ++c;
    }
    qDebug() << "Loaded : " << c <<"region records.";
    inFile.close();
    QString t("Loaded : ");
    t += QString::number(c);
    t += " regions.";
    if (c) {
        //!QMessageBox::information(0, "Info", t);
        ui->loadMotions->setEnabled(true);
    }
    else QMessageBox::critical(0, "Info", t);
}

void MainWindow::parseMotions(QString fileName)
{
    QFile inFile(fileName);
    if (!inFile.open(QIODevice::ReadOnly)) {
        QMessageBox::critical(0, "Error", "File " + fileName + " not found.");
        exit(1);
    }
    QTextStream in(&inFile);

    if (!motions) delete motions;
    motions = new QVector<Motion>;
    time_max = -1;

    QStringList line;
    long long id = 0;
    int from, to, month;
    int c = 0;
    int line_count = 0;
    while (in.status() != QTextStream::ReadPastEnd) {
        QString l = in.readLine();
        ++line_count;
        if (l.trimmed().at(0) == '#') continue;
        line = l.split(";");

        bool ok;
        if (line.count() < 3) break;
        id = line.at(0).toLongLong(&ok, 10);
        if (!ok) break;
        from = line.at(1).toInt(&ok, 10);
        if (!ok) break;
        to =  line.at(2).toInt(&ok, 10);
        if (!ok) break;
        month =  line.at(3).toInt(&ok, 10);

        if (ok && from && to && month && id) {
            Motion *m = new Motion;
            m->from = id2myid(from);
            m->to = id2myid(to);
            if (m->from == -1) {
                QString s("Bad import file '"+ fileName + "' in line:" + QString::number(line_count) + " - " + QString::number(from));
                QMessageBox::critical(0, "Error", s);
                exit(0);
            }
            if (m->to == -1) {
                QString s("Bad import file '"+ fileName + "' in line:" + QString::number(line_count) + " - " + QString::number(to));
                QMessageBox::critical(0, "Error", s);
                exit(0);
            }
            m->month = month;
            m->id = id;
            motions->append(*m);
            if (month > time_max) time_max = month;
            if (id > max_id) max_id = id;
            //qDebug() << "[" << id << "]\t[" << m->from << "]\t[" << m->to << "]\t[" << m->month<<"]";

            // for count filter
            if (!obj_ids.contains(id))
                obj_ids.push_back(id);
        }
        ++c;
    }
    mvCount.resize(obj_ids.size() + 1);

    qDebug() << "Loaded: " << c <<" motions.";
    inFile.close();

    QString t("Loaded : ");
    t += QString::number(c);
    t += " motions.";
    if (c) //!QMessageBox::information(0, "Info", t);
    {
        allocateMemoryForMotions();
        calculateMotions();
        initObjects();
    }
    else QMessageBox::critical(0, "Info", t);
}

void MainWindow::parseCenters(QString fileName)
{
    QFile inFile(fileName);
    if (!inFile.open(QIODevice::ReadOnly)) {
        QMessageBox::critical(0, "Error", "File " + fileName + " not found.");
        exit(1);
    }
    QTextStream in(&inFile);

    //centers = new QMap <int, QPoint>;
    m_centers.resize(ids.size());
    stObjects.resize(ids.size());
    anstObjects.resize(ids.size());
    for (int i = 0; i < ids.size(); ++i) {
        stObjects[i] = new Object();
        stObjects[i]->setRegionId(i);
    }

    QStringList line;
    int x, y;
    long long id = 0;
    int c = 0;
    while (in.status() != QTextStream::ReadPastEnd) {
        id = 0;
        QString l = in.readLine();
        if (l.trimmed().at(0) == '#') continue;
        line = l.split(";");

        bool ok;
        if (line.count() < 2) break;
        id = line.at(0).toLongLong(&ok, 10);
        if (!ok) break;
        x = line.at(1).toInt(&ok, 10);
        if (!ok) break;
        y =  line.at(2).toInt(&ok, 10);

        if (ok && x && y && id) {
            QPoint p(x, y);
            m_centers[id2myid(id)] = p;
            stObjects[id2myid(id)]->setCenter(p);
            //            qDebug() << "[" << id << "]\t[" << x << "]\t[" << y <<"]";
        }
        ++c;
    }
    qDebug() << "Loaded : " << c <<"centers.";
    inFile.close();

    QString t("Loaded : ");
    t += QString::number(c);
    t += " records.";
    //    QMessageBox::information(0, "Debug Info", t);
    if (!c) QMessageBox::critical(0, "Debug Info", "File 'data/centers.txt' not found");
}

void MainWindow::on_loadRegions_triggered()
{
    QString new_name = QFileDialog::getOpenFileName(this, tr("Load regions"), ".", "*.txt");

    if (!new_name.isEmpty()){
        reg_name = new_name;
        parseRegions(reg_name);
    }
}

void MainWindow::on_loadMotions_triggered()
{
    QString new_name = QFileDialog::getOpenFileName(this, tr("Load motions"), ".", "*.txt");

    if (!new_name.isEmpty()){
        mov_name = new_name;
        parseMotions(mov_name);
    }
}

void MainWindow::on_Back_clicked()
{
    if (anState == BACKWARD) return;
    anState = BACKWARD;

    ui->Back->setIcon(QIcon(":images/buttons/back/normal"));
    ui->Pause->setIcon(QIcon(":images/buttons/pause/pressed"));
    ui->Forward->setIcon(QIcon(":images/buttons/forward/pressed"));

    timer->start(animationSpeed);
}

void MainWindow::on_Pause_clicked()
{
    if (anState == PAUSE) return;
    anState = PAUSE;

    ui->Back->setIcon(QIcon(":images/buttons/back/pressed"));
    ui->Pause->setIcon(QIcon(":images/buttons/pause/normal"));
    ui->Forward->setIcon(QIcon(":images/buttons/forward/pressed"));

    timer->stop();
    //    loc_timer.stop();
}

void MainWindow::on_Forward_clicked()
{
    if (anState == FORWARD) return;
    anState = FORWARD;

    ui->Back->setIcon(QIcon(":images/buttons/back/pressed"));
    ui->Pause->setIcon(QIcon(":images/buttons/pause/pressed"));
    ui->Forward->setIcon(QIcon(":images/buttons/forward/normal"));

    timer->start(animationSpeed);
}

void MainWindow::on_timeout()
{
    if (loc_timer.state() != QTimeLine::Running) {
        animation();
    }
}

void MainWindow::changeAnimationSpeed(int s)
{
    animationSpeed = s;
    qDebug() << "new speed = " << s << "\n";
    loc_timer.setDuration(animationSpeed * 10);
    timer->setInterval(animationSpeed);
    update();
}

inline int MainWindow::id2myid(long long id)
{
    return ids.indexOf(id);
}

inline long long MainWindow::myid2id(int myid)
{
    return ids.at(myid);
}

inline int MainWindow::objids2myid(long long id)
{
    return obj_ids.indexOf(id);
}

inline long long MainWindow::myids2objid(int myid)
{
    return obj_ids.at(myid);
}

void MainWindow::allocateMemoryForMotions()
{
    if (ids.size() == 0) {
        return;
    }

    int n = ids.size();
    ids_size = ids.size();

    QProgressDialog *pr_dialog = new QProgressDialog("Progress reading motions. Please wait...", "cancel", 0, time_max, this);
    pr_dialog->setWindowModality(Qt::WindowModal);
    pr_dialog->show();
    movTable.resize(time_max);
    for (int t = 0; t < time_max; ++t) { // month
        // allocate square matrix n x n for each month
        movTable[t] = new MovingObjects **[n];
        for (int i = 0; i < n; ++i) { // from
            movTable[t][i] = new MovingObjects *[n];
            for (int j = 0; j < n; ++j) { // to
                MovingObjects* obj = new MovingObjects();
                obj->setFrom(i);
                obj->setTo(j);
                movTable[t][i][j] = obj;
            }
        }
        pr_dialog->setValue(t);
        if (pr_dialog->wasCanceled()) return;
        pr_dialog->update();
    }
    pr_dialog->setValue(time_max);

    scene->clear();

    // allocate memory for objects
    mvObjects.resize(n);
    anmvObjects.resize(n);
    for (int i = 0; i < n; ++i) {
        anmvObjects[i].resize(n);
        mvObjects[i].resize(n);
        for  (int j = 0; j < n; ++j) {
            mvObjects[i][j] = new MovingObjects;
            mvObjects[i][j]->setFrom(i);
            mvObjects[i][j]->setTo(j);
        }
    }
}

void MainWindow::calculateMotions()
{
    // init matrix
    const int n = ids.size();
    for (int m = 0; m < time_max; ++m)
        for (int from = 0; from < n; ++from)
            for (int to = 0; to < n; ++to)
                movTable[m][from][to]->init();

    // fill content of matrix
    for (int i = 0; i < motions->count(); ++i) {
        Motion m = motions->at(i);
        movTable[m.month - 1][m.from][m.to]->incCount();
        movTable[m.month - 1][m.from][m.to]->setCenterFrom(m_centers.at(m.from));
        movTable[m.month - 1][m.from][m.to]->setCenterTo(m_centers.at(m.to));
        movTable[m.month - 1][m.from][m.to]->setFiltered(false);
    }

}

void MainWindow::initObjects()
{
    const int n = ids.size();
    // dynamic objects
    for (int from = 0; from < n; ++from) {
        for (int to = 0; to < n; ++to) {
            if (from != to) {
                MovingObjects *obj = new MovingObjects();
                obj->setFrom(from);
                obj->setTo(to);
                obj->setCenterFrom(m_centers.at(from));
                obj->setCenterTo(m_centers.at(to));
                mvObjects[from][to] = obj;
                obj->setCount(0);

                QGraphicsItemAnimation *animationObject = new QGraphicsItemAnimation;
                animationObject->setItem(obj);
                animationObject->setTimeLine(&loc_timer);

                anmvObjects[from][to] = animationObject;

                scene->addItem(obj);
            }
        }
    }

    // static objects
    for (int i = 0; i < n; ++i) {
        QGraphicsItemAnimation *animationObject = new QGraphicsItemAnimation;
        animationObject->setItem(stObjects.at(i));
        animationObject->setTimeLine(&loc_timer);

        anstObjects[i] = animationObject;

        scene->addItem(stObjects.at(i));
    }

    // init static objects
    for (int i = 0; i < n; ++i) {
        stObjects[i]->init();
    }

    qDebug() << "Object init successful.";
}

void MainWindow::on_Filter_clicked()
{
    on_Pause_clicked();
    filterDialog->show();
    setCurMonth(0);
}

void MainWindow::on_properties_activated()
{
    on_Pause_clicked();
    optionsDialog->show();
}

void MainWindow::keyPressEvent(QKeyEvent *e)
{
    QString key = e->text();
    if (key == "�" || key == "�") {
        on_Back_clicked();
        return;
    } else if (key == "x" || key == "X") {
        on_Pause_clicked();
        return;
    } else if (key == "�" || key == "�") {
        on_Forward_clicked();
        return;
    } else if (key == "�" || key == "�") {
        on_Filter_clicked();
        return;
    } else if (key == "�" || key == "�") {
        exit(0);
    }


    switch (e->key()){
    case Qt::Key_Z:
        on_Back_clicked();
        break;

    case Qt::Key_X:
        on_Pause_clicked();
        break;

    case Qt::Key_C:
        on_Forward_clicked();
        break;

    case Qt::Key_Equal:
    case Qt::Key_Plus:
        optionsDialog->getSpeedSpinBox()->setValue(optionsDialog->getSpeedSpinBox()->value() - 100);
        break;

    case Qt::Key_Underscore:
    case Qt::Key_Minus:
        optionsDialog->getSpeedSpinBox()->setValue(optionsDialog->getSpeedSpinBox()->value() + 100);
        break;

    case Qt::Key_F:
        on_Filter_clicked();
        break;

    case Qt::Key_Q:
        exit(0);
    }
}

void MainWindow::on_hotkeys_triggered()
{
    QMessageBox::information(0, tr("Hot keys"),
                             tr("<b>z</b> - animation backward<hr>"
                                "<b>x</b> - pause<hr>"
                                "<b>c</b> - animation forward<hr>"
                                "<b>q</b> - quit<hr>"
                                "<b>f</b> - filter dialog<hr>"
                                "<b>+</b> - increase animation speed(1 sec)<hr>"
                                "<b>-</b> - decrease animation speed(1 sec)")

                             );
}
