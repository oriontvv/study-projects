#include "movingobjects.h"

#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QColor>

MovingObjects::MovingObjects()
{
    from = to = 0;
    init();
}

void MovingObjects::init()
{
    count = 0;
    visibled = false;
    filtered = true;
}

QRectF MovingObjects::boundingRect() const
{
    qreal adjust = 0.5;
    return QRectF(-18 - adjust, -22 - adjust, 36 + adjust, 60 + adjust);
}

void MovingObjects::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if (!visibled || !filtered) return;

    painter->setBrush(QBrush(QColor(20, 200, 20)));
//    painter->setBrush(QBrush(Qt::blue));

    int size = 15;
    int shift = -7;
    painter->drawEllipse(shift, shift, size, size);

    painter->setBrush(Qt::black);
    painter->setFont(QFont("Tahoma", 7, 10));
    painter->drawText(shift, shift, size, size, Qt::AlignCenter, QString::number(count));
}

void MovingObjects::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {

    } else if (event->button() == Qt::RightButton) {
        update();
    }
}

void MovingObjects::decCount(int c)
{
    count -= c;
    visibled = (count > 0);
}

void MovingObjects::incCount(int c)
{
    count += c;
    visibled = (count > 0);
}
