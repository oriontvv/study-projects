#ifndef OBJECT_H
#define OBJECT_H

#include <QGraphicsItem>
#include <QPoint>

class Object : public QGraphicsItem
{
public:
    Object();

    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
//    void mousePressEvent(QGraphicsSceneMouseEvent *event);

    int getCountIn() {return countIn;}
    int getCountOut() {return countOut;}
    int getRegionId() {return regionId;}
    int getX() {return center.x();}
    int getY() {return center.y();}
    QPoint getCenter() {return center;}
    QString getToolTip() {return toolTipString;}
    bool isVisibledMine() {return visibled;}
    bool isFilterled() {return filtered;}

    void setCountIn(int c) {countIn = c;}
    void setCountOut(int c) {countOut = c;}
    void setRegionId(int id) {regionId = id;}
    void setCenter(int x, int y) {center.setX(x), center.setY(y);}
    void setCenter(QPoint c) {center = c;}
    void setToolTip(QString str) {toolTipString = str;}
    void setVisibledMine(bool v) {visibled = v;}
    void setFiltered(bool f) {filtered = f;}
    inline void updateVisibility() {visibled = (countIn > 0 || countOut > 0);}

    void incFromCount(int c = 1);
    void incToCount(int c = 1);
    void decFromCount(int c = 1);
    void decToCount(int c = 1);

    void init();

protected:
    void advance(int step);

private:
    int countIn;
    int countOut;
    int regionId;
    QPoint center;
    QString toolTipString;
    bool visibled;
    bool filtered;

};

#endif // OBJECT_H
