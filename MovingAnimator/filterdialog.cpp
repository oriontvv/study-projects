#include "filterdialog.h"
#include "ui_filterdialog.h"

#include <QHBoxLayout>
#include <QCheckBox>
#include <QSpinBox>
#include <QDebug>

#define N 16

FilterDialog::FilterDialog(QWidget *parent) :
        QDialog(parent),
        ui(new Ui::FilterDialog)
{
    ui->setupUi(this);

    from.resize(N);
    to.resize(N);

    connect(ui->fromAll, SIGNAL(clicked(bool)), this, SLOT(fromAll(bool)));
    connect(ui->toAll, SIGNAL(clicked(bool)), this, SLOT(toAll(bool)));

    from[0] = ui->from0;
    from[1] = ui->from1;
    from[2] = ui->from2;
    from[3] = ui->from3;
    from[4] = ui->from4;
    from[5] = ui->from5;
    from[6] = ui->from6;
    from[7] = ui->from7;
    from[8] = ui->from8;
    from[9] = ui->from9;
    from[10] = ui->from10;
    from[11] = ui->from11;
    from[12] = ui->from12;
    from[13] = ui->from13;
    from[14] = ui->from14;
    from[15] = ui->from15;

    to[0] = ui->to0;
    to[1] = ui->to1;
    to[2] = ui->to2;
    to[3] = ui->to3;
    to[4] = ui->to4;
    to[5] = ui->to5;
    to[6] = ui->to6;
    to[7] = ui->to7;
    to[8] = ui->to8;
    to[9] = ui->to9;
    to[10] = ui->to10;
    to[11] = ui->to11;
    to[12] = ui->to12;
    to[13] = ui->to13;
    to[14] = ui->to14;
    to[15] = ui->to15;
}

FilterDialog::~FilterDialog()
{
    delete ui;
}

void FilterDialog::fromAll(bool checked)
{
    for (int i = 0; i < N; ++i)
        from[i]->setChecked(checked);
}

void FilterDialog::toAll(bool checked)
{
    for (int i = 0; i < N; ++i)
        to[i]->setChecked(checked);
}

QVector <bool> FilterDialog::getDirFrom()
{
    QVector <bool> res;
    res.resize(N);

    for (int i = 0; i < N; ++i)
        res[i] = from[i]->isChecked();

    return res;
}

QVector <bool> FilterDialog::getDirTo()
{
    QVector <bool> res;
    res.resize(N);

    for (int i = 0; i < N; ++i)
        res[i] = to[i]->isChecked();

    return res;
}

int FilterDialog::getMonthFrom()
{
    if (!ui->Month->isChecked())
        return 0;

    return ui->monthFromComboBox->currentIndex();
}

int FilterDialog::getMonthTo()
{
    if (!ui->Month->isChecked())
        return 11;

    return ui->monthToComboBox->currentIndex();
}

int FilterDialog::getCountFrom()
{
    if (!ui->Count->isChecked())
        return -1;
    return ui->countSpinBox->value();
}


int FilterDialog::getCountTo()
{
    if (!ui->Count->isChecked())
        return -1;

    return ui->countToSpinBox->value();
}

bool FilterDialog::getCount()
{
    return ui->Count->isChecked();
}

void FilterDialog::setCount(bool f)
{
    ui->Count->setChecked(f);
}

void FilterDialog::on_Count_clicked(bool checked)
{
    ui->Month->setChecked(!checked);
    ui->Direction->setChecked(!checked);
}

void FilterDialog::on_countToSpinBox_valueChanged(int value)
{
    ui->countSpinBox->setMaximum(value);
}

void FilterDialog::on_countSpinBox_valueChanged(int value)
{
    ui->countToSpinBox->setMinimum(value);
}
