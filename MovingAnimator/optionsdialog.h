#ifndef OPTIONSDIALOG_H
#define OPTIONSDIALOG_H

#include <QDialog>
#include <QSpinBox>

namespace Ui {
    class OptionsDialog;
}

class OptionsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit OptionsDialog(QWidget *parent = 0);
    ~OptionsDialog();
    QSpinBox *getSpeedSpinBox();

signals:
    void changedSpeed(int);

public slots:
    int getSpeed();
    void setSpeed(int speed);


private:
    Ui::OptionsDialog *ui;

};

#endif // OPTIONSDIALOG_H
