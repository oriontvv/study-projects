#include "optionsdialog.h"
#include "ui_optionsdialog.h"

#define FAST 50
#define NORMAL 100
#define SLOW 200

OptionsDialog::OptionsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OptionsDialog)
{
    ui->setupUi(this);
}

OptionsDialog::~OptionsDialog()
{
    delete ui;
}

int OptionsDialog::getSpeed()
{
   return ui->SpeedSpinBox->value();
}

void OptionsDialog::setSpeed(int speed)
{
    if (speed <= 0) return;
    ui->SpeedSpinBox->setValue(speed);
 }

QSpinBox * OptionsDialog::getSpeedSpinBox()
{
    return ui->SpeedSpinBox;
}
