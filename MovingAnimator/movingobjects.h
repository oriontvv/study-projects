#ifndef MOVINGOBJECTS_H
#define MOVINGOBJECTS_H

#include <QGraphicsItem>
#include <QPoint>
#include <QTimer>
#include <QTimerEvent>

class MovingObjects : public QGraphicsItem
{
public:
    MovingObjects();

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

    void setFrom(int f) {from = f;}
    void setTo(int t) {to = t;}
    void setCenterTo(QPoint p) {center_to = p;}
    void setCenterFrom(QPoint p) {center_from = p;}
    void setVisibled(bool v) {visibled = v;}
    void setFiltered(bool f) {filtered = f;}
    void setCount(int c) {count = c; visibled = (c > 0);}

    int getFrom() {return from;}
    int getTo() {return to;}
    bool isVisibled() {return visibled;}
    bool isFiltered() {return filtered;}
    QPoint getCenterFrom() {return center_from;}
    QPoint getCenterTo() {return center_to;}
    int getCount() {return count;}

    void incCount(int c = 1);
    void decCount(int c = 1);

    void init();

private:
    int from;
    int to;
    QPoint center_from;
    QPoint center_to;
    bool visibled;
    bool filtered;
    int count;
};

#endif // MOVINGOBJECTS_H
