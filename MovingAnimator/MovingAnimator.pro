#-------------------------------------------------
#
# Project created by QtCreator 2011-02-21T16:50:42
#
#-------------------------------------------------

QT       += core gui

TARGET = MovingAnimator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    object.cpp \
    optionsdialog.cpp \
    movingobjects.cpp \
    filterdialog.cpp

HEADERS  += mainwindow.h \
    object.h \
    optionsdialog.h \
    movingobjects.h \
    filterdialog.h

FORMS    += mainwindow.ui \
    optionsdialog.ui \
    filterdialog.ui

RESOURCES += \
    res.qrc
