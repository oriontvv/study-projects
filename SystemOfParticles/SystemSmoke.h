
/*
 * NAME: Taranov Vassiliy Vassilevich, 325
 * ASGN: N5
 */

#ifndef SYSTEMSMOKE_H
#define SYSTEMSMOKE_H

#include "SystemOfParticles.h"

class SystemSmoke : public SystemOfParticles
{
public:
    void init(int n = 1);//participles count
    void initOneParticle(int index);
    void updateOneParticle(int index);
};

#endif // SYSTEMSMOKE_H
