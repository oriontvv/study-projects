
/*
 * NAME: Taranov Vassiliy Vassilevich, 325
 * ASGN: N5
 */

#ifndef OGLSCENE_H
#define OGLSCENE_H

#include <QGLWidget>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QImage>
#include <QTimer>
#include <QSound>

#include <QDebug>

#include "Camera.h"
#include "SystemFire.h"
#include "SystemSmoke.h"
#include "SystemSnow.h"
#include "SystemBang.h"

#define WALL_LIST 7
#define FLOOR_LIST 8

enum EffectMode{
    NULL_EFFECT,
    FIRE_EFFECT,
    SNOW_EFFECT,
    SMOKE_EFFECT,
    BANG_EFFECT
};

class GLScene : public QGLWidget
{
    Q_OBJECT

private:
    int texturesCount;
    GLuint *textureID;
    QSound *background;
    void updateBackground(QString fname);


    EffectMode effectMode;

    Camera *camera;
    void rotateCameraToVisor();

    SystemFire *fire;
    SystemSmoke *smoke;
    SystemSnow *snow;
    SystemBang *bang;
    bool animatingTextures;

    QTimer *timer;

    void renderAllObjects();
    void renderWalls();
    void renderFloor();
    void renderFire();
    void renderSnow();
    void renderSmoke();
    void renderBang();
    void renderAxies();

    void creataAllLists();
    void createWallList();
    void createFloorList();

    void loadTextures();


public:
    GLScene(QWidget* pwgt = 0);
    ~GLScene();

protected:
    virtual void initializeGL();
    virtual void resizeGL(int nWidth, int nHeight);
    virtual void paintGL();
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void keyReleaseEvent(QKeyEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void moveMouseToCenter(int x, int y);

public slots:
    void updateSystem();

};

#endif // OGLSCENE_H
