
/*
 * NAME: Taranov Vassiliy Vassilevich, 325
 * ASGN: N5
 */

#include "Camera.h"
#include <cmath>

#include <qdebug>




Camera::Camera()
{
    x = 1.0;
    y = 1.0;
    z = 3.0;

    r = 2.5;

    angel1 = 100.0;
    angel2 = 210.0;

    MoveToCenter();

    speed_move = 0.04;
    sensitivity = 0.1;

    SetKeyStates(false, false, false, false, false, false);
//    qDebug() << "Init camera";
}

float Camera::DToR(float n)
{
    return 1.0 * M_PI * n / 180.0;
}

void Camera::Translate()
{
//    if (w && s) w = s = false;
//    if (a && d) a = d = false;
//    if (space && c) space = c = false;
    if (keyW && !keyS){
        x += speed_move * r * sin(DToR(angel1)) * sin(DToR(angel2));
        y += speed_move * r * cos(DToR(angel1));
        z += speed_move * r * sin(DToR(angel1)) * cos(DToR(angel2));
    }
    if (keyS && !keyW){
        x -= speed_move * r * sin(DToR(angel1)) * sin(DToR(angel2));
        y -= speed_move * r * cos(DToR(angel1));
        z -= speed_move * r * sin(DToR(angel1)) * cos(DToR(angel2));
    }
    if (keyA && !keyD){
        x += speed_move * r * sin(DToR(angel1)) * cos(DToR(angel2));
        z -= speed_move * r * sin(DToR(angel1)) * sin(DToR(angel2));
    }
    if (keyD && !keyA){
        x -= speed_move * r * sin(DToR(angel1)) * cos(DToR(angel2));
        z += speed_move * r * sin(DToR(angel1)) * sin(DToR(angel2));
    }
    if (keySpace && !keyC){
        y += speed_move;
    }
    if (keyC && !keySpace){
        y -= speed_move;
    }
    MoveToCenter();
//    qDebug() << "Move camera : x = " << x << "\t y = " <<  y << "\t z = " << z;
}

void Camera::Rotate(float x, float y)
{
    angel1 += sensitivity * y;
    angel2 += sensitivity * x;

    MoveToCenter();
//    qDebug() << "Rotate camera: anlge1 = " << angel1 << "\tangle2 = " << angel2;
}

void Camera::MoveToCenter()
{
    x1 = x + r * sin(DToR(angel1)) * sin(DToR(angel2));
    y1 = y + r * cos(DToR(angel1));
    z1 = z + r * sin(DToR(angel1)) * cos(DToR(angel2));
}

void Camera::SetKeyStates(bool w, bool s, bool a, bool d, bool space, bool c)
{
    keyW = w;
    keyS = s;
    keyA = a;
    keyD = d;
    keySpace = space;
    keyC = c;
}
