
/*
 * NAME: Taranov Vassiliy Vassilevich, 325
 * ASGN: N5
 */

#ifndef SYSTEMOFPARTICLES_H
#define SYSTEMOFPARTICLES_H

#include <stdlib.h>

#define random(x) (1.0 * (x) * rand() / RAND_MAX)
#define _random(x) (1.0 * (x) * rand() / RAND_MAX - (x) / 2.0 )

struct CVector3f
{
    float x;
    float y;
    float z;
    CVector3f(float X = 0, float Y = 0, float Z = 0){
        x = X;
        y = Y;
        z = Z;
    }
    const CVector3f operator+(const CVector3f &r)const{
        return CVector3f(x + r.x, y + r.y, z + r.z);
    }
};

struct OneParticle
{
    int life;
    CVector3f position;
    CVector3f speed;
    CVector3f acceleration;
    float size;
    OneParticle(){}
};

class SystemOfParticles
{
protected:
    int count;
    float time;
    float gravity;
    int updateSpeed;
    OneParticle *all;//all particles

public:
    SystemOfParticles();
    virtual ~SystemOfParticles();

    //set
    void setCount(int n);
    void setTime(float t);

    //get
    int getCount();
    float getTime();
    OneParticle *getAll();

    //virtual methods
    virtual void update();
    virtual void init(int amount = 1) = 0;
    virtual void initOneParticle(int index) = 0;
    virtual void updateOneParticle(int index) = 0;

    void incUpdateSpeed();
    void decUpdateSpeed();
};

#endif // SYSTEMOFPARTICLES_H
