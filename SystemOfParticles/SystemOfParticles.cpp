
/*
 * NAME: Taranov Vassiliy Vassilevich, 325
 * ASGN: N5
 */

#include "SystemOfParticles.h"


SystemOfParticles::SystemOfParticles()
{
    time = 0;
    updateSpeed = 30;
}

SystemOfParticles::~SystemOfParticles()
{
    if (all) delete all;
}

int SystemOfParticles::getCount()
{
    return  count;
}

void SystemOfParticles::setCount(int n)
{
    if(count != n){
        count = n;
        if (all) delete all;
        all = new OneParticle[count];
        for(int i = 0; i < count; ++i)
            initOneParticle(i);
    }
}

OneParticle *SystemOfParticles::getAll()
{
    return all;
}

void SystemOfParticles::setTime(float t)
{
    time = t;
}

float SystemOfParticles::getTime()
{
    return time;
}

void SystemOfParticles::incUpdateSpeed()
{
    updateSpeed += 10;
    if (updateSpeed > 150) updateSpeed = 150;
}

void SystemOfParticles::decUpdateSpeed()
{
    updateSpeed -= 10;
    if (updateSpeed < 10) updateSpeed = 10;
}

void SystemOfParticles::update()
{
    time++;

    for(int i = 0; i < count; ++i){
        updateOneParticle(i);
    }
}
