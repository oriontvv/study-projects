
/*
 * NAME: Taranov Vassiliy Vassilevich, 325
 * ASGN: N5
 */

#include <QApplication>
#include "GLScene.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    GLScene scene;
    scene.resize(640, 480);
    scene.show();

    return app.exec();
}
