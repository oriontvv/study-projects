
/*
 * NAME: Taranov Vassiliy Vassilevich, 325
 * ASGN: N5
 */

#include "SystemSnow.h"

#include <cmath>

void SystemSnow::init(int n)
{
    time = 0;
    count = n;
    gravity = -0.0005;
    all = new OneParticle[count];


    for(int i = 0; i < count; ++i)
        initOneParticle(i);

    //skip firts updates...
    for(int i = 0; i < count; ++i)
        skipFirstUpdates(i);

    for(int i = 0; i < 100; ++i)
            update();
}

void SystemSnow::skipFirstUpdates(int i)
{
    //position vector
    all[i].position.x = _random(6);
    all[i].position.y = _random(3);
    all[i].position.z = _random(6);

    all[i].speed.x = _random(0.004);// _random(0.04);
    all[i].speed.y = 0;
    all[i].speed.z = _random(0.004);//_random(0.04);
}

void SystemSnow::initOneParticle(int i)
{
    //position vector
    all[i].position.x = _random(6);
    all[i].position.y = _random(0.5) + 1.5;
    all[i].position.z = _random(6);

    all[i].speed.x = _random(0.004);// _random(0.04);
    all[i].speed.y = 0;
    all[i].speed.z = _random(0.004);//_random(0.04);

}

void SystemSnow::updateOneParticle(int i)
{
    all[i].size = random(0.02) + 0.02;

    // update the speed and position values
    all[i].speed.x += _random(0.002);
    all[i].speed.y += gravity;
    all[i].speed.z += _random(0.002);

    all[i].position.x += all[i].speed.x;
    all[i].position.y += random(all[i].speed.y/2) + all[i].speed.y/2;
    all[i].position.z += all[i].speed.z;

    if (all[i].position.y < -1.5 || 
        fabs(all[i].position.x) > 3 ||
        fabs(all[i].position.z) >3 ){
      initOneParticle(i);
    }

}

