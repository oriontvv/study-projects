
/*
 * NAME: Taranov Vassiliy Vassilevich, 325
 * ASGN: N5
 */

#include "GLScene.h"
#include <QImage>

GLScene::GLScene(QWidget* pwgt) : QGLWidget(pwgt)
{
    camera = new Camera;
    moveMouseToCenter(0, 0);
    setMouseTracking(true);
    setCursor(Qt::BlankCursor);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateSystem()));

    animatingTextures = true;
    fire = new SystemFire;
    smoke = new SystemSmoke;
    snow = new SystemSnow;
    bang = new SystemBang;

    background = new QSound("sounds\\fire.wav", this);
    background->setLoops(-1);//forever

    effectMode = FIRE_EFFECT;
    background->play();
}

GLScene::~GLScene()
{
    if (camera) delete camera;
    if (timer) delete timer;
    if (fire) delete fire;
    if (snow) delete snow;
    if (smoke) delete smoke;
    if (bang) delete bang;
    if (background) delete background;
}

void GLScene::initializeGL()
{
    qglClearColor(Qt::gray);
    glDisable(GL_BLEND);

    glClearDepth(1.0);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);

    glEnable(GL_NORMALIZE);
    glEnable(GL_AUTO_NORMAL);

    loadTextures();

    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT_FACE);

//    glEnable(GL_FOG);
//    //setup mode
//    glFogi(GL_FOG_MODE, GL_LINEAR);
//    //setup color
////    glFogfv(GL_FOG_COLOR, fogColor);
//    //power of fog;)
//    glFogf(GL_FOG_DENSITY, 0.2);
//    glFogf(GL_FOG_HINT, GL_NICEST);
//    glFogf(GL_FOG_START, 3);
//    glFogf(GL_FOG_END, 5);

    creataAllLists();

    //pass participles count
    fire->init(3000);
    smoke->init(500);
    snow->init(1000);
    bang->init(500);


    timer->start(30);
//    background->play();
}

void GLScene::resizeGL(int nWidth, int nHeight)
{
    glViewport(0, 0, nWidth, nHeight);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0 ,(GLdouble) ( width() / height()), 0.1, 50.0);
}

void GLScene::paintGL()
{

    qglClearColor(Qt::black);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    camera->Translate();

    gluLookAt(camera->getX(), camera->getY(), camera->getZ(),
              camera->getX1(), camera->getY1(), camera->getZ1(),
              0.0, 1.0, 0.0);

    renderAllObjects();
}

void GLScene::loadTextures()
{
    glEnable(GL_TEXTURE_2D);
    texturesCount = 1 + // wall
                    1 + // floor
                    24 + // fire
                    1 + // smoke
                    1  // tube for smoke
                    ;
    textureID = new GLuint[texturesCount];

    glGenTextures(texturesCount, textureID);

    //wall
    QImage *wall = new QImage("textures\\wall.jpg", "jpg");
    *wall = QGLWidget::convertToGLFormat(*wall);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glBindTexture(GL_TEXTURE_2D, textureID[0]);
    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB,
                      wall->width(), wall->height(),
                      GL_RGBA, GL_UNSIGNED_BYTE, wall->bits());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    delete wall;

    //floor
    QImage *floor = new QImage("textures\\floor.jpg", "jpg");
    *floor = QGLWidget::convertToGLFormat(*floor);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glBindTexture(GL_TEXTURE_2D, textureID[1]);
    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8,
                      floor->width(), floor->height(),
                      GL_RGBA, GL_UNSIGNED_BYTE, floor->bits());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    delete floor;

    //fire - animational texture - just load 24 png textures
    for(int i = 0; i < 24; ++i){
        QString tmp("textures\\fire\\" + QString::number(i, 10) + ".PNG");
        QImage fire(tmp, "PNG");
        fire = QGLWidget::convertToGLFormat(fire);

        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glBindTexture(GL_TEXTURE_2D, textureID[2 + i]);

        gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8,
                          fire.width(), fire.height(),
                          GL_RGBA, GL_UNSIGNED_BYTE, fire.bits());

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    }

    //smoke - simple texture
    QImage smoke("textures\\fog.png", "png");
    smoke = QGLWidget::convertToGLFormat(smoke);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glBindTexture(GL_TEXTURE_2D, textureID[26]);

    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8,
                      smoke.width(), smoke.height(),
                      GL_RGBA, GL_UNSIGNED_BYTE, smoke.bits());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    //tube for smoke - simple texture
    QImage tube("textures\\tube.png", "png");
    tube = QGLWidget::convertToGLFormat(tube);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glBindTexture(GL_TEXTURE_2D, textureID[27]);

    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8,
                      tube.width(), tube.height(),
                      GL_RGBA, GL_UNSIGNED_BYTE, tube.bits());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);


    glDisable(GL_TEXTURE_2D);
}

void GLScene::renderAllObjects()
{
    renderWalls();
    renderFloor();

    //render effect
    switch(effectMode){
        case NULL_EFFECT:
            //do nothing
            break;
        case FIRE_EFFECT:
            renderFire();
            break;
        case SNOW_EFFECT:
            renderSnow();
            break;
        case SMOKE_EFFECT:
            renderSmoke();
            break;
        case BANG_EFFECT:
            renderBang();
            break;
        default:
            effectMode = NULL_EFFECT;
            break;
    }
}

void GLScene::renderAxies()
{
//    glDisable(GL_BLEND);
//    glDisable(GL_CULL_FACE);
    glDisable(GL_TEXTURE_2D);

    glPushMatrix();
    glScalef(0.3, 0.3, 0.3);

    glBegin(GL_TRIANGLES);
        glColor3f(0, 0, 0);
        glVertex3f(0, 0, 0);
        glColor3f(1, 0, 0);
        glVertex3f(1, 0, 0);
        glColor3f(0, 1, 0);
        glVertex3f(0, 1, 0);
    glEnd();

    glBegin(GL_TRIANGLES);
        glColor3f(0, 0, 0);
        glVertex3f(0, 0, 0);
        glColor3f(1, 0, 0);
        glVertex3f(1, 0, 0);
        glColor3f(0, 0, 1);
        glVertex3f(0, 0, 1);
    glEnd();

    glBegin(GL_TRIANGLES);
        glColor3f(0, 0, 0);
        glVertex3f(0, 0, 0);
        glColor3f(0, 1, 0);
        glVertex3f(0, 1, 0);
        glColor3f(0, 0, 1);
        glVertex3f(0, 0, 1);
    glEnd();


    glPopMatrix();

//    glEnable(GL_BLEND);
    glEnable(GL_TEXTURE_2D);
//    glDisable(GL_LIGHTING);
//    glEnable(GL_CULL_FACE);
}

void GLScene::renderWalls()
{
    glPushMatrix();
    glCallList(WALL_LIST);
    glPopMatrix();
}

void GLScene::renderFloor()
{
    glPushMatrix();
    glCallList(FLOOR_LIST);
    glPopMatrix();
}

void GLScene::renderFire()
{
    glPushMatrix();
//    glColor4f(0, 0, 0, 0);
    glDisable(GL_CULL_FACE);
    glDisable(GL_LIGHTING);
//    glEnable(GL_DEPTH_TEST);
    glEnable(GL_ALPHA_TEST);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
//    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//    glDepthMask(GL_FALSE);

    for(int i = 0; i < fire->getCount(); ++i){
        glPushMatrix();
        OneParticle *cur = &(fire->getAll()[i]);
        glTranslatef(cur->position.x, cur->position.y, cur->position.z);
        rotateCameraToVisor();
        glRotatef(-90, 1, 0, 0);
        if (animatingTextures) glBindTexture(GL_TEXTURE_2D, textureID[2 + 23-cur->life]);
        else glBindTexture(GL_TEXTURE_2D, textureID[2 + 10]);
//        if(random(2) > 1)
//            if(fire->getAll()[i].position.y > -0.6 + random(0.1))
//            glBindTexture(GL_TEXTURE_2D, textureID[26]);

//        glBindTexture(GL_TEXTURE_2D, textureID[4]);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

        glNormal3f(0.0, 0.0, 1.0);
        glBegin(GL_QUADS);
            glTexCoord2f(1, 1); glVertex3f(  cur->size, 0,   cur->size);
            glTexCoord2f(1, 0); glVertex3f(  cur->size, 0, - cur->size);
            glTexCoord2f(0, 0); glVertex3f(- cur->size, 0, - cur->size);
            glTexCoord2f(0, 1); glVertex3f(- cur->size, 0,   cur->size);
        glEnd();

        glPopMatrix();
    }

    glEnable(GL_CULL_FACE);
//    glEnable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_ALPHA_TEST);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_BLEND);
    glPopMatrix();
}

void GLScene::renderSnow()
{
    glDisable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);

    glEnable(GL_ALPHA_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);

    glDisable(GL_CULL_FACE);

    glEnable(GL_BLEND);
//    glBlendFunc(GL_SRC_ALPHA, GL_ONE);

    glEnable(GL_DEPTH_TEST);

    glPushMatrix();
    glColor4f(0, 0, 0, 1);
    glDepthMask(GL_FALSE);

    for(int i = 0; i < snow->getCount(); ++i){
        glPushMatrix();
        OneParticle *cur = &(snow->getAll()[i]);
        glTranslatef(cur->position.x, cur->position.y, cur->position.z);
        rotateCameraToVisor();
        glRotatef(-90, 1, 0, 0);
        glBindTexture(GL_TEXTURE_2D, textureID[26]);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_ADD);

        glNormal3f(0.0, 0.0, 1.0);
        glBegin(GL_QUADS);
            glTexCoord2f(1, 1); glVertex3f(  cur->size, 0,   cur->size);
            glTexCoord2f(1, 0); glVertex3f(  cur->size, 0, - cur->size);
            glTexCoord2f(0, 0); glVertex3f(- cur->size, 0, - cur->size);
            glTexCoord2f(0, 1); glVertex3f(- cur->size, 0,   cur->size);
        glEnd();
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
        glPopMatrix();
    }

    glEnable(GL_CULL_FACE);
    glEnable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_ALPHA_TEST);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_BLEND);
    glPopMatrix();
}

void GLScene::renderSmoke()
{
    glDisable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    glColor4f(0, 0, 0, 1);

    // cylinder
    glPushMatrix();
    glTranslatef(0, -0.8, 0);
    glRotatef(90, 1, 0, 0);

    glBindTexture(GL_TEXTURE_2D, textureID[27]);
    GLUquadricObj* pQuadric = gluNewQuadric();
    gluQuadricTexture(pQuadric, GL_TRUE);
    gluCylinder(pQuadric, 0.06 , 0.1, 0.7, 12, 12);
    glPopMatrix();
    //end cylinder
    glDisable(GL_CULL_FACE);

    glEnable(GL_ALPHA_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);



    glEnable(GL_BLEND);
//    glBlendFunc(GL_SRC_ALPHA, GL_ONE);


    glPushMatrix();
    glDepthMask(GL_FALSE);

    for(int i = 0; i < smoke->getCount(); ++i){
        glPushMatrix();
        OneParticle *cur = &(smoke->getAll()[i]);
        glTranslatef(cur->position.x, cur->position.y, cur->position.z);
        rotateCameraToVisor();
        glRotatef(-90, 1, 0, 0);
        glBindTexture(GL_TEXTURE_2D, textureID[26]);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_ADD);

        glNormal3f(0.0, 0.0, 1.0);
        glBegin(GL_QUADS);
            glTexCoord2f(1, 1); glVertex3f(  cur->size, 0,   cur->size);
            glTexCoord2f(1, 0); glVertex3f(  cur->size, 0, - cur->size);
            glTexCoord2f(0, 0); glVertex3f(- cur->size, 0, - cur->size);
            glTexCoord2f(0, 1); glVertex3f(- cur->size, 0,   cur->size);
        glEnd();
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
        glPopMatrix();
    }

    glEnable(GL_CULL_FACE);
    glEnable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_ALPHA_TEST);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_BLEND);
    glPopMatrix();
}

void GLScene::renderBang()
{
    glPushMatrix();
//    glColor4f(0, 0, 0, 0);
    glDisable(GL_CULL_FACE);
    glDisable(GL_LIGHTING);
//    glEnable(GL_DEPTH_TEST);
    glEnable(GL_ALPHA_TEST);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
//    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//    glDepthMask(GL_FALSE);

    for(int i = 0; i < bang->getCount(); ++i){
        glPushMatrix();
        OneParticle *cur = &(bang->getAll()[i]);
        glTranslatef(cur->position.x, cur->position.y, cur->position.z);
        rotateCameraToVisor();
        glRotatef(-90, 1, 0, 0);
//        if (animatingTextures) glBindTexture(GL_TEXTURE_2D, textureID[2 + 23-cur->life]);
//        else glBindTexture(GL_TEXTURE_2D, textureID[2 + 10]);
         glBindTexture(GL_TEXTURE_2D, textureID[2 + 10]);

        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

        glNormal3f(0.0, 0.0, 1.0);
        glBegin(GL_QUADS);
            glTexCoord2f(1, 1); glVertex3f(  cur->size, 0,   cur->size);
            glTexCoord2f(1, 0); glVertex3f(  cur->size, 0, - cur->size);
            glTexCoord2f(0, 0); glVertex3f(- cur->size, 0, - cur->size);
            glTexCoord2f(0, 1); glVertex3f(- cur->size, 0,   cur->size);
        glEnd();

        glPopMatrix();
    }

    glEnable(GL_CULL_FACE);
//    glEnable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_ALPHA_TEST);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_BLEND);
    glPopMatrix();

}

void GLScene::creataAllLists()
{
    createWallList();
    createFloorList();
}

void GLScene::createFloorList()
{
    glNewList(FLOOR_LIST, GL_COMPILE);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureID[1]);

    glScalef(3, 3, 3);

    glNormal3d(0.0, 1.0, 0.0);
    glBegin(GL_QUADS);
        glTexCoord2f(5, 5); glVertex3f(1,  -0.5, 1);
        glTexCoord2f(5, 0); glVertex3f( 1,  -0.5, -1);
        glTexCoord2f(0, 0); glVertex3f( -1,  -0.5,  -1);
        glTexCoord2f(0, 5); glVertex3f(-1,  -0.5,  1);
    glEnd();

    glDisable(GL_TEXTURE_2D);
    glEndList();
}

void GLScene::createWallList()
{
    glNewList(WALL_LIST, GL_COMPILE);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureID[0]); // "wall.jpg"
//    qDebug() << "error = " << glGetError();

    glScalef(3, 3, 3);

    //up
//    glNormal3d(0.0, -1.0, 0.0);
//    glBegin(GL_QUADS);
////        qglColor(Qt::white);
////        glColor4f(0.0, 0.0, 0.0, 0.0);
//        glTexCoord2f(2, 2); glVertex3f(-1,  0.5, -1);
//        glTexCoord2f(2, 0); glVertex3f( 1,  0.5, -1);
//        glTexCoord2f(0, 0); glVertex3f( 1,  0.5,  1);
//        glTexCoord2f(0, 2); glVertex3f(-1,  0.5,  1);
//    glEnd();

     //walls
    glBegin(GL_QUADS);
//        glColor4f(0.1, 0.2, 0.5, 0.3);
        //front
        glNormal3d(0.0, 0.0, 1.0);
        glTexCoord2f(2, 2);        glVertex3f( 1,   0.5, -1);
        glTexCoord2f(2, 0);        glVertex3f(-1,   0.5, -1);
        glTexCoord2f(0, 0);        glVertex3f(-1,  -0.5, -1);
        glTexCoord2f(0, 2);        glVertex3f( 1,  -0.5, -1);

        //left
        glNormal3d(1.0, 0.0, 0.0);
        glTexCoord2f(2, 2);        glVertex3f(-1,   0.5, -1);
        glTexCoord2f(2, 0);        glVertex3f(-1,   0.5,  1);
        glTexCoord2f(0, 0);        glVertex3f(-1,  -0.5,  1);
        glTexCoord2f(0, 2);        glVertex3f(-1,  -0.5, -1);

        //back
        glNormal3d(0.0, 0.0, -1.0);
        glTexCoord2f(2, 2);        glVertex3f(-1,   0.5,  1);
        glTexCoord2f(2, 0);        glVertex3f( 1,   0.5,  1);
        glTexCoord2f(0, 0);        glVertex3f( 1,  -0.5,  1);
        glTexCoord2f(0, 2);        glVertex3f(-1,  -0.5,  1);

        //right
        glNormal3d(-1.0, 0.0, 0.0);
        glTexCoord2f(2, 2);        glVertex3f( 1,   0.5,  1);
        glTexCoord2f(2, 0);        glVertex3f( 1,   0.5, -1);
        glTexCoord2f(0, 0);        glVertex3f( 1,  -0.5, -1);
        glTexCoord2f(0, 2);        glVertex3f( 1,  -0.5,  1);
    glEnd();

    //under floor
    glNormal3d(0.0, 1.0, 0.0);
    glBegin(GL_QUADS);
        glTexCoord2f(2, 2); glVertex3f(-1,  -0.5, -1);
        glTexCoord2f(2, 0); glVertex3f( 1,  -0.5, -1);
        glTexCoord2f(0, 0); glVertex3f( 1,  -0.5,  1);
        glTexCoord2f(0, 2); glVertex3f(-1,  -0.5,  1);
    glEnd();

    glDisable(GL_TEXTURE_2D);
    glEndList();
}

void GLScene::rotateCameraToVisor()
{
    //undo all rotates
    GLfloat _old[16];
    glGetFloatv(GL_MODELVIEW_MATRIX, _old);
    const GLfloat _new[16] = {  1.0, 0.0, 0.0, 0.0,
                                0.0, 1.0, 0.0, 0.0,
                                0.0, 0.0, 1.0, 0.0,
                                _old[12], _old[13], _old[14], _old[15]};
    glLoadMatrixf(_new);
}

void GLScene::moveMouseToCenter(int x, int y)
{
    if(x == width() / 2 && y == height() / 2) return;
    QPoint pos = mapToGlobal(QPoint(0, 0));
    SetCursorPos(width() / 2 + pos.x(), height() / 2 + pos.y());
//    updateGL();
    //redrawing();
//    updateGL();
}

void GLScene::mouseMoveEvent(QMouseEvent *event)
{
    int delta_x = event->x() - width() / 2;
    int delta_y = event->y() - height() / 2;

    camera->Rotate(-delta_x, delta_y);

    moveMouseToCenter(event->x(), event->y());
}

void GLScene::keyPressEvent(QKeyEvent *event)
{
    switch(event->key()){
        case Qt::Key_W:
            camera->setKeyW(true);
            break;
        case Qt::Key_S:
            camera->setKeyS(true);
            break;
        case Qt::Key_A:
            camera->setKeyA(true);
            break;
        case Qt::Key_D:
            camera->setKeyD(true);
            break;
        case Qt::Key_X:
            camera->setKeySpace(true);
            break;
        case Qt::Key_C:
            camera->setKeyC(true);
            break;
        case Qt::Key_T:
            animatingTextures = !animatingTextures;
            break;
        case Qt::Key_1:
            if (effectMode != FIRE_EFFECT) {
                effectMode = FIRE_EFFECT;
                updateBackground("sounds\\fire.wav");
            }
            else {
                effectMode = NULL_EFFECT;
                background->stop();
            }
            break;
        case Qt::Key_2:
            if (effectMode != SNOW_EFFECT) {
                effectMode = SNOW_EFFECT;
                updateBackground("sounds\\snow.wav");

            }
            else {
                effectMode = NULL_EFFECT;
                background->stop();
            }
            break;
        case Qt::Key_3:
            if (effectMode != SMOKE_EFFECT) {
                effectMode = SMOKE_EFFECT;
                updateBackground("sounds\\smoke.wav");
            }
            else {
                effectMode = NULL_EFFECT;
                background->stop();
            }
            break;
        case Qt::Key_4:
            if (effectMode != BANG_EFFECT) {
                effectMode = BANG_EFFECT;
                updateBackground("sounds\\bang.wav");
            }
            else {
                effectMode = NULL_EFFECT;
                background->stop();
            }
            break;
        case Qt::Key_Space:
            effectMode = (EffectMode)(((int)(effectMode) + 1) % 5);
            switch(effectMode){
                case NULL_EFFECT:
                    background->stop();
                    break;
                case FIRE_EFFECT:
                     updateBackground("sounds\\fire.wav");
                     break;
                case SNOW_EFFECT:
                     updateBackground("sounds\\snow.wav");
                     break;
                case SMOKE_EFFECT:
                     updateBackground("sounds\\smoke.wav");
                     break;
                case BANG_EFFECT:
                     updateBackground("sounds\\bang.wav");
                     break;
            }
            break;
        case Qt::Key_Greater:
            fire->incUpdateSpeed();
            break;
        case Qt::Key_Less:
            fire->decUpdateSpeed();
            break;
        case Qt::Key_Q:
            exit(0);
    }
//    redrawing();
//    updateGL();
}

void GLScene::keyReleaseEvent(QKeyEvent *event)
{
    switch(event->key()){
        case Qt::Key_W:
            camera->setKeyW(false);
            break;
        case Qt::Key_S:
            camera->setKeyS(false);
            break;
        case Qt::Key_A:
            camera->setKeyA(false);
            break;
        case Qt::Key_D:
            camera->setKeyD(false);
            break;
        case Qt::Key_X:
            camera->setKeySpace(false);
            break;
        case Qt::Key_C:
            camera->setKeyC(false);
            break;
    }
//    camera->Translate();
//    redrawing();
}

void GLScene::updateBackground(QString fname)
{
    delete background;
    background = new QSound(fname);
    background->setLoops(-1);
    background->play();

}

void GLScene::updateSystem()
{
    switch(effectMode){
        case NULL_EFFECT:
            break;
        case FIRE_EFFECT:
            fire->update();
            smoke->update();
            break;
        case SMOKE_EFFECT:
            smoke->update();
            break;
        case SNOW_EFFECT:
            snow->update();
            break;
        case BANG_EFFECT:
            bang->update();
            break;
        default:
            effectMode = NULL_EFFECT;
            break;
    }
    updateGL();
}
