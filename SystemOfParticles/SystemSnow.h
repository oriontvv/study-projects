
/*
 * NAME: Taranov Vassiliy Vassilevich, 325
 * ASGN: N5
 */

#ifndef SYSTEMSNOW_H
#define SYSTEMSNOW_H

#include "SystemOfParticles.h"

class SystemSnow : public SystemOfParticles
{
public:
    void init(int n = 1);//participles count
    void initOneParticle(int index);
    void updateOneParticle(int index);
    void skipFirstUpdates(int index);
};

#endif // SYSTEMSNOW_H
