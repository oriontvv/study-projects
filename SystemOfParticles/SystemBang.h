
/*
 * NAME: Taranov Vassiliy Vassilevich, 325
 * ASGN: N5
 */

#ifndef SYSTEMBANG_H
#define SYSTEMBANG_H

#include "SystemOfParticles.h"

class SystemBang : public SystemOfParticles
{
public:
    void init(int n = 1);//participles count
    void initOneParticle(int index);
    void updateOneParticle(int index);
};

#endif // SYSTEMBANG_H
