
/*
 * NAME: Taranov Vassiliy Vassilevich, 325
 * ASGN: N5
 */

#include "SystemSmoke.h"
#include <cmath>

void SystemSmoke::init(int n)
{
    time = 0;
    count = n;
    gravity = 0.0002;
    all = new OneParticle[count];


    for(int i = 0; i < count; ++i)
        initOneParticle(i);

    //skip firts updates...
    for(int i = 0; i < 1000; ++i)
        update();
}

void SystemSmoke::initOneParticle(int i)
{
    // generating new particle

    all[i].life = (int)(random(10) + 7);

    //position vector

    float angle = random(2 * M_PI);
    float radius = random(0.01);

    all[i].position.x = radius * sin(angle);
    all[i].position.y = random(0.9) - 0.7;
    all[i].position.z = radius * cos(angle);

    all[i].speed.x = _random(0.004);// _random(0.04);
    all[i].speed.y = 0.0;
    all[i].speed.z = _random(0.004);//_random(0.04);

    // init direction
//    all[i].acceleration.x = -0.05 * all[i].speed.x;
//    all[i].acceleration.y = gravity;
//    all[i].acceleration.z = -0.05 * all[i].speed.z;

    all[i].acceleration.x = 0.0;
    all[i].acceleration.y = gravity;
    all[i].acceleration.z = 0.0;


    all[i].size = random(0.04) + 0.04;
}

void SystemSmoke::updateOneParticle(int i)
{
//        all[i].life--;
        //detect life
        if (all[i].position.y < -0.5){
//           all[i].life = fmod(-50 * all[i].position.y - random(7) + 24, 24);
        }
        else{
//            all[i].life = fmod(-50 * all[i].position.y  + 24, 24);
        }

    //    all[i].life--;

        // update the speed and position values
        all[i].speed.x += _random(0.0002);
        all[i].speed.y += gravity;
        all[i].speed.z += _random(0.0002);

        all[i].position.x += all[i].speed.x;
        all[i].position.y += all[i].speed.y;
        all[i].position.z += all[i].speed.z;

        if (all[i].position.y > 1.5 || all[i].life == 0){
            initOneParticle(i);
        }
        else {
    //        if(fabs(all[i].speed.y) > 0.01) all[i].life--;

        }
    //    qDebug()<<"i="<<i<<"\tx=" << all[i].position.x << "\ty=" <<all[i].position.y << "\tz="<<all[i].position.z;


}
