
/*
 * NAME: Taranov Vassiliy Vassilevich, 325
 * ASGN: N5
 */

#ifndef CAMERA_H
#define CAMERA_H


#ifndef M_PI
#define M_PI 3.14159265

class Camera
{
private:
    float x, y, z; //curent position
    float x1, y1, z1; // center point
    float angel1, angel2;
    float r;
    float speed_move;
    float sensitivity;

    //is key pressed
    bool keyW, keyS, keyA, keyD, keySpace, keyC;
public:
    Camera();
    void Translate();
    void Rotate(float x, float y);
    float DToR(float n); // degree to radians
    void MoveToCenter();

	//seters:
    void SetKeyStates(bool _w, bool _s, bool _a, bool _d, bool _space, bool _c);
    void setKeyW(bool n) {keyW = n;}
    void setKeyS(bool n) {keyS = n;}
    void setKeyA(bool n) {keyA = n;}
    void setKeyD(bool n) {keyD = n;}
    void setKeySpace(bool n) {keySpace = n;}
    void setKeyC(bool n) {keyC = n;}
    void setAnge1(bool angle) {angel1 = angle;}
    void setAnge2(bool angle) {angel2 = angle;}

    //geters:
    float getX() {return x;}
    float getY() {return y;}
    float getZ() {return z;}

    float getX1() {return x1;}
    float getY1() {return y1;}
    float getZ1() {return z1;}

    bool getKeyW() {return keyW;}
    bool getKeyS() {return keyS;}
    bool getKeyA() {return keyA;}
    bool getKeyD() {return keyD;}
    bool getKeySpace() {return keySpace;}
    bool getKeyC() {return keyC;}

    float getAngle1() {return angel1;}
    float getAngle2() {return angel2;}
};
#endif //M_PI

#endif // CAMERA_H

