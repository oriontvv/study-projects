
/*
 * NAME: Taranov Vassiliy Vassilevich, 325
 * ASGN: N5
 */

#ifndef SYSTEMFIRE_H
#define SYSTEMFIRE_H

#include "SystemOfParticles.h"

class SystemFire : public SystemOfParticles
{
public:
    void init(int n = 1);//participles count
    void initOneParticle(int index);
    void updateOneParticle(int index);
};

#endif // SYSTEMFIRE_H
