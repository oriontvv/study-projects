#ifndef SYSTEMOFPARTICLES_H
#define SYSTEMOFPARTICLES_H

struct OneParticle
{
    int life;
    float x, y, z; //current position
    float x1, y1, z1; //draught vector
    int size;
    float gravity;//no comment;)
};

class SystemOfParticles
{
protected:
    int count;
    OneParticle *all;
    int time;

public:
    SystemOfParticles();
    virtual ~SystemOfParticles();
    void setCount(int n);
    int getCount();
    OneParticle *getAll();

    virtual void update() = 0;
    void init() = 0;
    void timeUpdate();
};

#endif // SYSTEMOFPARTICLES_H
