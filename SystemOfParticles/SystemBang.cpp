
/*
 * NAME: Taranov Vassiliy Vassilevich, 325
 * ASGN: N5
 */

#include "SystemBang.h"
#include <cmath>

void SystemBang::init(int n)
{
    time = 0;
    count = n;
    gravity = -0.001;

    all = new OneParticle[count];

    for(int i = 0; i < count; ++i)
        initOneParticle(i);

    //skip firts updates...
//    for(int i = 0; i < 1000; ++i)
//        update();
}

void SystemBang::initOneParticle(int i)
{
    // generating new particle

//    all[i].life = (int)(random(10) + 7);
    all[i].life = 24;

    //position vector

    all[i].position.x = random(0.01);
    all[i].position.y = random(0.01) + 0.7;
    all[i].position.z = random(0.01);


    // generate a speed direction via spherical system of coordinates
    float angle_h = random(2 * M_PI);// 0..2*pi
    float angle_v = _random(2 * M_PI);// -pi..pi
    float r = random(0.03);

    all[i].speed.x = r * sin(angle_v) * sin(angle_h);
    all[i].speed.y = r * cos(angle_v);
    all[i].speed.z = r * sin(angle_v) * cos(angle_h);

    // init direction
    all[i].acceleration.x = 0.0;
    all[i].acceleration.y = gravity;
    all[i].acceleration.z = 0.0;

    all[i].size = random(0.02);

//    qDebug() << "init:\tx = " << all[i].position.x << "\tz = "<<all[i].position.z;
//    qDebug() <<"init [" << i << "]:\tlife:" <<all[i].life;
}

void SystemBang::updateOneParticle(int i)
{
//    all[i].life = 10;
    //detect life
//    if (all[i].position.y < -1){
//       all[i].life = ((int)(-24 * (all[i].position.y - 0.5) - random(7) + 24)) % 24;
//    }
//    else{
//        all[i].life = ((int)(-24 * (all[i].position.y - 0.5)  + 24)) % 24;
//    }
//    all[i].size = random(0.03);
    all[i].size += _random(0.001 * abs( all[i].life));
    all[i].life--;

    // update the speed and position values

    all[i].speed.x += all[i].acceleration.x;
    all[i].speed.y += all[i].acceleration.y;
    all[i].speed.z += all[i].acceleration.z;

    all[i].position.x += all[i].speed.x;
    all[i].position.y += all[i].speed.y;
    all[i].position.z += all[i].speed.z;

//    if (all[i].position.y < -0.5){
//        initOneParticle(i);
//    }
    if (all[i].life == -15){
//        for(int q = 0; q < 100000; ++q);
        initOneParticle(i);
    }
}

