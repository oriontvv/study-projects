# -------------------------------------------------
# Project created by QtCreator 2009-12-12T18:23:35
# -------------------------------------------------
QT += opengl
TARGET = SystemOfParticles
TEMPLATE = app
SOURCES += main.cpp \
    Camera.cpp \
    SystemFire.cpp \
    GLScene.cpp \
    SystemOfParticles.cpp \
    SystemSnow.cpp \
    SystemSmoke.cpp \
    SystemBang.cpp
HEADERS += Camera.h \
    SystemFire.h \
    GLScene.h \
    SystemOfParticles.h \
    SystemSnow.h \
    SystemSmoke.h \
    SystemBang.h
