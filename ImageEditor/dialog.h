#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QDoubleSpinBox>

namespace Ui {
    class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();
    int getRadius();
    double * getMatrix();

private:
    Ui::Dialog *ui;

    QDoubleSpinBox *matrix[8][8];

public slots:
    void update_matrix(int new_radius);
};

#endif // DIALOG_H
