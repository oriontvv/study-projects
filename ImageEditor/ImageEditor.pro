#-------------------------------------------------
#
# Project created by QtCreator 2011-09-22T22:00:55
#
#-------------------------------------------------

QT       += core gui

TARGET = ImageEditor
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mywidget.cpp \
    dialog.cpp

HEADERS  += mainwindow.h \
    mywidget.h \
    dialog.h

FORMS    += mainwindow.ui \
    dialog.ui
