#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    for (int i = 0; i < 7; i++)
    {
        for (int j = 0; j < 7; j++)
        {
            QDoubleSpinBox * elem = new QDoubleSpinBox;
            elem->setMinimum(-10000);
            elem->setMaximum(+10000);
            matrix[i][j] = elem;
            elem->hide();
            ui->kernel->addWidget(elem, i, j);

        }
    }
    connect(ui->radius, SIGNAL(valueChanged(int)), this, SLOT(update_matrix(int)));
}

Dialog::~Dialog()
{
    delete ui;
}

int Dialog::getRadius()
{
    return ui->radius->value();
}

void Dialog::update_matrix(int new_radius)
{
    for (int i = 0; i < 7; i++)
    {
        for (int j = 0; j < 7; j++)
        {
            matrix[i][j]->hide();
        }
    }

    for (int i = 0; i < new_radius*2 + 1; i++)
    {
        for (int j = 0; j < new_radius*2 + 1; j++)
        {
            matrix[i][j]->show();
            matrix[i][j]->setValue(0.0);
        }
    }

    // for minimum size )
    resize(10, 10);
}

double * Dialog::getMatrix()
{
    int size = ui->radius->value()*2 + 1;
    double * m = new double[size * size];

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            m[i*size + j] = matrix[i][j]->value();
        }
    }

    return m;
}
