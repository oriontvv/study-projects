#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "dialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Dialog * dialog;

    QPoint p1, p2; // koordinati podkartinki
    int w, h; // razmeri podkartinki
    int W, H; // razmeri kartinki
    QImage * image;
    QImage * initImage;

    void setEnabledUI(bool f);

    void ContrastCorrection();
    void ChannelContrastCorrection();
    void Gauss();
    void Sharpening();
    void Mediana();
    void GrayWorld();
    void Waves();
    void CustomKernel(double * matrix, int radius);
    QRgb Interpolation(double x, double y, int int_x, int int_y);

private slots:
    void setP(QPoint _p1, QPoint _p2);
    void on_open_clicked();
    void on_save_clicked();
    void on_initImage_clicked();
    void on_comboBox_currentIndexChanged(int index);

    void on_pushButton_clicked();
    void on_rotate_clicked();
    void on_scale_clicked();
};

#endif // MAINWINDOW_H
