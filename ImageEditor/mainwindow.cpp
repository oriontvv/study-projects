#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <cmath>

#include <QDebug>

#define CONTRAST_CORRECTION 0
#define CHANNEL_CONTRAST_CORRECTION  1
#define GAUSS 2
#define SHARPENING 3
#define MEDIANA 4
#define GRAY_WORLD 5
#define CUSTOM_KERNEL 6
#define WAVES 7

double const PI = 4.0 * atan(1.0); //3.14...

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    image = NULL;
    initImage = NULL;
    p1 = QPoint(0, 0);
    dialog = new Dialog;
    dialog->update_matrix(1);

    connect(ui->myWidget, SIGNAL(imageSelected(QPoint,QPoint)), this, SLOT(setP(QPoint,QPoint)));
    connect(ui->deselect, SIGNAL(clicked()), ui->myWidget, SLOT(unselect()));
    connect(ui->kernel, SIGNAL(clicked()), dialog, SLOT(show()));

    setEnabledUI(false);

    on_comboBox_currentIndexChanged(0);

    resize(800, 600);

    double sigma = 0.9;
    int Radius = int(3*sigma);
    const int Size = 2*Radius + 1;
    ui->progressBar->setRange(0, 2*w);
    int progress = 0;

/*
    qDebug()<<"---------";
    double k[Size]; // kernel
    for (int i = 0; i < Size; i++)
    {
        k[i] = 1.0 / sqrt(2*PI) / sigma * exp(-(i-Radius)*(i-Radius) / 2.0 / sigma / sigma );
        qDebug() << k[i];
    }

    qDebug()<<"++++++++";
    for (int i = 0; i < Size; i++)
    {
        k[i] = 1.0 / sqrt(2*PI) / sigma * exp(-(i-Radius)*(i-Radius) / 2.0 / sigma / sigma ) * ((-i+Radius))/sigma / sigma;
        qDebug() << k[i];
    }*/
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setEnabledUI(bool f)
{
    ui->myWidget->setEnabled(f);
    ui->edit->setEnabled(f);
    ui->view->setEnabled(f);
    ui->save->setEnabled(f);
}

void MainWindow::setP(QPoint _p1, QPoint _p2)
{
    p1 = _p1;
    p2 = _p2;
    w = p2.x() - p1.x();
    h = p2.y() - p1.y();
}

void MainWindow::on_open_clicked()
{
    QString name = QFileDialog::getOpenFileName(this, tr("Open image"), QDir::currentPath());
    if (!name.isEmpty())
    {
        QImage* new_image = new QImage;
        new_image->load(name);

        if (new_image->isNull())
        {
            QMessageBox::information(this, tr("Error"), tr("Cannot load image %1").arg(name));
            return;
        }

        QImage * tmp = image;
        image = new_image;
        if (tmp) delete tmp;
        initImage = new QImage(*image);

        W = image->width();
        H = image->height();

        p1 = QPoint(0, 0);
        p2 = QPoint(new_image->width(), new_image->height());
        setP(p1, p2);
        ui->myWidget->setImage(image);
        ui->myWidget->setPoint(p1);
        ui->myWidget->setPoint1(p2);
        setEnabledUI(true);
    }
}

void MainWindow::on_save_clicked()
{
    QString name = QFileDialog::getSaveFileName(this, tr("Save image"), QDir::currentPath(),  "*.bmp");
    if (!name.isEmpty())
    {
        if (!ui->myWidget->isSelected())
        {
            image->copy(p1.x(), p1.y(), w, h).save(name);
        }
        else
        {
            image->save(name);
        }
    }
}

void MainWindow::on_initImage_clicked()
{
    if (image) delete image;
    image = new QImage(*initImage);
    ui->myWidget->setImage(image);
}

void MainWindow::on_comboBox_currentIndexChanged(int index)
{
    ui->integerParamWidget->hide();
    ui->doublePowerWidget->hide();
    ui->gausParamWidget->hide();
    ui->kernelWidget->hide();

    if (index == GAUSS)
    {
        ui->gausParamWidget->show();
    }
    if (index == MEDIANA)
    {
        ui->integerSpinBox->setRange(1, 10);
        ui->integerParamWidget->show();
    }
    if (index == CUSTOM_KERNEL)
    {
        ui->kernelWidget->show();
    }
    if (index == WAVES)
    {
        ui->doubleSpinBox_2->setRange(10, 200);
        ui->doubleSpinBox_2->setValue(130.0);
        ui->doublePowerWidget->show();
    }
}

inline double fit(double value, double min=0, double max=255)
{
    if (value < min) return min;
    if (value > max) return max;
    return value;
}

void MainWindow::ContrastCorrection()
{
    int min = 256, max = 0;
    QImage * new_img = new QImage(*image);

    ui->progressBar->setRange(0, 2*w);
    int progress = 0;

    for (int x = p1.x(); x < p2.x(); x++)
    {
        for (int y = p1.y(); y < p2.y(); y++)
        {   QRgb rgb = image->pixel(x, y);
            //BT.709: Y = 0.2125�R + 0.7154�G + 0.0721�B,
            int Gray = 0.2125 * qRed(rgb) + 0.7154 * qGreen(rgb) + 0.0721 * qBlue(rgb);
            if (Gray < min) min = Gray;
            if (Gray > max) max = Gray;
        }
        ui->progressBar->setValue(++progress);
    }

    for (int x = p1.x(); x < p2.x(); x++)
    {
        for (int y = p1.y(); y < p2.y(); y++)
        {
            QRgb rgb = image->pixel(x, y);
            int Gray = 0.2125 * qRed(rgb) + 0.7154 * qGreen(rgb) + 0.0721 * qBlue(rgb);
            double Gray_ = 255.0 * (Gray - min) / (max - min);
            int r = fit(Gray_ / Gray * qRed(rgb));
            int g = fit(Gray_ / Gray * qGreen(rgb));
            int b = fit(Gray_ / Gray * qBlue(rgb));

            new_img->setPixel(x, y, qRgb(r, g, b));
        }
        ui->progressBar->setValue(++progress);
    }

    QImage * tmp = image;
    image = new_img;
    if (tmp) delete tmp;

    ui->myWidget->setImage(image);
}

void MainWindow::ChannelContrastCorrection()
{
    int minR = 256, minG = 256, minB = 256;
    int maxR = 0, maxG = 0, maxB = 0;
    QImage * new_img = new QImage(*image);

    ui->progressBar->setRange(0, 2*w);
    int progress = 0;

    for (int x = p1.x(); x < p2.x(); x++)
    {
        for (int y = p1.y(); y < p2.y(); y++)
        {
            QRgb rgb = image->pixel(x, y);
            if (minR > qRed(rgb)) minR = qRed(rgb);
            if (minG > qGreen(rgb)) minG = qGreen(rgb);
            if (minB > qBlue(rgb)) minB = qBlue(rgb);

            if (maxR < qRed(rgb)) maxR = qRed(rgb);
            if (maxG < qGreen(rgb)) maxG = qGreen(rgb);
            if (maxB < qBlue(rgb)) maxB = qBlue(rgb);
        }
        ui->progressBar->setValue(++progress);
    }

    for (int x = p1.x(); x < p2.x(); x++)
    {
        for (int y = p1.y(); y < p2.y(); y++)
        {
            QRgb rgb = image->pixel(x, y);
            double newR = fit(255.0 *(qRed(rgb) - minR) / (maxR - minR));
            double newG = fit(255.0 *(qGreen(rgb) - minG) / (maxG - minG));
            double newB = fit(255.0 *(qBlue(rgb) - minB) / (maxB - minB));

            new_img->setPixel(x, y, qRgb(newR, newG, newB));
        }
        ui->progressBar->setValue(++progress);
    }

    QImage * tmp = image;
    image = new_img;
    if (tmp) delete tmp;

    ui->myWidget->setImage(image);
}

void MainWindow::Gauss()
{
    QImage * new_img = new QImage(*image);

    double sigma = 0.6;
    int Radius = int(3*sigma);
    const int Size = 2*Radius + 1;
    ui->progressBar->setRange(0, 2*w);
    int progress = 0;

    double k[Size]; // kernel
    for (int i = 0; i < Size; i++)
    {
        k[i] = 1.0 / sqrt(2*PI) / sigma * exp(-(i-Radius)*(i-Radius) / 2.0 / sigma / sigma );
        qDebug() << k[i];
    }

    for(int x = p1.x(); x < p2.x(); x++)
    {
        for(int y = p1.y(); y < p2.y(); y++)
        {
            double r = 0;
            double g = 0;
            double b = 0;
            for (int i = x-Radius; i <= x+Radius; i++)
            {
                if (0 <= i && i < W)
                {
                    QRgb rgb = image->pixel(i, y);
                    r += qRed(rgb) * k[i-x+Radius];
                    g += qGreen(rgb) * k[i-x+Radius];
                    b += qBlue(rgb) * k[i-x+Radius];
                }
            }
            new_img->setPixel(x, y, qRgb(fit(r), fit(g), fit(b)));
        }
        ui->progressBar->setValue(++progress);
    }

//    QImage * tmp = image;
//    image = new_img;
//    new_img = tmp;

    for(int x = p1.x(); x < p2.x(); x++)
    {
        for(int y = p1.y(); y < p2.y(); y++)
        {
            double r = 0, g = 0, b = 0;
            for (int j = y-Radius; j <= y+Radius; j++)
            {
                if (0 <= j && j < H)
                {
                    QRgb rgb = new_img->pixel(x, j);
                    r += qRed(rgb) * k[j-y+Radius];
                    g += qGreen(rgb) * k[j-y+Radius];
                    b += qBlue(rgb) * k[j-y+Radius];
                }
            }
            image->setPixel(x, y, qRgb(fit(r), fit(g), fit(b)));
        }
        ui->progressBar->setValue(++progress);
    }

    if (new_img) delete new_img;
    ui->myWidget->setImage(image);
}

void MainWindow::Sharpening()
{
    double matrix[] = {-0.1, -0.2, -0.1, -0.2, 2.2, -0.2, -0.1, -0.2, -0.1};
    CustomKernel((double *)&matrix, 1);
}

void MainWindow::Mediana()
{
    ui->progressBar->setRange(0, w);
    int n = ui->integerSpinBox->value();
    QImage *new_img = new QImage(*image);
    int amount = (2 * n + 2) * (2 * n + 2);
    QVector<long> masR(amount), masG(amount), masB(amount);

    int progress = 0;
    for(int x = p1.x(); x < p2.x(); x++)
    {
        ui->progressBar->setValue(++progress);
        for(int y = p1.y(); y < p2.y(); y++)
        {
            int q = 0;
            for(int x1 = x - n; x1 <= x + n; x1++)
                for(int y1 = y - n; y1 <= y + n; y1++)
                    if(x1 >=0 && x1 < W && y1 >=0 && y1 < H)
                    {
                        QRgb rgb = image->pixel(x1, y1);
                        masR[q] = qRed(rgb);
                        masG[q] = qGreen(rgb);
                        masB[q++] = qBlue(rgb);
                    }
            qSort(masR);
            qSort(masG);
            qSort(masB);
            new_img->setPixel(x, y, qRgb(masR[q / 2], masG[q / 2], masB[q / 2]));
        }
    }

    QImage *tmp = image;
    image = new_img;
    if (tmp) delete tmp;
    ui->myWidget->setImage(image);
}

void MainWindow::GrayWorld()
{
    double R_ = 0.0, G_ = 0.0, B_ = 0.0;
    double AVG;
    QImage * new_img = new QImage(*image);

    ui->progressBar->setRange(0, 2*w);
    int progress = 0;
    for (int x = p1.x(); x <p2.x(); x++)
    {
        for (int y = p1.y(); y <p2.y(); y++)
        {
            QRgb rgb = image->pixel(x, y);
            R_ += qRed(rgb);
            G_ += qGreen(rgb);
            B_ += qBlue(rgb);
        }
        ui->progressBar->setValue(++progress);
    }
    R_ /= (double)(W*H);
    G_ /= (double)(W*H);
    B_ /= (double)(W*H);
    AVG = (R_ + G_ + B_) / 3.0;

    for (int x = p1.x(); x <p2.x(); x++)
    {
        for (int y = p1.y(); y <p2.y(); y++)
        {
            QRgb rgb = image->pixel(x, y);
            double r = AVG * (double)qRed(rgb) / R_;
            double g = AVG * (double)qGreen(rgb) / G_;
            double b = AVG * (double)qBlue(rgb) / B_;
            r = fit(r);
            g = fit(g);
            b = fit(b);
            new_img->setPixel(x, y, qRgb(r, g, b));
        }
        ui->progressBar->setValue(++progress);
    }

    QImage * tmp = image;
    image = new_img;
    if (tmp) delete tmp;

    ui->myWidget->setImage(image);
}

void MainWindow::Waves()
{
    QImage * new_img = new QImage(*image);

    double power = ui->doubleSpinBox_2->value();

    for (int x = p1.x(); x < p2.x(); x++)
    {
        for (int y = p1.y(); y < p2.y(); y++)
        {
            new_img->setPixel(x, y, qRgb(0, 0, 0));
        }
    }

    ui->progressBar->setRange(0, w);
    for (int x = p1.x(); x < p2.x(); x++)
    {
        for (int y = p1.y(); y < p2.y(); y++)
        {
            double new_x = x + 20*sin(2*PI * y/power);
            if (new_x >= p1.x() && new_x < p2.x())
            {
                new_img->setPixel(x, y, image->pixel(new_x, y));
            }
        }
        ui->progressBar->setValue(x+1);
    }

    QImage * tmp = image;
    image = new_img;
    if (tmp) delete tmp;

    ui->myWidget->setImage(image);
}

void MainWindow::CustomKernel(double * matrix, int radius)
{
    int size = 2*radius + 1;
    QImage * new_img = new QImage(*image);

    ui->progressBar->setRange(0, w);

    for (int x = p1.x(); x < p2.x(); x++)
    {
        for (int y = p1.y(); y < p2.y(); y++)
        {
            double sumR = 0.0, sumG = 0.0, sumB = 0.0;
            for (int i = -radius; i <= radius; i++)
                for (int j = -radius; j <= radius; j++)
                    if (i+x >= 0 && i+x < W && j+y >=0 && j+y < H)
                    {
                        QRgb rgb = image->pixel(i+x, j+y);
                        sumR += qRed(rgb) * matrix[size*(i+radius) + (j+radius)];
                        sumG += qGreen(rgb) * matrix[size*(i+radius) + (j+radius)];
                        sumB += qBlue(rgb) * matrix[size*(i+radius) + (j+radius)];
                    }
            new_img->setPixel(x, y, qRgb(fit(sumR), fit(sumG), fit(sumB)));
        }
        ui->progressBar->setValue(x+1);
    }

    QImage * tmp = image;
    image = new_img;
    if (tmp) delete tmp;

    ui->myWidget->setImage(image);
}

void MainWindow::on_pushButton_clicked()
{
    int filter = ui->comboBox->currentIndex();

    switch (filter)
    {
    case CONTRAST_CORRECTION:
        ContrastCorrection(); break;

    case CHANNEL_CONTRAST_CORRECTION:
        ChannelContrastCorrection(); break;

    case GAUSS:
        Gauss(); break;

    case SHARPENING:
        Sharpening(); break;

    case MEDIANA:
        Mediana(); break;

    case GRAY_WORLD:
        GrayWorld(); break;

    case CUSTOM_KERNEL:{
        double * matrix = dialog->getMatrix();
        CustomKernel(matrix, dialog->getRadius());
        delete matrix;
        break;}

    case WAVES:
        Waves(); break;

    }

}

QRgb MainWindow::Interpolation(double X, double Y, int int_x, int int_y)
{
    double x = X - int_x;
    double y = Y - int_y;
    x = fit(x, 0.0, 1.0);
    y = fit(y, 0.0, 1.0);
    int_x = fit(int_x, 0.0, W - 2);
    int_y = fit(int_y, 0.0, H - 2);

    QRgb p00 = image->pixel(int_x, int_y);
    QRgb p10 = image->pixel(int_x + 1, int_y);
    QRgb p01 = image->pixel(int_x, int_y + 1);
    QRgb p11 = image->pixel(int_x + 1, int_y + 1);

    double r, g, b;
    r = (1.0 - x) * (1.0 - y) * qRed(p00) + x * (1.0 - y) * qRed(p10) + (1.0 - x) * y * qRed(p01) + x * y * qRed(p11);
    g = (1.0 - x) * (1.0 - y) * qGreen(p00) + x * (1.0 - y) * qGreen(p10) + (1.0 - x) * y * qGreen(p01) + x * y * qGreen(p11);
    b = (1.0 - x)*(1.0 - y) * qBlue(p00) + x*(1.0 - y) * qBlue(p10) + (1.0 - x) * y * qBlue(p01) + x * y * qBlue(p11);

    return qRgb(r, g, b);
}

double min(double x, double y)
{
    if (x < y) return x;
    return y;
}

double min(double a, double b, double c, double d)
{
    return min(a, min(b, min(c, d)));
}

double max(double x, double y)
{
    if (x > y) return x;
    return y;
}

double max(double a, double b, double c, double d)
{
    return max(a, max(b, max(c, d)));
}

void MainWindow::on_rotate_clicked()
{
    double angle = ui->rotatingPower->value();
    angle = PI * angle / 180; // to radians

    QImage * new_img = new QImage(*image);
    ui->progressBar->setRange(0, w);

    QPoint center = (p1 + p2) / 2;
    double x00, y00, x01, y01, x10, y10, x11, y11;

    x00 = center.x() + (p1.x() - center.x())*cos(angle) - (p1.y() - center.y())*sin(angle);
    y00 = center.y() + (p1.x() - center.x())*sin(angle) + (p1.y() - center.y())*cos(angle);
    x01 = center.x() + (p2.x() - center.x())*cos(angle) - (p1.y() - center.y())*sin(angle);
    y01 = center.y() + (p2.x() - center.x())*sin(angle) + (p1.y() - center.y())*cos(angle);
    x10 = center.x() + (p1.x() - center.x())*cos(angle) - (p2.y() - center.y())*sin(angle);
    y10 = center.y() + (p1.x() - center.x())*sin(angle) + (p2.y() - center.y())*cos(angle);
    x11 = center.x() + (p2.x() - center.x())*cos(angle) - (p2.y() - center.y())*sin(angle);
    y11 = center.y() + (p2.x() - center.x())*sin(angle) + (p2.y() - center.y())*cos(angle);

    QPoint p1_(min(x00, x01, x10, x11), min(y00, y01, y10, y11));
    QPoint p2_(max(x00, x01, x10, x11), max(y00, y01, y10, y11));

    for (int x = p1.x(); x < p2.x(); x++)
    {
        for (int y = p1.y(); y < p2.y(); y++)
        {
            new_img->setPixel(x, y, qRgb(0, 0, 0));
        }
    }

    for (double x = p1_.x(); x <= p2_.x(); x++)
    {
        for (double y = p1_.y(); y <= p2_.y(); y++)
        {
            double x_ = 1.0 * center.x() + (x - center.x())*cos(-angle) - (y - center.y())*sin(-angle);
            double y_ = 1.0 * center.y() + (x - center.x())*sin(-angle) + (y - center.y())*cos(-angle);
            if (x >= 0 && x < W && y >= 0 && y < H && x_ >= p1.x() && x_ <= p2.x()+1 && y_ >= p1.y() && y_ <= p2.y()+1)
            {
                new_img->setPixel(x, y, Interpolation(x_, y_, int(x_), int(y_)));
            }
        }
        ui->progressBar->setValue(x+1);
    }

    QImage * tmp = image;
    image = new_img;
    if (tmp) delete tmp;

    ui->myWidget->setImage(image);
}

void MainWindow::on_scale_clicked()
{
    double Koeff = ui->scalePower->value();

    QImage * new_img = new QImage(*image);
    ui->progressBar->setRange(0, w);
    QPoint center = (p1 + p2) / 2;
    QPoint p1_ = center + (p1 - center) * Koeff;
    QPoint p2_ = center + (p2 - center) * Koeff;

    for (int x = p1.x(); x < p2.x(); x++)
    {
        for (int y = p1.y(); y < p2.y(); y++)
        {
            new_img->setPixel(x, y, qRgb(0, 0, 0));
        }
    }

    for (int x = p1_.x(); x < p2_.x(); x++)
    {
        for (int y = p1_.y(); y < p2_.y(); y++)
        {
            double x_ = (x - center.x()) / Koeff + center.x();
            double y_ = (y - center.y()) / Koeff + center.y();

            if (x_ >= 0 && x_ < W && y_ >= 0 && y_ < H && x >= 0 && x < W && y >= 0 && y < H)
            {
                new_img->setPixel(x, y, Interpolation(x_, y_, int(x_), int(y_)));
            }
        }
        ui->progressBar->setValue(x+1);
    }

    QImage * tmp = image;
    image = new_img;
    if (tmp) delete tmp;

    ui->myWidget->setImage(image);
}
