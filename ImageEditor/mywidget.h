#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QLabel>
#include <QWidget>

class MyWidget : public QLabel
{
    Q_OBJECT

private:
    QPoint point; // nachalo kartinki
    QPoint point1; //konec kartinki
    bool selecting;
    QImage *image;
    void mousePressEvent(QMouseEvent *ev);
    void mouseMoveEvent(QMouseEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);

public:
    explicit MyWidget(QWidget *parent = 0);
    void setImage(QImage *image);
    bool isSelected();
    void setPoint(QPoint p) {point = p;}
    void setPoint1(QPoint p) {point1 = p;}

signals:
    void imageSelected(QPoint, QPoint);

public slots:
    void unselect();

};

#endif // MYWIDGET_H
