#include "mywidget.h"
#include <QMouseEvent>
#include <QPainter>


MyWidget::MyWidget(QWidget *parent) :
    QLabel(parent)
{
    setMouseTracking(true);
    image = NULL;
    selecting = false;
}

void MyWidget::mousePressEvent(QMouseEvent *ev)
{
    if (ev->buttons() & Qt::RightButton)
    {
        selecting = false;
        unselect();
        return;
    }
    point = ev->pos();
    selecting = true;
}

void MyWidget::mouseMoveEvent(QMouseEvent *ev)
{
    if (selecting)
    {
        QPixmap p = QPixmap::fromImage(*image);
        QPainter painter(&p);
        painter.setPen(QPen(Qt::yellow, 2, Qt::DashDotLine));
        painter.drawRect(point.x(), point.y(), ev->pos().x() - point.x(), ev->pos().y() - point.y());

        setPixmap(p);
    }
}

void MyWidget::mouseReleaseEvent(QMouseEvent *ev)
{
    if (!selecting)
    {
        unselect();
        return;
    }
    selecting = false;
    int x1 = point.x(), x2 = ev->pos().x();
    int y1 = point.y(), y2 = ev->pos().y();

    if (x2 < x1)
    {
        int t = x1;
        x1 = x2;
        x2 = t;
    }
    if (y2 < y1)
    {
        int t = y1;
        y1 = y2;
        y2 = t;
    }
    point = QPoint(x1, y1);
    point1 = QPoint(x2, y2);
    emit imageSelected(point, point1);
}

void MyWidget::setImage(QImage *img)
{
    image = img;
    QPixmap p = QPixmap::fromImage(*image);
    QPainter painter(&p);
    painter.setPen(QPen(Qt::yellow, 2, Qt::DashDotLine));
    painter.drawRect(point.x(), point.y(), point1.x()-point.x(), point1.y()-point.y());

    setPixmap(p);
    selecting = false;
}

bool MyWidget::isSelected()
{
    return selecting;
}

void MyWidget::unselect()
{
    setPixmap(QPixmap::fromImage(*image));
    selecting = false;
    point = QPoint(0, 0);
    point1 = QPoint(image->width(), image->height());
    emit imageSelected(QPoint(0, 0), QPoint(image->width(), image->height()));
}
