#include "form.h"
#include "ui_form.h"

#include <QTextCodec>
#include <QFileDialog>
#include <QTextStream>
#include <QPainter>
#include <fstream>
#include <math.h>
#include <time.h>

#define PEDESTRAIN 1
#define BACKGROUND -1

#include <QDebug>


Form::Form(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);
    QTextCodec::setCodecForTr(QTextCodec::codecForName("Windows-1251"));

    kernel_x[0] = 0.107982;
    kernel_x[1] = 0.797885;
    kernel_x[2] = 0.107982;

    kernel_y[0] = 0.431928;
    kernel_y[1] = 0;
    kernel_y[2] = -0.431928;

    connect(ui->bootstrapping, SIGNAL(clicked()), SLOT(bootstrapping()));
}

Form::~Form()
{
    delete ui;
}

void Form::on_learnPictures_open_clicked()
{
    QString directory = QFileDialog::getExistingDirectory(this, tr("������� ����� � ���������� �������������"));
    if (!directory.isEmpty()) {
        ui->learnPictures->setText(directory);
    }
}

void Form::on_learnLocations_open_clicked()
{
    QString name = QFileDialog::getOpenFileName(this, tr("������� ���� � ���������� ���������"));
    if (!name.isEmpty()) {
        ui->learnLocations->setText(name);
    }
}

QList<std::pair <QString, int> > Form::openIDL(QString name)
{
    QList<std::pair<QString, int> > idl;

    std::ifstream input(name.toAscii());
    if (!input.is_open()) {
        logging(tr("���� '%1' �� ������").arg(name), true);
        return idl;
    }
    int y0, x0, y1, x1;
    std::string name_;
    while (!input.eof())
    {
        input >> name_ >> y0 >> x0 >> y1 >> x1;
        if (!name_.empty() && !input.eof()) {
            idl.append(std::pair<QString, int>(QString::fromStdString(name_), x0));
        }
    }
    logging(tr("���� '%1' ������� ��������. ���������� �������: %2.").arg(name, QString::number(idl.size())));

    input.close();
    return idl;
}

void Form::logging(QString str, bool is_error, bool make_new_Line)
{
    if (make_new_Line) {
        ui->logger->append("");
    }

    if (is_error) {
        ui->logger->append(tr("<font color=red><b>%1<\\b><\\font>").arg(str));
    } else {
        ui->logger->append(str);
    }
    ui->logger->moveCursor(QTextCursor::End);
    ui->logger->repaint();
}

void Form::on_learnGo_clicked()
{
    if (ui->learnPictures->text().isEmpty()) {
        logging(tr("�������� ���� � ��������� ������������!"), true);
        return;
    }
    if (ui->learnLocations->text().isEmpty()) {
        logging(tr("�������� ���� � ������� ��������� ���������!"), true);
        return;
    }

    QList<std::pair <QString, int> > loc = openIDL(ui->learnLocations->text());

    QVector<feature_node * > trainFeatures;
    QVector<int> trainLabels;

    int timer = clock();
    int difference = ui->differenceSpinBox->value();

    logging(tr("���������� ���������..."));
    for (int i = 0; i < loc.size(); i++) {
        QString name = loc[i].first;
        int x = loc[i].second;
        QImage picture(ui->learnPictures->text()+'\\'+name+".png");

        // process pedestrain
        trainFeatures.append(getFeaturesStruct(picture, x));
        trainLabels.append(PEDESTRAIN);

        // process backgound
        int count = 0, total = ui->backgoundSpinBox->value();
        while (count < total) {
            int posible_backgound_x0 = rand() % (picture.width() - WIDTH);
            if (abs(posible_backgound_x0 - x) > difference) {
                trainFeatures.append(getFeaturesStruct(picture, posible_backgound_x0));
                trainLabels.append(BACKGROUND);
                count++;
            }
        }
        logging(name + tr(".png\t[��]"), false, false);
    }

    // fill train structure
    int labelsSize = trainLabels.size();
    problem prob;
    prob.l = labelsSize;
    prob.bias = -1;
    prob.n = NUM_FEATURES;

    prob.y = new int[labelsSize];
    prob.x = new feature_node*[labelsSize];

    for (int i = 0; i < labelsSize; i++) {
        prob.x[i] = trainFeatures.at(i);
        prob.y[i] = trainLabels.at(i);
    }

    parameter param;
    param.solver_type = L2R_L2LOSS_SVC_DUAL;
    param.C = 1;
    param.eps = 1e-4;
    param.nr_weight = 0;
    param.weight_label = NULL;
    param.weight = NULL;

    logging(tr("���������� ��������� ���������. ����� ��������� � ����: %1.\n����������� �����: %2 ������(�).").
            arg(QString::number(labelsSize), QString::number(double(clock() - timer) / CLOCKS_PER_SEC)));
    timer = clock();

    // train classifier
    model * classificator = train(&prob, &param);
    logging(tr("�������� ���������. ����������� �����: %1 ������(�).").arg( QString::number(double(clock() - timer) / CLOCKS_PER_SEC)));

    QString file = QFileDialog::getSaveFileName(0, tr("�������� ��� �����, � ������� ����� �������� �������������"));
    if (!file.isEmpty()) {
        if (-1 == save_model(file.toAscii(), classificator)) {
            logging(tr("���������� ��������� ������������� � ���� '%1'").arg(file), true);
        } else {
            logging(tr("������������� ������� �������� � ���� '%1'").arg(file));
            ui->recognizeClassificator->setText(file);
            ui->classifyClassificator->setText(file);
        }
    } else {
        logging(tr("�� ������ ���� ��� ���������� ��������������. ������������� ��������.").arg(file), true);
    }

    // free memory
    free_and_destroy_model(&classificator);
    destroy_param(&param);
    delete[] prob.y;
    for (int i = 0; i < labelsSize; i++)
        delete[] prob.x[i];
    delete[] prob.x;
}

feature_node * Form::getFeaturesStruct(QImage & picture, int shift)
{
    int wid = picture.width(), hgt = picture.height(), sh = (KERNEL_LEN-1)/2;
    double summa;

    // gray
    for (int i = 0; i < WIDTH; i++)
        for (int j = 0; j < HEIGHT; j++)
            gray[j][i] = qGray(picture.pixel(i + shift, j));

#define BETWEEN(a, x, b) ((a) <= (x) && (x) < (b))
    // a for sobel_x
    for (int i = 0; i < WIDTH; i++)
        for (int j = 0; j < HEIGHT; j++) {
            summa =  0.0;
            for (int k = 0; k < KERNEL_LEN; k++)
                if (BETWEEN(0, j+k-sh, hgt))
                    summa += gray[j+k-sh][i]*kernel_x[k];
            a[j][i] = summa;
        }
    // sobel_x
    for (int i = 0; i < WIDTH; i++)
        for (int j = 0; j < HEIGHT; j++) {
            summa =  0.0;
            for (int k = 0; k < KERNEL_LEN; k++)
                if (BETWEEN(0, i+k-sh, wid))
                    summa += a[j][i+k-sh]*kernel_y[k];
            sobel_x[j][i] = summa;
        }

    // a for sobel_y
    for (int i = 0; i < WIDTH; i++)
        for (int j = 0; j < HEIGHT; j++) {
            summa =  0.0;
            for (int k = 0; k < KERNEL_LEN; k++)
                if (BETWEEN(0, j+k-sh, hgt))
                    summa += gray[j+k-sh][i]*kernel_y[k];
            a[j][i] = summa;
        }
    // sobel_y
    for (int i = 0; i < WIDTH; i++)
        for (int j = 0; j < HEIGHT; j++) {
            summa =  0.0;
            for (int k = 0; k < KERNEL_LEN; k++)
                if (BETWEEN(0, i+k-sh, wid))
                    summa += a[j][i+k-sh]*kernel_x[k];
            sobel_y[j][i] = summa;
        }
#undef BETWEEN

    // features
    feature_node * feature_nodes = new feature_node[NUM_FEATURES+1];
    feature_nodes[NUM_FEATURES].index = -1;
    for (int i = 0; i < NUM_FEATURES; i++) {
        feature_nodes[i].index = i + 1;
        feature_nodes[i].value = 0.0;
    }

    double K = 180.0 / M_PI;
    for (int i = 0; i < WIDTH; i++)
        for (int j = 0; j < HEIGHT; j++) {
            double a = K * atan2(sobel_y[j][i], sobel_x[j][i]) + 360.0; // positive
            int angle_quantized = (int(a) % 360) / 45;
            feature_nodes[ ((j / BLOCK_SIZE) * BLOCK_COUNT_X + i / BLOCK_SIZE) * BLOCK_SIZE + angle_quantized].value++;
        }

    return feature_nodes;
}

void Form::on_recognizePicture_open_clicked()
{
    QString name = QFileDialog::getOpenFileName(this, tr("������� ����������� ��� �������������"));
    if (!name.isEmpty()) {
        ui->recognizePicture->setText(name);
        ui->pictureDrawer->setPixmap(QPixmap(name));
    }
}

void Form::on_recognizeClassificator_open_clicked()
{
    QString name = QFileDialog::getOpenFileName(this, tr("������� ���� � ��������������"));
    if (!name.isEmpty()) {
        ui->recognizeClassificator->setText(name);
    }
}

void Form::on_recognizeGo_clicked()
{
    if (ui->recognizePicture->text().isEmpty()) {
        logging(tr("�������� ���� ��� �������������!"), true);
        return;
    }
    if (ui->recognizeClassificator->text().isEmpty()) {
        logging(tr("�� ������ �������������!"), true);
        return;
    }

    // load classifictor
    model * classificator = load_model(ui->recognizeClassificator->text().toAscii());
    if (!classificator) {
        logging(tr("������ ��� �������� ��������������."), true);
        return;
    }
    logging(tr("������������� ������� ��������"));

    QImage image(ui->recognizePicture->text());
    QPainter p(&image);
    p.setPen(QPen(Qt::green, 2));

    QList<int> locations = recognize(classificator, image, ui->recognizeStepSpinBox->value());
    logging(tr("������������� ���������. ������� ���������: %1").arg(QString::number(locations.size())));

    foreach (int x, locations) {
        p.drawRect(x, 1, WIDTH, HEIGHT - 1);
    }

    ui->pictureDrawer->setPixmap(QPixmap::fromImage(image));
    free_and_destroy_model(&classificator);
}

QList<int> Form::recognize(model * classificator, QImage & image, int step)
{
    QList<std::pair<int, double> > similar;
    double prob_estimates[1];
    for (int x = 0; x < image.width() - WIDTH; x += step) {
        feature_node * features = getFeaturesStruct(image, x);

        if (predict(classificator, features) == 1) {
            predict_values(classificator, features, prob_estimates);
            similar.append(std::pair<int, double>(x, prob_estimates[0]));
        }

        delete[] features;
    }

    // removal of the local not maxima
    QList<int> detected;
    int size = similar.size();
    for (int current = 0; current < size; current++) {
        bool is_maxima = true;
        for (int other = 0; other < size; other++)
            if (current != other &&
                    abs(similar[current].first - similar[other].first) < WIDTH
                    && similar[current].second < similar[other].second) {
                is_maxima = false;
                break;
            }
        if (is_maxima) detected.append(similar[current].first);
    }
    return detected;
}

void Form::on_classifyPictures_open_clicked()
{
    QString directory = QFileDialog::getExistingDirectory(this, tr("������� ���� � �������� � �������������"));
    if (!directory.isEmpty()) {
        ui->classifyPictures->setText(directory);
    }
}

void Form::on_classifyClassificator_open_clicked()
{
    QString name = QFileDialog::getOpenFileName(this, tr("������� ���� � ��������������"));
    if (!name.isEmpty()) {
        ui->classifyClassificator->setText(name);
    }
}


void Form::on_classifyGo_clicked()
{
    if (ui->classifyPictures->text().isEmpty()) {
        logging(tr("�������� ����� ��� �������������!"), true);
        return;
    }
    if (ui->classifyClassificator->text().isEmpty()) {
        logging(tr("�� ������ �������������!"), true);
        return;
    }

    // load classifictor
    model * classificator = load_model(ui->recognizeClassificator->text().toAscii());
    if (!classificator) {
        logging(tr("������ ��� �������� ��������������."), true);
        return;
    }
    logging(tr("������������� ������� ��������"));

    QString file_name = QFileDialog::getSaveFileName(0, tr("�������� ��� �����, � ������� ����� ��������� ��������� �������"));
    qDebug()<<file_name;
    if (file_name.isEmpty()) {
        logging(tr("�� ������ ���� ��� ���������� �������."), true);
        return;
    }

    ui->estimateLocationsDetected->setText(file_name);
    std::ofstream output(file_name.toAscii());
    if (!output.is_open()) {
        logging(tr("������ ��� �������� ����� '%1' �� ������.").arg(file_name), true);
        return;
    }

    int step = ui->classifyStepStepSpinBox->value(), count = 0;
    QDir directory(ui->classifyPictures->text());

    int timer = clock();

    logging(tr("������������� ��������. ���: %1").arg(QString::number(step)));
    foreach(QString image_name, directory.entryList(QStringList("*.png"))) {
        QImage image(ui->classifyPictures->text() + '\\' + image_name);
        QString image_name_witout_ext = image_name.remove(".png", Qt::CaseInsensitive);
        count = 0;
        foreach (int x, recognize(classificator, image, step)) {
            output << image_name_witout_ext.toStdString() << "\t0\t" << x << '\t' << HEIGHT << '\t' << x + WIDTH << '\n';
            ++count;
        }
        logging(image_name + ".png\t" + QString::number(count),false, false);
    }
    logging(tr("������������� ���������. ����� ���������� %1 ������(�)").
            arg(QString::number((double)(clock() - timer)/CLOCKS_PER_SEC)));
    output.close();
}



void Form::on_estimateLocationsDetected_open_clicked()
{
    QString name = QFileDialog::getOpenFileName(this, tr("������� ���� � ���������� ���������"));
    if (!name.isEmpty()) {
        ui->estimateLocationsDetected->setText(name);
    }
}

void Form::on_estimateLocationsTrue_open_clicked()
{
    QString name = QFileDialog::getOpenFileName(this, tr("������� ���� � ������� ���������"));
    if (!name.isEmpty()) {
        ui->estimateLocationsTrue->setText(name);
    }
}

void Form::on_estimateGo_clicked()
{
    if (ui->estimateLocationsDetected->text().isEmpty()) {
        logging(tr("�������� ���� � ���������� ���������!"), true);
        return;
    }
    if (ui->estimateLocationsTrue->text().isEmpty()) {
        logging(tr("�������� ���� � ������� ���������!"), true);
        return;
    }

    double tp = 0, recall, precision, gt, det, f_measure;

    QList<std::pair<QString, int> > detected = openIDL(ui->estimateLocationsDetected->text());
    QList<std::pair<QString, int> > trust = openIDL(ui->estimateLocationsTrue->text());

    for (int i = 0; i < detected.size(); i++) {
        std::pair<QString, int> pairDetected = detected[i];
        for (int j = 0; j < trust.size(); j++) {
            std::pair<QString, int> pairTrust = trust[j];
            if (pairTrust.first == pairDetected.first)
                if (abs(pairTrust.second - pairDetected.second) <= WIDTH / 2) {
                    ++tp;
                }
        }
    }

    gt = trust.size();
    det = detected.size();
    recall = tp / gt;
    precision = tp / det;
    f_measure = 2 * recall * precision / (recall + precision);

    logging(tr("TP:\t%1\nGT:\t%2\nDET:\t%3\nRECALL:\t%4\nPRECISION:\t%5\nF_MEASURE:\t%6").arg(
                QString::number(tp),
                QString::number(gt),
                QString::number(det),
                QString::number(recall),
                QString::number(precision),
                QString::number(f_measure)));
}

void Form::on_pushButton_3_clicked()
{
    QString name = QFileDialog::getSaveFileName(this, tr("��������� ���"), 0, "*.html");
    if (!name.isEmpty()) {
        QFile file(name);
        if (file.open(QIODevice::WriteOnly)) {
            QTextStream ts(&file);
            ts << ui->logger->toHtml();
        }
        file.close();
    }
}

void Form::bootstrapping()
{
    QList<std::pair <QString, int> > loc = openIDL(ui->learnLocations->text());
    QVector<feature_node * > currentFeaures, noiseFeatures, pedFeatures;
    QVector<int> currentLabels;
    problem prob;
    parameter param;
    param.solver_type = L2R_L2LOSS_SVC_DUAL;
    param.C = 1;
    param.eps = 1e-4;
    param.nr_weight = 0;
    param.weight_label = NULL;
    param.weight = NULL;
    model * classificator;

    logging(tr("���������� ���������..."));
    for (int i = 1; i <=200; i++) {
        QString name =loc[i-1].first;
        QImage img(ui->learnPictures->text()+'/'+name+".png");
        int x = loc[i-1].second;
        feature_node * f = getFeaturesStruct(img, x);
        pedFeatures.append(f);
        currentFeaures.append(f);
        currentLabels.append(PEDESTRAIN);
    }

    // generate random noise
    logging(tr("�������� ��������� �����..."), false, false);
    for (int j = 0; j < loc.size(); j++) {
        QImage picture(ui->learnPictures->text()+'\\'+loc[j].first+".png");
        int count = 0, total = ui->backgoundSpinBox->value();
        while (count < total) {
            int posible_backgound_x0 = rand() % (picture.width() - WIDTH);
            if (abs(posible_backgound_x0 - loc[j].second) > ui->differenceSpinBox->value()) {
                feature_node * f = getFeaturesStruct(picture, posible_backgound_x0);
                noiseFeatures.append(f);
                currentFeaures.append(f);
                currentLabels.append(BACKGROUND);
                count++;
            }
        }
    }
    // train
    logging(tr("�������� � ������ �����������..."), false, false);
    int index, len = currentFeaures.size();
    prob.l = len;
    prob.bias = -1;
    prob.n = NUM_FEATURES;
    prob.y = new int[len];
    prob.x = new feature_node*[len];
    for(index = 0; index < currentFeaures.size(); index++) {
        prob.x[index] = currentFeaures.at(index);
        prob.y[index] = currentLabels.at(index);
    }

    if (classificator) free_and_destroy_model(&classificator);
    classificator = train(&prob, &param);

    for (int i = 0; i < currentFeaures.size(); i++)
        if (currentFeaures[i]) delete[] currentFeaures[i];
    delete[] prob.x;
    delete[] prob.y;
    currentFeaures.clear();
    currentLabels.clear();

    for (int i = 0; i < pedFeatures.size(); i++) {
        currentFeaures.append(pedFeatures[i]);
        currentLabels.append(PEDESTRAIN);
    }

    int step = ui->bootstrappingStep->value();
    for (int bootIndex = 0; bootIndex < ui->bootStrappingCount->value(); bootIndex++) {
        logging(tr("bootstrapping. �������� %1").arg(QString::number(bootIndex)));
        int strong_count = 0;
        for (int i = 0; i < loc.size(); i++) {
            QString name = loc[i].first;
            int x = loc[i].second;
            QImage picture(ui->learnPictures->text()+'\\'+name+".png");

            foreach (int sh, recognize(classificator, picture, step)) {
                if (abs(sh - x) > WIDTH / 2) {
                    // it's a strong noise
                    strong_count++;
                    currentFeaures.append(getFeaturesStruct(picture, sh));
                    currentLabels.append(BACKGROUND);
                }
            }
        }

        logging(tr("������� ������� ����� %1.").arg(QString::number(strong_count)));

        if (strong_count == 0) {
            logging(tr("bootstrapping ����������. ��� ������� ���� ������"));
            break;
        }

        // retrain
        logging(tr("��������������..."));
        len = currentFeaures.size();
        prob.l = len;
        prob.bias = -1;
        prob.n = NUM_FEATURES;
        prob.y = new int[len];
        prob.x = new feature_node*[len];
        for(index = 0; index < len; index++) {
            prob.x[index] = currentFeaures.at(index);
            prob.y[index] = currentLabels.at(index);
        }
        free_and_destroy_model(&classificator);
        classificator = train(&prob, &param);

        // free memory
        if (prob.y) delete[] prob.y;
        for (int i = 0; i < len; i++)
            if (prob.x[i]) delete[] prob.x[i];
        if (prob.x) delete[] prob.x;
    }

}
