#-------------------------------------------------
#
# Project created by QtCreator 2011-10-15T12:24:21
#
#-------------------------------------------------

QT       += core gui

TARGET = Recognizer
TEMPLATE = app


SOURCES += main.cpp\
        form.cpp \
    liblinear/tron.cpp \
    liblinear/linear.cpp \
    liblinear/blas/dscal.c \
    liblinear/blas/dnrm2.c \
    liblinear/blas/ddot.c \
    liblinear/blas/daxpy.c

HEADERS  += form.h \
    liblinear/tron.h \
    liblinear/linear.h \
    liblinear/blas/blasp.h \
    liblinear/blas/blas.h

FORMS    += form.ui

RESOURCES += \
    res.qrc
