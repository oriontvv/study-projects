#ifndef FORM_H
#define FORM_H

#include <QMainWindow>
#include "liblinear/linear.h"


#define WIDTH 80
#define HEIGHT 200
#define BLOCK_SIZE 8
#define BLOCK_COUNT_X WIDTH / BLOCK_SIZE
#define BLOCK_COUNT_Y HEIGHT / BLOCK_SIZE
#define NUM_FEATURES 8 * BLOCK_COUNT_X * BLOCK_COUNT_Y

#define KERNEL_LEN 3

namespace Ui {
    class Form;
}

class Form : public QMainWindow
{
    Q_OBJECT

public:
    explicit Form(QWidget *parent = 0);
    ~Form();

private slots:
    void on_learnPictures_open_clicked();

    void on_learnLocations_open_clicked();

    void on_learnGo_clicked();

    void on_recognizePicture_open_clicked();

    void on_recognizeClassificator_open_clicked();

    void on_recognizeGo_clicked();

    void on_classifyPictures_open_clicked();

    void on_classifyClassificator_open_clicked();

    void on_classifyGo_clicked();

    void on_estimateLocationsDetected_open_clicked();

    void on_estimateLocationsTrue_open_clicked();

    void on_estimateGo_clicked();

    void on_pushButton_3_clicked();

    void bootstrapping();

private:
    Ui::Form *ui;
    QList<std::pair <QString, int> > openIDL(QString name);
    void logging(QString str, bool is_error = false, bool make_new_Line = true);

    double kernel_x[KERNEL_LEN], kernel_y[KERNEL_LEN];
    int gray[HEIGHT][WIDTH];
    double a[HEIGHT][WIDTH], sobel_x[HEIGHT][WIDTH], sobel_y[HEIGHT][WIDTH];

    QList<int> recognize(model * classificator, QImage & image, int step);
    feature_node * getFeaturesStruct(QImage & picture, int shift);
};

#endif // FORM_H
