#-------------------------------------------------
#
# Project created by QtCreator 2012-02-15T19:47:03
#
#-------------------------------------------------

QT       += core gui

TARGET = test
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dialog.cpp

HEADERS  += mainwindow.h \
    dialog.h

FORMS    += mainwindow.ui \
    dialog.ui
