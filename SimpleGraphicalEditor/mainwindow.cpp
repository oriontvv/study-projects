#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QString>
#include <QFileDialog>
#include <QMessageBox>
#include <QDir>
#include <QPainter>
#include <QProgressDialog>
#include <QInputDialog>


#include <math.h>
#define MATH_PI 3.1415926535897932384626433832795

#define IMG_UPD(old, img) \
    if (old) delete old;\
    old = new QImage((*img));

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    makeConnects();

    kernelSettingsDialog = new KernelSettingsDialog;
    kernelSettingsDialog->setKernelSize(0);

    image = NULL;
    originalImage = NULL;
    subImage = NULL;

    // there is nothing to save yet
    fileName = "";
    ui->actionSave->setDisabled(true);
    ui->actionShowNormal->setDisabled(true);
    setWindowTitle(tr("Simple Graphical Editor") + fileName);
    ui->undoAllPushButton->setDisabled(true);
    ui->zoomGroupBox->setDisabled(true);
    ui->rotateGroupBox->setDisabled(true);
    ui->menuFilter->setDisabled(true);
    ui->menuView->setDisabled(true);

}

MainWindow::~MainWindow()
{
    delete ui;
    delete kernelSettingsDialog;
}

void MainWindow::makeConnects()
{
    // file
    connect(ui->actionOpen, SIGNAL(triggered()), SLOT(open()));
    connect(ui->actionSave, SIGNAL(triggered()), SLOT(save()));

    // filters
    connect(ui->actionKernel, SIGNAL(triggered()), SLOT(kernel()));
    connect(ui->actionSharpness, SIGNAL(triggered()), SLOT(sharpness()));
    connect(ui->actionGlass_effect, SIGNAL(triggered()), SLOT(glass()));
    connect(ui->actionContrast_correction, SIGNAL(triggered()), SLOT(contrastCorrection()));
    connect(ui->actionAutolevels, SIGNAL(triggered()), SLOT(autolevels()));
    connect(ui->actionMedian, SIGNAL(triggered()), SLOT(median()));
    connect(ui->actionGause, SIGNAL(triggered()), SLOT(gaussianBlur()));
    connect(ui->actionGrayworld, SIGNAL(triggered()), SLOT(grayworld()));
    connect(ui->actionAbout, SIGNAL(triggered()), SLOT(about()));

    // view
    connect(ui->actionShowNormal, SIGNAL(triggered()), SLOT(showNormal()));
    connect(ui->actionZoom, SIGNAL(triggered()), SLOT(zoom()));
    connect(ui->actionRotate, SIGNAL(triggered()), SLOT(rotate()));

    // form
    connect(ui->scalePushButton, SIGNAL(clicked()), SLOT(zoom()));
    connect(ui->rotatePushButton, SIGNAL(clicked()), SLOT(rotate()));
    connect(ui->undoAllPushButton, SIGNAL(clicked()), SLOT(showNormal()));
}

void MainWindow::open()
{
    QString newName = QFileDialog::getOpenFileName(this, tr("Open File"), QDir::currentPath());
    if (!newName.isEmpty()) {
        QImage* newImage = new QImage;
        newImage->load(newName);

        if (newImage->isNull()) {
            QMessageBox::information(this, tr("Bad file"), tr("Cannot load %1.").arg(newName));
            return;
        }

        // file is ok
        IMG_UPD(originalImage, newImage);
        IMG_UPD(image, newImage);
        IMG_UPD(subImage, newImage);

        fileName = newName;
        setWindowTitle(tr("Simple Graphical Editor : ") + fileName);
        ui->imageWidget->updateImage(image);

        resize(800, 600);

        ui->actionSave->setEnabled(true);
        ui->actionShowNormal->setEnabled(true);
        ui->undoAllPushButton->setEnabled(true);
        ui->zoomGroupBox->setEnabled(true);
        ui->rotateGroupBox->setEnabled(true);
        ui->menuFilter->setEnabled(true);
        ui->menuView->setEnabled(true);

        delete newImage;
    }
}

void MainWindow::save()
{
    QString saveFileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                        QDir::currentPath(),  "*.bmp - picture file");
    if (!saveFileName.isEmpty()) {
        image->save(saveFileName);
    }
}

void MainWindow::showNormal()
{
    IMG_UPD(image, originalImage);
    ui->imageWidget->updateImage(image);
//    resize(800, 600);
}

void MainWindow::kernel()
{
    if (QDialog::Accepted == kernelSettingsDialog->exec()) {
        int size = kernelSettingsDialog->getSize();
        applyKernel(kernelSettingsDialog->getKernel(), (size-1)/2);
    }
}

void MainWindow::sharpness()
{
    int size = 3;
    double** k = new double*[size];
    for (int i = 0; i < size; ++i) {
        k[i] = new double[size];
    }
    k[0][0] = k[0][2] = k[2][0] = k[2][2] = -0.1;
    k[0][1] = k[1][0] = k[1][2] = k[2][1] = -0.2;
    k[1][1] = 2.2;

    applyKernel(k, 1);
}

void MainWindow::glass()
{
    IMG_UPD(subImage, ui->imageWidget->getSubImage());
    QImage *img = new QImage(*subImage);
    bool ok_pressed;
    int power = QInputDialog::getInteger(this, tr("Median filter"),
                                         tr("Please input R - power of filter"), 10, 1, 30, 1, &ok_pressed);
    if (!ok_pressed) return;

    int w = subImage->width(), h = subImage->height();
    for (int i = 0; i < w; ++i) {
        for (int j = 0; j < h; ++j) {
            int x = i + ((double)qrand() / RAND_MAX - 0.5) * power;
            int y = j + ((double)qrand() / RAND_MAX - 0.5) * power;
            if (x >= 0 && x < w && y >= 0 && y < h)
                img->setPixel(i, j, subImage->pixel(x, y));
        }
    }

    ui->imageWidget->updateSubImage(img);
}

// some service functions
inline int rgb2grey(QRgb pixel)
{
    return 0.2125*qRed(pixel) + 0.7154*qGreen(pixel) + 0.0721*qBlue(pixel);
}

inline int max(int a, int b)
{
    return a < b ? b : a;
}

inline int min(int a, int b)
{
    return a < b ? a : b;
}
inline double Dmax(double a, double b)
{
    return a < b ? b : a;
}

inline double Dmin(double a, double b)
{
    return a < b ? a : b;
}

void MainWindow::contrastCorrection()
{
    if (subImage) delete subImage;
    subImage = ui->imageWidget->getSubImage();

    int w = subImage->width();
    int h = subImage->height();
    int gray_min = 256, gray_max = 0;

    // find min and max gray value
    for (int x = 0; x < w; ++x) {
        for (int y = 0; y < h; ++y) {
            int gray = rgb2grey(subImage->pixel(x, y));
            if (gray_min > gray) gray_min = gray;
            if (gray_max < gray) gray_max = gray;
        }
    }

    // linear correction
    double k = 255.0 / (gray_max - gray_min);
    for (int x = 0; x < w; ++x) {
        for (int y = 0; y < h; ++y) {
            int gray_old = rgb2grey(subImage->pixel(x, y));
            double gray_new = k * (gray_old - gray_min);
            double K = gray_new / gray_old;

            QRgb pixel = subImage->pixel(x, y);
            int r = min(K*qRed(pixel), 255);
            int g = min(K*qGreen(pixel), 255);
            int b = min(K*qBlue(pixel), 255);

            subImage->setPixel(x, y, qRgb(r, g, b));

        }
    }
    ui->imageWidget->updateSubImage(subImage);
}

// Bilinear
QRgb MainWindow::interpolation(double cur_x, double cur_y, QImage *img)
{
    int x0 = (int)cur_x;
    int y0 = (int)cur_y;
    double x = cur_x - x0;
    double y = cur_y - y0;
    x = Dmin(Dmax(x, 0.0), 1.0);
    y = Dmin(Dmax(y, 0.0), 1.0);

    x0 = min(max(x0, 0.0), img->width() - 2.0);
    y0 = min(max(y0, 0.0), img->height() - 2.0);

    double Red = (1.0 - x)*(1.0 - y) * qRed(img->pixel(x0, y0)) +
            (x)*(1.0 - y) * qRed(img->pixel(x0+1, y0)) +
            (1.0 - x)*(y) * qRed(img->pixel(x0, y0+1)) +
            (x)*(y) * qRed(img->pixel(x0+1, y0+1));

    double Green = (1.0 - x)*(1.0 - y) * qGreen(img->pixel(x0, y0)) +
            (x)*(1.0 - y) * qGreen(img->pixel(x0+1, y0)) +
            (1.0 - x)*(y) * qGreen(img->pixel(x0, y0+1)) +
            (x)*(y) * qGreen(img->pixel(x0+1, y0+1));

    double Blue = (1.0 - x)*(1.0 - y) * qBlue(img->pixel(x0, y0)) +
            (x)*(1.0 - y) * qBlue(img->pixel(x0+1, y0)) +
            (1.0 - x)*(y) * qBlue(img->pixel(x0, y0+1)) +
            (x)*(y) * qBlue(img->pixel(x0+1, y0+1));

    return qRgb(Red, Green, Blue);
}

// autolevels
void MainWindow::autolevels()
{
    QRgb rgb;
    subImage = ui->imageWidget->getSubImage();
    int max_r = 0, max_g = 0, max_b = 0;
    int min_r = 256, min_g = 256, min_b = 256;
    int r, g, b;

    //search min and max for each componentes
    for (int x = 0; x < subImage->width(); ++x) {
        for (int y = 0; y < subImage->height(); ++y) {
            rgb = subImage->pixel(x, y);
            r = qRed(rgb);
            g = qGreen(rgb);
            b = qBlue(rgb);
            max_r = max(r, max_r);
            max_g = max(g, max_g);
            max_b = max(b, max_b);
            min_r = min(r, min_r);
            min_g = min(g, min_g);
            min_b = min(b, min_b);
        }
    }

    // linear correction
    QImage *img = new QImage(*subImage);
    max_r -= min_r;
    max_g -= min_g;
    max_b -= min_b;
    for (int x = 0; x < subImage->width(); ++x) {
        for (int y = 0; y < subImage->height(); ++y) {
            rgb = subImage->pixel(x, y);
            r = 255 * (qRed(rgb) - min_r) / max_r;
            g = 255 * (qGreen(rgb) - min_g) / max_g;
            b = 255 * (qBlue(rgb) - min_b) / max_b;
            img->setPixel(x, y, qRgb(r, g, b));
        }
    }
    ui->imageWidget->updateSubImage(img);
}

void MainWindow::median()
{
    IMG_UPD(subImage, ui->imageWidget->getSubImage());
    IMG_UPD(image, ui->imageWidget->getImg());
    QPoint startPos = ui->imageWidget->getStartPos();
    int w = subImage->width(), h = subImage->height();
    int bigW = image->width(), bigH = image->height();

    QProgressDialog *progress_dialog = new QProgressDialog(tr("Median filter is running. Please wait..."),
                                                           "cancel", 0, w, this);
    progress_dialog->setWindowModality(Qt::WindowModal);
    int progress = 0;

    bool ok_pressed;
    int n = QInputDialog::getInteger(this, tr("Median filter"),
                                     tr("Please input R - power of filter"), 1, 1, 10, 1, &ok_pressed);
    if (!ok_pressed) return;

    unsigned char masR[256], masG[256], masB[256];

    int sum, r, g, b, Q, W;
    int total_count = 0;
    QRgb pixel;
    for (int x = startPos.x(); x < startPos.x() + w; ++x) {
        progress_dialog->setValue(++progress);
        if (progress_dialog->wasCanceled()) return;

        for (int y = startPos.y(); y < startPos.y() + h; ++y) {
            for (int i = 0; i < 256; ++i)
                masR[i] = masG[i] = masB[i] = 0;
            total_count = 0;
            Q = x+n+1; // for optimize
            for (int i = x - n; i < Q; ++i) {
                W = y+n+1;// for optimize
                for (int j = y - n; j < W; ++j) {
                    if (i >=0 && i < bigW && j >=0 && j < bigH) {
                        pixel = image->pixel(i, j);
                        ++masR[qRed(pixel)];
                        ++masG[qGreen(pixel)];
                        ++masB[qBlue(pixel)];
                        ++total_count;
                    }
                }
            }
            // find median colors
            total_count /= 2;
            for (r = 0, sum = 0; sum < total_count; sum+=masR[r], ++r);
            for (g = 0, sum = 0; sum < total_count; sum+=masG[g], ++g);
            for (b = 0, sum = 0; sum < total_count; sum+=masB[b], ++b);
            image->setPixel(x, y, qRgb(r, g, b));
        }
    }

    ui->imageWidget->updateSubImage(&image->copy(startPos.x(), startPos.y(), w, h));
    progress_dialog->setValue(w);
    progress_dialog->update();
    delete progress_dialog;
}

// fast separable realization of gaussian blur
void MainWindow::gaussianBlur()
{
    subImage = ui->imageWidget->getSubImage();
    QImage *img = new QImage(*subImage);
    QPoint startPos = ui->imageWidget->getStartPos();

    int s_w = startPos.x(), s_h = startPos.y(); // startPos width and height
    int w = img->width(), h = img->height();
    IMG_UPD(image, ui->imageWidget->getImg());
    QImage img_full(*image);
    QImage img_tmp(*image);

    bool ok;
    double sigma = QInputDialog::getDouble(this, tr("Gaussian blur"),
                                           tr("Please input sigma = "), 1.0, 0.3, 5.0, 4, &ok);
    if (!ok) sigma = 1.0; // default value

    int R = int(3*sigma);
    const int size = 2*R + 1;

    double kernel[size];
    for (int i = 0; i < size; ++i)
        kernel[i] = 0.0;

    double K = 1.0 / sqrt(2*MATH_PI) / sigma;
    for (int i = 0; i < size; ++i) {
        kernel[i] = K * exp(-(i-R)*(i-R) / 2.0 / sigma / sigma );
    }

    // apply for columns
    for (int x = s_w; x < s_w + w; ++x) {
        for (int y = s_h; y < s_h + h; ++y) {
            double r = 0, g = 0, b = 0;
            for (int i = -R; i <= R; ++i) {
                if (0 <= x+i && x+i < image->width()) {
                    QRgb p = image->pixel(x+i, y);
                    r += qRed(p) * kernel[i+R];
                    g += qGreen(p) * kernel[i+R];
                    b += qBlue(p) * kernel[i+R];
                }
            }
            r = max(min(r, 255), 0);
            g = max(min(g, 255), 0);
            b = max(min(b, 255), 0);

            img_tmp.setPixel(x, y, qRgb(r, g, b));
        }
    }

    // apply for rows
    for (int x = s_w; x < s_w + w; ++x) {
        for (int y = s_h; y < s_h + h; ++y) {
            double r = 0, g = 0, b = 0;
            for (int j = -R; j <= R; ++j) {
                if (0 <= y+j && y+j < image->height()) {
                    QRgb p = img_tmp.pixel(x, y+j);
                    r += qRed(p) * kernel[j+R];
                    g += qGreen(p) * kernel[j+R];
                    b += qBlue(p) * kernel[j+R];
                }
            }
            r = max(min(r, 255), 0);
            g = max(min(g, 255), 0);
            b = max(min(b, 255), 0);
            img_full.setPixel(x, y, qRgb(r, g, b));
        }
    }
    delete subImage;
    subImage = new QImage(img_full.copy(s_w, s_h, w, h));

    ui->imageWidget->updateSubImage(subImage);
}

// R - radius of the kernel
void MainWindow::applyKernel(double **kernel, int R)
{
    QPoint startPos = ui->imageWidget->getStartPos();
    QSize subImageSize = ui->imageWidget->getSubImageSize();

    int s_w = startPos.x(), s_h = startPos.y(); // startPos width and height
    int w = subImageSize.width(), h = subImageSize.height();
    IMG_UPD(image, ui->imageWidget->getImg());
    QImage img(*image);

    for (int x = s_w; x < s_w + w; ++x) {
        for (int y = s_h; y < s_h + h; ++y) {
            double r = 0.0, g = 0.0, b = 0.0;
            for (int i = -R; i <= R; ++i) {
                for (int j = -R; j <= R; ++j) {
                    if (0 <= x+i && x+i < image->width() && 0 <= y+j && y+j < image->height()) {
                        QRgb p = image->pixel(x+i, y+j);
                        r += qRed(p) * kernel[i+R][j+R];
                        g += qGreen(p) * kernel[i+R][j+R];
                        b += qBlue(p) * kernel[i+R][j+R];
                    }
                }
            }
            r = max(min((int)r, 255), 0);
            g = max(min((int)g, 255), 0);
            b = max(min((int)b, 255), 0);
            img.setPixel(x, y, qRgb(r, g, b));
        }
    }
    delete subImage;
    subImage = new QImage(img.copy(s_w, s_h, w, h));

    ui->imageWidget->updateSubImage(subImage);
}

void MainWindow::grayworld()
{
    subImage = ui->imageWidget->getSubImage();
    QImage* img = new QImage(*subImage);
    int w = subImage->width(), h = subImage->height();
    double r_avg = 0.0, g_avg = 0.0, b_avg = 0.0;

    for (int x = 0; x < w; ++x) {
        for (int y = 0; y < h; ++y) {
            QRgb rgb = subImage->pixel(x, y);
            r_avg += qRed(rgb);
            g_avg += qGreen(rgb);
            b_avg += qBlue(rgb);
        }
    }

    r_avg /= w * h;
    g_avg /= w * h;
    b_avg /= w * h;

    double avg = (r_avg + g_avg + b_avg) / 3.0;

    for (int x = 0; x < w; ++x) {
        for (int y = 0; y < h; ++y) {
            QRgb rgb = subImage->pixel(x, y);
            double r = min(1.0 * qRed(rgb) * avg / r_avg, 255);
            double g = min(1.0 * qGreen(rgb) * avg / g_avg, 255);
            double b = min(1.0 * qBlue(rgb) * avg / b_avg, 255);

            img->setPixel(x, y, qRgb(r, g, b));
        }
    }

    ui->imageWidget->updateSubImage(img);
}

void MainWindow::zoom()
{
    subImage = ui->imageWidget->getSubImage();
    int w = subImage->width(), h = subImage->height();
    double K = ui->scaleSpinBox->value();

    QImage *img = new QImage(*ui->imageWidget->getImg());

    QPoint startPos = ui->imageWidget->getStartPos();
    QPoint endPos = ui->imageWidget->getStartPos() + QPoint(w, h);
    QPoint center = (startPos + endPos) / 2;

    QPoint newStartPos = center + (startPos - center) * K;
    QPoint newEndPos = center + (endPos - center) * K;

    if (K < 1.0) {
        // fill black
        for (int x = startPos.x(); x < endPos.x(); ++x) {
            for (int y = startPos.y(); y < endPos.y(); ++y) {
                img->setPixel(x, y, qRgb(0, 0, 0));
            }
        }
    }

    double cur_x, cur_y;
    for (int x = newStartPos.x(); x < newEndPos.x(); ++x) {
        for (int y = newStartPos.y(); y < newEndPos.y(); ++y) {
            cur_x = (x - center.x()) / K + center.x() - startPos.x();
            cur_y = (y - center.y()) / K + center.y() - startPos.y();
            if (x >= 0 && x < img->width() && y >= 0 && y < img->height()) {
                img->setPixel(x, y, interpolation(cur_x, cur_y, subImage));
            }
        }
    }

    ui->imageWidget->updateImage(img);
}

void MainWindow::rotate()
{
    double alpha = ui->rotateAngleSpinBox->value() * MATH_PI / 180.0;

    IMG_UPD(subImage, ui->imageWidget->getSubImage());
    int w = subImage->width(), h = subImage->height();

    QImage *img = new QImage(*ui->imageWidget->getImg());

    QPoint startPos = ui->imageWidget->getStartPos();
    QPoint endPos = ui->imageWidget->getStartPos() + QPoint(w, h);
    QPoint c = (startPos + endPos) / 2; // center

    // korners of selected image are moved to
    double x00 = c.x() + (startPos.x() - c.x())*cos(alpha) - (startPos.y() - c.y())*sin(alpha);
    double y00 = c.y() + (startPos.x() - c.x())*sin(alpha) + (startPos.y() - c.y())*cos(alpha);

    double x01 = c.x() + (endPos.x() - c.x())*cos(alpha) - (startPos.y() - c.y())*sin(alpha);
    double y01 = c.y() + (endPos.x() - c.x())*sin(alpha) + (startPos.y() - c.y())*cos(alpha);

    double x10 = c.x() + (startPos.x() - c.x())*cos(alpha) - (endPos.y() - c.y())*sin(alpha);
    double y10 = c.y() + (startPos.x() - c.x())*sin(alpha) + (endPos.y() - c.y())*cos(alpha);

    double x11 = c.x() + (endPos.x() - c.x())*cos(alpha) - (endPos.y() - c.y())*sin(alpha);
    double y11 = c.y() + (endPos.x() - c.x())*sin(alpha) + (endPos.y() - c.y())*cos(alpha);

    // find the square that contain all new korners
    double newStartX = Dmin(Dmin(x00, x01), Dmin(x10, x11));
    double newStartY = Dmin(Dmin(y00, y01), Dmin(y10, y11));

    double newEndX = Dmax(Dmax(x00, x01), Dmax(x10, x11));
    double newEndY = Dmax(Dmax(y00, y01), Dmax(y10, y11));

    // fill black
    for (int x = startPos.x(); x < endPos.x(); ++x) {
        for (int y = startPos.y(); y < endPos.y(); ++y) {
            img->setPixel(x, y, qRgb(0, 0, 0));
        }
    }

    double cur_x, cur_y;
    for (int x = newStartX; x <= newEndX; ++x) {
        for (int y = newStartY; y <= newEndY; ++y) {
            cur_x = (double)c.x() + ((double)x - c.x())*cos(-alpha) - ((double)y - c.y())*sin(-alpha) - startPos.x();
            cur_y = (double)c.y() + ((double)x - c.x())*sin(-alpha) + ((double)y - c.y())*cos(-alpha) - startPos.y();
            if (x >= 0 && x < img->width() && y >= 0 && y < img->height() &&
                    cur_x >= 0 && cur_x <= subImage->width()+1 &&
                    cur_y >= 0 && cur_y <= subImage->height()+1) {
                img->setPixel(x, y, interpolation(cur_x, cur_y, subImage));
            }
        }
    }

    ui->imageWidget->updateImage(img);

}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About"),
                       tr("<h2>Simple Graphical Editor 1.0</h2>"
                          "<p>Used <a href='http://qt.nokia.com/'>Qt 4.7</a>"
                          "<p>date: <i>19.10.2011</i>"
                          "<p>It is a simple application that "
                          "allows to load/save, rotate, zoom images "
                          "and also to apply the next filters:"
                          "<ul><li>Gaussian blur</li>"
                          "<li>Median</li>"
                          "<li>Gray world</li>"
                          "<li>Autolevels</li>"
                          "<li>Linear contrast correction</li>"
                          "<li>Sharpness</li>"
                          "<li>Glass effect</li>"
                          "<li>Apply custom kernel</li>"
                          "</ul>"
                          ));
}
