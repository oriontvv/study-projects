#ifndef KERNELSETTINGSDIALOG_H
#define KERNELSETTINGSDIALOG_H

#include <QDialog>
#include <QDoubleSpinBox>

namespace Ui {
class KernelSettingsDialog;
}

class KernelSettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit KernelSettingsDialog(QWidget *parent = 0);
    ~KernelSettingsDialog();

    double** getKernel();
    int getSize();

private:
    Ui::KernelSettingsDialog *ui;
    QDoubleSpinBox* kernel[7][7];
    int size;

    void createKernelTable(int size);


public slots:
    void setKernelSize(int value);
private slots:
    void on_pushButton_clicked();
};

#endif // KERNELSETTINGSDIALOG_H
