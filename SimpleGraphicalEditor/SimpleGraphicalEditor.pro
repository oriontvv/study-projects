#-------------------------------------------------
#
# Project created by QtCreator 2011-09-15T21:43:15
#
#-------------------------------------------------

QT       += core gui

TARGET = SimpleGraphicalEditor
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    imageviewer.cpp \
    kernelsettingsdialog.cpp

HEADERS  += mainwindow.h \
    imageviewer.h \
    kernelsettingsdialog.h

FORMS    += mainwindow.ui \
    kernelsettingsdialog.ui
