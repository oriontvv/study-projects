#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QWidget>
#include <QScrollArea>
#include <QLabel>

#include <QDebug>


// a class deal with the drawing image
// class is inherited from QScrollArea and support to select subimage

class ImageViewer : public QScrollArea
{
    Q_OBJECT
public:
    explicit ImageViewer(QWidget *parent = 0);
    void updateImage(QImage* img);
    void updateSubImage(QImage *img);
    QImage* getSubImage();
    QImage* getImg();
    QPoint getStartPos();
    QSize getSubImageSize();

private:
    QLabel* imageLabel;
    QImage* image;  // worimage without any added attribute such a selected rectangle
    QPixmap* pixmap; // image with the mouse's rectangle
    QPoint startSubArea;
    QSize subImgSize;

    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void keyPressEvent(QKeyEvent *);

};

#endif // IMAGEVIEWER_H
