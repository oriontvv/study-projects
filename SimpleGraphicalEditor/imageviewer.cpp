#include "imageviewer.h"
#include <QImage>
#include <QMouseEvent>
#include <QPainter>

#include <QScrollBar>

#define IMG_UPD(old, img) \
    if (old) delete old;\
    old = new QImage(*img);

ImageViewer::ImageViewer(QWidget *parent) :
    QScrollArea(parent)
{
    image = NULL;
    startSubArea = QPoint(0, 0);
    subImgSize = QSize(0, 0);
    pixmap = NULL;

    imageLabel = new QLabel;
    imageLabel->setBackgroundRole(QPalette::Base);
    imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    imageLabel->setScaledContents(true);

    setBackgroundRole(QPalette::Dark);
    setWidget(imageLabel);
}

// set new work image
void ImageViewer::updateImage(QImage *img)
{
    IMG_UPD(image, img);
    startSubArea = QPoint(0, 0);
    subImgSize = img->size();

    if (pixmap) delete pixmap;
    pixmap = new QPixmap(QPixmap::fromImage(*image));

    imageLabel->setPixmap(*pixmap);
    imageLabel->resize(img->size());
}

void ImageViewer::updateSubImage(QImage *img)
{
    subImgSize = img->size();

    QPainter p(image);
    p.drawImage(startSubArea, *img);

    QImage imgWithRect(*image);
    QPainter pp(&imgWithRect);
    pp.setPen(QPen(Qt::green, 2));
    pp.drawRect(startSubArea.x(), startSubArea.y(), img->width(), img->height());

    if (pixmap) delete pixmap;
    pixmap = new QPixmap(QPixmap::fromImage(imgWithRect));

    imageLabel->setPixmap(*pixmap);
}

void ImageViewer::mousePressEvent(QMouseEvent *e)
{
    if (image) {
        startSubArea = e->pos() + QPoint(horizontalScrollBar()->value(),
                                         verticalScrollBar()->value());
        setCursor(Qt::CrossCursor);
    }
}

void ImageViewer::mouseMoveEvent(QMouseEvent *e)
{
    if (image) {
        QPixmap p = QPixmap(QPixmap::fromImage(*image));
        QPainter painter(&p);

        painter.setPen(QPen(Qt::green, 2));
        painter.drawRect(startSubArea.x(),
                         startSubArea.y(),
                         e->pos().x() + horizontalScrollBar()->value() - startSubArea.x(),
                         e->pos().y() + verticalScrollBar()->value() - startSubArea.y());

        imageLabel->setPixmap(p);
    }
}

void ImageViewer::mouseReleaseEvent(QMouseEvent *e)
{
    if (image) {
        subImgSize = QSize(e->pos().x() + horizontalScrollBar()->value() - startSubArea.x(),
                           e->pos().y() + verticalScrollBar()->value() - startSubArea.y());

        if (subImgSize.width() < 0) {
            startSubArea.setX(startSubArea.x() + subImgSize.width());
            subImgSize.setWidth(-subImgSize.width());
        }
        if (subImgSize.height() < 0) {
            startSubArea.setY(startSubArea.y() + subImgSize.height());
            subImgSize.setHeight(-subImgSize.height());
        }
        setCursor(Qt::ArrowCursor);
    }
}

void ImageViewer::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape) {
        startSubArea = QPoint(0, 0);
        subImgSize = image->size();

        QPixmap p = QPixmap(QPixmap::fromImage(*image));
        imageLabel->setPixmap(p);
    }
}

QImage* ImageViewer::getSubImage()
{
    QImage *img = new QImage(image->copy(startSubArea.x(), startSubArea.y(),
                                         subImgSize.width(), subImgSize.height() ));
    return img;
}

QPoint ImageViewer::getStartPos()
{
    return startSubArea;
}

QImage* ImageViewer::getImg()
{
    return image;
}

QSize ImageViewer::getSubImageSize()
{
    return subImgSize;
}
