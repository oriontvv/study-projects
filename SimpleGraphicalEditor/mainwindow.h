#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "kernelsettingsdialog.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    QString fileName;
    QImage* originalImage; // normal scaling
    QImage* image;  // work image
    QImage* subImage;
    QPoint subImageStartPosition;
    KernelSettingsDialog *kernelSettingsDialog;

    QImage* getSelectedRegion();
    void makeConnects();
    void applyKernel(double** kernel, int R);
    QRgb interpolation(double x, double y, QImage* subImg);

private slots:
    void open();
    void save();

    void showNormal();
    void zoom();
    void rotate();

    void kernel();
    void sharpness();
    void glass();
    void contrastCorrection();
    void autolevels();
    void median();
    void gaussianBlur();
    void grayworld();

    void about();
};

#endif // MAINWINDOW_H



