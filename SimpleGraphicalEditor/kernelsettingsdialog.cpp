#include "kernelsettingsdialog.h"
#include "ui_kernelsettingsdialog.h"

#define MAX_KERNEL_SIZE 7
#define MIN_KERNEL_SIZE 2

KernelSettingsDialog::KernelSettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::KernelSettingsDialog)
{
    ui->setupUi(this);
    connect(ui->sizeComboBox, SIGNAL(activated(int)), SLOT(setKernelSize(int)));

    size = 0;
    for (int i = 0; i < 7; ++i) {
        for (int j = 0; j < 7; ++j) {
            QDoubleSpinBox* cell = new QDoubleSpinBox();
            cell->setValue(0.0);
            cell->setMinimum(-100);
            ui->kernelWidget->addWidget(cell, i, j);
            kernel[i][j] = cell;
        }
    }
}

KernelSettingsDialog::~KernelSettingsDialog()
{
    delete ui;
}

void KernelSettingsDialog::setKernelSize(int value)
{
    size = value*2 + 3; // from ui combobox (0,1,2 -> 3,5,7)
    if (size != 3 && size != 5 && size != 7) {
        // incorrect kernel size
        return;
    }
    createKernelTable(size);
}

void KernelSettingsDialog::createKernelTable(int size)
{
    // hide previous kernel
    for (int i = 0; i < 7; ++i) {
        for (int j = 0; j < 7; ++j) {
            kernel[i][j]->hide();
        }
    }

    // show only new kernel
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            kernel[i][j]->show();
        }
    }
}

int KernelSettingsDialog::getSize()
{
    return size; // return Radius
}

double** KernelSettingsDialog::getKernel()
{
    double** k = new double*[size];
    double sum = 0.0;
    for (int i = 0; i < size; ++i) {
        k[i] = new double[size];
        for (int j = 0; j < size; ++j) {
            k[i][j] = kernel[i][j]->value();
            sum += k[i][j];
        }
    }

    // normalize
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            k[i][j] /= sum;
        }
    }

    return k;
}


void KernelSettingsDialog::on_pushButton_clicked()
{
    for (int i = 0; i < MAX_KERNEL_SIZE; ++i)
        for (int j = 0; j < MAX_KERNEL_SIZE; ++j)
            kernel[i][j]->setValue(0);
}
