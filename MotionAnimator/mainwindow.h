#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItemAnimation>
#include <QTimer>
#include <QTimeLine>
#include <QThread>

#include "object.h"
#include "movingobjects.h"

#include "optionsdialog.h"
#include "filterdialog.h"

// normal ~ 1200
#define SLOW 500
#define NORMAL 100
#define FAST 100

namespace Ui {
    class MainWindow;
}

struct Motion {
    long long id;
    int from;
    int to;
    int month;
};

enum AnimationState {
    BACKWARD = -1, PAUSE = 0, FORWARD = 1
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    void keyPressEvent(QKeyEvent *e);
    QGraphicsScene *scene;
    QGraphicsView *view;
    OptionsDialog *optionsDialog;
    FilterDialog *filterDialog;

    QVector <long long> ids;
    QVector <long long> obj_ids;
    QMap <int, QString> *regions;
    QVector <Motion> *motions;
    QVector <Motion> *fmotions;
    QVector <QPoint> m_centers;
    QVector <Object *> stObjects;
    QVector <QVector <MovingObjects *> > mvObjects;
    QVector <QVector <QGraphicsItemAnimation *> > anmvObjects;
    QVector <QGraphicsItemAnimation *> anstObjects;
    QVector <MovingObjects***> movTable;

    AnimationState anState;

    QTimer *timer;
    QTimeLine loc_timer;
    QString reg_name, mov_name;

    int cur_month;
    int animationSpeed;
    int time_max;
    int ids_size; // ids.size
    long long max_id;
    QVector<int>mvCount;
    void parseRegions(QString fileName);
    void parseMotions(QString fileName);
    void parseCenters(QString fileName);

    void animation();
    void filter(int dir);

    bool incMonth();
    bool decMonth();
    void calculateMotions();
    void initObjects();
    void allocateMemoryForMotions();
    void calculateCurentMotions(int dir);
    inline int id2myid(long long);
    inline long long myid2id(int);
    inline int objids2myid(long long);
    inline long long myids2objid(int);


public slots:
    void on_properties_activated();
    void on_Filter_clicked();
    void on_Forward_clicked();
    void on_Pause_clicked();
    void on_Back_clicked();
    void on_loadMotions_triggered();
    void on_loadRegions_triggered();
    void changeAnimationSpeed(int);
    void setCurMonth(int);

    void on_timeout();
    void on_hotkeys_triggered();
};

#endif // MAINWINDOW_H
