#include "object.h"
#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>

Object::Object()
{
    init();
    center.setX(0);
    center.setY(0);
    regionId = 0;
}

QRectF Object::boundingRect() const
{
    qreal adjust = 0.5;
    return QRectF(-18 - adjust, -22 - adjust, 36 + adjust, 60 + adjust);
}

QPainterPath Object::shape() const
{
    QPainterPath path;
    path.addRect(-10, -20, 20, 40);
    return path;
}

void Object::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if (!visibled || !filtered) return;

    // background
    //painter->drawImage(center.x() - 12, center.y() - 12, QImage(":/images/object.png"));

    int x_lu = center.x(), y_lu = center.y();
    int w = 15, h = 40;

    // in
//    painter->drawRect(x_lu - w, y_lu - h/2, w, h);
    // out
//    painter->drawRect(x_lu, y_lu - h/2, w, h);

    // count height in & out
    int h_in = countIn * 30 / (countIn + countOut);
    int h_out = countOut * 30 / (countIn + countOut);

    painter->setBrush(Qt::green);
    painter->drawRect(x_lu - w, y_lu + h/2 - h_in, w, h_in + 2);

    painter->setBrush(Qt::red);
    painter->drawRect(x_lu, y_lu + h/2 - h_out, w, h_out + 2);


    painter->setFont(QFont("Tahoma", 7, 2));
    painter->setBrush(Qt::white);
    //in
//    painter->drawText(x_lu - w, y_lu - h/2, w, 10, Qt::AlignCenter, QString::number(countIn));
    painter->drawText(x_lu - w, y_lu + h/2 - 10, w, 10, Qt::AlignCenter, QString::number(countIn));
    // out
//    painter->drawText(x_lu, y_lu - h/2, w, 10, Qt::AlignCenter, QString::number(countOut));
    painter->drawText(x_lu, y_lu + h/2 - 10, w, 10, Qt::AlignCenter, QString::number(countOut));
//    painter->drawText(center.x(), center.y() + 7, "in:"+QString::number(countIn) + ",out:"+ QString::number(countOut));

//    painter->setBrush(Qt::red);
//    painter->drawEllipse(center, 5, 5);
}

void Object::advance(int step)
{

}

void Object::init()
{
    countIn = 0;
    countOut = 0;
    visibled = false;
    filtered = true;
}

void Object::incFromCount(int c)
{
    countOut += c;
    if (countOut <= 0) countOut = 0;
    updateVisibility();
}

void Object::incToCount(int c)
{
    countIn += c;
    if (countIn <= 0) countIn = 0;
    updateVisibility();
}

void Object::decFromCount(int c)
{
    countOut -= c;
    if (countOut <= 0) countOut = 0;
    updateVisibility();
}

void Object::decToCount(int c)
{
    countIn -= c;
    if (countIn <= 0) {countIn = 0;}
    updateVisibility();
}
