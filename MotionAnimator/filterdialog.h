#ifndef FILTERDIALOG_H
#define FILTERDIALOG_H

#include <QDialog>
#include <QCheckBox>

namespace Ui {
    class FilterDialog;
}

class FilterDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FilterDialog(QWidget *parent = 0 );
    void addRegionNames(QVector<QString> &region_names);
    ~FilterDialog();
    QVector <QCheckBox*> from;
    QVector <QCheckBox*> to;

    QVector <bool> getDirFrom();
    QVector <bool> getDirTo();


    int getMonthFrom();
    int getMonthTo();

    int getCountFrom();
    int getCountTo();

    bool getCount();
    void setCount(bool f);


private:
    Ui::FilterDialog *ui;

public slots:
    void fromAll(bool checked);
    void toAll(bool checked);

private slots:
    void on_Count_clicked(bool checked);
    void on_countToSpinBox_valueChanged(int arg1);
    void on_countSpinBox_valueChanged(int arg1);
};

#endif // FILTERDIALOG_H
