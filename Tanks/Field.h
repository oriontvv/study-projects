#ifndef FIELD_H
#define FIELD_H

#include <QWidget>
#include <QImage>

class Field : public QWidget
{
    Q_OBJECT

public:
    Field(QWidget *parent = 0);
    ~Field();
    QImage *img;

private:
    void paintEvent(QPaintEvent *event);

};

#endif // FIELD_H
