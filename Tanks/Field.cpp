#include "Field.h"

#include <QPainter>

Field::Field(QWidget *parent) : QWidget(parent)
{
    setFixedSize(640, 480);
    img = new QImage(640, 480, QImage::Format_ARGB32);
}

Field::~Field()
{
    delete img;
}

void Field::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.drawImage(0, 0, *img);
    painter.end();
}
