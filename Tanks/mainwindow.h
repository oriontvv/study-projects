#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <tank.h>
#include <Field.h>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::MainWindow *ui;
    enum GameState{
        GS_NULL,
        GS_PREPARE_SHOOT,
        GS_SHOOTING,
        GS_END
    }gameState;

    Tank *tank1;
    Tank *tank2;
//    Field *field;

    void Start();
    void drawTank(QPainter &painter);

    QString gameStateInfo(GameState n);
public slots:
    void reDrawField();
};

#endif // MAINWINDOW_H
