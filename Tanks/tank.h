#ifndef TANK_H
#define TANK_H

#include <QObject>
#include <QString>
#include <QImage>

struct Point2D{
    int x;
    int y;
};

class Tank : public QObject
{
    Q_OBJECT

public:
    Tank(const QString &base_name, const QString &gun_name);
    ~Tank();
    QImage *base_img, *gun_img;
    Point2D base_lu;//left up coordinate;
    Point2D gun_lu;//left up coordinate;
    int power;
    int angle;
    int health;

public slots:
    void setPower(int value);
    void setAngle(int value);
    void setHealth(int value);
};

#endif // TANK_H
