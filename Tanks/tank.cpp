#include "tank.h"
#include <QDebug>

Tank::Tank(const QString &base_name, const QString &gun_name)
    : power(0), angle(0), health(100)
{
    base_img = new QImage(base_name, "png");
    gun_img = new QImage(gun_name, "PNG");
//    qDebug()<<"base_img->width = " << base_img->width();

}

Tank::~Tank()
{
    delete gun_img;
    delete base_img;
}

void Tank::setAngle(int value)
{
    angle = value;
}

void Tank::setPower(int value)
{
    power = value;
}

void Tank::setHealth(int value)
{
    health = value;
}
