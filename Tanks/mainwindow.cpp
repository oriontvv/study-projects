#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QPainter>
#include <QBrush>

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    gameState = GS_NULL;
    tank1 = new Tank("pictures\\tank\\base1.png", "pictures\\tank\\_gun1.png");
    tank1->base_lu.x = 50;
    tank1->base_lu.y = 400;

    tank1->gun_lu.x = tank1->base_lu.x + tank1->base_img->width() / 2;
    tank1->gun_lu.y = tank1->base_lu.y;


    tank2 = new Tank("pictures\\tank\\base2.png", "pictures\\tank\\gun2.png");
    tank2->base_lu.x = ui->FieldWidget->width() - 200;
    tank2->base_lu.y = 400;


    Start();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete tank1;
    delete tank2;
//    delete field;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
            ui->retranslateUi(this);
            break;
        default:
            break;
    }
}

void MainWindow::Start()
{
    gameState = GS_PREPARE_SHOOT;
    while(gameState != GS_END){
        ui->GameStateLabel->setText(gameStateInfo(gameState));
        gameState = GS_END;
    }
    ui->GameStateLabel->setText(gameStateInfo(gameState));
    reDrawField();
}

void MainWindow::drawTank(QPainter &painter)
{
    painter.scale(0.9, 0.9);

    tank1->setAngle(ui->spinBoxAngle->value());
    tank1->setPower(ui->spinBoxPower->value());

    painter.save();

    painter.translate(tank1->gun_lu.x + tank1->gun_img->width() / 2,
                      tank1->gun_lu.y + tank1->gun_img->height() / 2);
    painter.rotate(-1.8 * tank1->angle);
    painter.drawImage(-20, -tank1->gun_img->height() / 2, *tank1->gun_img);

    painter.restore();

    painter.drawImage(tank1->base_lu.x, tank1->base_lu.y, *tank1->base_img);
    painter.drawImage(tank2->base_lu.x, tank2->base_lu.y, *tank2->base_img);
}

void MainWindow::reDrawField()
{
    QPainter painter(ui->FieldWidget->img);
    painter.fillRect(0, 0, ui->FieldWidget->width(), ui->FieldWidget->height(), QBrush(Qt::gray));

    drawTank(painter);

    painter.end();
    ui->FieldWidget->update();
}

QString MainWindow::gameStateInfo(GameState n)
{
    switch(n){
        case GS_END:
            return QString("Game is over!");
            break;
        case GS_NULL:
            return QString("Game not start yet!");
            break;
        case GS_PREPARE_SHOOT:
            return QString("Player preparing to shooting...");
            break;
        case GS_SHOOTING:
            return QString("Player shooting...");
            break;
        default:
            return  QString("Error: incorrect gameState!");
            break;
    }
}

