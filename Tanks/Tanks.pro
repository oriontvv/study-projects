# -------------------------------------------------
# Project created by QtCreator 2010-01-28T01:42:47
# -------------------------------------------------
TARGET = Tanks
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    Field.cpp \
    tank.cpp
HEADERS += mainwindow.h \
    Field.h \
    tank.h
FORMS += mainwindow.ui
