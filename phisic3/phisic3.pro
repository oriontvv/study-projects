# -------------------------------------------------
# Project created by QtCreator 2009-12-10T19:55:10
# -------------------------------------------------
TARGET = phisic3
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    InputWidget.cpp \
    OutputWidget.cpp \
    FftWrapper.cpp
HEADERS += mainwindow.h \
    InputWidget.h \
    OutputWidget.h \
    FftWrapper.h
FORMS += mainwindow.ui
