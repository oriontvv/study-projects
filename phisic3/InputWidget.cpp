#include "InputWidget.h"
#include <QPainter>
#include <QMouseEvent>
#include <QCursor>
#include <QPixmap>
#include <QPen>
#include <QBrush>
#include <cmath>
#include <QMessageBox>

InputWidget::InputWidget(QWidget *parent) : QWidget(parent)
{
    cursor_img = new QImage(30, 30, QImage::Format_ARGB32);

    cursor_img->load("pen.png", "png");
    cursor = new QCursor(QPixmap::fromImage(*cursor_img));
    setCursor(*cursor);
    img = new QImage(width(), height(), QImage::Format_ARGB32);
    for(int x = 0; x < width(); ++x)
        for(int y = 0; y < height(); ++y)
            img->setPixel(x, y, Qt::gray);
}

InputWidget::~InputWidget()
{
    if (img) delete img;
    if (cursor_img) delete cursor_img;
}

void InputWidget::resizeImage()
{
    if (img->width() == width() && img->height() == height()) return;
    QImage *tmp = new QImage(width(), height(), QImage::Format_ARGB32);
    for(int x = 0; x < img->width(); ++x)
        for(int y = 0; y < img->height(); ++y)
            if (qRed(img->pixel(x, y)) == 255) tmp->setPixel(x, y, Qt::red);
    delete img;
    img = tmp;
}

void InputWidget::paintEvent(QPaintEvent *event)
{
//    qDebug() << "Input::update";

    resizeImage();

    QPainter painter(this);

    painter.fillRect(1, 1, width() - 2, height() - 2 ,QBrush(Qt::gray));
    painter.setPen(QPen(Qt::black, 2));
    painter.drawRect(0, 0, width() - 1, height() - 1);

//    painter.setWindow(-width() / 2, height() / 2, width(), -height() );
    painter.drawLine(0, height() / 2, width(), height() / 2);

    painter.drawImage(0, 0, *img);
    painter.end();

}

void InputWidget::mouseMoveEvent(QMouseEvent *event)
{
    QPainter painter(img);
    painter.setPen(QPen(QColor(Qt::red)));
    if (pow(last_x - event->x(), 2) + pow(last_y - event->y(), 2) <= 7000)
    painter.drawLine(last_x, last_y, event->x(), event->y());
    last_x = event->x();
    last_y = event->y();

    painter.end();
    update();
}

void InputWidget::clearInput()
{
    if (img) delete img;
    img = new QImage(width(), height(), QImage::Format_ARGB32);
    for(int x = 0; x < width(); ++x)
        for(int y = 0; y < height(); ++y)
            img->setPixel(x, y, Qt::gray);
    qDebug() << "Input::clear";
    update();
}

void InputWidget::setMode(int n)
{
    clearInput();
    mode = n;
    QPainter painter(img);
    painter.setPen(QPen(QColor(Qt::red)));
    if (n == 0) {
        //manual
        return;
    }
    else if (n == 1){
        // exp
        int x = 0, y = height()/2;
        for(int x = 0; y > 0; x += 1){
            float  y1 = -exp(1.0*(x + 1)) + height()/2;
            painter.drawLine(x, y, x + 1, y1);
            y = y1;
        }
    }
    else if (n == 2){
        // exp^-x2
        int x = 0, y = height()/2;
        for(x = 0; y > 0; x += 1){
            float q = 100 * exp(-(x + 1.0)*(x + 1.0));
            if (q < 0.0001) break;
            float  y1 = -q + height()/2;
            painter.drawLine(x, y, x + 1, y1);
            y = y1;
        }
        painter.drawLine(x, height()/2, width(), height()/2);
    }


    else if (n == 3){
        // sin
        int x, y = height() / 2;
        for(int x = 0; x < width()-1; ++x){
            int  y1 = -100.0*sin(1.0*(x + 1)/10.0) + height() / 2;
            painter.drawLine(x, y, x + 1, y1);
            y = y1;
        }
    }
    else if (n == 4){
        // cos
        int x = 0, y = height() / 2 - 5;
        for(int x = 0; x < width(); ++x){
            int  y1 = -10*cos(x + 1) + height() / 2;
            painter.drawLine(x, y, x + 1, y1);
            y = y1;
        }
    }
    else if (n == 5){
        // x
        int x = 0, y = height();
        for(int x = 0; ; x += 1){
            int  y1 = -(x+1) + height();
            if (y1 < 0) break;
            painter.drawLine(x, y, x + 1, y1);
//            qDebug() << "x = "<<x << "\ty = " <<y
            y = y1;
        }
    }
    else if (n == 6){
        // x
        int x = 0, y = height();
        for(int x = 0; ; x += 1){
            int  y1 = -(x+1)*(x+1) + height();
            if (y1 < 0) break;
            painter.drawLine(x, y, x + 1, y1);
//            qDebug() << "x = "<<x << "\ty = " <<y
            y = y1;
        }
    }

    painter.end();

}

float *InputWidget::getData()
{
    float *data_float = new float[img->width()];
    for(int i = 0; i < img->width(); ++i)
        data_float[i] = 0.0;
    switch(mode){
        int x;
        case 0://manual - get from img
            for(x = 0; x < img->width(); ++x){
                int count = 0;
                for(int y = img->height() - 1; y >= 0; --y)
                    if(qRed(img->pixel(x, y)) == 255){
                        data_float[x] = height() / 2 - y;
                        count++;break;
                        if (count > 2){
                            delete data_float;
                            qDebug() << "ERROR in "<<x;
                            return NULL;
                            break;
                        }
                    }
            }
            break;
        case 1://exp

            for(x = 0; x < img->width(); ++x){
                data_float[x] = exp(1.0*x);
                if (data_float[x] > height()) break;
            }
            for(int i = x; i < img->width(); ++i)
                data_float[i] = 0.0;
            break;
        case 2://exp^(-x^2)
            for(x = 0; 1; x += 1){
                data_float[x] = 100 * exp(-(x + 1.0)*(x + 1.0));
                if (data_float[x] < 0.0001) break;
            }
            for(int i = x; i < img->width(); ++i)
                data_float[i] = 0.0;
            break;
        case 3://sin
            for(int x = 0; x < img->width(); ++x)
                data_float[x] = sin(-1.0*x/10);
            break;
        case 4://cos
            for(int x = 0; x < img->width(); ++x)
                data_float[x] = cos(-1.0*x/10);
            break;
        case 5://x
            for(int x = 0; x < img->height(); ++x)
                data_float[x] = x;
            for(int i = img->height(); i < img->width(); ++i)
                data_float[i] = 0.0;
            break;
        case 6://x^2
            for(int x = 0; x < sqrt(img->height()); ++x)
                data_float[x] = x*x;
            break;
    }

    return data_float;
}

