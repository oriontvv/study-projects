#ifndef INPUTWIDGET_H
#define INPUTWIDGET_H

#include <QWidget>
#include <QImage>
#include <QDebug>
#include <QCursor>

class InputWidget : public QWidget
{
    Q_OBJECT

private:
    QImage *img, *cursor_img;
    QCursor *cursor;
    void resizeImage();
    int last_x, last_y;
    int mode;

public:
    InputWidget(QWidget *parent = NULL);
    ~InputWidget();
    float *getData();

public slots:
    void clearInput();
    void setMode(int);

protected:
    void paintEvent(QPaintEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
};

#endif // INPUTWIDGET_H
