/****************************************************************************
** Meta object code from reading C++ file 'OutputWidget.h'
**
** Created: Sun 13. Dec 04:40:10 2009
**      by: The Qt Meta Object Compiler version 61 (Qt 4.5.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../OutputWidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'OutputWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 61
#error "This file was generated using the moc from 4.5.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_OutputWidget[] = {

 // content:
       2,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   12, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x0a,
      30,   28,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_OutputWidget[] = {
    "OutputWidget\0\0clearOutput()\0n\0"
    "setSize(int)\0"
};

const QMetaObject OutputWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_OutputWidget,
      qt_meta_data_OutputWidget, 0 }
};

const QMetaObject *OutputWidget::metaObject() const
{
    return &staticMetaObject;
}

void *OutputWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OutputWidget))
        return static_cast<void*>(const_cast< OutputWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int OutputWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: clearOutput(); break;
        case 1: setSize((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
