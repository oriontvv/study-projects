#ifndef OUTPUTWIDGET_H
#define OUTPUTWIDGET_H

#include <QWidget>
#include <QImage>
#include <QDebug>

class OutputWidget : public QWidget
{
    Q_OBJECT

private:
    QImage *img;
    int size;
public:
    OutputWidget(QWidget *parent = NULL);
    ~OutputWidget();
    void setData(float *data);

public slots:
    void clearOutput();
    void setSize(int n);

protected:
    void paintEvent(QPaintEvent *event);
};

#endif // OUTPUTWIDGET_H
