#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui>
#include <QSpinBox>

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void runFft();
    void about();
    void setSize(int);
private:
    Ui::MainWindow *ui;
    int size;

};

#endif // MAINWINDOW_H
