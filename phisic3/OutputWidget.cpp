#include "OutputWidget.h"
#include <QPainter>
#include <cmath>

OutputWidget::OutputWidget(QWidget *parent) : QWidget(parent)
{
    img = new QImage(width(), height(), QImage::Format_ARGB32);
    for(int x = 0; x < width(); ++x)
        for(int y = 0; y < height(); ++y)
            img->setPixel(x, y, Qt::gray);
    size = 1024;
}

OutputWidget::~OutputWidget()
{
    if (img) delete img;
}

void OutputWidget::paintEvent(QPaintEvent *event)
{
//    qDebug() << "Output::update";
    QPainter painter(this);

    painter.fillRect(1, 1, width() - 2, height() - 2 ,QBrush(Qt::gray));
    painter.setPen(QPen(Qt::black, 2));
    painter.drawRect(0, 0, width() - 1, height() - 1);

    painter.drawImage(0, 0, *img);
    painter.end();

}

void OutputWidget::clearOutput()
{
    if (img) delete img;
    img = new QImage(width(), height(), QImage::Format_ARGB32);
    for(int x = 0; x < width(); ++x)
        for(int y = 0; y < height(); ++y)
            img->setPixel(x, y, Qt::gray);
    update();
}
void OutputWidget::setSize(int n)
{
    if (n == 0) size = 1024;
    else if (n == 1) size = 2048;
    else if (n == 2) size = 4096;
    else if (n == 3) size = 8192;
}
void OutputWidget::setData(float *data)
{
    if (img) delete img;
    img = new QImage(width(), height(), QImage::Format_ARGB32);
    for(int x = 0; x < width(); ++x)
        for(int y = 0; y < height(); ++y)
            img->setPixel(x, y, Qt::gray);

    QPainter painter(img);
    painter.setPen(QPen(QColor(Qt::blue), 1));
    int q = (size/2 > width()) ? width() : size/2;
    for(int x = 0; x < q; ++x)
//        painter.drawPoint(x,  height() / 2 - data[x]);
//        if(data[x+1] < height())
            painter.drawLine(x,  height() - data[x], x + 1,  height() - data[x+1]);

    painter.end();

//    qDebug() << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\noutput\n";
//    for(int x = 0; x < 100; ++x)
//       qDebug() << "x = " << x << "\ty = " << data[x];


    update();
}
