#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <FftWrapper.h>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("Windows-1251"));

    ui->setupUi(this);
    QStringList lst, lst1;
    lst << tr("�������") << "exp"  << "exp^(-x^2)" << "sin" << "cos" << "x" << "x^2";
    ui->comboBoxMode->addItems(lst);
    ui->comboBoxMode->setCurrentIndex(0);

    lst1 << "1" << "2" << "3" << "4";
    ui->comboBoxSize->addItems(lst1);
    ui->comboBoxSize->setCurrentIndex(0);
    size = 1024;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setSize(int n)
{
    if (n == 0) size = 1024;
    else if (n == 1) size = 2048;
    else if (n == 2) size = 4096;
    else if (n == 3) size = 8192;
    ui->widgeOutput->setSize(n);
}

void MainWindow::runFft()
{
    float *data;

    qDebug() << "fft go!!!!";
    data = ui->widgetInput->getData();
    if (data == NULL){
        QMessageBox::critical(0, tr("������!"), tr("������������ ������� ������"), QMessageBox::Ok);
        return;
    }


    int len = size;// ui->widgetInput->width();
    float tmp[len];
    for(int i = 0; i < ui->widgetInput->width(); ++i)
        tmp[i] = data[i];
    for(int i = ui->widgetInput->width(); i < len; ++i)
        tmp[i] = 0.0;


    // modify amplitudes
    Fft fft;
    float magnitudes[len/2+1];//1024 / 2 + 1
    fft.SetMode(FFT_REAL, len);
    Cmplx *spectrogram = new Cmplx[len];

    //qDebug() << "####################################################\n input";
    //for(int i=0; i<len/2; ++i)
     //   qDebug() << "tmp["<<i<<"] = " << tmp[i];

    fft.FftReal(tmp, spectrogram);

    for(int i = 0; i <= len/2; ++i)
        magnitudes[i] = sqrt(pow(spectrogram[i].re, 2.0) + pow(spectrogram[i].im, 2.0));


    ui->widgeOutput->setData(magnitudes);
    //free memory
    delete  spectrogram;
}

void MainWindow::about()
{
    QMessageBox::information(0,
            "About", tr(
            "<h3><font color = blue><u>������</u><font color = black>:<b>�������������� �����</b><br>"
            "<u><font color = blue>����� </u>  :<font color = black><b>������� �. �.<\b>     <br>"
            "<u><font color = blue>������</u>  :  <font color = black><b>325<\b>     <br>"
            "<u><font color = blue>mailto</u> :   <font color = black><b>taranov.vv@gmail.com<\b>     <br>"));
}
