/********************************************************************************
** Form generated from reading ui file 'mainwindow.ui'
**
** Created: Sun 13. Dec 04:39:55 2009
**      by: Qt User Interface Compiler version 4.5.3
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QStatusBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "InputWidget.h"
#include "OutputWidget.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *label;
    InputWidget *widgetInput;
    QFrame *line;
    QLabel *label_2;
    OutputWidget *widgeOutput;
    QFrame *line_2;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_3;
    QComboBox *comboBoxMode;
    QSpacerItem *verticalSpacer_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *labelSize;
    QComboBox *comboBoxSize;
    QSpacerItem *verticalSpacer_3;
    QPushButton *pushButtonClear;
    QPushButton *pushButton;
    QSpacerItem *verticalSpacer;
    QPushButton *pushButtonAbout;
    QPushButton *pushButtonExit;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(770, 400);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout_2 = new QHBoxLayout(centralWidget);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setMargin(11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setStyleSheet(QString::fromUtf8("color: rgb(255, 0, 0);\n"
"font: 75 italic 14pt \"MS Shell Dlg 2\";"));

        verticalLayout_2->addWidget(label);

        widgetInput = new InputWidget(centralWidget);
        widgetInput->setObjectName(QString::fromUtf8("widgetInput"));

        verticalLayout_2->addWidget(widgetInput);

        line = new QFrame(centralWidget);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_2->addWidget(line);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setStyleSheet(QString::fromUtf8("color: rgb(0, 0, 255);\n"
"font: 75 italic 14pt \"MS Shell Dlg 2\";"));

        verticalLayout_2->addWidget(label_2);

        widgeOutput = new OutputWidget(centralWidget);
        widgeOutput->setObjectName(QString::fromUtf8("widgeOutput"));

        verticalLayout_2->addWidget(widgeOutput);

        verticalLayout_2->setStretch(0, 1);
        verticalLayout_2->setStretch(1, 15);
        verticalLayout_2->setStretch(3, 1);
        verticalLayout_2->setStretch(4, 15);

        horizontalLayout_2->addLayout(verticalLayout_2);

        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);

        horizontalLayout_2->addWidget(line_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        verticalLayout_4->addWidget(label_3);

        comboBoxMode = new QComboBox(centralWidget);
        comboBoxMode->setObjectName(QString::fromUtf8("comboBoxMode"));

        verticalLayout_4->addWidget(comboBoxMode);


        verticalLayout->addLayout(verticalLayout_4);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        labelSize = new QLabel(centralWidget);
        labelSize->setObjectName(QString::fromUtf8("labelSize"));

        verticalLayout_3->addWidget(labelSize);

        comboBoxSize = new QComboBox(centralWidget);
        comboBoxSize->setObjectName(QString::fromUtf8("comboBoxSize"));

        verticalLayout_3->addWidget(comboBoxSize);


        verticalLayout->addLayout(verticalLayout_3);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_3);

        pushButtonClear = new QPushButton(centralWidget);
        pushButtonClear->setObjectName(QString::fromUtf8("pushButtonClear"));

        verticalLayout->addWidget(pushButtonClear);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout->addWidget(pushButton);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        pushButtonAbout = new QPushButton(centralWidget);
        pushButtonAbout->setObjectName(QString::fromUtf8("pushButtonAbout"));

        verticalLayout->addWidget(pushButtonAbout);

        pushButtonExit = new QPushButton(centralWidget);
        pushButtonExit->setObjectName(QString::fromUtf8("pushButtonExit"));

        verticalLayout->addWidget(pushButtonExit);


        horizontalLayout_2->addLayout(verticalLayout);

        horizontalLayout_2->setStretch(0, 10);
        horizontalLayout_2->setStretch(2, 2);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 770, 21));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);
        QObject::connect(pushButtonExit, SIGNAL(clicked()), MainWindow, SLOT(close()));
        QObject::connect(pushButtonClear, SIGNAL(clicked()), widgetInput, SLOT(clearInput()));
        QObject::connect(pushButton, SIGNAL(clicked()), MainWindow, SLOT(runFft()));
        QObject::connect(pushButtonClear, SIGNAL(clicked()), widgeOutput, SLOT(clearOutput()));
        QObject::connect(comboBoxMode, SIGNAL(activated(int)), widgetInput, SLOT(setMode(int)));
        QObject::connect(pushButtonAbout, SIGNAL(clicked()), MainWindow, SLOT(about()));
        QObject::connect(comboBoxSize, SIGNAL(activated(int)), MainWindow, SLOT(setSize(int)));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "\320\237\321\200\320\265\320\276\320\261\321\200\320\260\320\267\320\276\320\262\320\260\320\275\320\270\320\265 \320\244\321\203\321\200\321\214\320\265", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "\320\222\321\205\320\276\320\264\320\275\320\276\320\271 \321\201\320\270\320\263\320\275\320\260\320\273", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\205\320\276\320\264\320\275\320\276\320\271 \321\201\320\270\320\263\320\275\320\260\320\273", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindow", "\320\240\320\265\320\266\320\270\320\274 \321\200\320\270\321\201\320\276\320\262\320\260\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        labelSize->setText(QApplication::translate("MainWindow", "\320\240\320\260\320\267\320\274\320\265\321\200", 0, QApplication::UnicodeUTF8));
        pushButtonClear->setText(QApplication::translate("MainWindow", "\320\236\321\207\320\270\321\201\321\202\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("MainWindow", "\320\237\321\200\320\265\320\276\320\261\321\200\320\260\320\267\320\276\320\262\320\260\321\202\321\214", 0, QApplication::UnicodeUTF8));
        pushButtonAbout->setText(QApplication::translate("MainWindow", "\320\230\320\275\321\204\320\276\321\200\320\274\320\260\321\206\320\270\321\217", 0, QApplication::UnicodeUTF8));
        pushButtonExit->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\205\320\276\320\264", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(MainWindow);
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
