#include "mainwindow.h"
#include "ui_mainwindow.h"



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    activeWidget = NULL;

    selectMenuSlot();

    connect(activeWidget, SIGNAL(exitSignal()), SLOT(exitSlot()));
    connect(activeWidget, SIGNAL(optionsSignal()), SLOT(optionsSlot()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::exitSlot()
{
    if (activeWidget) delete activeWidget;
    exit(0);
}

void MainWindow::optionsSlot()
{
    activeWidget = new OptionsWidget();
    setCentralWidget(activeWidget);
    connect(activeWidget, SIGNAL(backToMenu()), SLOT(selectMenuSlot()));
}

void MainWindow::selectMenuSlot()
{
//    disconnect(activeWidget, SIGNAL(backToMenu()), this, SLOT(selectMenuSlot()));
    activeWidget = new MainMenuWidget();
    setCentralWidget(activeWidget);
    connect(activeWidget, SIGNAL(optionsSignal()), SLOT(optionsSlot()));
}
