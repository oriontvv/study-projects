#ifndef OPTIONSWIDGET_H
#define OPTIONSWIDGET_H

#include <QWidget>

namespace Ui {
    class OptionsWidget;
}

class OptionsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit OptionsWidget(QWidget *parent = 0);
    ~OptionsWidget();

signals:
    void backToMenu();

private slots:
    void on_backPushButton_clicked();
    void on_savePushButton_clicked();

private:
    Ui::OptionsWidget *ui;
};

#endif // OPTIONSWIDGET_H
