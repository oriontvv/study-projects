#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "mainmenuwidget.h"
#include "optionswidget.h"

#include <QDebug>

namespace Ui {
    class MainWindow;
}



class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QWidget *activeWidget;

private slots:
    void exitSlot();
    void optionsSlot();
    void selectMenuSlot();
};

#endif // MAINWINDOW_H
