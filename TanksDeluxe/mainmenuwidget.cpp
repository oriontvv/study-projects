#include "mainmenuwidget.h"
#include "ui_mainmenuwidget.h"


#include <QDebug>

enum MenuElement {SingleGame = 0, NetworkGame, Options, Exit};

MainMenuWidget::MainMenuWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainMenuWidget)
{
    ui->setupUi(this);
    qDebug() << "Menu constructor...";
}

MainMenuWidget::~MainMenuWidget()
{
    delete ui;
}

void MainMenuWidget::selectPositionEmitter(int mode)
{
    switch (mode) {
    case SingleGame:
        emit singleGameSignal();
        break;
    case NetworkGame:
        emit networkGameSignal();
        break;
    case Options:
        emit optionsSignal();
        break;
    case Exit:
        emit exitSignal();
        break;
    }
}

void MainMenuWidget::on_listWidget_clicked(const QModelIndex &index)
{
    selectPositionEmitter(index.row());
}

void MainMenuWidget::on_listWidget_activated(const QModelIndex &index)
{
    selectPositionEmitter(index.row());
}
