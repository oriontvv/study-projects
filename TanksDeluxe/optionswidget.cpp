#include "optionswidget.h"
#include "ui_optionswidget.h"

#include <QDebug>

OptionsWidget::OptionsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OptionsWidget)
{
    ui->setupUi(this);
}

OptionsWidget::~OptionsWidget()
{
    delete ui;
}

void OptionsWidget::on_backPushButton_clicked()
{
    qDebug() << "back clicked";
    emit backToMenu();
}

void OptionsWidget::on_savePushButton_clicked()
{

}
