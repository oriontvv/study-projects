#-------------------------------------------------
#
# Project created by QtCreator 2011-06-04T22:17:03
#
#-------------------------------------------------

QT       += core gui

TARGET = TanksDeluxe
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
        mainmenuwidget.cpp \
    optionswidget.cpp

HEADERS  += mainwindow.h \
        mainmenuwidget.h \
    optionswidget.h

FORMS    += mainwindow.ui \
        mainmenuwidget.ui \
    optionswidget.ui
