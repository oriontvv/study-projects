#ifndef MAINMENUWIDGET_H
#define MAINMENUWIDGET_H

#include <QWidget>
#include <QModelIndex>

namespace Ui {
    class MainMenuWidget;
}

class MainMenuWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MainMenuWidget(QWidget *parent = 0);
    ~MainMenuWidget();

private slots:
    void on_listWidget_clicked(const QModelIndex &index);
    void on_listWidget_activated(const QModelIndex &index);

signals:
    void exitSignal();
    void singleGameSignal();
    void networkGameSignal();
    void optionsSignal();

private:
    Ui::MainMenuWidget *ui;
    void selectPositionEmitter(int mode);
};

#endif // MAINMENUWIDGET_H
