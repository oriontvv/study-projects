#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QDir>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QImage>
#include <QDialog>
#include <QPainter>

#include <vector>
#include <fstream>
#include <cmath>
#include <time.h>

#define MATH_PI 3.1415926535897932384626433832795

#define SUBHGT 25
#define SUBWTH 10
#define NUM_FEATURES (SUBWTH * SUBHGT * 8) // 8 - length of histogram
#define BACKGROUND_COUNT 3
#define STEP 5 // number of pixel using in sliding window
#define DELTA 85// diff between background and pedestrain

#define HESHELME_BESHELME(x, y) (((x)>0)<<2 | ((y)>0)<<1 | ((x) > ((x)*(y) > 0 ? (y) : -(y))))


template <class T>
T inline min(T a, T b) { return (a < b) ? a : b; }

template <class T>
T inline max(T a, T b) { return (a > b) ? a : b; }

QMap<QString, QList<BoundedRect> >parseLocations(QString filename)
{
    QMap <QString, QList<BoundedRect> > locations;
    QFile file(filename);
    if (!file.open (QIODevice::ReadOnly)) {
        throw "file '" + filename + "' not found";
        QString msg = QString("File '%1' can't be opened").arg(filename);
        QMessageBox::critical(0, "emtpy path", msg);
        return locations;
    }

    QTextStream in(&file);
    while (!in.atEnd()) {
        QString filename;
        in >> filename;
        if (!filename.isEmpty()) {
            BoundedRect rec;
            in >> rec.y0 >> rec.x0 >> rec.y1 >> rec.x1;
            QList <BoundedRect> l = locations[filename];
            l.append(rec);
            locations.remove(filename);
            locations.insert(filename, l);
        }
    }
    file.close();
    return locations;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // initialize kernel of Sobel filter
    kernel_size = 2;
    g1 = new double[2*kernel_size + 1];
    g2 = new double[2*kernel_size + 1];

    //sobel
    //    g1[0] = 1.0; g1[1] = 2.0; g1[2] = 1.0;
    //    g2[0] = 1.0; g2[1] = 0.0; g2[2] = -1.0;

    // shar
    //        g2[0] = 3.0; g2[1] = 10.0; g2[2] = 3.0;
    //        g1[0] = 1.0; g1[1] = 00.0; g1[2] = -1.0;

    //rec:92.8934 prec:84.7222, f =:88.6199
    //gauss_d, 2 rec:91.37 prec:88, f =:89.77

    // diff. gaussian, separable
    g1[0] = 0.0375263;
    g1[1] = 0.239103;
    g1[2] = 0.443269;
    g1[3] = 0.239103;
    g1[4] = 0.0375263;

    g2[0] = 0.0926575;
    g2[1] = 0.295189;
    g2[2] = 0;
    g2[3] = -0.295189;
    g2[4] = -0.0926575;

    progress = new QProgressBar;
    ui->statusBar->addWidget(progress, 1);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_training_locations_open_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Open location file"), QDir::currentPath(), tr("*.*"));
    if (!filename.isEmpty()) {
        ui->training_locations_path->setText(filename);
        try {
            training_locations = parseLocations(filename);
        } catch (QString s) {
            QMessageBox::critical(this, tr("Error"), s);
            return;
        } catch (...) {
            QMessageBox::critical(this, tr("Error"), tr("I/O error"));
            return;
        }
        
        QString str = tr("parsed <b>%1</b> records").arg(QString::number(training_locations.size()));
        QMessageBox::information(this, tr("Parsed"), str, QMessageBox::Ok);
    }
}

void MainWindow::on_training_pictures_open_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("You must to select catalog with pictures"), QDir::currentPath());
    if (!dir.isEmpty()) {
        ui->training_pictures_path->setText(dir);
    }
}

inline double rgb2gray(QRgb color)
{
    return 0.2125*qRed(color) + 0.7154*qGreen(color) + 0.0721*qBlue(color);
}

inline int random(int a, int b)
{
    return (qrand()%(b-a)) + a;
}

void MainWindow::on_training_start_clicked()
{
    if (ui->training_pictures_path->text().isEmpty()) {
        QMessageBox::critical(this, tr("emtpy path"), tr("You must to select pictures path"));
        ui->training_pictures_path->setFocus();
        return;
    }
    if (ui->training_locations_path->text().isEmpty()) {
        QMessageBox::critical(this, tr("emtpy file"), tr("You must to select location file"));
        ui->training_locations_path->setFocus();
        return;
    }
    long time = clock();
    training_locations = parseLocations(ui->training_locations_path->text());
    QString path = ui->training_pictures_path->text() + '\\';
    int num_instances = training_locations.size();
    int cnt = 0;
    
    progress->setRange(0, num_instances);
    progress->show();
    
    // calculate train base
    int l = trainFeatures.size();
    for (int i = 0; i < l; ++i)
        if (trainFeatures[i]) delete trainFeatures[i];
    trainFeatures.clear();
    trainLabels.clear();
    trainFeatures.reserve(2000);
    trainLabels.reserve(2000);
    int* features;

    QList <QString> filenames = training_locations.keys();
    for (int img_ind = 0; img_ind < num_instances; ++img_ind) {
        QString filename = filenames.at(img_ind);
        progress->setValue(cnt+1);
        try {
            QImage img(path + filename + ".png");
            int img_w = img.width();
            BoundedRect br;
            if (training_locations.contains(filename)) {
                br = training_locations[filename][0];
                features = calculateFeatures(&img, br.x0, br.y0, br.x1, br.y1);
                trainFeatures.push_back(features);
                trainLabels.push_back(1);
            }

            // add BACKGROUND_COUNT backgraunds, e.g. labels will be -1
            int old_center = br.x0 + SUBWTH / 2;
            for (int i = 0; i < BACKGROUND_COUNT;) {
                // find random br, that not contains the pedestrain
                int new_x = random(40, img_w - 40); // new_center
                if (abs(new_x - old_center) > DELTA) {
                    features = calculateFeatures(&img, new_x-40, 0, new_x+40, PED_HGT);
                    trainFeatures.push_back(features);
                    trainLabels.push_back(-1);
                    ++i;
                }
            }

        } catch (...) {
            QMessageBox::critical(this, tr("Error"), tr("Incorrect file name '%1'.png").arg(filename), QMessageBox::Cancel);
            return;
        }
        ++cnt;
    }

    int L = int(trainLabels.size());

    problem prob;
    prob.l = L;
    prob.bias = -1; // bias feature
    prob.n = NUM_FEATURES; // number of features + label

    prob.y = new int[L]; // allocate space for labels
    prob.x = new feature_node*[L]; // allocate space for features

    for (int i = 0; i < L; ++i)
    {
        prob.x[i] = new feature_node[NUM_FEATURES+1];
        prob.x[i][NUM_FEATURES].index = -1;  // -1 marks the end of list
        for (int j = 0; j < NUM_FEATURES; j++)
        {
            prob.x[i][j].index = 1+j; // 1-based feature number
            prob.x[i][j].value = trainFeatures[i][j];
        }
        prob.y[i] = trainLabels[i];
    }

    // default values of params. don't change them if not sure (unless param.C)
    struct parameter param;
    param.solver_type = L2R_L2LOSS_SVC_DUAL;
    param.C = 1;
    param.eps = 1e-4;
    param.nr_weight = 0;
    param.weight_label = NULL;
    param.weight = NULL;

    time = clock();
    struct model* pedestrianModel = train(&prob, &param);

    progress->hide();
    
    // save classifier to file
    QString filename = QFileDialog::getSaveFileName(this, tr("select filename for classifier"), QDir::currentPath(), tr("*.*"));
    if (!filename.isEmpty()) {
        if(save_model(filename.toAscii(), pedestrianModel)) {
            QMessageBox::critical(this, tr("Error"), tr("Can't save model to file '%1'").arg(filename), QMessageBox::Cancel);
            return;
        }
        ui->classification_classifier_path->setText(filename);
        ui->detection_classifier_name->setText(filename);
    }
    free_and_destroy_model(&pedestrianModel);
    destroy_param(&param);
    delete[] prob.y;
    for (int i = 0; i < L; i++)
        delete[] prob.x[i];
    delete[] prob.x;
}

// return features vector
int* MainWindow::calculateFeatures(QImage *img, int x0, int y0, int x1, int y1)
{
    const int w = x1-x0, h = y1-y0;
    double s = 0.0;

    int *features = new int[NUM_FEATURES];
    for (int i = 0; i < NUM_FEATURES; ++i)
        features[i] = 0;

    // calculate gray values
    for (int y = y0; y < y0+h; ++y) {
        for (int x = x0; x < x0+w; ++x) {
            img_gray[y-y0][x-x0] = rgb2gray(img->pixel(x, y));
        }
    }

    // find gradient by x
    int v;
    for (int y = 0; y < h; ++y) {
        for (int x = 0; x < w; ++x) {
            s = 0.0;
            for (int k = -kernel_size; k <= kernel_size; ++k) {
                v = min(max(y+k, 0), h-1);
                s += img_gray[v][x] * g2[k+kernel_size];
            }
            tmp_img[y][x] = s;
        }
    }
    for (int y = 0; y < h; ++y) {
        for (int x = 0; x < w; ++x) {
            s = 0.0;
            for (int k = -kernel_size; k <= kernel_size; ++k) {
                v = min(max(x+k, 0), w-1);
                s += tmp_img[y][v] * g1[k+kernel_size];
            }
            grad_x[y][x] = s;
        }
    }

    // find gradient  y
    for (int y = 0; y < h; ++y) {
        for (int x = 0; x < w; ++x) {
            s = 0.0;
            for (int k = -kernel_size; k <= kernel_size; ++k) {
                v = min(max(y+k, 0), h-1);
                s += img_gray[v][x] * g1[k+kernel_size];
            }
            tmp_img[y][x] = s;
        }
    }
    for (int y = 0; y < h; ++y) {
        for (int x = 0; x < w; ++x) {
            s = 0.0;
            for (int k = -kernel_size; k <= kernel_size; ++k) {
                v = min(max(x+k, 0), w-1);
                s += tmp_img[y][v] * g2[k+kernel_size];
            }
            grad_y[y][x] = s;
        }
    }

    for (int x = 0; x < w; ++x) {
        for (int y = 0; y < h; ++y) {
            // find direction and quantize it into 0..8, (do magic:)
            features[((y/8)*SUBWTH + x/8)*8 + HESHELME_BESHELME(grad_y[y][x], grad_x[y][x])]++;
        }
    }

    return features;
}

void MainWindow::on_classification_classifier_open_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Open classifier file"), QDir::currentPath(), tr("*.*"));
    if (!filename.isEmpty()) {
        ui->classification_classifier_path->setText(filename);
    }
}

void MainWindow::on_classification_pictures_open_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("select catalog with pictures"), QDir::currentPath());
    if (!dir.isEmpty()) {
        ui->classification_pictures_path->setText(dir);
    }
}

void MainWindow::on_classification_my_locations_open_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Open found location file"), QDir::currentPath(), tr("*.*"));
    if (!filename.isEmpty()) {
        ui->classification_my_locations_path->setText(filename);
    }
}

void MainWindow::on_classification_correct_locations_open_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Open correct location file"), QDir::currentPath(), tr("*.*"));
    if (!filename.isEmpty()) {
        ui->classification_correct_locations_path->setText(filename);
    }
}

void MainWindow::on_classification_classify_clicked()
{
    if (ui->classification_pictures_open->text().isEmpty()) {
        QMessageBox::critical(this, tr("emtpy path"), tr("You must to select pictures path for classification"));
        ui->classification_pictures_open->setFocus();
        return;
    }
    if (ui->classification_classifier_open->text().isEmpty()) {
        QMessageBox::critical(this, tr("emtpy file"), tr("You must to select location file for classification"));
        ui->classification_classifier_open->setFocus();
        return;
    }
    
    QDir imagesDir(ui->classification_pictures_path->text());
    imagesDir.setNameFilters(QStringList("*.png"));
    QStringList images = imagesDir.entryList();

    // open classifier
    struct model* model;
    QString classifier_filename = ui->classification_classifier_path->text();
    if ((model = load_model(classifier_filename.toAscii())) == 0 ){
        QMessageBox::critical(this, tr("Error"), tr("Can't load model to file '%1'").arg(classifier_filename), QMessageBox::Cancel);
        return;
    }

    int images_count = images.length();
    progress->setRange(0, images_count);
    progress->show();

    QMap<QString, QList<int> > my_locations;
    QString imgPath = ui->classification_pictures_path->text() + '\\';

    feature_node x[NUM_FEATURES+1];
    x[NUM_FEATURES].index = -1;  // -1 marks the end of list
    double prob_estimates[1];

    QMap <int, double> estimates;
    for (int i = 0; i < images_count; ++i) {
        progress->setValue(i+1);
        QString filename = images.at(i);
        QImage image(imgPath + filename);
        estimates.clear();
        for (int x0 = 0, img_w = image.width(); x0 + PED_WID < img_w; x0 += STEP){
            int* features = calculateFeatures(&image, x0, 0, x0+PED_WID, PED_HGT);
            for (int j = 0; j < NUM_FEATURES; ++j) {
                x[j].index = 1+j; // 1-based feature number
                x[j].value = features[j];
            }
            predict_values(model,(const feature_node*) &x, prob_estimates);
            int predict_label = predict(model, x);
            if (predict_label == 1 /* && prob_estimates[0] > 0*/) {
                estimates.insert(x0, prob_estimates[0]);
            }
        }

        // suppression of local not maximas
        QList <int> keys = estimates.keys();
        for (int i = 0; i < keys.size(); ++i) {
            int cur_x0 = keys.at(i);
            double cur_value = estimates[cur_x0];
            bool is_real = true;
            for (int j = 0; j < keys.size(); ++j){
                if (i != j) {
                    int x0 = keys.at(j);
                    if (abs(cur_x0 - x0) < PED_WID/2 && cur_value < estimates[x0]) {
                        is_real = false;
                        break;
                    }
                }
            }
            if (is_real) {
                QString name = filename.remove(".png");
                QList<int> l = my_locations[name];
                my_locations.remove(name);
                l.append(cur_x0);
                my_locations.insert(name, l);
            }
        }
    }
    progress->hide();

    // save found locations to file
    QString filename = QFileDialog::getSaveFileName(this, tr("select filename for the found locations"), QDir::currentPath(), tr("*.*"));
    if (!filename.isEmpty()) {
        std::ofstream out(filename.toAscii());
        ui->classification_my_locations_path->setText(filename);
        foreach(QString filename, my_locations.keys()){
            foreach (int x0, my_locations[filename]) {
                out << filename.remove(".png").toStdString() << " 0 " << x0 << " " << SUBHGT << " " << x0 + SUBWTH << "\n";
            }
        }
        out.close();
    }
}

void MainWindow::on_classification_quality_evaluation_clicked()
{
    if (ui->classification_correct_locations_path->text().isEmpty()) {
        QMessageBox::critical(this, tr("emtpy path"), tr("You must to select file with correct locations"));
        ui->classification_correct_locations_path->setFocus();
        return;
    }
    if (ui->classification_my_locations_path->text().isEmpty()) {
        QMessageBox::critical(this, tr("emtpy path"), tr("You must to select file with found locations"));
        ui->classification_my_locations_path->setFocus();
        return;
    }

    QMap <QString, QList<BoundedRect> > correct_locations = parseLocations(ui->classification_correct_locations_path->text());
    QMap <QString, QList<BoundedRect> > my_locations =  parseLocations(ui->classification_my_locations_path->text());

    double recall, precision;
    double GT = 0;
    double DET = 0;
    double TP = 0, TP_ = 0;

    // calculate TP
    foreach (QString name, my_locations.keys()) {
        if (correct_locations.contains(name)) {
            for (int my = 0; my < my_locations[name].size(); ++my) {
                int my_x0 = my_locations[name][my].x0;
                for (int cor = 0; cor < correct_locations[name].size(); ++cor) {
                    int cor_x0 = correct_locations[name][cor].x0;
                    if (abs(my_x0 - cor_x0) <= PED_WID / 2) {
                        TP++;
                    }
                }
            }
        }
    }
    // calculate TP_
    foreach (QString name, correct_locations.keys()) {
        if (my_locations.contains(name)) {
            for (int cor = 0; cor < correct_locations[name].size(); ++cor) {
                int cor_x0 = correct_locations[name][cor].x0;
                for (int my = 0; my < my_locations[name].size(); ++my) {
                    int my_x0 = my_locations[name][my].x0;
                    if (abs(my_x0 - cor_x0) <= PED_WID / 2) {
                        TP_++;
                    }
                }
            }
        }
    }

    // calculate DET
    foreach (QString name, my_locations.keys()) {
        DET += my_locations[name].size();
    }

    // calculate GT
    foreach (QString name, correct_locations.keys()) {
        GT += correct_locations[name].size();
    }

    recall = TP_ / GT;
    precision = TP / DET;

    QString message = tr("<h3>TP:%3<br>GT:%4<br>DET:%5</h3><hr><h2>Recall : <i>%1%</i><br>Precision : <i>%2\%</i><hr>F : <i>%6%</i></h2>").
            arg(QString::number(recall*100), QString::number(precision*100), QString::number(TP),
                QString::number(GT), QString::number(DET), QString::number(2*precision*recall/(precision+recall)*100));
    QMessageBox::information(this, tr("Quality"), message, QMessageBox::Ok);
}

void MainWindow::on_detection_start_clicked()
{
    if (ui->detection_img_name->text().isEmpty()) {
        QMessageBox::critical(this, tr("emtpy path"), tr("You must to select picture for detection"));
        ui->detection_image_open->setFocus();
        return;
    }
    if (ui->detection_classifier_name->text().isEmpty()) {
        QMessageBox::critical(this, tr("emtpy path"), tr("You must to select classifier for detection"));
        ui->detection_classifier_open->setFocus();
        return;
    }

    // load classifier
    model* model;
    if ((model = load_model(ui->detection_classifier_name->text().toAscii())) == 0) {
        QMessageBox::critical(this, tr("Error"), tr("Can't load classifier"));
        return;
    }
    feature_node x[NUM_FEATURES+1];
    x[NUM_FEATURES].index = -1;  // -1 marks the end of list
    double prob_estimates[1];

    QImage image(ui->detection_img_name->text());
    QPainter painter(&image);
    painter.setPen(QPen(Qt::green, 1));

    QMap <int, double> estimates;

    for (int x0 = 0, img_w = image.width(); x0 + PED_WID < img_w; x0 += STEP){
        int* features = calculateFeatures(&image, x0, 0, x0+PED_WID, PED_HGT);
        for (int j = 0; j < NUM_FEATURES; ++j) {
            x[j].index = 1+j; // 1-based feature number
            x[j].value = features[j];
        }
        predict_values(model,(const feature_node*) &x, prob_estimates);
        int predict_label = predict(model, x);
        if (/*prob_estimates[0] > 0 &&*/ predict_label == 1) {
            estimates.insert(x0, prob_estimates[0]);
        }
    }

    // suppression of local not maximas
    QList <int> keys = estimates.keys();
    for (int i = 0; i < keys.size(); ++i) {
        int cur_x0 = keys.at(i);
        double cur_value = estimates[cur_x0];
        bool is_real = true;
        for (int j = 0; j <= keys.size(); ++j){
            if (i != j) {
                int x0 = keys.at(j);
                if (abs(cur_x0 - x0) < PED_WID/2 && cur_value < estimates[x0]) {
                    is_real = false;
                    break;
                }
            }
        }
        if (is_real) {
            painter.drawRect(cur_x0, 0, PED_WID, PED_HGT);
        }
    }

    ui->detection_image_viewer->setPixmap(QPixmap::fromImage(image));
}

void MainWindow::on_detection_classifier_open_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Open classifier"), "..", tr("*.*"));
    if (!filename.isEmpty()) {
        ui->detection_classifier_name->setText(filename);
    }
}

void MainWindow::on_detection_image_open_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Open picture file"), QDir::currentPath(), tr("*.*"));
    if (!filename.isEmpty()) {
        ui->detection_img_name->setText(filename);
        ui->detection_image_viewer->setPixmap(QPixmap(filename));
    }
}
