#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMap>
#include <QProgressBar>

#include "liblinear-1.8/linear.h"

namespace Ui {
class MainWindow;
}

struct BoundedRect {
    int x0, y0;
    int x1, y1;
    void setValues(int X0, int Y0, int X1, int Y1) {
        x0 = X0, y0 = Y0, x1 = X1, y1 = Y1;
    }
};

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_training_locations_open_clicked();
    void on_training_pictures_open_clicked();
    void on_training_start_clicked();

    void on_classification_classifier_open_clicked();
    void on_classification_pictures_open_clicked();
    void on_classification_my_locations_open_clicked();
    void on_classification_correct_locations_open_clicked();
    void on_classification_classify_clicked();
    void on_classification_quality_evaluation_clicked();

    void on_detection_start_clicked();
    void on_detection_classifier_open_clicked();
    void on_detection_image_open_clicked();
    
private:
    Ui::MainWindow *ui;
    
    // for Sobel filter
    double* g1;
    double* g2;
    int kernel_size;
    
    // contains the coordinates of the rectangle with the pedestrains in each file
    QMap<QString, QList<BoundedRect> >training_locations;

    int* calculateFeatures(QImage* image, int x0, int y0, int x1, int y1);
    const static int PED_WID = 80;
    const static int PED_HGT = 200;
    double img_gray[PED_HGT+1][PED_WID+1], tmp_img[PED_HGT+1][PED_WID+1];
    double grad_x[PED_HGT+1][PED_WID+1], grad_y[PED_HGT+1][PED_WID+1];

    // structures for storing features
    std::vector< int* > testFeatures;
    std::vector< int* > trainFeatures;
    // structures for storing labels
    std::vector<int> testLabels;
    std::vector<int> trainLabels;

    QProgressBar *progress;
};

#endif // MAINWINDOW_H
