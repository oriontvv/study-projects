#-------------------------------------------------
#
# Project created by QtCreator 2011-10-03T01:46:57
#
#-------------------------------------------------

QT       += core gui

TARGET = PedestrianRecognizer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    liblinear-1.8/linear.cpp \
    liblinear-1.8/tron.cpp \
    liblinear-1.8/blas/dscal.c \
    liblinear-1.8/blas/dnrm2.c \
    liblinear-1.8/blas/ddot.c \
    liblinear-1.8/blas/daxpy.c

HEADERS  += mainwindow.h \
    liblinear-1.8/tron.h \
    liblinear-1.8/linear.h \
    liblinear-1.8/blas/blasp.h \
    liblinear-1.8/blas/blas.h

FORMS    += mainwindow.ui
