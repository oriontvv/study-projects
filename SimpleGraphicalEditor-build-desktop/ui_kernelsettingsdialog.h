/********************************************************************************
** Form generated from reading UI file 'kernelsettingsdialog.ui'
**
** Created: Thu 22. Sep 02:07:41 2011
**      by: Qt User Interface Compiler version 4.7.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KERNELSETTINGSDIALOG_H
#define UI_KERNELSETTINGSDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>

QT_BEGIN_NAMESPACE

class Ui_KernelSettingsDialog
{
public:
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QSpacerItem *horizontalSpacer;
    QGridLayout *kernelWidget;
    QSpacerItem *horizontalSpacer_2;
    QGridLayout *gridLayout_4;
    QLabel *label;
    QSpacerItem *verticalSpacer_2;
    QComboBox *sizeComboBox;
    QSpacerItem *verticalSpacer_3;
    QPushButton *pushButton;
    QSpacerItem *verticalSpacer;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *KernelSettingsDialog)
    {
        if (KernelSettingsDialog->objectName().isEmpty())
            KernelSettingsDialog->setObjectName(QString::fromUtf8("KernelSettingsDialog"));
        KernelSettingsDialog->resize(400, 300);
        gridLayout_2 = new QGridLayout(KernelSettingsDialog);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 5, 1, 1);

        kernelWidget = new QGridLayout();
        kernelWidget->setObjectName(QString::fromUtf8("kernelWidget"));

        gridLayout->addLayout(kernelWidget, 0, 6, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 0, 7, 1, 1);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        label = new QLabel(KernelSettingsDialog);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_4->addWidget(label, 2, 0, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer_2, 3, 0, 1, 2);

        sizeComboBox = new QComboBox(KernelSettingsDialog);
        sizeComboBox->setObjectName(QString::fromUtf8("sizeComboBox"));

        gridLayout_4->addWidget(sizeComboBox, 2, 1, 1, 1);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer_3, 1, 0, 1, 2);

        pushButton = new QPushButton(KernelSettingsDialog);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        gridLayout_4->addWidget(pushButton, 4, 0, 1, 2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer, 5, 0, 1, 1);


        gridLayout->addLayout(gridLayout_4, 0, 2, 1, 1);


        gridLayout_2->addLayout(gridLayout, 0, 0, 2, 1);

        buttonBox = new QDialogButtonBox(KernelSettingsDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout_2->addWidget(buttonBox, 2, 0, 1, 1);


        retranslateUi(KernelSettingsDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), KernelSettingsDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), KernelSettingsDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(KernelSettingsDialog);
    } // setupUi

    void retranslateUi(QDialog *KernelSettingsDialog)
    {
        KernelSettingsDialog->setWindowTitle(QApplication::translate("KernelSettingsDialog", "Kernel Settings", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("KernelSettingsDialog", "size", 0, QApplication::UnicodeUTF8));
        sizeComboBox->clear();
        sizeComboBox->insertItems(0, QStringList()
         << QApplication::translate("KernelSettingsDialog", "3", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("KernelSettingsDialog", "5", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("KernelSettingsDialog", "7", 0, QApplication::UnicodeUTF8)
        );
        pushButton->setText(QApplication::translate("KernelSettingsDialog", "Initialize", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class KernelSettingsDialog: public Ui_KernelSettingsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KERNELSETTINGSDIALOG_H
