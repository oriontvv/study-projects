/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Fri 23. Sep 17:30:39 2011
**      by: Qt User Interface Compiler version 4.7.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>
#include "imageviewer.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionExit;
    QAction *actionShowNormal;
    QAction *actionAbout;
    QAction *actionKernel;
    QAction *actionContrast_correction;
    QAction *actionAutolevels;
    QAction *actionMedian;
    QAction *actionZoom;
    QAction *actionZoomIn;
    QAction *actionZoomOut;
    QAction *actionRotate;
    QAction *actionGause;
    QAction *actionGrayworld;
    QAction *actionSharpness;
    QAction *actionGlass_effect;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_3;
    ImageViewer *imageWidget;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_2;
    QPushButton *exitButton;
    QGroupBox *zoomGroupBox;
    QGridLayout *gridLayout_3;
    QHBoxLayout *horizontalLayout;
    QLabel *scaleLabel;
    QDoubleSpinBox *scaleSpinBox;
    QPushButton *scalePushButton;
    QSpacerItem *verticalSpacer;
    QGroupBox *rotateGroupBox;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *angleLabel;
    QSpinBox *rotateAngleSpinBox;
    QPushButton *rotatePushButton;
    QPushButton *undoAllPushButton;
    QSpacerItem *verticalSpacer_2;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuFilter;
    QMenu *menuView;
    QMenu *menuHelp;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(513, 377);
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        actionShowNormal = new QAction(MainWindow);
        actionShowNormal->setObjectName(QString::fromUtf8("actionShowNormal"));
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        actionKernel = new QAction(MainWindow);
        actionKernel->setObjectName(QString::fromUtf8("actionKernel"));
        actionContrast_correction = new QAction(MainWindow);
        actionContrast_correction->setObjectName(QString::fromUtf8("actionContrast_correction"));
        actionAutolevels = new QAction(MainWindow);
        actionAutolevels->setObjectName(QString::fromUtf8("actionAutolevels"));
        actionMedian = new QAction(MainWindow);
        actionMedian->setObjectName(QString::fromUtf8("actionMedian"));
        actionZoom = new QAction(MainWindow);
        actionZoom->setObjectName(QString::fromUtf8("actionZoom"));
        actionZoomIn = new QAction(MainWindow);
        actionZoomIn->setObjectName(QString::fromUtf8("actionZoomIn"));
        actionZoomOut = new QAction(MainWindow);
        actionZoomOut->setObjectName(QString::fromUtf8("actionZoomOut"));
        actionRotate = new QAction(MainWindow);
        actionRotate->setObjectName(QString::fromUtf8("actionRotate"));
        actionGause = new QAction(MainWindow);
        actionGause->setObjectName(QString::fromUtf8("actionGause"));
        actionGrayworld = new QAction(MainWindow);
        actionGrayworld->setObjectName(QString::fromUtf8("actionGrayworld"));
        actionSharpness = new QAction(MainWindow);
        actionSharpness->setObjectName(QString::fromUtf8("actionSharpness"));
        actionGlass_effect = new QAction(MainWindow);
        actionGlass_effect->setObjectName(QString::fromUtf8("actionGlass_effect"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout_3 = new QHBoxLayout(centralWidget);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        imageWidget = new ImageViewer(centralWidget);
        imageWidget->setObjectName(QString::fromUtf8("imageWidget"));

        horizontalLayout_3->addWidget(imageWidget);

        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout_2 = new QGridLayout(groupBox);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        exitButton = new QPushButton(groupBox);
        exitButton->setObjectName(QString::fromUtf8("exitButton"));

        gridLayout_2->addWidget(exitButton, 5, 1, 1, 1);

        zoomGroupBox = new QGroupBox(groupBox);
        zoomGroupBox->setObjectName(QString::fromUtf8("zoomGroupBox"));
        gridLayout_3 = new QGridLayout(zoomGroupBox);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        scaleLabel = new QLabel(zoomGroupBox);
        scaleLabel->setObjectName(QString::fromUtf8("scaleLabel"));

        horizontalLayout->addWidget(scaleLabel);

        scaleSpinBox = new QDoubleSpinBox(zoomGroupBox);
        scaleSpinBox->setObjectName(QString::fromUtf8("scaleSpinBox"));
        scaleSpinBox->setDecimals(1);
        scaleSpinBox->setMinimum(0.3);
        scaleSpinBox->setSingleStep(0.1);
        scaleSpinBox->setValue(1.2);

        horizontalLayout->addWidget(scaleSpinBox);


        gridLayout_3->addLayout(horizontalLayout, 0, 0, 1, 1);

        scalePushButton = new QPushButton(zoomGroupBox);
        scalePushButton->setObjectName(QString::fromUtf8("scalePushButton"));

        gridLayout_3->addWidget(scalePushButton, 0, 1, 1, 1);


        gridLayout_2->addWidget(zoomGroupBox, 0, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 4, 1, 1, 1);

        rotateGroupBox = new QGroupBox(groupBox);
        rotateGroupBox->setObjectName(QString::fromUtf8("rotateGroupBox"));
        gridLayout = new QGridLayout(rotateGroupBox);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        angleLabel = new QLabel(rotateGroupBox);
        angleLabel->setObjectName(QString::fromUtf8("angleLabel"));

        horizontalLayout_2->addWidget(angleLabel);

        rotateAngleSpinBox = new QSpinBox(rotateGroupBox);
        rotateAngleSpinBox->setObjectName(QString::fromUtf8("rotateAngleSpinBox"));
        rotateAngleSpinBox->setMinimum(-1000);
        rotateAngleSpinBox->setMaximum(1000);
        rotateAngleSpinBox->setSingleStep(15);
        rotateAngleSpinBox->setValue(90);

        horizontalLayout_2->addWidget(rotateAngleSpinBox);


        gridLayout->addLayout(horizontalLayout_2, 0, 0, 1, 1);

        rotatePushButton = new QPushButton(rotateGroupBox);
        rotatePushButton->setObjectName(QString::fromUtf8("rotatePushButton"));

        gridLayout->addWidget(rotatePushButton, 0, 1, 1, 1);


        gridLayout_2->addWidget(rotateGroupBox, 1, 1, 1, 1);

        undoAllPushButton = new QPushButton(groupBox);
        undoAllPushButton->setObjectName(QString::fromUtf8("undoAllPushButton"));

        gridLayout_2->addWidget(undoAllPushButton, 3, 1, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer_2, 2, 1, 1, 1);


        horizontalLayout_3->addWidget(groupBox);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 513, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuFilter = new QMenu(menuBar);
        menuFilter->setObjectName(QString::fromUtf8("menuFilter"));
        menuView = new QMenu(menuBar);
        menuView->setObjectName(QString::fromUtf8("menuView"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuBar->addAction(menuFilter->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionSave);
        menuFile->addSeparator();
        menuFile->addAction(actionExit);
        menuFilter->addAction(actionKernel);
        menuFilter->addAction(actionSharpness);
        menuFilter->addSeparator();
        menuFilter->addAction(actionContrast_correction);
        menuFilter->addAction(actionAutolevels);
        menuFilter->addSeparator();
        menuFilter->addAction(actionMedian);
        menuFilter->addAction(actionGause);
        menuFilter->addAction(actionGrayworld);
        menuFilter->addSeparator();
        menuFilter->addAction(actionGlass_effect);
        menuView->addAction(actionShowNormal);
        menuView->addSeparator();
        menuView->addAction(actionZoom);
        menuView->addAction(actionRotate);
        menuHelp->addAction(actionAbout);

        retranslateUi(MainWindow);
        QObject::connect(actionExit, SIGNAL(triggered()), MainWindow, SLOT(close()));
        QObject::connect(exitButton, SIGNAL(clicked()), MainWindow, SLOT(close()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        actionOpen->setText(QApplication::translate("MainWindow", "&Open...", 0, QApplication::UnicodeUTF8));
        actionOpen->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", 0, QApplication::UnicodeUTF8));
        actionSave->setText(QApplication::translate("MainWindow", "&Save...", 0, QApplication::UnicodeUTF8));
        actionSave->setShortcut(QApplication::translate("MainWindow", "Ctrl+S", 0, QApplication::UnicodeUTF8));
        actionExit->setText(QApplication::translate("MainWindow", "E&xit", 0, QApplication::UnicodeUTF8));
        actionExit->setShortcut(QApplication::translate("MainWindow", "Ctrl+Q", 0, QApplication::UnicodeUTF8));
        actionShowNormal->setText(QApplication::translate("MainWindow", "Normal", 0, QApplication::UnicodeUTF8));
        actionAbout->setText(QApplication::translate("MainWindow", "About", 0, QApplication::UnicodeUTF8));
        actionKernel->setText(QApplication::translate("MainWindow", "&Kernel", 0, QApplication::UnicodeUTF8));
        actionContrast_correction->setText(QApplication::translate("MainWindow", "&Contrast correction", 0, QApplication::UnicodeUTF8));
        actionAutolevels->setText(QApplication::translate("MainWindow", "&Autolevels", 0, QApplication::UnicodeUTF8));
        actionMedian->setText(QApplication::translate("MainWindow", "&Median", 0, QApplication::UnicodeUTF8));
        actionZoom->setText(QApplication::translate("MainWindow", "Zoom", 0, QApplication::UnicodeUTF8));
        actionZoomIn->setText(QApplication::translate("MainWindow", "ZoomIn", 0, QApplication::UnicodeUTF8));
        actionZoomOut->setText(QApplication::translate("MainWindow", "ZoomOut", 0, QApplication::UnicodeUTF8));
        actionRotate->setText(QApplication::translate("MainWindow", "Rotate", 0, QApplication::UnicodeUTF8));
        actionGause->setText(QApplication::translate("MainWindow", "Gaussian &Blur", 0, QApplication::UnicodeUTF8));
        actionGrayworld->setText(QApplication::translate("MainWindow", "&Grayworld", 0, QApplication::UnicodeUTF8));
        actionSharpness->setText(QApplication::translate("MainWindow", "Sharpness", 0, QApplication::UnicodeUTF8));
        actionGlass_effect->setText(QApplication::translate("MainWindow", "Glass effect", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("MainWindow", "Tools", 0, QApplication::UnicodeUTF8));
        exitButton->setText(QApplication::translate("MainWindow", "Exit", 0, QApplication::UnicodeUTF8));
        exitButton->setShortcut(QApplication::translate("MainWindow", "Ctrl+Q", 0, QApplication::UnicodeUTF8));
        zoomGroupBox->setTitle(QApplication::translate("MainWindow", "Zoom", 0, QApplication::UnicodeUTF8));
        scaleLabel->setText(QApplication::translate("MainWindow", "K = ", 0, QApplication::UnicodeUTF8));
        scalePushButton->setText(QApplication::translate("MainWindow", "scale", 0, QApplication::UnicodeUTF8));
        rotateGroupBox->setTitle(QApplication::translate("MainWindow", "Rotation", 0, QApplication::UnicodeUTF8));
        angleLabel->setText(QApplication::translate("MainWindow", "angle = ", 0, QApplication::UnicodeUTF8));
        rotatePushButton->setText(QApplication::translate("MainWindow", "rotate", 0, QApplication::UnicodeUTF8));
        undoAllPushButton->setText(QApplication::translate("MainWindow", "Undo all changes", 0, QApplication::UnicodeUTF8));
        undoAllPushButton->setShortcut(QApplication::translate("MainWindow", "Ctrl+Z", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("MainWindow", "&File", 0, QApplication::UnicodeUTF8));
        menuFilter->setTitle(QApplication::translate("MainWindow", "F&ilter", 0, QApplication::UnicodeUTF8));
        menuView->setTitle(QApplication::translate("MainWindow", "&View", 0, QApplication::UnicodeUTF8));
        menuHelp->setTitle(QApplication::translate("MainWindow", "&Help", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
