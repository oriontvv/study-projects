/********************************************************************************
** Form generated from reading UI file 'form.ui'
**
** Created: Sun 16. Oct 17:42:10 2011
**      by: Qt User Interface Compiler version 4.7.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_H
#define UI_FORM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QStatusBar>
#include <QtGui/QTextBrowser>
#include <QtGui/QToolBar>
#include <QtGui/QToolBox>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Form
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer;
    QToolBox *toolBox;
    QWidget *learn;
    QGridLayout *gridLayout_2;
    QSpacerItem *verticalSpacer;
    QPushButton *learnLocations_open;
    QPushButton *learnPictures_open;
    QLineEdit *learnPictures;
    QLabel *label;
    QLineEdit *learnLocations;
    QSpinBox *backgoundSpinBox;
    QLabel *label_2;
    QSpinBox *differenceSpinBox;
    QPushButton *learnGo;
    QSpacerItem *horizontalSpacer_2;
    QFrame *line;
    QLabel *label_5;
    QSpinBox *bootStrappingCount;
    QLabel *label_6;
    QSpinBox *bootstrappingStep;
    QPushButton *bootstrapping;
    QWidget *classify;
    QGridLayout *gridLayout_7;
    QSpacerItem *verticalSpacer_5;
    QLineEdit *classifyPictures;
    QLineEdit *classifyClassificator;
    QLabel *label_4;
    QPushButton *classifyGo;
    QSpinBox *classifyStepStepSpinBox;
    QPushButton *classifyPictures_open;
    QPushButton *classifyClassificator_open;
    QSpacerItem *horizontalSpacer_3;
    QWidget *page;
    QGridLayout *gridLayout_14;
    QLineEdit *estimateLocationsDetected;
    QPushButton *estimateLocationsDetected_open;
    QSpacerItem *verticalSpacer_6;
    QPushButton *estimateLocationsTrue_open;
    QLineEdit *estimateLocationsTrue;
    QPushButton *estimateGo;
    QWidget *recogrize;
    QGridLayout *gridLayout_4;
    QLineEdit *recognizePicture;
    QPushButton *recognizePicture_open;
    QLineEdit *recognizeClassificator;
    QPushButton *recognizeClassificator_open;
    QLabel *pictureDrawer;
    QLabel *label_3;
    QPushButton *recognizeGo;
    QSpinBox *recognizeStepSpinBox;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *verticalSpacer_2;
    QTextBrowser *logger;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Form)
    {
        if (Form->objectName().isEmpty())
            Form->setObjectName(QString::fromUtf8("Form"));
        Form->resize(674, 483);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icon/title.png"), QSize(), QIcon::Normal, QIcon::Off);
        Form->setWindowIcon(icon);
        Form->setStyleSheet(QString::fromUtf8("background-color: rgb(222, 222, 222);"));
        centralWidget = new QWidget(Form);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/icon/exit.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton->setIcon(icon1);
        pushButton->setIconSize(QSize(40, 40));

        gridLayout->addWidget(pushButton, 12, 0, 1, 2);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 12, 2, 1, 1);

        toolBox = new QToolBox(centralWidget);
        toolBox->setObjectName(QString::fromUtf8("toolBox"));
        learn = new QWidget();
        learn->setObjectName(QString::fromUtf8("learn"));
        learn->setGeometry(QRect(0, 0, 404, 233));
        gridLayout_2 = new QGridLayout(learn);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 10, 0, 1, 5);

        learnLocations_open = new QPushButton(learn);
        learnLocations_open->setObjectName(QString::fromUtf8("learnLocations_open"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/icon/open.png"), QSize(), QIcon::Normal, QIcon::Off);
        learnLocations_open->setIcon(icon2);

        gridLayout_2->addWidget(learnLocations_open, 0, 4, 1, 1);

        learnPictures_open = new QPushButton(learn);
        learnPictures_open->setObjectName(QString::fromUtf8("learnPictures_open"));
        learnPictures_open->setIcon(icon2);

        gridLayout_2->addWidget(learnPictures_open, 2, 4, 1, 1);

        learnPictures = new QLineEdit(learn);
        learnPictures->setObjectName(QString::fromUtf8("learnPictures"));

        gridLayout_2->addWidget(learnPictures, 2, 0, 1, 4);

        label = new QLabel(learn);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_2->addWidget(label, 3, 0, 1, 1);

        learnLocations = new QLineEdit(learn);
        learnLocations->setObjectName(QString::fromUtf8("learnLocations"));

        gridLayout_2->addWidget(learnLocations, 0, 0, 1, 4);

        backgoundSpinBox = new QSpinBox(learn);
        backgoundSpinBox->setObjectName(QString::fromUtf8("backgoundSpinBox"));
        backgoundSpinBox->setMinimum(1);
        backgoundSpinBox->setMaximum(1000);
        backgoundSpinBox->setValue(3);

        gridLayout_2->addWidget(backgoundSpinBox, 3, 2, 1, 1);

        label_2 = new QLabel(learn);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_2->addWidget(label_2, 4, 0, 1, 1);

        differenceSpinBox = new QSpinBox(learn);
        differenceSpinBox->setObjectName(QString::fromUtf8("differenceSpinBox"));
        differenceSpinBox->setMinimum(1);
        differenceSpinBox->setMaximum(1000);
        differenceSpinBox->setValue(80);

        gridLayout_2->addWidget(differenceSpinBox, 4, 2, 1, 1);

        learnGo = new QPushButton(learn);
        learnGo->setObjectName(QString::fromUtf8("learnGo"));
        QFont font;
        font.setPointSize(18);
        learnGo->setFont(font);
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/icon/start.png"), QSize(), QIcon::Normal, QIcon::Off);
        learnGo->setIcon(icon3);
        learnGo->setIconSize(QSize(40, 40));

        gridLayout_2->addWidget(learnGo, 3, 4, 2, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_2, 3, 3, 2, 1);

        line = new QFrame(learn);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line, 6, 0, 1, 5);

        label_5 = new QLabel(learn);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout_2->addWidget(label_5, 7, 0, 1, 1);

        bootStrappingCount = new QSpinBox(learn);
        bootStrappingCount->setObjectName(QString::fromUtf8("bootStrappingCount"));
        bootStrappingCount->setMinimum(1);
        bootStrappingCount->setValue(3);

        gridLayout_2->addWidget(bootStrappingCount, 7, 2, 1, 1);

        label_6 = new QLabel(learn);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout_2->addWidget(label_6, 8, 0, 1, 1);

        bootstrappingStep = new QSpinBox(learn);
        bootstrappingStep->setObjectName(QString::fromUtf8("bootstrappingStep"));
        bootstrappingStep->setMinimum(1);
        bootstrappingStep->setValue(10);

        gridLayout_2->addWidget(bootstrappingStep, 8, 2, 1, 1);

        bootstrapping = new QPushButton(learn);
        bootstrapping->setObjectName(QString::fromUtf8("bootstrapping"));

        gridLayout_2->addWidget(bootstrapping, 7, 4, 2, 1);

        toolBox->addItem(learn, QString::fromUtf8("\320\236\320\261\321\203\321\207\320\265\320\275\320\270\320\265"));
        classify = new QWidget();
        classify->setObjectName(QString::fromUtf8("classify"));
        classify->setGeometry(QRect(0, 0, 394, 216));
        gridLayout_7 = new QGridLayout(classify);
        gridLayout_7->setSpacing(6);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_7->addItem(verticalSpacer_5, 7, 0, 1, 4);

        classifyPictures = new QLineEdit(classify);
        classifyPictures->setObjectName(QString::fromUtf8("classifyPictures"));

        gridLayout_7->addWidget(classifyPictures, 2, 0, 1, 3);

        classifyClassificator = new QLineEdit(classify);
        classifyClassificator->setObjectName(QString::fromUtf8("classifyClassificator"));

        gridLayout_7->addWidget(classifyClassificator, 3, 0, 1, 3);

        label_4 = new QLabel(classify);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_7->addWidget(label_4, 4, 0, 1, 1);

        classifyGo = new QPushButton(classify);
        classifyGo->setObjectName(QString::fromUtf8("classifyGo"));
        classifyGo->setFont(font);
        classifyGo->setIcon(icon3);
        classifyGo->setIconSize(QSize(40, 40));

        gridLayout_7->addWidget(classifyGo, 4, 3, 1, 1);

        classifyStepStepSpinBox = new QSpinBox(classify);
        classifyStepStepSpinBox->setObjectName(QString::fromUtf8("classifyStepStepSpinBox"));
        classifyStepStepSpinBox->setMinimum(1);
        classifyStepStepSpinBox->setValue(10);

        gridLayout_7->addWidget(classifyStepStepSpinBox, 4, 1, 1, 1);

        classifyPictures_open = new QPushButton(classify);
        classifyPictures_open->setObjectName(QString::fromUtf8("classifyPictures_open"));
        classifyPictures_open->setIcon(icon2);

        gridLayout_7->addWidget(classifyPictures_open, 2, 3, 1, 1);

        classifyClassificator_open = new QPushButton(classify);
        classifyClassificator_open->setObjectName(QString::fromUtf8("classifyClassificator_open"));
        classifyClassificator_open->setIcon(icon2);

        gridLayout_7->addWidget(classifyClassificator_open, 3, 3, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_7->addItem(horizontalSpacer_3, 4, 2, 1, 1);

        toolBox->addItem(classify, QString::fromUtf8("\320\232\320\273\320\260\321\201\321\201\320\270\321\204\320\270\320\272\320\260\321\206\320\270\321\217"));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        page->setGeometry(QRect(0, 0, 394, 216));
        gridLayout_14 = new QGridLayout(page);
        gridLayout_14->setSpacing(6);
        gridLayout_14->setContentsMargins(11, 11, 11, 11);
        gridLayout_14->setObjectName(QString::fromUtf8("gridLayout_14"));
        estimateLocationsDetected = new QLineEdit(page);
        estimateLocationsDetected->setObjectName(QString::fromUtf8("estimateLocationsDetected"));

        gridLayout_14->addWidget(estimateLocationsDetected, 0, 0, 1, 1);

        estimateLocationsDetected_open = new QPushButton(page);
        estimateLocationsDetected_open->setObjectName(QString::fromUtf8("estimateLocationsDetected_open"));
        estimateLocationsDetected_open->setIcon(icon2);

        gridLayout_14->addWidget(estimateLocationsDetected_open, 0, 2, 1, 1);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_14->addItem(verticalSpacer_6, 3, 0, 1, 3);

        estimateLocationsTrue_open = new QPushButton(page);
        estimateLocationsTrue_open->setObjectName(QString::fromUtf8("estimateLocationsTrue_open"));
        estimateLocationsTrue_open->setIcon(icon2);

        gridLayout_14->addWidget(estimateLocationsTrue_open, 1, 2, 1, 1);

        estimateLocationsTrue = new QLineEdit(page);
        estimateLocationsTrue->setObjectName(QString::fromUtf8("estimateLocationsTrue"));

        gridLayout_14->addWidget(estimateLocationsTrue, 1, 0, 1, 1);

        estimateGo = new QPushButton(page);
        estimateGo->setObjectName(QString::fromUtf8("estimateGo"));
        estimateGo->setFont(font);
        estimateGo->setIcon(icon3);
        estimateGo->setIconSize(QSize(40, 40));

        gridLayout_14->addWidget(estimateGo, 2, 2, 1, 1);

        toolBox->addItem(page, QString::fromUtf8("\320\236\321\206\320\265\320\275\320\272\320\260 \320\272\320\260\321\207\320\265\321\201\321\202\320\262\320\260 \320\272\320\273\320\260\321\201\321\201\320\270\321\204\320\270\320\272\320\260\321\206\320\270\320\270"));
        recogrize = new QWidget();
        recogrize->setObjectName(QString::fromUtf8("recogrize"));
        recogrize->setGeometry(QRect(0, 0, 394, 216));
        gridLayout_4 = new QGridLayout(recogrize);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        recognizePicture = new QLineEdit(recogrize);
        recognizePicture->setObjectName(QString::fromUtf8("recognizePicture"));

        gridLayout_4->addWidget(recognizePicture, 0, 0, 1, 3);

        recognizePicture_open = new QPushButton(recogrize);
        recognizePicture_open->setObjectName(QString::fromUtf8("recognizePicture_open"));
        recognizePicture_open->setIcon(icon2);

        gridLayout_4->addWidget(recognizePicture_open, 0, 3, 1, 1);

        recognizeClassificator = new QLineEdit(recogrize);
        recognizeClassificator->setObjectName(QString::fromUtf8("recognizeClassificator"));

        gridLayout_4->addWidget(recognizeClassificator, 1, 0, 1, 3);

        recognizeClassificator_open = new QPushButton(recogrize);
        recognizeClassificator_open->setObjectName(QString::fromUtf8("recognizeClassificator_open"));
        recognizeClassificator_open->setIcon(icon2);

        gridLayout_4->addWidget(recognizeClassificator_open, 1, 3, 1, 1);

        pictureDrawer = new QLabel(recogrize);
        pictureDrawer->setObjectName(QString::fromUtf8("pictureDrawer"));

        gridLayout_4->addWidget(pictureDrawer, 4, 0, 1, 4);

        label_3 = new QLabel(recogrize);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_4->addWidget(label_3, 2, 0, 1, 1);

        recognizeGo = new QPushButton(recogrize);
        recognizeGo->setObjectName(QString::fromUtf8("recognizeGo"));
        recognizeGo->setFont(font);
        recognizeGo->setIcon(icon3);
        recognizeGo->setIconSize(QSize(40, 40));

        gridLayout_4->addWidget(recognizeGo, 2, 3, 1, 1);

        recognizeStepSpinBox = new QSpinBox(recogrize);
        recognizeStepSpinBox->setObjectName(QString::fromUtf8("recognizeStepSpinBox"));
        recognizeStepSpinBox->setMinimum(1);
        recognizeStepSpinBox->setValue(10);

        gridLayout_4->addWidget(recognizeStepSpinBox, 2, 1, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_4, 2, 2, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer_2, 3, 0, 1, 4);

        toolBox->addItem(recogrize, QString::fromUtf8("\320\240\320\260\321\201\320\277\320\276\320\267\320\275\320\260\320\262\320\260\320\275\320\270\320\265"));

        gridLayout->addWidget(toolBox, 0, 0, 11, 3);

        logger = new QTextBrowser(centralWidget);
        logger->setObjectName(QString::fromUtf8("logger"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Consolas"));
        font1.setPointSize(8);
        logger->setFont(font1);

        gridLayout->addWidget(logger, 1, 3, 12, 2);

        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/icon/clean.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_2->setIcon(icon4);
        pushButton_2->setIconSize(QSize(34, 34));

        gridLayout->addWidget(pushButton_2, 0, 3, 1, 1);

        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/icon/save.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_3->setIcon(icon5);
        pushButton_3->setIconSize(QSize(34, 34));

        gridLayout->addWidget(pushButton_3, 0, 4, 1, 1);

        Form->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Form);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 674, 21));
        Form->setMenuBar(menuBar);
        mainToolBar = new QToolBar(Form);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        Form->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(Form);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        Form->setStatusBar(statusBar);

        retranslateUi(Form);
        QObject::connect(pushButton, SIGNAL(clicked()), Form, SLOT(close()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), logger, SLOT(clear()));

        toolBox->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Form);
    } // setupUi

    void retranslateUi(QMainWindow *Form)
    {
        Form->setWindowTitle(QApplication::translate("Form", "\320\224\320\265\321\202\320\265\320\272\321\202\320\276\321\200 \320\277\320\265\321\210\320\265\321\205\320\276\320\264\320\276\320\262", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("Form", "\320\222\321\213\321\205\320\276\320\264", 0, QApplication::UnicodeUTF8));
        learnLocations_open->setText(QApplication::translate("Form", "\320\273\320\276\320\272\320\260\321\206\320\270\320\270", 0, QApplication::UnicodeUTF8));
        learnPictures_open->setText(QApplication::translate("Form", "\320\270\320\267\320\276\320\261\321\200\320\260\320\266\320\265\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Form", "\320\272\320\276\320\273\320\270\321\207\320\265\321\201\321\202\320\262\320\276 \321\210\321\203\320\274\320\260 \320\275\320\260 1 \320\277\320\265\321\210\320\265\321\205\320\276\320\264\320\260", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Form", "\321\200\320\260\320\267\320\275\320\270\321\206\320\260 \320\274\320\265\320\266\320\264\321\203 \320\277\320\265\321\210\320\265\321\205\320\276\320\264\320\276\320\274 \320\270 \321\210\321\203\320\274\320\276\320\274", 0, QApplication::UnicodeUTF8));
        learnGo->setText(QApplication::translate("Form", "\320\277\321\203\321\201\320\272", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("Form", "\320\272\320\276\320\273\320\270\321\207\320\265\321\201\321\202\320\262\320\276 \320\270\321\202\320\265\321\200\320\260\321\206\320\270\320\271", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("Form", "\321\210\320\260\320\263(\320\262 \320\277\320\270\320\272\321\201\320\265\320\273\320\260\321\205)", 0, QApplication::UnicodeUTF8));
        bootstrapping->setText(QApplication::translate("Form", "Bootstrapping", 0, QApplication::UnicodeUTF8));
        toolBox->setItemText(toolBox->indexOf(learn), QApplication::translate("Form", "\320\236\320\261\321\203\321\207\320\265\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("Form", "\320\250\320\260\320\263(\320\262 \320\277\320\270\320\272\321\201\320\265\320\273\320\260\321\205)", 0, QApplication::UnicodeUTF8));
        classifyGo->setText(QApplication::translate("Form", "\320\277\321\203\321\201\320\272", 0, QApplication::UnicodeUTF8));
        classifyPictures_open->setText(QApplication::translate("Form", "\320\270\320\267\320\276\320\261\321\200\320\260\320\266\320\265\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        classifyClassificator_open->setText(QApplication::translate("Form", "\320\272\320\273\320\260\321\201\321\201\320\270\321\204\320\270\320\272\320\260\321\202\320\276\321\200", 0, QApplication::UnicodeUTF8));
        toolBox->setItemText(toolBox->indexOf(classify), QApplication::translate("Form", "\320\232\320\273\320\260\321\201\321\201\320\270\321\204\320\270\320\272\320\260\321\206\320\270\321\217", 0, QApplication::UnicodeUTF8));
        estimateLocationsDetected_open->setText(QApplication::translate("Form", "\320\276\320\261\320\275\320\260\321\200\321\203\320\266\320\265\320\275\320\275\321\213\320\265 \320\273\320\276\320\272\320\260\321\206\320\270\320\270", 0, QApplication::UnicodeUTF8));
        estimateLocationsTrue_open->setText(QApplication::translate("Form", "\320\262\320\265\321\200\320\275\321\213\320\265 \320\273\320\276\320\272\320\260\321\206\320\270\320\270", 0, QApplication::UnicodeUTF8));
        estimateGo->setText(QApplication::translate("Form", "\320\277\321\203\321\201\320\272", 0, QApplication::UnicodeUTF8));
        toolBox->setItemText(toolBox->indexOf(page), QApplication::translate("Form", "\320\236\321\206\320\265\320\275\320\272\320\260 \320\272\320\260\321\207\320\265\321\201\321\202\320\262\320\260 \320\272\320\273\320\260\321\201\321\201\320\270\321\204\320\270\320\272\320\260\321\206\320\270\320\270", 0, QApplication::UnicodeUTF8));
        recognizePicture_open->setText(QApplication::translate("Form", "\320\270\320\267\320\276\320\261\321\200\320\260\320\266\320\265\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        recognizeClassificator_open->setText(QApplication::translate("Form", "\320\272\320\273\320\260\321\201\321\201\320\270\321\204\320\270\320\272\320\260\321\202\320\276\321\200", 0, QApplication::UnicodeUTF8));
        pictureDrawer->setText(QString());
        label_3->setText(QApplication::translate("Form", "\320\250\320\260\320\263(\320\262 \320\277\320\270\320\272\321\201\320\265\320\273\321\217\321\205)", 0, QApplication::UnicodeUTF8));
        recognizeGo->setText(QApplication::translate("Form", "\320\277\321\203\321\201\320\272", 0, QApplication::UnicodeUTF8));
        toolBox->setItemText(toolBox->indexOf(recogrize), QApplication::translate("Form", "\320\240\320\260\321\201\320\277\320\276\320\267\320\275\320\260\320\262\320\260\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("Form", "\320\236\321\207\320\270\321\201\321\202\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("Form", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Form: public Ui_Form {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_H
