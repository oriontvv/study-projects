/****************************************************************************
** Meta object code from reading C++ file 'form.h'
**
** Created: Sun 16. Oct 14:53:57 2011
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Recognizer/form.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'form.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Form[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
       6,    5,    5,    5, 0x08,
      38,    5,    5,    5, 0x08,
      71,    5,    5,    5, 0x08,
      92,    5,    5,    5, 0x08,
     127,    5,    5,    5, 0x08,
     168,    5,    5,    5, 0x08,
     193,    5,    5,    5, 0x08,
     228,    5,    5,    5, 0x08,
     268,    5,    5,    5, 0x08,
     292,    5,    5,    5, 0x08,
     336,    5,    5,    5, 0x08,
     376,    5,    5,    5, 0x08,
     400,    5,    5,    5, 0x08,
     426,    5,    5,    5, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Form[] = {
    "Form\0\0on_learnPictures_open_clicked()\0"
    "on_learnLocations_open_clicked()\0"
    "on_learnGo_clicked()\0"
    "on_recognizePicture_open_clicked()\0"
    "on_recognizeClassificator_open_clicked()\0"
    "on_recognizeGo_clicked()\0"
    "on_classifyPictures_open_clicked()\0"
    "on_classifyClassificator_open_clicked()\0"
    "on_classifyGo_clicked()\0"
    "on_estimateLocationsDetected_open_clicked()\0"
    "on_estimateLocationsTrue_open_clicked()\0"
    "on_estimateGo_clicked()\0"
    "on_pushButton_3_clicked()\0bootstrapping()\0"
};

const QMetaObject Form::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Form,
      qt_meta_data_Form, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Form::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Form::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Form::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Form))
        return static_cast<void*>(const_cast< Form*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int Form::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: on_learnPictures_open_clicked(); break;
        case 1: on_learnLocations_open_clicked(); break;
        case 2: on_learnGo_clicked(); break;
        case 3: on_recognizePicture_open_clicked(); break;
        case 4: on_recognizeClassificator_open_clicked(); break;
        case 5: on_recognizeGo_clicked(); break;
        case 6: on_classifyPictures_open_clicked(); break;
        case 7: on_classifyClassificator_open_clicked(); break;
        case 8: on_classifyGo_clicked(); break;
        case 9: on_estimateLocationsDetected_open_clicked(); break;
        case 10: on_estimateLocationsTrue_open_clicked(); break;
        case 11: on_estimateGo_clicked(); break;
        case 12: on_pushButton_3_clicked(); break;
        case 13: bootstrapping(); break;
        default: ;
        }
        _id -= 14;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
