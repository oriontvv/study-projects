/********************************************************************************
** Form generated from reading UI file 'detectorwidget.ui'
**
** Created: Sun 16. Oct 20:11:45 2011
**      by: Qt User Interface Compiler version 4.7.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DETECTORWIDGET_H
#define UI_DETECTORWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpinBox>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DetectorWidget
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QLabel *image;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QPushButton *openTrainImagesPushButton;
    QPushButton *openTrainLocationsPushButton;
    QPushButton *startTrainPushButton;
    QSpinBox *noizeCountSpinBox;
    QLabel *label;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_5;
    QPushButton *openClassificationImagesPushButton;
    QPushButton *startClassificationPushButton;
    QPushButton *openClassifierPushButton;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_3;
    QPushButton *startDetectionPushButton;
    QPushButton *openDetectionClassifierPushButton;
    QLabel *label_4;
    QPushButton *openDetectionImagePushButton;
    QSpinBox *stepDetectionSpinBox;
    QProgressBar *progressBar;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_4;
    QPushButton *openLocationFoundPushButton;
    QPushButton *startQualityPushButton;
    QPushButton *openLocationCorrectPushButton;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *DetectorWidget)
    {
        if (DetectorWidget->objectName().isEmpty())
            DetectorWidget->setObjectName(QString::fromUtf8("DetectorWidget"));
        DetectorWidget->resize(847, 555);
        centralWidget = new QWidget(DetectorWidget);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout_2 = new QGridLayout(centralWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        image = new QLabel(centralWidget);
        image->setObjectName(QString::fromUtf8("image"));

        gridLayout_2->addWidget(image, 0, 1, 2, 4);

        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        openTrainImagesPushButton = new QPushButton(groupBox);
        openTrainImagesPushButton->setObjectName(QString::fromUtf8("openTrainImagesPushButton"));

        gridLayout->addWidget(openTrainImagesPushButton, 0, 0, 1, 2);

        openTrainLocationsPushButton = new QPushButton(groupBox);
        openTrainLocationsPushButton->setObjectName(QString::fromUtf8("openTrainLocationsPushButton"));

        gridLayout->addWidget(openTrainLocationsPushButton, 0, 2, 1, 1);

        startTrainPushButton = new QPushButton(groupBox);
        startTrainPushButton->setObjectName(QString::fromUtf8("startTrainPushButton"));

        gridLayout->addWidget(startTrainPushButton, 1, 2, 1, 1);

        noizeCountSpinBox = new QSpinBox(groupBox);
        noizeCountSpinBox->setObjectName(QString::fromUtf8("noizeCountSpinBox"));
        noizeCountSpinBox->setValue(3);

        gridLayout->addWidget(noizeCountSpinBox, 1, 1, 1, 1);

        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 1, 0, 1, 1);


        gridLayout_2->addWidget(groupBox, 3, 1, 1, 1);

        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_5 = new QGridLayout(groupBox_2);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        openClassificationImagesPushButton = new QPushButton(groupBox_2);
        openClassificationImagesPushButton->setObjectName(QString::fromUtf8("openClassificationImagesPushButton"));

        gridLayout_5->addWidget(openClassificationImagesPushButton, 0, 0, 1, 1);

        startClassificationPushButton = new QPushButton(groupBox_2);
        startClassificationPushButton->setObjectName(QString::fromUtf8("startClassificationPushButton"));

        gridLayout_5->addWidget(startClassificationPushButton, 2, 0, 1, 2);

        openClassifierPushButton = new QPushButton(groupBox_2);
        openClassifierPushButton->setObjectName(QString::fromUtf8("openClassifierPushButton"));

        gridLayout_5->addWidget(openClassifierPushButton, 0, 1, 1, 1);


        gridLayout_2->addWidget(groupBox_2, 3, 2, 1, 1);

        groupBox_4 = new QGroupBox(centralWidget);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        gridLayout_3 = new QGridLayout(groupBox_4);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        startDetectionPushButton = new QPushButton(groupBox_4);
        startDetectionPushButton->setObjectName(QString::fromUtf8("startDetectionPushButton"));

        gridLayout_3->addWidget(startDetectionPushButton, 2, 2, 1, 2);

        openDetectionClassifierPushButton = new QPushButton(groupBox_4);
        openDetectionClassifierPushButton->setObjectName(QString::fromUtf8("openDetectionClassifierPushButton"));

        gridLayout_3->addWidget(openDetectionClassifierPushButton, 0, 2, 1, 2);

        label_4 = new QLabel(groupBox_4);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_3->addWidget(label_4, 2, 0, 1, 1);

        openDetectionImagePushButton = new QPushButton(groupBox_4);
        openDetectionImagePushButton->setObjectName(QString::fromUtf8("openDetectionImagePushButton"));

        gridLayout_3->addWidget(openDetectionImagePushButton, 0, 0, 1, 2);

        stepDetectionSpinBox = new QSpinBox(groupBox_4);
        stepDetectionSpinBox->setObjectName(QString::fromUtf8("stepDetectionSpinBox"));
        stepDetectionSpinBox->setSingleStep(5);
        stepDetectionSpinBox->setValue(5);

        gridLayout_3->addWidget(stepDetectionSpinBox, 2, 1, 1, 1);


        gridLayout_2->addWidget(groupBox_4, 3, 4, 1, 1);

        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));

        gridLayout_2->addWidget(progressBar, 2, 1, 1, 4);

        groupBox_3 = new QGroupBox(centralWidget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        gridLayout_4 = new QGridLayout(groupBox_3);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        openLocationFoundPushButton = new QPushButton(groupBox_3);
        openLocationFoundPushButton->setObjectName(QString::fromUtf8("openLocationFoundPushButton"));

        gridLayout_4->addWidget(openLocationFoundPushButton, 0, 0, 1, 2);

        startQualityPushButton = new QPushButton(groupBox_3);
        startQualityPushButton->setObjectName(QString::fromUtf8("startQualityPushButton"));

        gridLayout_4->addWidget(startQualityPushButton, 2, 0, 1, 4);

        openLocationCorrectPushButton = new QPushButton(groupBox_3);
        openLocationCorrectPushButton->setObjectName(QString::fromUtf8("openLocationCorrectPushButton"));

        gridLayout_4->addWidget(openLocationCorrectPushButton, 0, 2, 1, 2);


        gridLayout_2->addWidget(groupBox_3, 3, 3, 1, 1);

        DetectorWidget->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(DetectorWidget);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 847, 21));
        DetectorWidget->setMenuBar(menuBar);
        mainToolBar = new QToolBar(DetectorWidget);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        DetectorWidget->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(DetectorWidget);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        DetectorWidget->setStatusBar(statusBar);

        retranslateUi(DetectorWidget);

        QMetaObject::connectSlotsByName(DetectorWidget);
    } // setupUi

    void retranslateUi(QMainWindow *DetectorWidget)
    {
        DetectorWidget->setWindowTitle(QApplication::translate("DetectorWidget", "DetectorWidget", 0, QApplication::UnicodeUTF8));
        image->setText(QString());
        groupBox->setTitle(QApplication::translate("DetectorWidget", "Training", 0, QApplication::UnicodeUTF8));
        openTrainImagesPushButton->setText(QApplication::translate("DetectorWidget", "open images...", 0, QApplication::UnicodeUTF8));
        openTrainLocationsPushButton->setText(QApplication::translate("DetectorWidget", "open locations...", 0, QApplication::UnicodeUTF8));
        startTrainPushButton->setText(QApplication::translate("DetectorWidget", "start", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("DetectorWidget", "noise/human", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("DetectorWidget", "Classification", 0, QApplication::UnicodeUTF8));
        openClassificationImagesPushButton->setText(QApplication::translate("DetectorWidget", "open images...", 0, QApplication::UnicodeUTF8));
        startClassificationPushButton->setText(QApplication::translate("DetectorWidget", "start", 0, QApplication::UnicodeUTF8));
        openClassifierPushButton->setText(QApplication::translate("DetectorWidget", "open classifier...", 0, QApplication::UnicodeUTF8));
        groupBox_4->setTitle(QApplication::translate("DetectorWidget", "Detection", 0, QApplication::UnicodeUTF8));
        startDetectionPushButton->setText(QApplication::translate("DetectorWidget", "start", 0, QApplication::UnicodeUTF8));
        openDetectionClassifierPushButton->setText(QApplication::translate("DetectorWidget", "open classifier...", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("DetectorWidget", "step", 0, QApplication::UnicodeUTF8));
        openDetectionImagePushButton->setText(QApplication::translate("DetectorWidget", "open image...", 0, QApplication::UnicodeUTF8));
        stepDetectionSpinBox->setSuffix(QApplication::translate("DetectorWidget", " px", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QApplication::translate("DetectorWidget", "Classification quality", 0, QApplication::UnicodeUTF8));
        openLocationFoundPushButton->setText(QApplication::translate("DetectorWidget", "location found...", 0, QApplication::UnicodeUTF8));
        startQualityPushButton->setText(QApplication::translate("DetectorWidget", "start", 0, QApplication::UnicodeUTF8));
        openLocationCorrectPushButton->setText(QApplication::translate("DetectorWidget", "location correct...", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class DetectorWidget: public Ui_DetectorWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DETECTORWIDGET_H
