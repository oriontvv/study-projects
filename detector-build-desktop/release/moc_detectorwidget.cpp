/****************************************************************************
** Meta object code from reading C++ file 'detectorwidget.h'
**
** Created: Sun 16. Oct 20:11:54 2011
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../detector/detectorwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'detectorwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DetectorWidget[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x08,
      58,   15,   15,   15, 0x08,
      97,   15,   15,   15, 0x08,
     139,   15,   15,   15, 0x08,
     173,   15,   15,   15, 0x08,
     220,   15,   15,   15, 0x08,
     258,   15,   15,   15, 0x08,
     306,   15,   15,   15, 0x08,
     344,   15,   15,   15, 0x08,
     387,   15,   15,   15, 0x08,
     428,   15,   15,   15, 0x08,
     471,   15,   15,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_DetectorWidget[] = {
    "DetectorWidget\0\0"
    "on_openDetectionImagePushButton_clicked()\0"
    "on_openTrainImagesPushButton_clicked()\0"
    "on_openTrainLocationsPushButton_clicked()\0"
    "on_startTrainPushButton_clicked()\0"
    "on_openDetectionClassifierPushButton_clicked()\0"
    "on_startDetectionPushButton_clicked()\0"
    "on_openClassificationImagesPushButton_clicked()\0"
    "on_openClassifierPushButton_clicked()\0"
    "on_startClassificationPushButton_clicked()\0"
    "on_openLocationFoundPushButton_clicked()\0"
    "on_openLocationCorrectPushButton_clicked()\0"
    "on_startQualityPushButton_clicked()\0"
};

const QMetaObject DetectorWidget::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_DetectorWidget,
      qt_meta_data_DetectorWidget, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DetectorWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DetectorWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DetectorWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DetectorWidget))
        return static_cast<void*>(const_cast< DetectorWidget*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int DetectorWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: on_openDetectionImagePushButton_clicked(); break;
        case 1: on_openTrainImagesPushButton_clicked(); break;
        case 2: on_openTrainLocationsPushButton_clicked(); break;
        case 3: on_startTrainPushButton_clicked(); break;
        case 4: on_openDetectionClassifierPushButton_clicked(); break;
        case 5: on_startDetectionPushButton_clicked(); break;
        case 6: on_openClassificationImagesPushButton_clicked(); break;
        case 7: on_openClassifierPushButton_clicked(); break;
        case 8: on_startClassificationPushButton_clicked(); break;
        case 9: on_openLocationFoundPushButton_clicked(); break;
        case 10: on_openLocationCorrectPushButton_clicked(); break;
        case 11: on_startQualityPushButton_clicked(); break;
        default: ;
        }
        _id -= 12;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
