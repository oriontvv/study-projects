#ifndef WORDS_H
#define WORDS_H

#include <QDialog>

namespace Ui {
    class Words;
}

class Words : public QDialog {
    Q_OBJECT
public:
    Words(QWidget *parent = 0);
    ~Words();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::Words *ui;

private slots:
    void on_remove_clicked();
    void on_add_clicked();
};

#endif // WORDS_H
