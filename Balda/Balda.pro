# -------------------------------------------------
# Project created by QtCreator 2010-05-28T18:36:27
# -------------------------------------------------
TARGET = Balda
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    cell.cpp \
    words.cpp
HEADERS += mainwindow.h \
    cell.h \
    words.h
FORMS += mainwindow.ui \
    words.ui
OTHER_FILES += ru_dic.txt
