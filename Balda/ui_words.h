/********************************************************************************
** Form generated from reading UI file 'words.ui'
**
** Created: Wed 2. Jun 13:57:41 2010
**      by: Qt User Interface Compiler version 4.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WORDS_H
#define UI_WORDS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_Words
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLineEdit *lineEditWord;
    QPushButton *add;
    QPushButton *remove;
    QSpacerItem *verticalSpacer;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *Words)
    {
        if (Words->objectName().isEmpty())
            Words->setObjectName(QString::fromUtf8("Words"));
        Words->resize(195, 204);
        verticalLayout = new QVBoxLayout(Words);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(Words);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        lineEditWord = new QLineEdit(Words);
        lineEditWord->setObjectName(QString::fromUtf8("lineEditWord"));

        verticalLayout->addWidget(lineEditWord);

        add = new QPushButton(Words);
        add->setObjectName(QString::fromUtf8("add"));

        verticalLayout->addWidget(add);

        remove = new QPushButton(Words);
        remove->setObjectName(QString::fromUtf8("remove"));

        verticalLayout->addWidget(remove);

        verticalSpacer = new QSpacerItem(20, 44, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        buttonBox = new QDialogButtonBox(Words);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(Words);

        QMetaObject::connectSlotsByName(Words);
    } // setupUi

    void retranslateUi(QDialog *Words)
    {
        Words->setWindowTitle(QApplication::translate("Words", "Change base", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Words", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \321\201\320\273\320\276\320\262\320\276:", 0, QApplication::UnicodeUTF8));
        add->setText(QApplication::translate("Words", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        remove->setText(QApplication::translate("Words", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Words: public Ui_Words {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WORDS_H
