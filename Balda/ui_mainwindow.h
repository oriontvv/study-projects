/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Wed 2. Jun 13:57:40 2010
**      by: Qt User Interface Compiler version 4.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QListView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QStatusBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QGridLayout *fields;
    QLabel *label;
    QVBoxLayout *control;
    QGroupBox *Map;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer;
    QLabel *map_name;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer_2;
    QLabel *word_number;
    QPushButton *loadMap;
    QPushButton *changeMap;
    QSpacerItem *verticalSpacer;
    QFrame *line_2;
    QPushButton *go;
    QLabel *label_2;
    QSpacerItem *verticalSpacer_2;
    QListView *listAnswer;
    QFrame *line;
    QPushButton *pushButton;
    QPushButton *exit;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(556, 418);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        fields = new QGridLayout();
        fields->setSpacing(6);
        fields->setObjectName(QString::fromUtf8("fields"));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));

        fields->addWidget(label, 0, 0, 1, 1);


        horizontalLayout->addLayout(fields);

        control = new QVBoxLayout();
        control->setSpacing(6);
        control->setObjectName(QString::fromUtf8("control"));
        Map = new QGroupBox(centralWidget);
        Map->setObjectName(QString::fromUtf8("Map"));
        Map->setFlat(false);
        verticalLayout = new QVBoxLayout(Map);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_3 = new QLabel(Map);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QFont font;
        font.setPointSize(8);
        label_3->setFont(font);

        horizontalLayout_3->addWidget(label_3);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        map_name = new QLabel(Map);
        map_name->setObjectName(QString::fromUtf8("map_name"));
        QFont font1;
        font1.setPointSize(9);
        font1.setBold(true);
        font1.setWeight(75);
        map_name->setFont(font1);

        horizontalLayout_3->addWidget(map_name);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_4 = new QLabel(Map);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font);

        horizontalLayout_2->addWidget(label_4);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        word_number = new QLabel(Map);
        word_number->setObjectName(QString::fromUtf8("word_number"));
        QFont font2;
        font2.setBold(true);
        font2.setWeight(75);
        word_number->setFont(font2);

        horizontalLayout_2->addWidget(word_number);


        verticalLayout->addLayout(horizontalLayout_2);

        loadMap = new QPushButton(Map);
        loadMap->setObjectName(QString::fromUtf8("loadMap"));

        verticalLayout->addWidget(loadMap);

        changeMap = new QPushButton(Map);
        changeMap->setObjectName(QString::fromUtf8("changeMap"));

        verticalLayout->addWidget(changeMap);


        control->addWidget(Map);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        control->addItem(verticalSpacer);

        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        control->addWidget(line_2);

        go = new QPushButton(centralWidget);
        go->setObjectName(QString::fromUtf8("go"));

        control->addWidget(go);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        control->addWidget(label_2);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        control->addItem(verticalSpacer_2);

        listAnswer = new QListView(centralWidget);
        listAnswer->setObjectName(QString::fromUtf8("listAnswer"));

        control->addWidget(listAnswer);

        line = new QFrame(centralWidget);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        control->addWidget(line);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        control->addWidget(pushButton);

        exit = new QPushButton(centralWidget);
        exit->setObjectName(QString::fromUtf8("exit"));

        control->addWidget(exit);


        horizontalLayout->addLayout(control);

        horizontalLayout->setStretch(0, 7);
        horizontalLayout->setStretch(1, 3);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 556, 20));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);
        QObject::connect(exit, SIGNAL(clicked()), MainWindow, SLOT(close()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "\320\221\320\260\320\273\320\264\320\260\321\200\320\265\321\210\320\260\321\202\320\265\320\273\321\214", 0, QApplication::UnicodeUTF8));
        label->setText(QString());
        Map->setTitle(QApplication::translate("MainWindow", "\320\241\320\273\320\276\320\262\320\260\321\200\321\214", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindow", "\320\230\320\274\321\217:", 0, QApplication::UnicodeUTF8));
        map_name->setText(QString());
        label_4->setText(QApplication::translate("MainWindow", "\320\232\320\276\320\273\320\270\321\207\320\265\321\201\321\202\320\262\320\276 \321\201\320\273\320\276\320\262:", 0, QApplication::UnicodeUTF8));
        word_number->setText(QString());
        loadMap->setText(QApplication::translate("MainWindow", "\320\227\320\260\320\263\321\200\321\203\320\267\320\270\321\202\321\214...", 0, QApplication::UnicodeUTF8));
        changeMap->setText(QApplication::translate("MainWindow", "\320\230\320\267\320\274\320\265\320\275\320\270\321\202\321\214 \321\201\320\273\320\276\320\262\320\260\321\200\321\214", 0, QApplication::UnicodeUTF8));
        go->setText(QApplication::translate("MainWindow", "\320\237\320\240\320\230\320\224\320\243\320\234\320\220\320\242\320\254 !", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "\320\235\320\260\320\271\320\264\320\265\320\275\320\275\321\213\320\265 \321\200\320\265\321\210\320\265\320\275\320\270\321\217:", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("MainWindow", "\320\236\320\261 \320\260\320\262\321\202\320\276\321\200\320\265", 0, QApplication::UnicodeUTF8));
        exit->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\205\320\276\320\264", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
