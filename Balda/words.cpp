#include "words.h"
#include "ui_words.h"

Words::Words(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Words)
{
    ui->setupUi(this);
}

Words::~Words()
{
    delete ui;
}

void Words::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void Words::on_add_clicked()
{

}

void Words::on_remove_clicked()
{

}
