#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include <QString>

#include "cell.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void updateSymbols();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::MainWindow *ui;
    QString base_name;
    void loadFile();
    QVector<QString> words;
    cell* cells[5][5];
    QChar symbols[7][7];

    //for dfs
    int mag[30];
    QPoint mag_pos[30];
    QVector <QPoint> usedAsFirst[30];
    int used[7][7];
    int im, np;
    void solution();
    int mayBeFirst(int x, int y);

private slots:
    void on_changeMap_clicked();
    void on_loadMap_clicked();
    void on_pushButton_clicked();
    void on_go_clicked();
};

#endif // MAINWINDOW_H
