#include "cell.h"
#include <QDebug>

cell::cell(QWidget *parent) :
    QLineEdit(parent)
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("Windows-1251"));
    setMaxLength(1);
    setAlignment(Qt::AlignCenter);

    connect(this, SIGNAL(textChanged(QString)), this, SLOT(test()));
}

void cell::test()
{
    if(text() != text().toUpper()){
        setText(text().toUpper());
    }

    qDebug() << text();
}
