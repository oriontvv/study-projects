
#include <fstream>
#include <QDebug>
#include <QtGui>
#include <QFile>
#include <qalgorithms.h>

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "words.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    for (int i = 0, row = 0, col = 0; i < 25; ++i){
        cell *_cell = new cell(this);
        _cell->setFont(QFont("Tahoma", 20, QFont::Bold));

        ui->fields->addWidget(_cell, row, col);
        cells[row][col] = _cell;

        col++;
        if(col == 5){
            col = 0;
            row++;
        }
    }
    //base_name.reserve(100);
    //base_name = "ru_dic.txt";
    //load words
    //loadFile();

//    qSort(words);
    resize(minimumSize());
    for(int i =0; i < 5; ++i)
        cells[2][i]->setText(QString(i+65));
    //on_go_clicked();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
            ui->retranslateUi(this);
            break;
        default:
            break;
    }
}

void MainWindow::on_go_clicked()
{
    //ui->displayAnswer->clear();
    updateSymbols();
    solution();
}

void MainWindow::updateSymbols()
{
    for(int i = 0; i < 7; ++i)
        for(int j = 0; j < 7; ++j)
            symbols[i][j] = '_';


    for(int i = 0; i < 5; ++i)
        for(int j = 0; j < 5; ++j){
            symbols[i + 1][j + 1] = cells[i][j]->text().at(0);
            if (!symbols[i + 1][j + 1].isPrint()) symbols[i + 1][j + 1] = '_';
            used[i][j] = 0;
        }
}

void MainWindow::solution()
{
    int dx[4] = {0, 1, 0, -1};
    int dy[4] = {1, 0, -1, 0};
    QChar answer[30]     ;
/*
    //search first position for solution
    int empty_counter;
    for(int i = 1; i < 6; ++i)
        for(int j = 1; j < 6; ++j)
            if(mayBeFirst(i, j) && !usedAsFirst->contains(QPoint(i, j))){

                im = 1;
                empty_counter = 0;
                mag_pos[0] = QPoint(i, j);
                answer[0] = symbols[i + 1][j + 1];
                used[i][j] = 1;
                if (answer[0] == '_') empty_counter++;
                mag[0] = -1;//start
                for(int q = 0; q < 7; ++q)
                    for(int w = 0; w < 7; ++w){
                        symbols[q][w] = '_';
                        used[q][w] = 0;
                    }

                while(im > 0){
                    if(np < 4){
                        int next_x = mag_pos[im - 1].x() + dx[np];
                        int next_y = mag_pos[im - 1].y() + dy[np];
                        if(next_x >= 0 && next_x < 5 && next_y >=0 && next_y < 5 && used[next_x][next_y] == 0)
                            if(0){

                            }
                            else{
                                //forward
                                used[next_x][next_y] = 1;
                                ++im;
                                mag[im] = np;
                                mag_pos[im] = QPoint(next_x, next_y);
                                answer[im] = symbols[next_x + 1][next_y + 1];
                                if (answer[im] == '_') empty_counter++;
                                np = -1;
                            }
                    }
                    else{
                        //back
                        im--;
                        used[mag_pos[im].x()][mag_pos[im].y()] = 0;
                        np = mag[im];
                    }
                    np++;
                }

                usedAsFirst->push_back(QPoint(i, j));
            }
            */
}

int MainWindow::mayBeFirst(int x, int y)
{
    QChar empty = QChar('_');
    return symbols[x-1][y] != empty || symbols[x+1][y] != empty ||
            symbols[x][y-1] != empty || symbols[x][y+1] != empty || symbols[x][y] != empty;
}



void MainWindow::on_pushButton_clicked()
{
    qDebug()<< "qweqwe";
    int q = 0;

}

void MainWindow::on_loadMap_clicked()
{
    QString new_name = QFileDialog::getOpenFileName(this, tr("Load words"), ".",
                                                    "*.txt");

    if(!new_name.isEmpty()){
        base_name = new_name;
        base_name = base_name.mid(base_name.lastIndexOf('/')).remove('/');
        loadFile();
    }
}

void MainWindow::loadFile()
{
    QFile infile(base_name);
    infile.open(QIODevice::ReadOnly);
    QTextStream in(&infile);

    QString tmp;
    words.reserve(48000);
    int  counter = 0;
    while(in.status() != QTextStream::ReadPastEnd){
        in >> tmp;
//        qDebug() << tmp;
//        ui->displayAnswer->append(tmp);
        if(!tmp.isEmpty())counter++;
        words.push_back(tmp);
    }
    ui->word_number->setText(QString::number(counter, 10));
    ui->map_name->setText(base_name);
}


void MainWindow::on_changeMap_clicked()
{
    Words *dlg = new Words;
    dlg->show();
}
