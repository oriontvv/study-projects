/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Tue 11. Oct 16:04:59 2011
**      by: Qt User Interface Compiler version 4.7.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QStatusBar>
#include <QtGui/QTabWidget>
#include <QtGui/QToolButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_3;
    QTabWidget *tabWidget;
    QWidget *training;
    QGridLayout *gridLayout;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *training_pictures_path;
    QLineEdit *training_locations_path;
    QToolButton *training_pictures_open;
    QToolButton *training_locations_open;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *training_start;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *verticalSpacer_4;
    QWidget *classification;
    QGridLayout *gridLayout_2;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_3;
    QLineEdit *classification_pictures_path;
    QLabel *label_4;
    QLineEdit *classification_classifier_path;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *classification_classify;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *verticalSpacer;
    QLabel *label_5;
    QLineEdit *classification_my_locations_path;
    QLabel *label_6;
    QLineEdit *classification_correct_locations_path;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *classification_quality_evaluation;
    QSpacerItem *horizontalSpacer_6;
    QToolButton *classification_my_locations_open;
    QToolButton *classification_correct_locations_open;
    QToolButton *classification_pictures_open;
    QToolButton *classification_classifier_open;
    QWidget *detection;
    QGridLayout *gridLayout_4;
    QLabel *detection_image_viewer;
    QPushButton *detection_classifier_open;
    QPushButton *detection_image_open;
    QPushButton *detection_start;
    QSpacerItem *horizontalSpacer_7;
    QSpacerItem *horizontalSpacer_8;
    QLabel *detection_img_name;
    QLabel *detection_classifier_name;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(640, 480);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout_3 = new QGridLayout(centralWidget);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        training = new QWidget();
        training->setObjectName(QString::fromUtf8("training"));
        gridLayout = new QGridLayout(training);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(training);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 2, 0, 1, 1);

        label_2 = new QLabel(training);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 3, 0, 1, 1);

        training_pictures_path = new QLineEdit(training);
        training_pictures_path->setObjectName(QString::fromUtf8("training_pictures_path"));

        gridLayout->addWidget(training_pictures_path, 2, 1, 1, 1);

        training_locations_path = new QLineEdit(training);
        training_locations_path->setObjectName(QString::fromUtf8("training_locations_path"));

        gridLayout->addWidget(training_locations_path, 3, 1, 1, 1);

        training_pictures_open = new QToolButton(training);
        training_pictures_open->setObjectName(QString::fromUtf8("training_pictures_open"));

        gridLayout->addWidget(training_pictures_open, 2, 2, 1, 1);

        training_locations_open = new QToolButton(training);
        training_locations_open->setObjectName(QString::fromUtf8("training_locations_open"));

        gridLayout->addWidget(training_locations_open, 3, 2, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        training_start = new QPushButton(training);
        training_start->setObjectName(QString::fromUtf8("training_start"));

        horizontalLayout_2->addWidget(training_start);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);


        gridLayout->addLayout(horizontalLayout_2, 5, 0, 1, 3);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_3, 6, 0, 1, 3);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_4, 1, 0, 1, 3);

        tabWidget->addTab(training, QString());
        classification = new QWidget();
        classification->setObjectName(QString::fromUtf8("classification"));
        gridLayout_2 = new QGridLayout(classification);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer_2, 0, 0, 1, 3);

        label_3 = new QLabel(classification);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_2->addWidget(label_3, 1, 0, 1, 1);

        classification_pictures_path = new QLineEdit(classification);
        classification_pictures_path->setObjectName(QString::fromUtf8("classification_pictures_path"));

        gridLayout_2->addWidget(classification_pictures_path, 1, 1, 1, 1);

        label_4 = new QLabel(classification);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_2->addWidget(label_4, 2, 0, 1, 1);

        classification_classifier_path = new QLineEdit(classification);
        classification_classifier_path->setObjectName(QString::fromUtf8("classification_classifier_path"));

        gridLayout_2->addWidget(classification_classifier_path, 2, 1, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        classification_classify = new QPushButton(classification);
        classification_classify->setObjectName(QString::fromUtf8("classification_classify"));

        horizontalLayout_3->addWidget(classification_classify);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_4);


        gridLayout_2->addLayout(horizontalLayout_3, 3, 0, 1, 4);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 4, 0, 1, 4);

        label_5 = new QLabel(classification);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout_2->addWidget(label_5, 5, 0, 1, 1);

        classification_my_locations_path = new QLineEdit(classification);
        classification_my_locations_path->setObjectName(QString::fromUtf8("classification_my_locations_path"));

        gridLayout_2->addWidget(classification_my_locations_path, 5, 1, 1, 1);

        label_6 = new QLabel(classification);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout_2->addWidget(label_6, 6, 0, 1, 1);

        classification_correct_locations_path = new QLineEdit(classification);
        classification_correct_locations_path->setObjectName(QString::fromUtf8("classification_correct_locations_path"));

        gridLayout_2->addWidget(classification_correct_locations_path, 6, 1, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_5);

        classification_quality_evaluation = new QPushButton(classification);
        classification_quality_evaluation->setObjectName(QString::fromUtf8("classification_quality_evaluation"));

        horizontalLayout_4->addWidget(classification_quality_evaluation);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_6);


        gridLayout_2->addLayout(horizontalLayout_4, 7, 0, 1, 4);

        classification_my_locations_open = new QToolButton(classification);
        classification_my_locations_open->setObjectName(QString::fromUtf8("classification_my_locations_open"));

        gridLayout_2->addWidget(classification_my_locations_open, 5, 2, 1, 1);

        classification_correct_locations_open = new QToolButton(classification);
        classification_correct_locations_open->setObjectName(QString::fromUtf8("classification_correct_locations_open"));

        gridLayout_2->addWidget(classification_correct_locations_open, 6, 2, 1, 1);

        classification_pictures_open = new QToolButton(classification);
        classification_pictures_open->setObjectName(QString::fromUtf8("classification_pictures_open"));

        gridLayout_2->addWidget(classification_pictures_open, 1, 2, 1, 1);

        classification_classifier_open = new QToolButton(classification);
        classification_classifier_open->setObjectName(QString::fromUtf8("classification_classifier_open"));

        gridLayout_2->addWidget(classification_classifier_open, 2, 2, 1, 1);

        tabWidget->addTab(classification, QString());
        detection = new QWidget();
        detection->setObjectName(QString::fromUtf8("detection"));
        gridLayout_4 = new QGridLayout(detection);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        detection_image_viewer = new QLabel(detection);
        detection_image_viewer->setObjectName(QString::fromUtf8("detection_image_viewer"));

        gridLayout_4->addWidget(detection_image_viewer, 0, 0, 3, 4);

        detection_classifier_open = new QPushButton(detection);
        detection_classifier_open->setObjectName(QString::fromUtf8("detection_classifier_open"));

        gridLayout_4->addWidget(detection_classifier_open, 4, 1, 1, 1);

        detection_image_open = new QPushButton(detection);
        detection_image_open->setObjectName(QString::fromUtf8("detection_image_open"));

        gridLayout_4->addWidget(detection_image_open, 3, 1, 1, 1);

        detection_start = new QPushButton(detection);
        detection_start->setObjectName(QString::fromUtf8("detection_start"));

        gridLayout_4->addWidget(detection_start, 3, 3, 2, 1);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_7, 3, 2, 2, 1);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_8, 3, 4, 2, 1);

        detection_img_name = new QLabel(detection);
        detection_img_name->setObjectName(QString::fromUtf8("detection_img_name"));

        gridLayout_4->addWidget(detection_img_name, 3, 0, 1, 1);

        detection_classifier_name = new QLabel(detection);
        detection_classifier_name->setObjectName(QString::fromUtf8("detection_classifier_name"));

        gridLayout_4->addWidget(detection_classifier_name, 4, 0, 1, 1);

        tabWidget->addTab(detection, QString());

        gridLayout_3->addWidget(tabWidget, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "\320\222\321\213\320\264\320\265\320\273\320\265\320\275\320\270\320\265 \320\277\320\265\321\210\320\265\321\205\320\276\320\264\320\276\320\262", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "\320\270\320\267\320\276\320\261\321\200\320\260\320\266\320\265\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "\320\273\320\276\320\272\320\260\321\206\320\270\320\270 \320\277\320\265\321\210\320\265\321\205\320\276\320\264\320\276\320\262", 0, QApplication::UnicodeUTF8));
        training_pictures_open->setText(QApplication::translate("MainWindow", "...", 0, QApplication::UnicodeUTF8));
        training_locations_open->setText(QApplication::translate("MainWindow", "...", 0, QApplication::UnicodeUTF8));
        training_start->setText(QApplication::translate("MainWindow", "\320\236\320\261\321\203\321\207\320\270\321\202\321\214 \320\272\320\273\320\260\321\201\321\201\320\270\321\204\320\270\320\272\320\260\321\202\320\276\321\200!", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(training), QApplication::translate("MainWindow", "\320\236\320\261\321\203\321\207\320\265\320\275\320\270\320\265 \320\272\320\273\320\260\321\201\321\201\320\270\321\204\320\270\320\272\320\260\321\202\320\276\321\200\320\260", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_ACCESSIBILITY
        classification->setAccessibleName(QString());
#endif // QT_NO_ACCESSIBILITY
        label_3->setText(QApplication::translate("MainWindow", "\320\270\320\267\320\276\320\261\321\200\320\260\320\266\320\265\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MainWindow", "\320\272\320\273\320\260\321\201\321\201\320\270\321\204\320\270\320\272\320\260\321\202\320\276\321\200", 0, QApplication::UnicodeUTF8));
        classification_classify->setText(QApplication::translate("MainWindow", "\320\232\320\273\320\260\321\201\321\201\320\270\321\204\320\270\321\206\320\270\321\200\320\276\320\262\320\260\321\202\321\214 \320\270\320\267\320\276\320\261\321\200\320\260\320\266\320\265\320\275\320\270\321\217!", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("MainWindow", "\320\276\320\261\320\275\320\260\321\200\321\203\320\266\320\265\320\275\320\275\321\213\320\265 \320\273\320\276\320\272\320\260\321\206\320\270\320\270", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("MainWindow", "\320\262\320\265\321\200\320\275\321\213\320\265 \320\273\320\276\320\272\320\260\321\206\320\270\320\270", 0, QApplication::UnicodeUTF8));
        classification_quality_evaluation->setText(QApplication::translate("MainWindow", "\320\236\321\206\320\265\320\275\320\272\320\260 \320\272\320\260\321\207\320\265\321\201\321\202\320\262\320\260 \320\272\320\273\320\260\321\201\321\201\320\270\321\204\320\270\321\206\320\270\320\270", 0, QApplication::UnicodeUTF8));
        classification_my_locations_open->setText(QApplication::translate("MainWindow", "...", 0, QApplication::UnicodeUTF8));
        classification_correct_locations_open->setText(QApplication::translate("MainWindow", "...", 0, QApplication::UnicodeUTF8));
        classification_pictures_open->setText(QApplication::translate("MainWindow", "...", 0, QApplication::UnicodeUTF8));
        classification_classifier_open->setText(QApplication::translate("MainWindow", "...", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(classification), QApplication::translate("MainWindow", "\320\232\320\273\320\260\321\201\321\201\320\270\321\204\320\270\320\272\320\260\321\206\320\270\321\217 \320\270\320\267\320\276\320\261\321\200\320\260\320\266\320\265\320\275\320\270\320\271", 0, QApplication::UnicodeUTF8));
        detection_image_viewer->setText(QString());
        detection_classifier_open->setText(QApplication::translate("MainWindow", "\320\272\320\273\320\260\321\201\321\201\320\270\321\204\320\270\320\272\320\260\321\202\320\276\321\200...", 0, QApplication::UnicodeUTF8));
        detection_image_open->setText(QApplication::translate("MainWindow", "\320\270\320\267\320\276\320\261\321\200\320\260\320\266\320\265\320\275\320\270\320\265...", 0, QApplication::UnicodeUTF8));
        detection_start->setText(QApplication::translate("MainWindow", "\320\224\320\265\321\202\320\265\320\272\321\202\320\270\321\200\320\276\320\262\320\260\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        detection_img_name->setText(QApplication::translate("MainWindow", "empty", 0, QApplication::UnicodeUTF8));
        detection_classifier_name->setText(QApplication::translate("MainWindow", "empty", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(detection), QApplication::translate("MainWindow", "\320\224\320\265\321\202\320\265\320\272\321\202\320\276\321\200 \320\277\320\265\321\210\320\265\321\205\320\276\320\264\320\276\320\262", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
