/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created: Tue 11. Oct 23:32:53 2011
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../PedestrianRecognizer/mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x08,
      49,   11,   11,   11, 0x08,
      85,   11,   11,   11, 0x08,
     113,   11,   11,   11, 0x08,
     157,   11,   11,   11, 0x08,
     199,   11,   11,   11, 0x08,
     245,   11,   11,   11, 0x08,
     296,   11,   11,   11, 0x08,
     333,   11,   11,   11, 0x08,
     380,   11,   11,   11, 0x08,
     409,   11,   11,   11, 0x08,
     448,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0on_training_locations_open_clicked()\0"
    "on_training_pictures_open_clicked()\0"
    "on_training_start_clicked()\0"
    "on_classification_classifier_open_clicked()\0"
    "on_classification_pictures_open_clicked()\0"
    "on_classification_my_locations_open_clicked()\0"
    "on_classification_correct_locations_open_clicked()\0"
    "on_classification_classify_clicked()\0"
    "on_classification_quality_evaluation_clicked()\0"
    "on_detection_start_clicked()\0"
    "on_detection_classifier_open_clicked()\0"
    "on_detection_image_open_clicked()\0"
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: on_training_locations_open_clicked(); break;
        case 1: on_training_pictures_open_clicked(); break;
        case 2: on_training_start_clicked(); break;
        case 3: on_classification_classifier_open_clicked(); break;
        case 4: on_classification_pictures_open_clicked(); break;
        case 5: on_classification_my_locations_open_clicked(); break;
        case 6: on_classification_correct_locations_open_clicked(); break;
        case 7: on_classification_classify_clicked(); break;
        case 8: on_classification_quality_evaluation_clicked(); break;
        case 9: on_detection_start_clicked(); break;
        case 10: on_detection_classifier_open_clicked(); break;
        case 11: on_detection_image_open_clicked(); break;
        default: ;
        }
        _id -= 12;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
