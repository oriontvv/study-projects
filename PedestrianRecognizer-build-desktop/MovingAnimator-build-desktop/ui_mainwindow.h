/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Mon 13. Jun 20:23:33 2011
**      by: Qt User Interface Compiler version 4.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QStatusBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *loadRegions;
    QAction *loadMotions;
    QAction *exit;
    QAction *properties;
    QAction *hotkeys;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *mapLayout;
    QSlider *timeSlider;
    QHBoxLayout *timeLine;
    QLabel *label;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_4;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer_5;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer_6;
    QLabel *label_5;
    QSpacerItem *horizontalSpacer_7;
    QLabel *label_6;
    QSpacerItem *horizontalSpacer_8;
    QLabel *label_7;
    QSpacerItem *horizontalSpacer_9;
    QLabel *label_8;
    QSpacerItem *horizontalSpacer_10;
    QLabel *label_9;
    QSpacerItem *horizontalSpacer_11;
    QLabel *label_10;
    QSpacerItem *horizontalSpacer_12;
    QLabel *label_11;
    QSpacerItem *horizontalSpacer_13;
    QLabel *label_12;
    QHBoxLayout *horizontalLayout;
    QPushButton *Filter;
    QSpacerItem *horizontalSpacer;
    QPushButton *Back;
    QPushButton *Pause;
    QPushButton *Forward;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *Exit;
    QMenuBar *menuBar;
    QMenu *menu;
    QMenu *menu_2;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(635, 471);
        loadRegions = new QAction(MainWindow);
        loadRegions->setObjectName(QString::fromUtf8("loadRegions"));
        loadMotions = new QAction(MainWindow);
        loadMotions->setObjectName(QString::fromUtf8("loadMotions"));
        exit = new QAction(MainWindow);
        exit->setObjectName(QString::fromUtf8("exit"));
        properties = new QAction(MainWindow);
        properties->setObjectName(QString::fromUtf8("properties"));
        hotkeys = new QAction(MainWindow);
        hotkeys->setObjectName(QString::fromUtf8("hotkeys"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout_2 = new QHBoxLayout(centralWidget);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        mapLayout = new QHBoxLayout();
        mapLayout->setSpacing(6);
        mapLayout->setObjectName(QString::fromUtf8("mapLayout"));

        verticalLayout->addLayout(mapLayout);

        timeSlider = new QSlider(centralWidget);
        timeSlider->setObjectName(QString::fromUtf8("timeSlider"));
        timeSlider->setAcceptDrops(false);
        timeSlider->setMinimum(0);
        timeSlider->setMaximum(12);
        timeSlider->setPageStep(3);
        timeSlider->setOrientation(Qt::Horizontal);
        timeSlider->setTickPosition(QSlider::TicksBelow);
        timeSlider->setTickInterval(1);

        verticalLayout->addWidget(timeSlider);

        timeLine = new QHBoxLayout();
        timeLine->setSpacing(6);
        timeLine->setObjectName(QString::fromUtf8("timeLine"));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));

        timeLine->addWidget(label);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        timeLine->addItem(horizontalSpacer_3);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        timeLine->addWidget(label_2);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        timeLine->addItem(horizontalSpacer_4);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        timeLine->addWidget(label_3);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        timeLine->addItem(horizontalSpacer_5);

        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        timeLine->addWidget(label_4);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        timeLine->addItem(horizontalSpacer_6);

        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        timeLine->addWidget(label_5);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        timeLine->addItem(horizontalSpacer_7);

        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        timeLine->addWidget(label_6);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        timeLine->addItem(horizontalSpacer_8);

        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        timeLine->addWidget(label_7);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        timeLine->addItem(horizontalSpacer_9);

        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        timeLine->addWidget(label_8);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        timeLine->addItem(horizontalSpacer_10);

        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        timeLine->addWidget(label_9);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        timeLine->addItem(horizontalSpacer_11);

        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        timeLine->addWidget(label_10);

        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        timeLine->addItem(horizontalSpacer_12);

        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        timeLine->addWidget(label_11);

        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        timeLine->addItem(horizontalSpacer_13);

        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        timeLine->addWidget(label_12);


        verticalLayout->addLayout(timeLine);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        Filter = new QPushButton(centralWidget);
        Filter->setObjectName(QString::fromUtf8("Filter"));

        horizontalLayout->addWidget(Filter);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        Back = new QPushButton(centralWidget);
        Back->setObjectName(QString::fromUtf8("Back"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/buttons/back/normal.png"), QSize(), QIcon::Normal, QIcon::Off);
        Back->setIcon(icon);
        Back->setIconSize(QSize(48, 48));

        horizontalLayout->addWidget(Back);

        Pause = new QPushButton(centralWidget);
        Pause->setObjectName(QString::fromUtf8("Pause"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/buttons/pause/normal.png"), QSize(), QIcon::Normal, QIcon::Off);
        Pause->setIcon(icon1);
        Pause->setIconSize(QSize(48, 48));

        horizontalLayout->addWidget(Pause);

        Forward = new QPushButton(centralWidget);
        Forward->setObjectName(QString::fromUtf8("Forward"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/buttons/forward/normal.png"), QSize(), QIcon::Normal, QIcon::Off);
        Forward->setIcon(icon2);
        Forward->setIconSize(QSize(48, 48));

        horizontalLayout->addWidget(Forward);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        Exit = new QPushButton(centralWidget);
        Exit->setObjectName(QString::fromUtf8("Exit"));

        horizontalLayout->addWidget(Exit);


        verticalLayout->addLayout(horizontalLayout);


        horizontalLayout_2->addLayout(verticalLayout);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 635, 20));
        menu = new QMenu(menuBar);
        menu->setObjectName(QString::fromUtf8("menu"));
        menu_2 = new QMenu(menuBar);
        menu_2->setObjectName(QString::fromUtf8("menu_2"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menu->menuAction());
        menuBar->addAction(menu_2->menuAction());
        menu->addAction(loadRegions);
        menu->addAction(loadMotions);
        menu->addSeparator();
        menu->addAction(exit);
        menu_2->addAction(properties);
        menu_2->addAction(hotkeys);

        retranslateUi(MainWindow);
        QObject::connect(Exit, SIGNAL(clicked()), MainWindow, SLOT(close()));
        QObject::connect(exit, SIGNAL(triggered()), MainWindow, SLOT(close()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        loadRegions->setText(QApplication::translate("MainWindow", "\320\227\320\260\320\263\321\200\321\203\320\267\320\270\321\202\321\214 \321\200\320\265\320\263\320\270\320\276\320\275\321\213...", 0, QApplication::UnicodeUTF8));
        loadMotions->setText(QApplication::translate("MainWindow", "\320\227\320\260\320\263\321\200\321\203\320\267\320\270\321\202\321\214 \320\277\320\265\321\200\320\265\320\264\320\262\320\270\320\266\320\265\320\275\320\270\321\217...", 0, QApplication::UnicodeUTF8));
        exit->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\205\320\276\320\264", 0, QApplication::UnicodeUTF8));
        properties->setText(QApplication::translate("MainWindow", "\320\235\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\270", 0, QApplication::UnicodeUTF8));
        hotkeys->setText(QApplication::translate("MainWindow", "\320\232\320\273\320\260\320\262\320\270\321\210\320\270 \321\203\320\277\321\200\320\260\320\262\320\273\320\265\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "\320\257\320\275\320\262\320\260\321\200\321\214", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "\320\244\320\265\320\262\321\200\320\260\320\273\321\214", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindow", "\320\234\320\260\321\200\321\202", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MainWindow", "\320\220\320\277\321\200\320\265\320\273\321\214", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("MainWindow", "\320\234\320\260\320\271", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("MainWindow", "\320\230\321\216\320\275\321\214", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("MainWindow", "\320\230\321\216\320\273\321\214", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("MainWindow", "\320\220\320\262\320\263\321\203\321\201\321\202", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("MainWindow", "\320\241\320\265\320\275\321\202\321\217\320\261\321\200\321\214", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("MainWindow", "\320\236\320\272\321\202\321\217\320\261\321\200\321\214", 0, QApplication::UnicodeUTF8));
        label_11->setText(QApplication::translate("MainWindow", "\320\235\320\276\321\217\320\261\321\200\321\214", 0, QApplication::UnicodeUTF8));
        label_12->setText(QApplication::translate("MainWindow", "\320\224\320\265\320\272\320\260\320\261\321\200\321\214", 0, QApplication::UnicodeUTF8));
        Filter->setText(QApplication::translate("MainWindow", "\320\244\320\270\320\273\321\214\321\202\321\200", 0, QApplication::UnicodeUTF8));
        Back->setText(QString());
        Pause->setText(QString());
        Forward->setText(QString());
        Exit->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\205\320\276\320\264", 0, QApplication::UnicodeUTF8));
        menu->setTitle(QApplication::translate("MainWindow", "\320\224\320\265\320\271\321\201\321\202\320\262\320\270\321\217", 0, QApplication::UnicodeUTF8));
        menu_2->setTitle(QApplication::translate("MainWindow", "\320\235\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\270", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
