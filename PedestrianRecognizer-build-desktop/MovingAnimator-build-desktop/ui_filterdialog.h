/********************************************************************************
** Form generated from reading UI file 'filterdialog.ui'
**
** Created: Mon 13. Jun 20:23:33 2011
**      by: Qt User Interface Compiler version 4.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FILTERDIALOG_H
#define UI_FILTERDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_FilterDialog
{
public:
    QHBoxLayout *horizontalLayout;
    QGridLayout *gridLayout_2;
    QGroupBox *Direction;
    QVBoxLayout *verticalLayout_5;
    QGridLayout *DirectionGridLayout;
    QGroupBox *FromGroupBox;
    QVBoxLayout *verticalLayout_3;
    QCheckBox *fromAll;
    QFrame *line;
    QCheckBox *from0;
    QCheckBox *from1;
    QCheckBox *from2;
    QCheckBox *from3;
    QCheckBox *from4;
    QCheckBox *from5;
    QCheckBox *from6;
    QCheckBox *from7;
    QCheckBox *from8;
    QCheckBox *from9;
    QCheckBox *from10;
    QCheckBox *from11;
    QCheckBox *from12;
    QCheckBox *from13;
    QCheckBox *from14;
    QCheckBox *from15;
    QGroupBox *ToGroupBox;
    QVBoxLayout *verticalLayout;
    QCheckBox *toAll;
    QFrame *line_2;
    QCheckBox *to0;
    QCheckBox *to1;
    QCheckBox *to2;
    QCheckBox *to3;
    QCheckBox *to4;
    QCheckBox *to5;
    QCheckBox *to6;
    QCheckBox *to7;
    QCheckBox *to8;
    QCheckBox *to9;
    QCheckBox *to10;
    QCheckBox *to11;
    QCheckBox *to12;
    QCheckBox *to13;
    QCheckBox *to14;
    QCheckBox *to15;
    QGroupBox *Month;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label;
    QComboBox *monthFromComboBox;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_2;
    QComboBox *monthToComboBox;
    QSpacerItem *horizontalSpacer_4;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *FilterDialog)
    {
        if (FilterDialog->objectName().isEmpty())
            FilterDialog->setObjectName(QString::fromUtf8("FilterDialog"));
        FilterDialog->resize(461, 576);
        horizontalLayout = new QHBoxLayout(FilterDialog);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        Direction = new QGroupBox(FilterDialog);
        Direction->setObjectName(QString::fromUtf8("Direction"));
        verticalLayout_5 = new QVBoxLayout(Direction);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        DirectionGridLayout = new QGridLayout();
        DirectionGridLayout->setObjectName(QString::fromUtf8("DirectionGridLayout"));
        FromGroupBox = new QGroupBox(Direction);
        FromGroupBox->setObjectName(QString::fromUtf8("FromGroupBox"));
        verticalLayout_3 = new QVBoxLayout(FromGroupBox);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        fromAll = new QCheckBox(FromGroupBox);
        fromAll->setObjectName(QString::fromUtf8("fromAll"));
        fromAll->setChecked(true);

        verticalLayout_3->addWidget(fromAll);

        line = new QFrame(FromGroupBox);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_3->addWidget(line);

        from0 = new QCheckBox(FromGroupBox);
        from0->setObjectName(QString::fromUtf8("from0"));

        verticalLayout_3->addWidget(from0);

        from1 = new QCheckBox(FromGroupBox);
        from1->setObjectName(QString::fromUtf8("from1"));

        verticalLayout_3->addWidget(from1);

        from2 = new QCheckBox(FromGroupBox);
        from2->setObjectName(QString::fromUtf8("from2"));

        verticalLayout_3->addWidget(from2);

        from3 = new QCheckBox(FromGroupBox);
        from3->setObjectName(QString::fromUtf8("from3"));

        verticalLayout_3->addWidget(from3);

        from4 = new QCheckBox(FromGroupBox);
        from4->setObjectName(QString::fromUtf8("from4"));

        verticalLayout_3->addWidget(from4);

        from5 = new QCheckBox(FromGroupBox);
        from5->setObjectName(QString::fromUtf8("from5"));

        verticalLayout_3->addWidget(from5);

        from6 = new QCheckBox(FromGroupBox);
        from6->setObjectName(QString::fromUtf8("from6"));

        verticalLayout_3->addWidget(from6);

        from7 = new QCheckBox(FromGroupBox);
        from7->setObjectName(QString::fromUtf8("from7"));

        verticalLayout_3->addWidget(from7);

        from8 = new QCheckBox(FromGroupBox);
        from8->setObjectName(QString::fromUtf8("from8"));

        verticalLayout_3->addWidget(from8);

        from9 = new QCheckBox(FromGroupBox);
        from9->setObjectName(QString::fromUtf8("from9"));

        verticalLayout_3->addWidget(from9);

        from10 = new QCheckBox(FromGroupBox);
        from10->setObjectName(QString::fromUtf8("from10"));

        verticalLayout_3->addWidget(from10);

        from11 = new QCheckBox(FromGroupBox);
        from11->setObjectName(QString::fromUtf8("from11"));

        verticalLayout_3->addWidget(from11);

        from12 = new QCheckBox(FromGroupBox);
        from12->setObjectName(QString::fromUtf8("from12"));

        verticalLayout_3->addWidget(from12);

        from13 = new QCheckBox(FromGroupBox);
        from13->setObjectName(QString::fromUtf8("from13"));

        verticalLayout_3->addWidget(from13);

        from14 = new QCheckBox(FromGroupBox);
        from14->setObjectName(QString::fromUtf8("from14"));

        verticalLayout_3->addWidget(from14);

        from15 = new QCheckBox(FromGroupBox);
        from15->setObjectName(QString::fromUtf8("from15"));

        verticalLayout_3->addWidget(from15);


        DirectionGridLayout->addWidget(FromGroupBox, 0, 0, 1, 1);

        ToGroupBox = new QGroupBox(Direction);
        ToGroupBox->setObjectName(QString::fromUtf8("ToGroupBox"));
        verticalLayout = new QVBoxLayout(ToGroupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        toAll = new QCheckBox(ToGroupBox);
        toAll->setObjectName(QString::fromUtf8("toAll"));
        toAll->setChecked(true);

        verticalLayout->addWidget(toAll);

        line_2 = new QFrame(ToGroupBox);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_2);

        to0 = new QCheckBox(ToGroupBox);
        to0->setObjectName(QString::fromUtf8("to0"));

        verticalLayout->addWidget(to0);

        to1 = new QCheckBox(ToGroupBox);
        to1->setObjectName(QString::fromUtf8("to1"));

        verticalLayout->addWidget(to1);

        to2 = new QCheckBox(ToGroupBox);
        to2->setObjectName(QString::fromUtf8("to2"));

        verticalLayout->addWidget(to2);

        to3 = new QCheckBox(ToGroupBox);
        to3->setObjectName(QString::fromUtf8("to3"));

        verticalLayout->addWidget(to3);

        to4 = new QCheckBox(ToGroupBox);
        to4->setObjectName(QString::fromUtf8("to4"));

        verticalLayout->addWidget(to4);

        to5 = new QCheckBox(ToGroupBox);
        to5->setObjectName(QString::fromUtf8("to5"));

        verticalLayout->addWidget(to5);

        to6 = new QCheckBox(ToGroupBox);
        to6->setObjectName(QString::fromUtf8("to6"));

        verticalLayout->addWidget(to6);

        to7 = new QCheckBox(ToGroupBox);
        to7->setObjectName(QString::fromUtf8("to7"));

        verticalLayout->addWidget(to7);

        to8 = new QCheckBox(ToGroupBox);
        to8->setObjectName(QString::fromUtf8("to8"));

        verticalLayout->addWidget(to8);

        to9 = new QCheckBox(ToGroupBox);
        to9->setObjectName(QString::fromUtf8("to9"));

        verticalLayout->addWidget(to9);

        to10 = new QCheckBox(ToGroupBox);
        to10->setObjectName(QString::fromUtf8("to10"));

        verticalLayout->addWidget(to10);

        to11 = new QCheckBox(ToGroupBox);
        to11->setObjectName(QString::fromUtf8("to11"));

        verticalLayout->addWidget(to11);

        to12 = new QCheckBox(ToGroupBox);
        to12->setObjectName(QString::fromUtf8("to12"));

        verticalLayout->addWidget(to12);

        to13 = new QCheckBox(ToGroupBox);
        to13->setObjectName(QString::fromUtf8("to13"));

        verticalLayout->addWidget(to13);

        to14 = new QCheckBox(ToGroupBox);
        to14->setObjectName(QString::fromUtf8("to14"));

        verticalLayout->addWidget(to14);

        to15 = new QCheckBox(ToGroupBox);
        to15->setObjectName(QString::fromUtf8("to15"));

        verticalLayout->addWidget(to15);


        DirectionGridLayout->addWidget(ToGroupBox, 0, 1, 1, 1);


        verticalLayout_5->addLayout(DirectionGridLayout);

        Month = new QGroupBox(Direction);
        Month->setObjectName(QString::fromUtf8("Month"));
        Month->setCheckable(true);
        horizontalLayout_4 = new QHBoxLayout(Month);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label = new QLabel(Month);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_4->addWidget(label);

        monthFromComboBox = new QComboBox(Month);
        monthFromComboBox->setObjectName(QString::fromUtf8("monthFromComboBox"));

        horizontalLayout_4->addWidget(monthFromComboBox);

        horizontalSpacer_3 = new QSpacerItem(14, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);

        label_2 = new QLabel(Month);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_4->addWidget(label_2);

        monthToComboBox = new QComboBox(Month);
        monthToComboBox->setObjectName(QString::fromUtf8("monthToComboBox"));

        horizontalLayout_4->addWidget(monthToComboBox);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);


        verticalLayout_5->addWidget(Month);


        gridLayout_2->addWidget(Direction, 0, 0, 1, 2);

        buttonBox = new QDialogButtonBox(FilterDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout_2->addWidget(buttonBox, 3, 0, 1, 2);


        horizontalLayout->addLayout(gridLayout_2);


        retranslateUi(FilterDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), FilterDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), FilterDialog, SLOT(reject()));

        monthToComboBox->setCurrentIndex(11);


        QMetaObject::connectSlotsByName(FilterDialog);
    } // setupUi

    void retranslateUi(QDialog *FilterDialog)
    {
        FilterDialog->setWindowTitle(QApplication::translate("FilterDialog", "\320\244\320\270\320\273\321\214\321\202\321\200", 0, QApplication::UnicodeUTF8));
        Direction->setTitle(QApplication::translate("FilterDialog", "\320\235\320\260\320\277\321\200\320\260\320\262\320\273\320\265\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        FromGroupBox->setTitle(QApplication::translate("FilterDialog", "\320\230\320\267 \321\200\320\265\320\263\320\270\320\276\320\275\320\260", 0, QApplication::UnicodeUTF8));
        fromAll->setText(QApplication::translate("FilterDialog", "\320\262\321\201\320\265 \321\200\320\265\320\263\320\270\320\276\320\275\321\213", 0, QApplication::UnicodeUTF8));
        from0->setText(QApplication::translate("FilterDialog", "\320\220\320\272\320\274\320\276\320\273\320\270\320\275\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        from1->setText(QApplication::translate("FilterDialog", "\320\220\320\272\321\202\321\216\320\261\320\265\320\275\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        from2->setText(QApplication::translate("FilterDialog", "\320\220\320\273\320\274\320\260\321\202\320\270\320\275\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        from3->setText(QApplication::translate("FilterDialog", "\320\220\321\202\321\213\321\200\320\260\321\203\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        from4->setText(QApplication::translate("FilterDialog", "\320\222\320\232\320\236", 0, QApplication::UnicodeUTF8));
        from5->setText(QApplication::translate("FilterDialog", "\320\226\320\260\320\274\320\261\321\213\320\273\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        from6->setText(QApplication::translate("FilterDialog", "\320\227\320\232\320\236", 0, QApplication::UnicodeUTF8));
        from7->setText(QApplication::translate("FilterDialog", "\320\232\320\260\321\200\320\260\320\263\320\260\320\275\320\264\320\270\320\275\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        from8->setText(QApplication::translate("FilterDialog", "\320\232\321\213\320\267\321\213\320\273\320\276\321\200\320\264\320\270\320\275\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        from9->setText(QApplication::translate("FilterDialog", "\320\232\320\276\321\201\321\202\320\260\320\275\320\260\320\271\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        from10->setText(QApplication::translate("FilterDialog", "\320\234\320\260\320\275\320\263\320\270\321\201\321\202\320\260\321\203\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        from11->setText(QApplication::translate("FilterDialog", "\320\241\320\232\320\236", 0, QApplication::UnicodeUTF8));
        from12->setText(QApplication::translate("FilterDialog", "\320\237\320\260\320\262\320\273\320\276\320\264\320\260\321\200\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        from13->setText(QApplication::translate("FilterDialog", "\320\256\320\232\320\236", 0, QApplication::UnicodeUTF8));
        from14->setText(QApplication::translate("FilterDialog", "\320\263\320\276\321\200\320\276\320\264 \320\220\320\273\320\274\320\260\321\202\321\213", 0, QApplication::UnicodeUTF8));
        from15->setText(QApplication::translate("FilterDialog", "\320\263\320\276\321\200\320\276\320\264 \320\220\321\201\321\202\320\260\320\275\320\260", 0, QApplication::UnicodeUTF8));
        ToGroupBox->setTitle(QApplication::translate("FilterDialog", "\320\222 \321\200\320\265\320\263\320\270\320\276\320\275", 0, QApplication::UnicodeUTF8));
        toAll->setText(QApplication::translate("FilterDialog", "\320\262\321\201\320\265 \321\200\320\265\320\263\320\270\320\276\320\275\321\213", 0, QApplication::UnicodeUTF8));
        to0->setText(QApplication::translate("FilterDialog", "\320\220\320\272\320\274\320\276\320\273\320\270\320\275\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        to1->setText(QApplication::translate("FilterDialog", "\320\220\320\272\321\202\321\216\320\261\320\265\320\275\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        to2->setText(QApplication::translate("FilterDialog", "\320\220\320\273\320\274\320\260\321\202\320\270\320\275\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        to3->setText(QApplication::translate("FilterDialog", "\320\220\321\202\321\213\321\200\320\260\321\203\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        to4->setText(QApplication::translate("FilterDialog", "\320\222\320\232\320\236", 0, QApplication::UnicodeUTF8));
        to5->setText(QApplication::translate("FilterDialog", "\320\226\320\260\320\274\320\261\321\213\320\273\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        to6->setText(QApplication::translate("FilterDialog", "\320\227\320\232\320\236", 0, QApplication::UnicodeUTF8));
        to7->setText(QApplication::translate("FilterDialog", "\320\232\320\260\321\200\320\260\320\263\320\260\320\275\320\264\320\270\320\275\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        to8->setText(QApplication::translate("FilterDialog", "\320\232\321\213\320\267\321\213\320\273\320\276\321\200\320\264\320\270\320\275\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        to9->setText(QApplication::translate("FilterDialog", "\320\232\320\276\321\201\321\202\320\260\320\275\320\260\320\271\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        to10->setText(QApplication::translate("FilterDialog", "\320\234\320\260\320\275\320\263\320\270\321\201\321\202\320\260\321\203\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        to11->setText(QApplication::translate("FilterDialog", "\320\241\320\232\320\236", 0, QApplication::UnicodeUTF8));
        to12->setText(QApplication::translate("FilterDialog", "\320\237\320\260\320\262\320\273\320\276\320\264\320\260\321\200\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        to13->setText(QApplication::translate("FilterDialog", "\320\256\320\232\320\236", 0, QApplication::UnicodeUTF8));
        to14->setText(QApplication::translate("FilterDialog", "\320\263\320\276\321\200\320\276\320\264 \320\220\320\273\320\274\320\260\321\202\321\213", 0, QApplication::UnicodeUTF8));
        to15->setText(QApplication::translate("FilterDialog", "\320\263\320\276\321\200\320\276\320\264 \320\220\321\201\321\202\320\260\320\275\320\260", 0, QApplication::UnicodeUTF8));
        Month->setTitle(QApplication::translate("FilterDialog", "\320\234\320\265\321\201\321\217\321\206", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("FilterDialog", "C", 0, QApplication::UnicodeUTF8));
        monthFromComboBox->clear();
        monthFromComboBox->insertItems(0, QStringList()
         << QApplication::translate("FilterDialog", "\321\217\320\275\320\262\320\260\321\200\321\214", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\321\204\320\265\320\262\321\200\320\260\320\273\321\214", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\320\274\320\260\321\200\321\202", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\320\260\320\277\321\200\320\265\320\273\321\214", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\320\274\320\260\320\271", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\320\270\321\216\320\275\321\214", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\320\270\321\216\320\273\321\214", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\320\260\320\262\320\263\321\203\321\201\321\202", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\321\201\320\265\320\275\321\202\321\217\320\261\321\200\321\214", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\320\276\320\272\321\202\321\217\320\261\321\200\321\214", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\320\275\320\276\321\217\320\261\321\200\321\214", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\320\264\320\265\320\272\320\260\320\261\321\200\321\214", 0, QApplication::UnicodeUTF8)
        );
        label_2->setText(QApplication::translate("FilterDialog", "\320\237\320\276", 0, QApplication::UnicodeUTF8));
        monthToComboBox->clear();
        monthToComboBox->insertItems(0, QStringList()
         << QApplication::translate("FilterDialog", "\321\217\320\275\320\262\320\260\321\200\321\214", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\321\204\320\265\320\262\321\200\320\260\320\273\321\214", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\320\274\320\260\321\200\321\202", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\320\260\320\277\321\200\320\265\320\273\321\214", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\320\274\320\260\320\271", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\320\270\321\216\320\275\321\214", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\320\270\321\216\320\273\321\214", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\320\260\320\262\320\263\321\203\321\201\321\202", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\321\201\320\265\320\275\321\202\321\217\320\261\321\200\321\214", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\320\276\320\272\321\202\321\217\320\261\321\200\321\214", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\320\275\320\276\321\217\320\261\321\200\321\214", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilterDialog", "\320\264\320\265\320\272\320\260\320\261\321\200\321\214", 0, QApplication::UnicodeUTF8)
        );
    } // retranslateUi

};

namespace Ui {
    class FilterDialog: public Ui_FilterDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FILTERDIALOG_H
