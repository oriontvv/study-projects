#ifndef FIELD_H
#define FIELD_H

#include <QLineEdit>

class Field : public QLineEdit
{
    Q_OBJECT
private:
//    void keyPressEvent(QKeyEvent *ev);
public:
    Field(QWidget *parent = NULL);
    void normalize();
};

#endif // FIELD_H
