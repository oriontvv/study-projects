/********************************************************************************
** Form generated from reading ui file 'mainwindow.ui'
**
** Created: Sun 20. Dec 18:00:25 2009
**      by: Qt User Interface Compiler version 4.5.3
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QStatusBar>
#include <QtGui/QTextBrowser>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "Field.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QGridLayout *gridLayout_2;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *verticalSpacer;
    QSpacerItem *horizontalSpacer_2;
    QGridLayout *gridLayout;
    Field *field11;
    Field *field21;
    Field *field31;
    Field *field41;
    Field *field51;
    Field *field32;
    Field *field33;
    Field *field34;
    Field *field35;
    Field *field12;
    Field *field13;
    Field *field22;
    Field *field23;
    Field *field42;
    Field *field52;
    Field *field43;
    Field *field53;
    Field *field54;
    Field *field44;
    Field *field24;
    Field *field14;
    Field *field15;
    Field *field25;
    Field *field45;
    Field *field55;
    QSpacerItem *verticalSpacer_2;
    QVBoxLayout *verticalLayout;
    QPushButton *runButton;
    QTextBrowser *textBrowser;
    QPushButton *pushButton;
    QPushButton *exitButton;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(381, 347);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setMargin(11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer, 1, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 1, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_2, 1, 2, 1, 1);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        field11 = new Field(centralWidget);
        field11->setObjectName(QString::fromUtf8("field11"));

        gridLayout->addWidget(field11, 0, 0, 1, 1);

        field21 = new Field(centralWidget);
        field21->setObjectName(QString::fromUtf8("field21"));

        gridLayout->addWidget(field21, 1, 0, 1, 1);

        field31 = new Field(centralWidget);
        field31->setObjectName(QString::fromUtf8("field31"));

        gridLayout->addWidget(field31, 2, 0, 1, 1);

        field41 = new Field(centralWidget);
        field41->setObjectName(QString::fromUtf8("field41"));

        gridLayout->addWidget(field41, 3, 0, 1, 1);

        field51 = new Field(centralWidget);
        field51->setObjectName(QString::fromUtf8("field51"));

        gridLayout->addWidget(field51, 4, 0, 1, 1);

        field32 = new Field(centralWidget);
        field32->setObjectName(QString::fromUtf8("field32"));

        gridLayout->addWidget(field32, 2, 1, 1, 1);

        field33 = new Field(centralWidget);
        field33->setObjectName(QString::fromUtf8("field33"));

        gridLayout->addWidget(field33, 2, 2, 1, 1);

        field34 = new Field(centralWidget);
        field34->setObjectName(QString::fromUtf8("field34"));

        gridLayout->addWidget(field34, 2, 3, 1, 1);

        field35 = new Field(centralWidget);
        field35->setObjectName(QString::fromUtf8("field35"));

        gridLayout->addWidget(field35, 2, 4, 1, 1);

        field12 = new Field(centralWidget);
        field12->setObjectName(QString::fromUtf8("field12"));

        gridLayout->addWidget(field12, 0, 1, 1, 1);

        field13 = new Field(centralWidget);
        field13->setObjectName(QString::fromUtf8("field13"));

        gridLayout->addWidget(field13, 0, 2, 1, 1);

        field22 = new Field(centralWidget);
        field22->setObjectName(QString::fromUtf8("field22"));

        gridLayout->addWidget(field22, 1, 1, 1, 1);

        field23 = new Field(centralWidget);
        field23->setObjectName(QString::fromUtf8("field23"));

        gridLayout->addWidget(field23, 1, 2, 1, 1);

        field42 = new Field(centralWidget);
        field42->setObjectName(QString::fromUtf8("field42"));

        gridLayout->addWidget(field42, 3, 1, 1, 1);

        field52 = new Field(centralWidget);
        field52->setObjectName(QString::fromUtf8("field52"));

        gridLayout->addWidget(field52, 4, 1, 1, 1);

        field43 = new Field(centralWidget);
        field43->setObjectName(QString::fromUtf8("field43"));

        gridLayout->addWidget(field43, 3, 2, 1, 1);

        field53 = new Field(centralWidget);
        field53->setObjectName(QString::fromUtf8("field53"));

        gridLayout->addWidget(field53, 4, 2, 1, 1);

        field54 = new Field(centralWidget);
        field54->setObjectName(QString::fromUtf8("field54"));

        gridLayout->addWidget(field54, 4, 3, 1, 1);

        field44 = new Field(centralWidget);
        field44->setObjectName(QString::fromUtf8("field44"));

        gridLayout->addWidget(field44, 3, 3, 1, 1);

        field24 = new Field(centralWidget);
        field24->setObjectName(QString::fromUtf8("field24"));

        gridLayout->addWidget(field24, 1, 3, 1, 1);

        field14 = new Field(centralWidget);
        field14->setObjectName(QString::fromUtf8("field14"));

        gridLayout->addWidget(field14, 0, 3, 1, 1);

        field15 = new Field(centralWidget);
        field15->setObjectName(QString::fromUtf8("field15"));

        gridLayout->addWidget(field15, 0, 4, 1, 1);

        field25 = new Field(centralWidget);
        field25->setObjectName(QString::fromUtf8("field25"));

        gridLayout->addWidget(field25, 1, 4, 1, 1);

        field45 = new Field(centralWidget);
        field45->setObjectName(QString::fromUtf8("field45"));

        gridLayout->addWidget(field45, 3, 4, 1, 1);

        field55 = new Field(centralWidget);
        field55->setObjectName(QString::fromUtf8("field55"));

        gridLayout->addWidget(field55, 4, 4, 1, 1);


        gridLayout_2->addLayout(gridLayout, 3, 1, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer_2, 4, 1, 1, 1);


        horizontalLayout->addLayout(gridLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        runButton = new QPushButton(centralWidget);
        runButton->setObjectName(QString::fromUtf8("runButton"));

        verticalLayout->addWidget(runButton);

        textBrowser = new QTextBrowser(centralWidget);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));

        verticalLayout->addWidget(textBrowser);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout->addWidget(pushButton);

        exitButton = new QPushButton(centralWidget);
        exitButton->setObjectName(QString::fromUtf8("exitButton"));

        verticalLayout->addWidget(exitButton);


        horizontalLayout->addLayout(verticalLayout);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 381, 20));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);
        QWidget::setTabOrder(field11, field12);
        QWidget::setTabOrder(field12, field13);
        QWidget::setTabOrder(field13, field14);
        QWidget::setTabOrder(field14, field15);
        QWidget::setTabOrder(field15, field21);
        QWidget::setTabOrder(field21, field22);
        QWidget::setTabOrder(field22, field23);
        QWidget::setTabOrder(field23, field24);
        QWidget::setTabOrder(field24, field25);
        QWidget::setTabOrder(field25, field31);
        QWidget::setTabOrder(field31, field32);
        QWidget::setTabOrder(field32, field33);
        QWidget::setTabOrder(field33, field34);
        QWidget::setTabOrder(field34, field35);
        QWidget::setTabOrder(field35, field41);
        QWidget::setTabOrder(field41, field42);
        QWidget::setTabOrder(field42, field43);
        QWidget::setTabOrder(field43, field44);
        QWidget::setTabOrder(field44, field45);
        QWidget::setTabOrder(field45, field51);
        QWidget::setTabOrder(field51, field52);
        QWidget::setTabOrder(field52, field53);
        QWidget::setTabOrder(field53, field54);
        QWidget::setTabOrder(field54, field55);
        QWidget::setTabOrder(field55, runButton);
        QWidget::setTabOrder(runButton, textBrowser);
        QWidget::setTabOrder(textBrowser, pushButton);
        QWidget::setTabOrder(pushButton, exitButton);

        retranslateUi(MainWindow);
        QObject::connect(exitButton, SIGNAL(clicked()), MainWindow, SLOT(close()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "BaldaSolver", 0, QApplication::UnicodeUTF8));
        runButton->setText(QApplication::translate("MainWindow", "Run", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("MainWindow", "About", 0, QApplication::UnicodeUTF8));
        exitButton->setText(QApplication::translate("MainWindow", "Exit", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(MainWindow);
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
