/****************************************************************************
** Meta object code from reading C++ file 'Field.h'
**
** Created: Mon 7. Dec 01:18:17 2009
**      by: The Qt Meta Object Compiler version 61 (Qt 4.5.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Field.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Field.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 61
#error "This file was generated using the moc from 4.5.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Field[] = {

 // content:
       2,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors

       0        // eod
};

static const char qt_meta_stringdata_Field[] = {
    "Field\0"
};

const QMetaObject Field::staticMetaObject = {
    { &QLineEdit::staticMetaObject, qt_meta_stringdata_Field,
      qt_meta_data_Field, 0 }
};

const QMetaObject *Field::metaObject() const
{
    return &staticMetaObject;
}

void *Field::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Field))
        return static_cast<void*>(const_cast< Field*>(this));
    return QLineEdit::qt_metacast(_clname);
}

int Field::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLineEdit::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
