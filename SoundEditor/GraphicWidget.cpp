#include "GraphicWidget.h"
#include <QDebug>
#include <QMouseEvent>

GraphicWidget::GraphicWidget(QWidget *parent) : QWidget(parent)
{
    //cursorWidget = cur;
    brush_power = 255;
    brush_size = 5;
    cur_pos = 0;
    //QPalette palette;
    //palette.setColor(backgroundRole(),Qt::lightGray);
    //setPalette(palette);
    setAutoFillBackground(true);
    img_cursor = new QImage(50, 50, QImage::Format_ARGB32);
    image[0] = new QImage;
    for(int i = 1; i < MAX_UNDO; ++i)
        image[i] = NULL;
    cursor = new QCursor;
}
GraphicWidget::~GraphicWidget()
{
    delete img_cursor;
    delete cursor;
    delete image;
}

void GraphicWidget::paintEvent(QPaintEvent *event)
{
    resize(image[cur_pos]->width(), image[cur_pos]->height());
    QPainter p(this);
    p.drawImage(0, 0, *image[cur_pos]);
    p.end();
    drawCursor();
}

void GraphicWidget::drawCursor()
{
    if(img_cursor) delete img_cursor;
    img_cursor = new QImage(50, 50, QImage::Format_ARGB32);

    QPainter p(img_cursor);

    p.setRenderHint(QPainter::Antialiasing, true);
    p.setBrush(QBrush(Qt::white));

    double distance;
    double limpidity;
    for(int x = 0; x < img_cursor->width(); ++x)
        for(int y = 0; y < img_cursor->height(); ++y){
            distance = sqrt(pow(x-img_cursor->width()/2,2) + pow(y-img_cursor->height()/2,2));
            //limpidity = 255 - ((255 - brush_power)  * distance / brush_size);
            limpidity = brush_power- 255 * distance / brush_size / 10;
            if (distance <= brush_size * 2.5 && limpidity >0 && limpidity <256)
                img_cursor->setPixel(x, y,qRgba(255, 255, 255, ( (int)limpidity)));
            else
                img_cursor->setPixel(x, y,qRgba(255, 255, 255, 0));
        }

    if(cursor)delete cursor;
    cursor = new QCursor(QPixmap::fromImage(*img_cursor));
    setCursor(*cursor);
}

void GraphicWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton)
        mousePressEvent(event);
}

void GraphicWidget::mousePressEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton){
        int r, g, b, a;
        for(int x = event->x() - 25; x < event->x() + 25; ++x)
            for(int y = event->y() - 25; y < event->y() + 25; ++y)
                if (x >= 0 && x < image[cur_pos]->width() && y >= 0 && y < image[cur_pos]->height()){
                    r = qRed(image[cur_pos]->pixel(x, y));
                    g = qGreen(image[cur_pos]->pixel(x, y));
                    b = qBlue(image[cur_pos]->pixel(x, y));
                    a = qAlpha(image[cur_pos]->pixel(x, y)) +
                        qAlpha(img_cursor->pixel(x - event->x() + 25 ,
                                                 y - event->y() + 25));
                    if (a < 0) a = 0;
                    if (a > 255) a = 255;
            //tmp_img->setPixel(x-25, y-25, qRgba(r, g, b, a));
            image[cur_pos]->setPixel(x, y, qRgba(r, g, b, a));
        }
        update(event->x() - 25, event->y() - 25, 50, 50);
    }
}

void GraphicWidget::mouseReleaseEvent(QMouseEvent *event)
{
    //emit my_changed();
    cur_pos = (cur_pos + 1 + MAX_UNDO) % MAX_UNDO;
    if (image[cur_pos]) delete image[cur_pos];
    image[cur_pos] = new QImage(*image[(cur_pos - 1 + MAX_UNDO) % MAX_UNDO]);
   //qDebug() << "UPD : cur_pos" << cur_pos;
    update();
}

void GraphicWidget::undo()
{
    cur_pos = (cur_pos - 1 + MAX_UNDO) % MAX_UNDO;
    if (image[cur_pos]) update();
    else cur_pos = (cur_pos + 1 + MAX_UNDO) % MAX_UNDO;
 //   qDebug() << "UNDO : cur_pos" << cur_pos;
}

void GraphicWidget::redo()
{
    cur_pos = (cur_pos + 1 + MAX_UNDO) % MAX_UNDO;
   // qDebug() << " REDO : cur_pos" << cur_pos;
    if (image[cur_pos]) update();
    else cur_pos = (cur_pos - 1 + MAX_UNDO) % MAX_UNDO;
}

