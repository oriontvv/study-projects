#include "MediaFile.h"

MediaFile::MediaFile()
{
    //DataSize = 0;
    Data = NULL;
    begin = NULL;
    //header = new QString;
}
MediaFile::~MediaFile()
{
    //if (!header->isNull()) delete header;
    if (Data) delete Data;
    if (begin) delete begin;
}

bool MediaFile::open(const QString *FileName)
{
    //header->clear();

    FILE *in = fopen(FileName->toAscii(), "rb");
    if (!in) return false;

    //====== read header
    fread(&TitleWave, sizeof(TitleWave), 1, in);
    //check
    if (strncmp(TitleWave.id_riff, "RIFF", 4)) return false;
    if (strncmp(TitleWave.wave, "WAVEfmt ", 8)) return false;
    if (strncmp(TitleWave.id_data, "data", 4)) return false;

    fclose(in);

    in = fopen(FileName->toAscii(), "rb");
    if (!in) return false;

    // header's len == 44 bytes
    begin = new byte[sizeof(TitleWave) + TitleWave.len_data + 10];
    fread(begin, sizeof(TitleWave), 1, in);
    fread(begin + sizeof(TitleWave), sizeof(quint16), TitleWave.len_data>>1, in);
    fclose(in);
    Data = (qint16 *)(begin + sizeof(TitleWave));

    return true;
}

void MediaFile::save(QString *FileName)
{
    FILE *out = fopen(FileName->toAscii(), "wb");
    fwrite(begin, sizeof(byte), sizeof(TitleWave) + TitleWave.len_data, out);
    fclose(out);
}

void MediaFile::setData(qint16 *d)
{
    //Data = (qint16 *)(begin + sizeof(TitleWave));
    for(quint32 i = 0; i < TitleWave.len_data / 2 ; ++i){
        Data[i] = d[i];
    }
}

