#ifndef MEDIAFILE_H
#define MEDIAFILE_H

#include <QString>
#include <iostream>
#include <fstream>
#include <cstring>
#include "bass.h"
#include <qDebug>
#include <stdio.h>

typedef struct
{
    char id_riff[4];
    quint32 len_riff;

    char wave[8];
    quint32 len_chunk;

    quint16 type;
    quint16 channels;
    quint32 freq;
    quint32 bytes;
    quint16 align;
    quint16 bits;

    char id_data[4];
    quint32 len_data;
} structTitleWave;

class MediaFile
{

public:
    MediaFile();
    ~MediaFile();
    qint16 *Data;
    structTitleWave TitleWave;
    bool open(const QString *FileName);
    void save(QString *name);
    byte *begin;
    void setData(qint16 *d);
};

#endif // MEDIAFILE_H
