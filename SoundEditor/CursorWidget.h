#ifndef CURSORWIDGET_H
#define CURSORWIDGET_H

#include <QWidget>
#include <QPainter>
#include <QBrush>
#include <QImage>
#include <QDebug>
#include <cmath>

class CursorWidget : public QWidget
{
    Q_OBJECT
public:
    CursorWidget(QWidget *parent = 0);
    ~CursorWidget();
    QImage *img;
public slots:
    void setSize(int s);
    void setPower(int p);
    int getSize() {return size;}
    int getPower() {return power;}

private:
    void paintEvent(QPaintEvent *event);
    int size;
    int power;

};

#endif // CURSORWIDGET_H
