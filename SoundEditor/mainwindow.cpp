#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPainter>
#include <cmath>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    file_name = new QString;
    bmp_file_name = new QString;

    //original_sound = new Phonon::MediaObject;
   // currect_sound = new Phonon::MediaObject;
    media_file = new MediaFile;
   // output = new Phonon::AudioOutput;
   // Phonon::createPath(currect_sound, output);
    cur_img_index = 0;
    OriginalSample = 0;
    OriginalChannel = CurrentChannel = 0;

    fftSize = 1024;
    volumeValue = 100;
    max_amplitude = 0;

    SoundBuffer = NULL;
    pMags = NULL;
    phases = NULL;
    img_w = 0;
    tmp_img = NULL;
    fft.SetMode(FFT_REAL,fftSize);

    pos = BASS_3DVECTOR(0.0f, 0.0f, 1.0f);// koordinati icto4nika zvuka
    SpecDc = 0;
    if(HIWORD(BASS_GetVersion() != BASSVERSION)){
        QMessageBox::critical(0, "ERROR!",
                              "An incorrect version of bass.dll was loaded",
                              QMessageBox::Ok);
        exit(0);
    }
    if (!BASS_Init(-1, 22050, BASS_DEVICE_3D, 0, NULL)){
        QMessageBox::critical(0, "ERROR!",
                              "bass.dll can't be initialize",
                              QMessageBox::Ok);
    }

    SetupGuiElementsPosition();
    GuiElementsEnable(false);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete file_name;
    BASS_Free();
}

void MainWindow::GuiElementsEnable(bool n)
{
    ui->actionPlay->setEnabled(n);
    ui->actionStop->setEnabled(n);
    ui->actionSave->setEnabled(n);
    ui->actionUndo->setEnabled(n);
    ui->actionReturn->setEnabled(n);
    playButton->setEnabled(n);
    stopButton->setEnabled(n);
    pauseButton->setEnabled(n);
    backButton->setEnabled(n);
    forwardButton->setEnabled(n);
    changeSpectrButton->setEnabled(n);
    changeBackSpectrButton->setEnabled(n);
    volumeSlider->setEnabled(n);
    fftSizeComboBox->setEnabled(n);
    file_progress->setEnabled(n);
}

void MainWindow::SetupGuiElementsPosition()
{
   // hLine = new QImage(100, 10, QImage::Format_ARGB32);
   // vLine = new QImage(10, 100, QImage::Format_ARGB32);
    /*QPainter p1(hLine);
    p1.setPen(QPen(QColor::black(), 2));
    p1.drawLine(0, 5, 100, 5);
    p1.begin(vLine);
    p1.drawLine(5, 0, 5, 100);
    p1.end();*/

    spectrColor = new QColor(Qt::black);
    sColor = new QColor(Qt::darkMagenta);
    cursor_widget = new CursorWidget;
    cursor_widget->setMinimumSize(50, 50);
    graphic_widget = new GraphicWidget;
    graphic_widget->resize(600, 500);
    QPalette palette;
    palette.setColor(backgroundRole(), *sColor);
    graphic_widget->setPalette(palette);
    graphic_widget->setAutoFillBackground(true);
    //graphic_widget->show();

    //QVBoxLayout *graphic = new QVBoxLayout;
    //QHBoxLayout *gr1 = new QHBoxLayout;
    //gr1->addWidget(vLine);
   // gr1->addWidget(graphic_area);
   // gr1->addWidget(vLine);
    graphic_area = new QScrollArea(this);
    graphic_area->setWidget(graphic_widget);
   // graphic_area->show();
    //graphic_area->setAlignment(Qt::AlignLeft);
    //graphic_area->setMinimumSize(600, 400);
    //graphic_area->resize(500, 500);
    timer = new QTimer(this);
    currentPositionLabel = new QLabel(this);
    currentPositionLabel->setText("0 / 0");
    fftSizeComboBox = new QComboBox;
    QStringList lst;
    lst << "256" << "512" << "1024" << "2048" << "4096" << "8192";
    fftSizeComboBox->addItems(lst);
    fftSizeComboBox->setCurrentIndex(2);
    main_widget = new QWidget(this);
    //file_progress = new Phonon::SeekSlider(currect_sound);
    //file_progress->setOrientation(Qt::Horizontal);
    file_progress = new QSlider(Qt::Horizontal);
    playButton = new QPushButton("Play");
    stopButton = new QPushButton("Stop");
    pauseButton = new QPushButton("Pause");
    openFileButton = new QPushButton("Open media file");
    changeSpectrButton = new QPushButton("Spectrogram background");
    changeBackSpectrButton = new QPushButton("Spectrogram color");
    backButton = new QPushButton("Undo");
    forwardButton = new QPushButton("Redo");
    volumeSlider = new QSlider(Qt::Horizontal);
    volumeSlider->setRange(0, 100);
    volumeSlider->setValue(100);

    brushSizeSpinbox = new QSpinBox;
    brushSizeSpinbox->setRange(1, 10);
    brushSizeSpinbox->setValue(5);
    brushPowerSpinbox = new QSpinBox;
    brushPowerSpinbox->setRange(1, 255);
    brushPowerSpinbox->setValue(255);
    brushSizeSlider = new QSlider(Qt::Horizontal);
    brushSizeSlider->setRange(1, 10);
    brushSizeSlider->setValue(5);
    brushSizeSlider->setTickInterval(3);
    brushSizeSlider->setTickPosition(QSlider::TicksBelow);
    brushPowerSlider = new QSlider(Qt::Horizontal);
    brushPowerSlider->setRange(1, 255);
    brushPowerSlider->setValue(255);
    brushPowerSlider->setTickInterval(51);
    brushPowerSlider->setTickPosition(QSlider::TicksBelow);

    connect(brushPowerSlider, SIGNAL(valueChanged(int)), brushPowerSpinbox, SLOT(setValue(int)));
    connect(brushPowerSpinbox, SIGNAL(valueChanged(int)), brushPowerSlider, SLOT(setValue(int)));
    connect(brushSizeSlider, SIGNAL(valueChanged(int)), brushSizeSpinbox, SLOT(setValue(int)));
    connect(brushSizeSpinbox, SIGNAL(valueChanged(int)), brushSizeSlider, SLOT(setValue(int)));
    connect(brushSizeSlider, SIGNAL(valueChanged(int)), cursor_widget, SLOT(setSize(int)));
    connect(brushPowerSlider, SIGNAL(valueChanged(int)), cursor_widget, SLOT(setPower(int)));
    connect(brushSizeSlider, SIGNAL(valueChanged(int)), graphic_widget, SLOT(setSize(int)));
    connect(brushPowerSlider, SIGNAL(valueChanged(int)), graphic_widget, SLOT(setPower(int)));
    connect(changeSpectrButton, SIGNAL(clicked()), this, SLOT(changeColor()));
    connect(changeBackSpectrButton, SIGNAL(clicked()), this, SLOT(changeBackColor()));
    connect(fftSizeComboBox, SIGNAL(activated(QString)), this, SLOT(changeFftSize(QString)));
    connect(volumeSlider, SIGNAL(valueChanged(int)), this, SLOT(changeVolumeValue(int)));
    connect(file_progress, SIGNAL(valueChanged(int)), this, SLOT(changePlayPosition(int)));
    connect(timer, SIGNAL(timeout()), this, SLOT(moveSliderPosition()));

    connect(graphic_widget, SIGNAL(get_pic()), this, SLOT(change_spec()));

    connect(playButton, SIGNAL(clicked()), this, SLOT(playFile()));
    connect(stopButton, SIGNAL(clicked()), this, SLOT(stopFile()));
    connect(pauseButton, SIGNAL(clicked()), this, SLOT(pauseFile()));
    connect(openFileButton, SIGNAL(clicked()), this, SLOT(openWavFile()));
    connect(backButton, SIGNAL(clicked()), this->graphic_widget, SLOT(undo()));
    connect(forwardButton, SIGNAL(clicked()), this->graphic_widget, SLOT(redo()));

    //layout setups:
    QHBoxLayout *player = new QHBoxLayout;
    player->setMargin(5);
    player->setSpacing(10);
    player->addStretch(1);
    player->addWidget(playButton);
    player->addWidget(pauseButton);
    player->addWidget(stopButton);
    player->addStretch(1);
    player->addWidget(currentPositionLabel);
    player->addStretch(1);
    player->addWidget(openFileButton);

    QHBoxLayout *volume_layout = new QHBoxLayout;
    volume_layout->addStretch(1);
    volume_layout->addWidget(new QLabel("Volume: "));
    volume_layout->addWidget(volumeSlider);
    volume_layout->addStretch(1);

    QVBoxLayout *layout_left = new QVBoxLayout;
    layout_left->setMargin(5);
    layout_left->setSpacing(10);
    layout_left->addWidget(graphic_area);
    layout_left->addWidget(file_progress);
    layout_left->addLayout(player);
    layout_left->addLayout(volume_layout);

    QHBoxLayout *fftSize_layout = new QHBoxLayout;
    fftSize_layout->setMargin(5);
    fftSize_layout->setSpacing(10);
    fftSize_layout->addWidget(new QLabel("FFT size: "));
    fftSize_layout->addWidget(fftSizeComboBox);

    QHBoxLayout *brush_size = new QHBoxLayout;
    brush_size->setMargin(5);
    brush_size->setSpacing(10);
    brush_size->addWidget(new QLabel("Brush size: "));
    brush_size->addWidget(brushSizeSpinbox);

    QHBoxLayout *brush_size_slider = new QHBoxLayout;
    brush_size_slider->addWidget(brushSizeSlider);
    QVBoxLayout *brush_size_main = new QVBoxLayout;
    brush_size_main->setMargin(5);
    brush_size_main->setSpacing(10);
    brush_size_main->addLayout(brush_size);
    brush_size_main->addLayout(brush_size_slider);

    QHBoxLayout *brush_power = new QHBoxLayout;
    brush_power->setMargin(5);
    brush_power->setSpacing(10);
    brush_power->addWidget(new QLabel("Brush power: "));
    brush_power->addWidget(brushPowerSpinbox);

    QHBoxLayout *brush_power_slider = new QHBoxLayout;
    brush_power_slider->addWidget(brushPowerSlider);
    QVBoxLayout *brush_power_main = new QVBoxLayout;
    brush_power_main->setMargin(5);
    brush_power_main->setSpacing(10);
    brush_power_main->addLayout(brush_power);
    brush_power_main->addLayout(brush_power_slider);

    QHBoxLayout *layout_cursor = new QHBoxLayout;
    layout_cursor->addStretch(1);
    layout_cursor->addWidget(new QLabel("Brush example: "));
    layout_cursor->addWidget(cursor_widget);
    layout_cursor->addStretch(1);

    QHBoxLayout *history_layout = new QHBoxLayout;
    history_layout->addStretch(1);
    history_layout->addWidget(backButton);
    history_layout->addWidget(forwardButton);
    history_layout->addStretch(1);

    QFrame *HLine1 = new QFrame;
    HLine1->setFrameStyle(QFrame::HLine | QFrame::Plain);
    QFrame *HLine2 = new QFrame;
    HLine2->setFrameStyle(QFrame::HLine | QFrame::Plain);
    QFrame *HLine3 = new QFrame;
    HLine3->setFrameStyle(QFrame::HLine | QFrame::Plain);
    QVBoxLayout *layout_right = new QVBoxLayout;
    layout_right->setMargin(5);
    layout_right->setSpacing(10);
    layout_right->addLayout(fftSize_layout);
    layout_right->addWidget(HLine1);
    layout_right->addLayout(layout_cursor);
    layout_right->addLayout(brush_size_main);
    layout_right->addLayout(brush_power_main);
    layout_right->addWidget(HLine2);
    layout_right->addWidget(changeBackSpectrButton);
    layout_right->addWidget(changeSpectrButton);
    layout_right->addWidget(HLine3);
    layout_right->addLayout(history_layout);
    layout_right->addStretch(1);

    QHBoxLayout *main_layout = new QHBoxLayout;
    main_layout->addLayout(layout_left, 5);
    main_layout->addLayout(layout_right, 1);

    main_widget->setLayout(main_layout);

    setCentralWidget(main_widget);
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
   /* switch(event->key()){
        case Qt::Key_Space:
           // if(BASS_ChannelIsActive(CurrentChannel)) stopFile();
           // else playFile();
            break;

        default:
            QWidget::keyPressEvent(event);
            break;

    }*/

}

void MainWindow::allocate_memory()
{
    for(int i = 0; i < img_w; ++i)
        if (phases[i]) delete phases[i];
    if (phases) delete phases;

    img_w = (int)(media_file->TitleWave.len_data >>1 - fftSize) / (fftSize >> 2);
   // qDebug() << "IN ALLOCATE : img_w = " << img_w;

    //qDebug() << "new img_w = " << img_w << " height = " << (fftSize >>1 + 1);
    phases = new float*[img_w + 1];
    for(int i = 0; i <= img_w; ++i){
        phases[i] = new float[fftSize/2 + 2];
        for(uint k = 0; k < fftSize>>1 + 2; ++k)
            phases[i][k] = 0.0;
    }
}

void MainWindow::openWavFile()
{
    QString *f = new QString(*file_name);
    file_name = new QString(QFileDialog::getOpenFileName(0, "Open file...", "",
                                                         "*.wav - support media file\n*.* - any file"));
    //file_name = new QString("CellPhone.wav");

    if (file_name->compare("")) {
        if(!f->isNull())delete f;

        if(!media_file->open(file_name)){
            QLabel *lab = new QLabel("<h1> <font color = red> ERROR!<hr> Can't load file " + *file_name);
            lab->show();
            return;
        }

        if(BASS_ChannelIsActive(CurrentChannel)) BASS_ChannelStop(CurrentChannel);
        //open file
        if(!(OriginalSample = BASS_SampleLoad(true, media_file->begin, 0,
                                              sizeof(media_file->TitleWave) + media_file->TitleWave.len_data,
                                              1, BASS_SAMPLE_3D| BASS_SAMPLE_LOOP))){
            QLabel *lab = new QLabel("<h1> <font color = red> ERROR!<hr> Can't load file " + *file_name);
            lab->show();
            return;
        }
        if(!(CurrentChannel = BASS_SampleGetChannel(OriginalSample, false))){
            QLabel *lab = new QLabel("<h1> <font color = red> ERROR!<hr> Can't create channel fo " + *file_name);
            lab->show();
            return;
        }
        //bind progress slider with media channel
        long long len = BASS_ChannelGetLength(CurrentChannel);
        //qDebug() << len << "\n";
        file_progress->setRange(0, len / 1000);
        file_progress->setValue(0);
        currentPositionLabel->setText("0 / " + QString::number(len / 1000));
        timer->stop();

        //allocate_memory(); // for phases
        my_repaint();// from data

        /*graphic_widget->cur_pos = 0;
         graphic_widget->image[graphic_widget->cur_pos] = new QImage(*tmp_img);
        if (graphic_widget->image[0]) delete graphic_widget->image[0];
        graphic_widget->image[0] = new QImage(*tmp_img);*/
        //qDebug() << "UPD : graphic_widget->cur_pos" << graphic_widget->cur_pos;

        graphic_widget->update();

        setWindowTitle(QString("Sound Editor << ") += (file_name->isNull() ? (*f) : (*file_name)));
        GuiElementsEnable(true);
    }
}

void MainWindow::saveWavFile()
{
    QString fname = QFileDialog::getSaveFileName(0, "Save file...", "",
                                             "*.wav - support media file");
    if (!fname.compare("")) return;
    media_file->save(&fname);
}

void MainWindow::changeColor()
{
    QColor *tmp = new QColor(*spectrColor);
    spectrColor = new QColor(QColorDialog::getColor(*spectrColor));
    if (!spectrColor->isValid()){
        delete spectrColor;
        spectrColor = tmp;
    }
    else{
        delete tmp;
    }
    my_update();
    my_repaint();
}

void MainWindow::changeBackColor()
{
    QColor *tmp = new QColor(*sColor);
    sColor = new QColor(QColorDialog::getColor(*sColor));
    if (!sColor->isValid()){
        delete sColor;
        sColor = tmp;
    }
    else{
        delete tmp;
    }
    QPalette palette;
    palette.setColor(backgroundRole(), *sColor);
    graphic_widget->setPalette(palette);
    my_update();
    my_repaint();
}

void MainWindow::about()
{
    QMessageBox::information(0,
            "About",
            "<h3><font color = blue><u>project</u><font color = black>:<b>Sound Editor v 1.0</b><br>"
            "<u><font color = blue>author</u><font color = black>:<b>Taranov V.V.<\b>     <br>"
            "<u><font color = blue>group</u>    <font color = black>:<b>325<\b>     <br>"
            "<u><font color = blue>mailto</u>    <font color = black>:<b>taranov.vv@gmail.com<\b>     <br>");
}

void MainWindow::playFile()
{
    my_update();
    if(!(OriginalSample = BASS_SampleLoad(true, media_file->begin, 0,
                                          sizeof(media_file->TitleWave) + media_file->TitleWave.len_data,
                                          1, BASS_SAMPLE_3D| BASS_SAMPLE_LOOP))){
        QLabel *lab = new QLabel("<h1> <font color = red> ERROR!<hr> Can't load file " + *file_name);
        lab->show();
        return;
    }
    if(!(CurrentChannel = BASS_SampleGetChannel(OriginalSample, false))){
        QLabel *lab = new QLabel("<h1> <font color = red> ERROR!<hr> Can't create channel fo " + *file_name);
        lab->show();
        return;
    }
    if(!BASS_ChannelPlay(CurrentChannel, false)) {
        QLabel *lab = new QLabel("<h1> <font color = red> ERROR!<hr>I Can't play file " + *file_name);
        lab->show();
    }
    timer->start(100);
     //my_update();
}

void MainWindow::stopFile()
{
    BASS_ChannelStop(CurrentChannel);
    if(!(CurrentChannel = BASS_SampleGetChannel(OriginalSample, false))){
        QLabel *lab = new QLabel("<h1> <font color = red> ERROR!<hr> Can't create channel fo " + *file_name);
        lab->show();
    }
    timer->stop();
    file_progress->setValue(0);
}

void MainWindow::edit()
{
    //graphic_widget->setCurrentCursor("img\brush1.bmp");
    //setCursor(QCursor();
}

void MainWindow::pauseFile()
{
    BASS_ChannelPause(CurrentChannel);
    timer->stop();
}

void MainWindow::changeFftSize(QString str)
{
    bool ok;
    fftSize = str.toInt(&ok, 10);
    fft.SetMode(FFT_REAL,fftSize);
    //my_update();
    my_repaint();
}

void MainWindow::changeVolumeValue(int value)
{
    volumeValue = value;
    BASS_SetVolume(value);
}

void MainWindow::changePlayPosition(int pos)
{
    BASS_ChannelSetPosition(CurrentChannel, pos * 1000);
    QString tmp;
    tmp.append(QString::number(pos));
    tmp.append(" / ");
    tmp.append(QString::number(file_progress->maximum()));
    currentPositionLabel->setText(tmp);
}

void MainWindow::moveSliderPosition()
{
    if(file_progress->value() >= file_progress->maximum()) file_progress->setValue(0);
    file_progress->setValue(file_progress->value() + 10);
    QString tmp;
    tmp.append(QString::number(file_progress->value()));
    tmp.append(" / ");
    tmp.append(QString::number(file_progress->maximum()));
    currentPositionLabel->setText(tmp);
}

void MainWindow::my_update()
{
    int w = 0;
    // allocate memory for buffers
    uint len = media_file->TitleWave.len_data;
    int len_fl = len>>1;
    for(uint i = 0; i < len_fl - fftSize; i += (fftSize >> 2), w++);
//    qDebug() << "IN UPDATE : w = " << w;
    SoundBuffer = new Cmplx[fftSize + 1];
    pMags = new float[fftSize + 1];

    float *tmpBuf1 = new float[fftSize + 1];
    float *tmpBuf = new float[len_fl + 1];
    for(int i = 0; i <= len_fl; ++i)
        tmpBuf[i] = 0.0;
    float amp;
    int alpha;
    for(int x = 0; x < w; ++x){
        for(int y = 0; y < graphic_widget->image[graphic_widget->cur_pos]->height(); ++y){
            alpha = qAlpha(graphic_widget->image[graphic_widget->cur_pos]->pixel(x, y));
            amp = 1.0 * pow(10.0, -140.0 / 256.0 * alpha / 20.0) * max_amplitude; // to decibells
            if (amp * 1000000.0 <= max_amplitude) amp = 0.0;
            SoundBuffer[graphic_widget->image[graphic_widget->cur_pos]->height() - 1 - y].re = amp * sin(phases[x][y - 1]);
            SoundBuffer[graphic_widget->image[graphic_widget->cur_pos]->height() - 1 - y].im = amp * cos(phases[x][y - 1]);
        }
        fft.InvFftReal(SoundBuffer, tmpBuf1);
        for(uint i = 0; i <= fftSize; ++i){
            //Hanning window
            tmpBuf1[i] *= (1 - cos(2.0 * M_PI * (i) / (fftSize - 1))) / 4.0;
        }
        for(uint i = 0; i <= fftSize; ++i){
            tmpBuf[i + x * fftSize / 4] += tmpBuf1[i];
        }
    }

    qint16 *m = new qint16[len_fl + 1];
    for(int i = 0; i <= len_fl ; ++i){
        m[i] = (quint16)(tmpBuf[i]);
        //if (i % 100 == 0) qDebug() << "i = " << i << " value = " << tmpBuf[i];
    }
    media_file->setData(m);

    if (SoundBuffer) delete SoundBuffer;
    if (pMags) delete pMags;
    if (tmpBuf1) delete tmpBuf1;
    if (m) delete m;
}

void MainWindow::my_repaint()
{
    //std::fstream out;
    //out.open("outwave.txt", std::ios::out);

    allocate_memory();//phases

    // allocate memory for buffers
    uint len = media_file->TitleWave.len_data;
    uint len_fl = len>>1;
    //qDebug() << "len = " << len;
    SoundBuffer = new Cmplx[fftSize + 1];
    pMags = new float[fftSize + 1];

    float *tmpBuf = new float[len_fl];
    for(uint i = 0; i < len_fl; ++i){
        tmpBuf[i] = (float)(media_file->Data[i]);
       // out << tmpBuf[i]<< "\n";
    }

    //search max
    max_amplitude = fabs(tmpBuf[0]);
    for(uint i = 1; i < len_fl; ++i)
        if (max_amplitude < fabs(tmpBuf[i])) max_amplitude = fabs(tmpBuf[i]);
    max_amplitude *= fftSize;
    if (max_amplitude == 0){
        QLabel *lab = new QLabel("<h1> <font color = red> ERROR!<hr> max_amplitude = 0 ");
        lab->show();
        return;
    }

    float *tmpBuf1 = new float[fftSize + 3];
    tmp_img = new QImage(img_w, fftSize / 2 + 1, QImage::Format_ARGB32);
    QPainter painter(tmp_img);
//    painter.setPen(*sColor);
//    painter.fillRect(0, 0, img_w, fftSize / 2 + 1, *sColor);
   // painter.fillRect(0, 0, img_w, fftSize / 2 + 1, Qt::lightGray);
    //painter.drawLine(0, 0, (int)(len_fl * 4 / fftSize) + 1, fftSize / 2 + 1);

    fft.SetMode(FFT_REAL, fftSize);

    int time = 0;
    for(uint i = 0; i <= len_fl - fftSize; i += (fftSize >> 2), time++){
        for(uint k = 0; k < fftSize; ++k){
            tmpBuf1[k] = tmpBuf[i + k];
            // Hanning window:
            tmpBuf1[k] *= (1 - cos(2.0 * M_PI * (k) / (fftSize - 1))) / 2.0;
        }
        fft.FftReal(tmpBuf1, SoundBuffer);

        for(uint j = 0; j <= fftSize >> 1; ++j)
            pMags[j] = sqrt(pow(SoundBuffer[j].re, 2.0) + pow(SoundBuffer[j].im, 2.0));

        //save phases
        for(uint j = 0; j <= fftSize >> 1; ++j){
            if ( SoundBuffer[j].re > 0)
                phases[time][j] = atan(1.0 * SoundBuffer[j].im / SoundBuffer[j].re);
            else if (SoundBuffer[j].re < 0)
                phases[time][j] = atan(1.0 * SoundBuffer[j].im / SoundBuffer[j].re) + M_PI;
            else phases[time][j] = M_PI / 2.0;
        }
        for(uint j = 0; j <= fftSize >> 1; ++j){
            if (pMags[j] * 1000000.0 < max_amplitude) pMags[j] = -120.0;
            else pMags[j] = 20.0 * log10(1.0 * pMags[j] / max_amplitude);
        }
        for (uint k = 0; k <= fftSize >> 1; ++k){
            tmp_img->setPixel(time, fftSize/2 - k, qRgba(
                                                         spectrColor->red(),
                                                         spectrColor->green(),
                                                         spectrColor->blue(),
                                                         - (int)pMags[k] * 256 / 140
                                                         )
                              );
        }
    }
    //out.close();
    //qDebug() << "IN REPAINT time = " << time;
    //qDebug() << "max_ampl = " << max_amplitude;
    if (SoundBuffer) delete SoundBuffer;
    if (pMags) delete pMags;
    if (tmpBuf1) delete tmpBuf1;
    if (tmpBuf) delete tmpBuf;

    if (graphic_widget->image[graphic_widget->cur_pos]) delete graphic_widget->image[graphic_widget->cur_pos];
    graphic_widget->image[graphic_widget->cur_pos] = new QImage(*tmp_img);
    graphic_widget->resize(tmp_img->width(), tmp_img->height());
    //qDebug() << "REPAINT  : graphic_widget->cur_pos = " << graphic_widget->cur_pos;

 //   graphic_widget->update();
  //  graphic_area->update();
   // graphic_widget->update();
}

void MainWindow::change_spec()
{
    if (graphic_widget->tmp) delete graphic_widget->tmp;
    graphic_widget->tmp = new QImage(*tmp_img);
}
