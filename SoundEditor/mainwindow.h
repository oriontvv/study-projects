#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui>
#include <QImage>
#include <QDialog>
#include <QFileDialog>
#include <QBoxLayout>
#include <QLabel>
#include <QAction>
#include <QSpinBox>
#include <QSlider>

#include "FftWrapper.h"
#include "GraphicWidget.h"
#include "CursorWidget.h"
#include "MediaFile.h"
//#include <Phonon>
#include "bass.h"

#include <fstream>

#ifndef M_PI
#define M_PI 3.14159265
#endif

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void openWavFile();
    void saveWavFile();
    void playFile();
    void stopFile();
    void pauseFile();
    void edit();
    void about();
    void changeColor();
    void changeBackColor();
    void changeFftSize(QString str);
    void changeVolumeValue(int value);
    void changePlayPosition(int pos);
    void moveSliderPosition();
    void change_spec();

private:
    Ui::MainWindow *ui;
    void keyPressEvent(QKeyEvent *event);
//    void mouseReleaseEvent(QMouseEvent *event);
    //void mouseMoveEvent(QMouseEvent *event);
    //void mousePressEvent(QMouseEvent *event);
    void my_repaint();
    void my_update();
    void allocate_memory();
    QString *file_name;
    QString *bmp_file_name;

    //sound elements:
    uint fftSize;
    Cmplx *SoundBuffer;
    float *pMags;
    float **phases;
    int img_w;
    float max_amplitude;

    HSAMPLE OriginalSample;
    HCHANNEL OriginalChannel;
    HCHANNEL CurrentChannel;

    int volumeValue;
    HSTREAM SoundStream;

    //HWNd win;
    BASS_3DVECTOR pos;
    Fft fft;
    BYTE *SpecBuf;
    HDC SpecDc;

    MediaFile *media_file;
//    Phonon::MediaObject *original_sound;
//    Phonon::MediaObject *currect_sound;
//    Phonon::AudioOutput *output;

    QColor *spectrColor;
    QColor *sColor;
    QImage *hLine, *vLine;
    QImage *tmp_img;
    int cur_img_index;

    //Gui elements:
    void GuiElementsEnable(bool);
    void SetupGuiElementsPosition();
    GraphicWidget *graphic_widget;
    CursorWidget *cursor_widget;
    QScrollArea *graphic_area;
    QWidget *main_widget;
    //Phonon::SeekSlider *file_progress;
    QSlider *file_progress;
    QPushButton *playButton;
    QPushButton *stopButton;
    QPushButton *pauseButton;
    QPushButton *openFileButton;
    QPushButton *changeSpectrButton;
    QPushButton *changeBackSpectrButton;
    QPushButton *backButton;
    QPushButton *forwardButton;
    QSpinBox *brushSizeSpinbox;
    QSlider *brushSizeSlider;
    QSpinBox *brushPowerSpinbox;
    QSlider *brushPowerSlider;
    QComboBox *fftSizeComboBox;
    QSlider *volumeSlider;
    QTimer *timer;
    QLabel *currentPositionLabel;
};

#endif // MAINWINDOW_H
