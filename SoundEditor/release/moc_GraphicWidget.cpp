/****************************************************************************
** Meta object code from reading C++ file 'GraphicWidget.h'
**
** Created: Sun 15. Nov 21:52:04 2009
**      by: The Qt Meta Object Compiler version 61 (Qt 4.5.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../GraphicWidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'GraphicWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 61
#error "This file was generated using the moc from 4.5.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_GraphicWidget[] = {

 // content:
       2,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   12, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
      27,   25,   14,   14, 0x0a,
      42,   40,   14,   14, 0x0a,
      60,   14,   56,   14, 0x0a,
      70,   14,   56,   14, 0x0a,
      81,   14,   14,   14, 0x0a,
      88,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_GraphicWidget[] = {
    "GraphicWidget\0\0get_pic()\0s\0setSize(int)\0"
    "p\0setPower(int)\0int\0getSize()\0getPower()\0"
    "undo()\0redo()\0"
};

const QMetaObject GraphicWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_GraphicWidget,
      qt_meta_data_GraphicWidget, 0 }
};

const QMetaObject *GraphicWidget::metaObject() const
{
    return &staticMetaObject;
}

void *GraphicWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_GraphicWidget))
        return static_cast<void*>(const_cast< GraphicWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int GraphicWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: get_pic(); break;
        case 1: setSize((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: setPower((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: { int _r = getSize();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 4: { int _r = getPower();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 5: undo(); break;
        case 6: redo(); break;
        default: ;
        }
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void GraphicWidget::get_pic()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
