/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created: Sun 15. Nov 21:52:00 2009
**      by: The Qt Meta Object Compiler version 61 (Qt 4.5.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 61
#error "This file was generated using the moc from 4.5.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       2,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   12, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x0a,
      26,   11,   11,   11, 0x0a,
      40,   11,   11,   11, 0x0a,
      51,   11,   11,   11, 0x0a,
      62,   11,   11,   11, 0x0a,
      74,   11,   11,   11, 0x0a,
      81,   11,   11,   11, 0x0a,
      89,   11,   11,   11, 0x0a,
     103,   11,   11,   11, 0x0a,
     125,  121,   11,   11, 0x0a,
     154,  148,   11,   11, 0x0a,
     181,  177,   11,   11, 0x0a,
     205,   11,   11,   11, 0x0a,
     226,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0openWavFile()\0saveWavFile()\0"
    "playFile()\0stopFile()\0pauseFile()\0"
    "edit()\0about()\0changeColor()\0"
    "changeBackColor()\0str\0changeFftSize(QString)\0"
    "value\0changeVolumeValue(int)\0pos\0"
    "changePlayPosition(int)\0moveSliderPosition()\0"
    "change_spec()\0"
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, 0 }
};

const QMetaObject *MainWindow::metaObject() const
{
    return &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: openWavFile(); break;
        case 1: saveWavFile(); break;
        case 2: playFile(); break;
        case 3: stopFile(); break;
        case 4: pauseFile(); break;
        case 5: edit(); break;
        case 6: about(); break;
        case 7: changeColor(); break;
        case 8: changeBackColor(); break;
        case 9: changeFftSize((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 10: changeVolumeValue((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: changePlayPosition((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: moveSliderPosition(); break;
        case 13: change_spec(); break;
        default: ;
        }
        _id -= 14;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
