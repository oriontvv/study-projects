/****************************************************************************
** Meta object code from reading C++ file 'CursorWidget.h'
**
** Created: Sun 15. Nov 21:52:06 2009
**      by: The Qt Meta Object Compiler version 61 (Qt 4.5.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../CursorWidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CursorWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 61
#error "This file was generated using the moc from 4.5.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CursorWidget[] = {

 // content:
       2,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   12, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors

 // slots: signature, parameters, type, tag, flags
      16,   14,   13,   13, 0x0a,
      31,   29,   13,   13, 0x0a,
      49,   13,   45,   13, 0x0a,
      59,   13,   45,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_CursorWidget[] = {
    "CursorWidget\0\0s\0setSize(int)\0p\0"
    "setPower(int)\0int\0getSize()\0getPower()\0"
};

const QMetaObject CursorWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_CursorWidget,
      qt_meta_data_CursorWidget, 0 }
};

const QMetaObject *CursorWidget::metaObject() const
{
    return &staticMetaObject;
}

void *CursorWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CursorWidget))
        return static_cast<void*>(const_cast< CursorWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int CursorWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: setSize((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: setPower((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: { int _r = getSize();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 3: { int _r = getPower();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        default: ;
        }
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
