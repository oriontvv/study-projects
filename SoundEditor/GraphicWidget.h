#ifndef GRAPHICWIDGET_H
#define GRAPHICWIDGET_H

//#include <QWidget>
#include <QScrollArea>
#include <QPainter>
#include <QCursor>
#include <QPixmap>
#include <QPalette>
#include <QImage>
#include <cmath>

#define MAX_UNDO 10

class GraphicWidget : public QWidget
{
    Q_OBJECT

public:
    GraphicWidget(QWidget *parent = 0);
    ~GraphicWidget();
    QImage *tmp;
    QImage *image[MAX_UNDO];
    int cur_pos;
public slots:
    void setSize(int s){brush_size = s; update();}
    void setPower(int p){brush_power = p; update();}
    int getSize() {return brush_size;}
    int getPower() {return brush_power;}
    void undo();
    void redo();

signals:
    void get_pic();

private:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void drawCursor();
    int brush_size;
    int brush_power;

    QImage *img_cursor;
    QCursor *cursor;
};

#endif // GRAPHICWIDGET_H
