
#include "CursorWidget.h"

CursorWidget::CursorWidget(QWidget *parent) : QWidget(parent)
{
    size = 5;
    power = 255;
    img = new QImage(50, 50, QImage::Format_ARGB32);
}
CursorWidget::~CursorWidget()
{
    delete img;
}
void CursorWidget::setSize(int s)
{
    size = s;
    update();
}
void CursorWidget::setPower(int p)
{
    if (p>0 && p < 255){
        power = p;
        update();
    }
}

void CursorWidget::paintEvent(QPaintEvent *event)
{

    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing, true);
    p.fillRect(0, 0, width(), height(), QBrush(Qt::gray));

    if (img) delete img;
    img = new QImage(50, 50, QImage::Format_ARGB32);
    double distance;
    int limpidity;
    for(int x = 0; x < width(); ++x)
        for(int y = 0; y < height(); ++y){
            distance = sqrt(pow(x-width()/2,2) + pow(y-height()/2,2));
            //limpidity = power - (int)((255 - power) * distance / size);
            limpidity = power - 255 * distance / size / 10;
            if (distance <= size * 2.5 && limpidity >0 && limpidity < 256)
                img->setPixel(x, y, qRgba(255, 255, 255, limpidity));
            else
                img->setPixel(x, y, qRgba(211, 211, 211, 0));
        }
    p.drawImage(0, 0, *img);
}
