# -------------------------------------------------
# Project created by QtCreator 2009-10-26T02:01:25
# -------------------------------------------------
TARGET = SoundEditor
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    FftWrapper.cpp \
    GraphicWidget.cpp \
    CursorWidget.cpp \
    MediaFile.cpp
HEADERS += mainwindow.h \
    FftWrapper.h \
    GraphicWidget.h \
    bass.h \
    CursorWidget.h \
    MediaFile.h
FORMS += mainwindow.ui
LIBS += bass.lib
